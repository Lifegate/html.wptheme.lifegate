var LongformSlider = new Swiper(".swiper-slider-longform", {
	loop: !1,
	direction: "horizontal",
	a11y: !0,
	slidesPerView: "auto",
	spaceBetween: 40,
	freeMode: !0,
	navigation: {nextEl: ".swiper-slider-longform__next", prevEl: ".swiper-slider-longform__prev"}
});

jQuery(document).ready(function () {
	LongformSlider.update();
});
