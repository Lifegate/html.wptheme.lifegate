var hideElements = !1, stringPhotoLabels = void 0 !== window.tbm ? window.tbm.photolabels : ["Foto ", " di "];
jQuery(document).ready(function () {
	function closeBigImage() {
		blockScroll(!1), jQuery(".fullwidth-gallery .featured-image--fullwidth-image").removeClass("visible"), jQuery(".fullwidth-gallery__overlay").removeClass("active"), jQuery(".swiper-keyboard-alert").removeClass("active"), BigSlideChange = 0
	}

	function fillCaption(slider, slideId) {
		console.log(slideId);
		var title = slider.find(".swiper-slide--small").eq(slideId).find("h4").text(),
			description = slider.find(".swiper-slide--small").eq(slideId).find("label").text();
		jQuery(".fullwidth-gallery .fullwidth-image__footer").find("h4").text(title).end().find("label").text(description)
	}

	jQuery(".fullwidth-gallery").each(function (index) {
		var Slider, Sliderbig, BigSlideChange = 0;
		window.innerHeight;
		jQuery(this).attr("id", "fullwidth-gallery-" + index);
		var slider = jQuery(this).find(".single-slider-annuncio__container"),
			sliderBig = jQuery(this).children(".featured-image--fullwidth-image").children(".featured-image__box-image"),
			slides = "undefined" !== jQuery(this).closest(".gallery-slider").attr("data-slidesperview") ? jQuery(this).closest(".gallery-slider").attr("data-slidesperview") : 1,
			centeredSlides = "undefined" === jQuery(this).closest(".gallery-slider").attr("data-slidesperview");
		Sliderbig = new Swiper(sliderBig, {
			spaceBetween: 0,
			slidesPerView: 1,
			centeredSlides: true,
			preloadImages: !1,
			keyboard: {
				enabled: false
			},
			lazy: !0,
			on: {
				slideChange: function () {
					3 <= ++BigSlideChange && "desktop" == viewport && jQuery(".swiper-keyboard-alert").addClass("active"), Slider.slideTo(Sliderbig.realIndex)
				}, click: function () {
					hideElements = hideElements ? (jQuery(".fullwidth-gallery .fullwidth-image__footer").removeClass("hidden"), !1) : (jQuery(".fullwidth-gallery .fullwidth-image__footer").addClass("hidden"), !0);
				}
			},
			navigation: {
				hideOnClick: !0,
				nextEl: jQuery(this).children(".featured-image--fullwidth-image").find(".button-slider--next"),
				prevEl: jQuery(this).children(".featured-image--fullwidth-image").find(".button-slider--prev")
			},
			pagination: {
				el: jQuery(this).children(".featured-image--fullwidth-image").find(".swiper-pagination--big"),
				type: "fraction",
				hideOnClick: !0,
				renderFraction: function (currentClass, totalClass) {
					return stringPhotoLabels[0] + '<span class="' + currentClass + '"></span>' + stringPhotoLabels[1] + '<span class="' + totalClass + '"></span>'
				}
			},
		}), Slider = new Swiper(slider, {
			freeMode: !1,
			slidesPerView: slides,
			checkInView: true,
			centeredSlides: true,
			spaceBetween: 0,
			keyboard: {
				enabled: false,
			},
			navigation: {
				nextEl: slider.siblings(".single-gallery-slider__nav").find(".button--next"),
				prevEl: slider.siblings(".single-gallery-slider__nav").find(".button--prev")
			},
			pagination: {el: slider.siblings(".swiper-pagination--small"), type: "bullets"},
			on: {
				init: function () {
					fillCaption(slider, 0)
				}, slideChange: function () {
//					alert("slideChange");
					Sliderbig.slideTo(Slider.realIndex), fillCaption(slider, Slider.realIndex);
				}, click: function () {
//					alert("click");
					!function (slider, slideId) {
						blockScroll(!0), slider.parent().siblings(".featured-image--fullwidth-image").addClass("visible"), slider.parent().siblings(".fullwidth-gallery__overlay").addClass("active"), fillCaption(slider, Slider.realIndex)
					}(slider)
				}
			}
		})
	}), jQuery(".button--collapse").on("click", function () {
		closeBigImage()
	}), navigateGallery = function (e) {
		// disabilito le frecce
		var keycode = (e = e || event).which || e.keyCode;
		if(37 === keycode){
			e.preventDefault();
			return false;
		}
		if(39 === keycode){
			e.preventDefault();
			return false;
		}
		e.target || e.srcElement;
		27 === keycode && closeBigImage(), 37 !== keycode && 39 !== keycode || (jQuery(".swiper-keyboard-alert").removeClass("active"), KeyboardAlertReset = !0)
	}, jQuery(document).keydown(navigateGallery), jQuery(".fullwidth-gallery .button--prev, .fullwidth-gallery .button--next, .fullwidth-gallery .button--expand, .fullwidth-gallery .swiper-slide, .fullwidth-gallery__overlay").off("click");


});
