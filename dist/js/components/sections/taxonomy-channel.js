var $=jQuery.noConflict();
$(document).ready(function () {
	var windowW=$(document).width();
	if (windowW<677){
		var boxes=[];
		$('#articles-grid .col-4').each(function () {
			boxes.push($(this).html());
			console.log('---'+$(this).html());
		});
		var renderHtml='<div class="row"><div class="col-12"><div class="owl-carousel owl-articles">';
		for(var i in boxes) {
			console.log(boxes[i]);
			renderHtml+='<div>'+boxes[i]+'</div>';
		}
		renderHtml+='</div></div></div>';
		$('#articles-grid .container').html(renderHtml);
		$('.owl-articles').owlCarousel({
			loop:true,
			margin:10,

			responsiveClass:true,
			responsive:{
				0:{
					items:1,
					nav:false
				}
			}
		})
	}
});
$(window).resize(function () {
	var windowW=$(document).width();
	if (windowW<768){
		var boxes=[];
		$('#articles-grid .col-4').each(function () {
			boxes.push($(this).html());
			console.log('---'+$(this).html());
		});
		if (boxes.length !== 0){
			var renderHtml='<div class="row"><div class="col-12"><div class="owl-carousel owl-articles">';
			for(var i in boxes) {
				console.log(boxes[i]);
				renderHtml+='<div>'+boxes[i]+'</div>';
			}
			renderHtml+='</div></div></div>';
			$('#articles-grid .container').html(renderHtml);
			$('.owl-articles').owlCarousel({
				loop:false,
				margin:10,

				responsiveClass:true,
				responsive:{
					0:{
						items:1,
						nav:false
					}
				}
			})
		}

	}else{
		var boxes=[];

		$('.owl-articles .owl-item>div').each(function () {
			boxes.push($(this).html());
			console.log('---'+$(this).html());
		});
		if (boxes.length !== 0){
			var renderHtml='<div class="row">';
			for(var i in boxes) {
				console.log(boxes[i]);
				renderHtml+='<div class="col-4">'+boxes[i]+'</div>';
				console.log(i);
				var ii=i+1;
				if (ii%3==0){
					renderHtml+='</div><div class="row">'
				}
			}
			renderHtml+='</div>';
			$('#articles-grid .container').html(renderHtml);
		}

	}
});
