var MagazineSlider = new Swiper(".swiper-slider-magazine", {
	observer: true,
	observeParents: true,
	loop: true,
	autoplay: {
		delay: 4000,
	},
	direction: "horizontal",
	slidesPerView: "auto",
	spaceBetween: 40,
	freeMode: !0,
	navigation: {nextEl: ".swiper-slider-magazine__next", prevEl: ".swiper-slider-magazine__prev"}
});

jQuery(document).ready(function () {
	MagazineSlider.update();
});
