var IniziativeSlider=new Swiper(".swiper-slider-iniziative",{loop:!1,direction:"horizontal",a11y:!0,slidesPerView:"auto",spaceBetween:40,freeMode:!0,navigation:{nextEl:".swiper-slider-iniziative__next",prevEl:".swiper-slider-iniziative__prev"}});
jQuery(document).ready(function () {
	IniziativeSlider.update();
});
