
var btt = jQuery('#back-to-top');

jQuery(window).scroll(function() {
	if (jQuery(window).scrollTop() > 300) {
		btt.addClass('show');
	} else {
		btt.removeClass('show');
	}
});

btt.on('click', function(e) {
	e.preventDefault();
	jQuery('html, body').animate({scrollTop:0}, '300');
});
