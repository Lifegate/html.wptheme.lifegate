var $=jQuery.noConflict();
	$('.owl-apertura').owlCarousel({
		loop:true,
		dots:false,
		autoplay:true,
		autoplayTimeout:4000,
		autoplayHoverPause:false,
		responsiveClass:true,
		responsive:{
			0:{
				items:1,
				nav:false
			}
		}
	});

	var owl=$('.owl-apertura');

	owl.on('changed.owl.carousel',function(property){

		var current = property.item.index;
		var windowW=$(window).width();

		var currentSlide=$(property.target).find(".owl-item").eq(current).find('.owl-inner-item').attr('id-slide');
		console.log($(property.target).find(".owl-item").eq(current));
		console.log(parseInt(currentSlide+1));

		$('.owl-posts').each(function () {
			$(this).removeClass('current-post');
			$(this).removeClass('next-post');
		});

		$('.owl-posts[id-div-slide="'+currentSlide+'"]').addClass('current-post');
		$('.owl-posts[id-div-slide="'+(parseInt(currentSlide)+1)+'"]').addClass('next-post');


	});



	var windowW=$(document).width();
	if (windowW<677){
		var boxes=[];
		$('.gotocarousel .col-4').each(function () {
			boxes.push($(this).html());
		});
		var renderHtml='<div class="row offset"><div class="col-12"><div class="owl-carousel owl-articles1">';
		for(var i in boxes) {
			console.log(boxes[i]);
			renderHtml+='<div>'+boxes[i]+'</div>';
		}
		renderHtml+='</div></div></div>';
		$('.gotocarousel').html(renderHtml);
		$('.owl-articles1').owlCarousel({
			loop:true,
			margin:10,

			responsiveClass:true,
			responsive:{
				0:{
					items:1,
					nav:false
				}
			}
		});
		$('.gotocarousel2 .col-4').each(function () {
			boxes.push($(this).html());
		});
		var renderHtml='<div class="row offset"><div class="col-12"><div class="owl-carousel owl-articles2">';
		for(var i in boxes) {
			console.log(boxes[i]);
			renderHtml+='<div>'+boxes[i]+'</div>';
		}
		renderHtml+='</div></div></div>';
		$('.gotocarousel2').html(renderHtml);
		$('.owl-articles2').owlCarousel({
			loop:true,
			margin:10,

			responsiveClass:true,
			responsive:{
				0:{
					items:1,
					nav:false
				}
			}
		})
	}

$(window).resize(function () {
	var windowW=$(document).width();
	if (windowW<768){
		var boxes=[];
		$('.gotocarousel .col-4').each(function () {
			boxes.push($(this).html());
		});
		if (boxes.length !== 0){
			var renderHtml='<div class="row offset"><div class="col-12"><div class="owl-carousel owl-articles1">';
			for(var i in boxes) {
				console.log(boxes[i]);
				renderHtml+='<div>'+boxes[i]+'</div>';
			}
			renderHtml+='</div></div></div>';
			$('.gotocarousel').html(renderHtml);
			$('.owl-articles1').owlCarousel({
				loop:false,
				margin:10,

				responsiveClass:true,
				responsive:{
					0:{
						items:1,
						nav:false
					}
				}
			})
		}

		var boxes2=[];

		$('.gotocarousel2 .col-4').each(function () {
			boxes2.push($(this).html());
		});
		if (boxes2.length !== 0){
			var renderHtml='<div class="row offset"><div class="col-12"><div class="owl-carousel owl-articles2">';
			for(var e in boxes2) {
				renderHtml+='<div>'+boxes2[e]+'</div>';
			}
			renderHtml+='</div></div></div>';
			$('.gotocarousel2').html(renderHtml);
			$('.owl-articles2').owlCarousel({
				loop:false,
				margin:10,

				responsiveClass:true,
				responsive:{
					0:{
						items:1,
						nav:false
					}
				}
			})
		}

	}else{
		var boxes=[];

		$('.owl-articles1 .owl-item>div').each(function () {
			boxes.push($(this).html());
		});
		if (boxes.length !== 0){
			var renderHtml='<div class="row offset">';
			for(var i in boxes) {
				console.log(boxes[i]);
				renderHtml+='<div class="col-4">'+boxes[i]+'</div>';
				console.log(i);
				var ii=i+1;
				if (ii%3==0){
					renderHtml+='</div><div class="row">'
				}
			}
			renderHtml+='</div>';
			$('.gotocarousel').html(renderHtml);
		}
		$('.owl-articles2 .owl-item>div').each(function () {
			boxes.push($(this).html());
		});

		var boxes2=[];
		$('.owl-articles2 .owl-item>div').each(function () {
			boxes2.push($(this).html());
		});

		if (boxes2.length !== 0){
			var renderHtml='<div class="row offset">';
			for(var e in boxes2) {
				renderHtml+='<div class="col-4">'+boxes2[e]+'</div>';
				console.log(i);
				var ee=e+1;
				if (ee%3==0){
					renderHtml+='</div><div class="row">'
				}
			}
			renderHtml+='</div>';
			$('.gotocarousel2').html(renderHtml);
		}

	}
});
