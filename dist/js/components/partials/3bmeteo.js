
var trebshowed = false;

hideTrebMeteo();

if (navigator.geolocation) {
	var options = {enableHighAccuracy: false,timeout: 10000,maximumAge: 0,desiredAccuracy: 0, frequency: 1 };
	watchID = navigator.geolocation.watchPosition(geo_onSuccess, geo_onError, options);
}

function geo_onSuccess(position){
	if(!trebshowed) {
		jQuery.ajax({
			type: "GET",
			url: window.tbm.ajaxurl,
			data: {
				action: "trebmeteo",
				lat: position.coords.latitude,
				long: position.coords.longitude,
			},
			success: function (data) {
				if (data.status == "success") {
					console.log("render 3bmeteo widget");
					var obj = jQuery.parseJSON(data.data);
//					$("#trebgiorno").html(obj.localita.previsione_giorno[0].data);

					$("#trebline").html("Qualità dell'aria: <spam id=\"trebquality\" class=\"treb" + obj.localita.previsione_giorno[0].qualita_aria + "\">" + obj.localita.previsione_giorno[0].qualita_aria + "</spam>");
					$("#treblocation").html(obj.localita.localita);
					$("#trebimg").attr("src", trebpath + obj.localita.previsione_giorno[0].qualita_aria + ".png");
					//$("#trebcta").html("<a class='title-link' href='https://www.lifegate.it/qualita-aria'>Scopri di più </a>");
					$("#trebpowered").html("<a href='https://www.lifegate.it/qualita-aria	'>Elaborazione dati a cura di <b>3BMeteo</b></a>");
					showTrebMeteo();
				}else{
					console.log("3bmeteo fail request");
					hideTrebMeteo();
				}
			},
			error: function (error) {
				// Errore chiamata
				console.log(error);
				hideTrebMeteo();
			}
		});
	}
}

function hideTrebMeteo(){
	$("#trebmeteo").hide();
	$(".trebcontainer").hide();
}

function showTrebMeteo(){
	$("#trebmeteo").show();
	$(".trebcontainer").show();
	trebshowed = true;
}

function geo_onError(error){
	hideTrebMeteo();
	console.log(error);
}
