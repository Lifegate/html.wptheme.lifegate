var popUpObj,lgPlayerWindow;function initMain($){"desktop"==viewport||"large-mobile"==viewport||viewport,jQuery("#drawer-menu").change(function(){window.location.href=jQuery(this).val()}),jQuery(".header-btn--hamburger").click(function(){jQuery(".drawer-menu").addClass("opened"),blockScroll(!0)}),jQuery(".drawer-menu .btn--close, .overlay--drawer-menu").click(function(){jQuery(".drawer-menu").removeClass("opened"),blockScroll(!1)}),jQuery(".table-wrapper").length||jQuery(".editorial").find("table").each(function(){jQuery(this).wrap('<div class="table-wrapper"></div>')}),popUpObj=function(url,title,w,h){var dualScreenLeft=void 0!==window.screenLeft?window.screenLeft:window.screenX,dualScreenTop=void 0!==window.screenTop?window.screenTop:window.screenY,width=window.innerWidth?window.innerWidth:document.documentElement.clientWidth?document.documentElement.clientWidth:screen.width,systemZoom=(window.innerHeight?window.innerHeight:document.documentElement.clientHeight?document.documentElement.clientHeight:screen.height,width/window.screen.availWidth),left=width-w-24/systemZoom+dualScreenLeft,top=dualScreenTop+180;lgPlayerWindow=window.open(url,title,"toolbar=no,location=no,scrollbars=no,statusbar=no,menubar=no,width="+w/systemZoom+",height="+h/systemZoom+",top="+top+",left="+left,!1),window.focus&&lgPlayerWindow.focus()},jQuery(document).on("click",".radio-panel-old .transparent,#radio-card-btn-old,.card-song-big-old",function(e){e.preventDefault();var title="lgradio";/Android|webOS|iPhone|iPad|iPod|Opera Mini/i.test(navigator.userAgent)&&(title="lg_"+Math.floor(1e3*Math.random())+1),popUpObj("/lifegate-radio",title,1289,789)}),jQuery(".single-longform").length,jQuery("body").on("click",".newsletter__tbmsubmit",function(e){e.preventDefault(),e.stopImmediatePropagation();var email=jQuery(".tbm-email"),button=jQuery(this);return email[0].checkValidity()?(button.addClass("spinner"),void 0!==window.tbm&&jQuery.ajax({url:window.tbm.ajaxurl,data:{action:"mailup_subscribe",Email:email.val(),termsAccept:"yes"}}).done(function(data){button.removeClass("spinner"),void 0!==typeof data.data&&jQuery(".newsletter__response").html(data.data)})):email[0].reportValidity(),!1}),jQuery(".social--wa a").on("click",function(){return window.location="whatsapp://send?text="+encodeURIComponent(window.location.href),!1})}var lgSongRequest=function(){var showSong,requestSong;return showSong=function(selector,text){"undefined"!==text&&(text=47<=text.length?text.substring(0,47)+" ...":text,$(selector).length<1||void 0!==$(selector).data("song")&&$(selector).data("song")===text.toLowerCase()||jQuery(selector).fadeOut(function(){$(this).text(text).fadeIn(),$(this).data("song",text.toLowerCase())}))},void 0!==window.tbm&&(requestSong=function(position){$.ajax({url:tbm.songurl,cache:!1}).done(function(a){void 0===a.data||"header"!==position&&"both"!==position||(a.data.result.title&&showSong("#radio-panel-btn .radio-panel__title",a.data.result.title),a.data.result.artist&&showSong("#radio-panel-btn .radio-panel__artist",a.data.result.artist)),void 0===a.data||"card"!==position&&"both"!==position||(a.data.result.title&&showSong(".card-song-big .card__title h3",a.data.result.title),a.data.result.artist&&showSong(".card-song-big .card__artist",a.data.result.artist))})}),{getSong:function(position){position=void 0!==position?position:"both",void 0!==window.tbm&&requestSong(position)}}}();jQuery(window).resize(jQuery.debounce(300,initMain)),jQuery(document).ready(function($){initMain(),"undefined"!=typeof viewport&&"mobile"!==viewport&&lgSongRequest.getSong()});

(function(document, history, location) {
	var HISTORY_SUPPORT = !!(history && history.pushState);

	var anchorScrolls = {
		ANCHOR_REGEX: /^#[^ ]+$/,
		OFFSET_HEIGHT_PX: 70,

		/**
		 * Establish events, and fix initial scroll position if a hash is provided.
		 */
		init: function() {
			this.scrollToCurrent();
			$(window).on('hashchange', $.proxy(this, 'scrollToCurrent'));
			$('body').on('click', 'a', $.proxy(this, 'delegateAnchors'));
		},

		/**
		 * Return the offset amount to deduct from the normal scroll position.
		 * Modify as appropriate to allow for dynamic calculations
		 */
		getFixedOffset: function() {
			return this.OFFSET_HEIGHT_PX;
		},

		/**
		 * If the provided href is an anchor which resolves to an element on the
		 * page, scroll to it.
		 * @param  {String} href
		 * @return {Boolean} - Was the href an anchor.
		 */
		scrollIfAnchor: function(href, pushToHistory) {
			var match, anchorOffset;

			if(!this.ANCHOR_REGEX.test(href)) {
				return false;
			}

			match = document.getElementById(href.slice(1));

			if(match) {
				anchorOffset = $(match).offset().top - this.getFixedOffset();
				$('html, body').animate({ scrollTop: anchorOffset});

				// Add the state to history as-per normal anchor links
				if(HISTORY_SUPPORT && pushToHistory) {
					history.pushState({}, document.title, location.pathname + href);
				}
			}

			return !!match;
		},

		/**
		 * Attempt to scroll to the current location's hash.
		 */
		scrollToCurrent: function(e) {
			if(this.scrollIfAnchor(window.location.hash) && e) {
				e.preventDefault();
			}
		},

		/**
		 * If the click event's target was an anchor, fix the scroll position.
		 */
		delegateAnchors: function(e) {
			var elem = e.target;

			if(this.scrollIfAnchor(elem.getAttribute('href'), true)) {
				e.preventDefault();
			}
		}
	};

	$(document).ready($.proxy(anchorScrolls, 'init'));
})(window.document, window.history, window.location);
