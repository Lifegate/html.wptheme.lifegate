window.tribooScriptModules = [{
	searchFormJs: {
		exists: document.getElementsByClassName("search-form-js").length,
		params: {
			url: "/app/themes/lifegate-2020/dist/js/components/partials/partial-search-form.js",
			key: "search-form-js"
		}
	}
}, {
	trendsJs: {
		exists: document.getElementsByClassName("trends-js").length,
		params: {url: "/app/themes/lifegate-2020/dist/js/components/sections/trends.js", key: "trends-js"},
		require: [{name: "swiper-js", url: "/app/themes/lifegate-2020/dist/js/libs/swiper/dist/js/swiper.min.js"}]
	}
}, {
	SliderIniziativeJs: {
		exists: document.getElementsByClassName("slider--iniziative").length,
		params: {
			url: "/app/themes/lifegate-2020/dist/js/components/sections/slider-iniziative.js",
			key: "slider-iniziative-js"
		},
		require: [{name: "swiper-js", url: "/app/themes/lifegate-2020/dist/js/libs/swiper/dist/js/swiper.min.js"}]
	}
}, {
	SliderLongformJs: {
		exists: document.getElementsByClassName("slider--longform").length,
		params: {
			url: "/app/themes/lifegate-2020/dist/js/components/sections/slider-longform.js",
			key: "slider-longform-js"
		},
		require: [{name: "swiper-js", url: "/app/themes/lifegate-2020/dist/js/libs/swiper/dist/js/swiper.min.js"}]
	}
},  {
	SliderMagazineJs: {
		exists: document.getElementsByClassName("slider--magazine").length,
		params: {
			url: "/app/themes/lifegate-2020/dist/js/components/sections/slider-magazine.js",
			key: "slider-magazine-js"
		},
		require: [{name: "swiper-js", url: "/app/themes/lifegate-2020/dist/js/libs/swiper/dist/js/swiper.min.js"}]
	}
}, {
	archiveVideoHero: {
		exists: document.getElementsByClassName("archive-video-hero-js").length,
		params: {
			url: "/app/themes/lifegate-2020/dist/js/components/sections/archive-video-hero.js",
			key: "archive-video-hero-js"
		}
	}
}, {
	CustomSlider: {
		exists: document.getElementsByClassName("custom-slider-js").length,
		params: {
			url: "/app/themes/lifegate-2020/dist/js/components/sections/custom-slider.js",
			key: "custom-slider-js"
		},
		require: [{name: "swiper-js", url: "/app/themes/lifegate-2020/dist/js/libs/swiper/dist/js/swiper.min.js"}]
	}
}, {
	singleGalleryJs: {
		exists: document.getElementsByClassName("single-gallery-js").length,
		params: {
			url: "/app/themes/lifegate-2020/dist/js/components/partials/partial-single-gallery-slider--V1.js",
			key: "single-gallery-js"
		}
	}
}, {
	fullwidthImage: {
		exists: document.getElementsByClassName("fullwidth-image-js").length,
		params: {
			url: "/app/themes/lifegate-2020/dist/js/components/sections/fullwidth-image.js",
			key: "fullwidth-image-js"
		},
		require: [{name: "swiper-js", url: "/app/themes/lifegate-2020/dist/js/libs/swiper/dist/js/swiper.min.js"}]
	}
}, {
	videoFullwidthJs: {
		exists: document.getElementsByClassName("video-fullwidth-js").length,
		params: {
			url: "/app/themes/lifegate-2020/dist/js/components/sections/video-fullwidth.js",
			key: "video-fullwidth-js"
		}
	}
}, {
	stickyContentSection: {
		exists: document.getElementsByClassName("sticky-section-js").length,
		params: {
			url: "/app/themes/lifegate-2020/dist/js/components/sections/sticky-content-section.js",
			key: "sticky-section-js"
		}
	}
}, {
	fullwidthSentences: {
		exists: document.getElementsByClassName("fullwidth-sentences-js").length,
		params: {
			url: "/app/themes/lifegate-2020/dist/js/components/sections/fullwidth-sentences.js",
			key: "fullwidth-sentences-js"
		}
	}
}, {
	backgroundMutation: {
		exists: document.getElementsByClassName("background-mutation-js").length,
		params: {
			url: "/app/themes/lifegate-2020/dist/js/components/sections/background-mutation.js",
			key: "background-mutation-js"
		}
	}
}, {
	spyMenu: {
		exists: document.getElementsByClassName("spy-menu-js").length,
		params: {url: "/app/themes/lifegate-2020/dist/js/components/partials/spy-menu.js", key: "spy-menu-js"}
	}
}, {
	longFormHero: {
		exists: document.getElementsByClassName("longform-hero-js").length,
		params: {
			url: "/app/themes/lifegate-2020/dist/js/components/sections/longform-hero.js",
			key: "longform-hero-js"
		},
	}
}, {
	fullwidthGallery: {
		exists: document.getElementsByClassName("fullwidth-gallery-js").length,
		params: {
			url: "/app/themes/lifegate-2020/dist/js/components/sections/fullwidth-gallery.js",
			key: "fullwidth-gallery-js"
		},
		require: [{name: "swiper-js", url: "/app/themes/lifegate-2020/dist/js/libs/swiper/dist/js/swiper.min.js"}]
	}
}, {
	paginationJs: {
		exists: document.getElementsByClassName("partial-pagination").length,
		params: {
			url: "/app/themes/lifegate-2020/dist/js/components/partials/partial-pagination.js",
			key: "pagination-js"
		}
	}
}, {
	jQueryUI: {
		exists: document.getElementsByClassName("jquery-ui-js").length,
		params: {url: "/app/themes/lifegate-2020/dist/js/libs/jquery-ui.min.js", key: "jquery-ui"}
	}
}, {
	mainLifegateJs: {
		exists: document.getElementsByClassName("main-lifegate-js").length,
		params: {url: "/app/themes/lifegate-2020/dist/js/main-lifegate.js", key: "main-lifegate-js"}
	}
}];
