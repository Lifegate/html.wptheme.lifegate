(function () {
	tinymce.PluginManager.add('tbm_tinymce_button', function (editor, url) {
		editor.addButton('tbm_tinymce_button', {
			text: 'LG Codex',
			icon: false,
			type: 'menubutton',
			menu: [
				{
					text: 'Sommario',
					onclick: function () {
						editor.insertContent('<ul class="partial-smooth-list lazyload" data-script="/app/themes/lifegate-2020/dist/js/components/partials/smooth-scroll.min.js">\n' +
							'<li><a href="#link1">Interlink</a></li>\n' +
							'<li><a href="#link2">Interlink</a></li>\n' +
							'<li><a href="#link3">Interlink</a></li>\n' +
							'<li><a href="#link4">Interlink</a></li>\n' +
							'<li><a href="#link5">Interlink</a></li>\n' +
							'<li><a href="#link6">Interlink</a></li>\n' +
							'</ul>\n' +
							'\n' +
							'<h2 id="link1">Interlink</h2>\n' +
							'<h2 id="link2">Interlink</h2>\n' +
							'<h2 id="link3">Interlink</h2>\n' +
							'<h2 id="link4">Interlink</h2>\n' +
							'<h2 id="link5">Interlink</h2>\n' +
							'<h2 id="link6">Interlink</h2>\n');
					}
				},
				{
					text: 'Citazione',
					onclick: function () {
						editor.insertContent('<blockquote><p>Citazione</p><p class="cite__author"><cite>Autore</cite></p></blockquote>');
					}
				}
			]
		});
	});
})();
