@extends('base')

@section('content')
	<style>
		.custom-slider .swiper-wrapper {
			min-height: 540px;
		}

		.swiper-slide {
			max-height: 80vw;
		}

		@media screen and (max-width: 736px) {
			.custom-slider .swiper-wrapper {
				min-height: 320px;
			}
		}
	</style>
	<!--page-programmi.twig page -->

	<div class="page page-programmi">
		<div class="site">

			@include('components.partials.widget-stories')
			@asset('css/components/sections/custom-slider.min.css')
			@asset('css/page-programmi.min.css')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.partial-breadcrumb')
					<div class="page-title">
						<h1>{!! __('Stories ','lifegate') !!}</h1>
					</div>
				</div>
			</div>
			<section class="featured-slider slider" data-slider-effect="coverflow" data-slider-loop="true"
					data-slider-slidesperview="auto" data-slider-space="0" data-slider-centered="true">

				<div class="custom-slider custom-slider-js">
					<div class="swiper-container">
						<div class="swiper-wrapper">
							@while (have_posts()) @php the_post() @endphp
							<div class="swiper-slide">
								<article class="card-radio-slider">
									<a href="{!! get_permalink() !!}" title="{!! the_title() !!}">
										<div class="card__thumbnail--big">
											<picture>
												<img class="lazyload"
														data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(450,450)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(900,900)) !!} 2x"
														alt="{!! the_title() !!}"/>
											</picture>
										</div>
										<div class="card__overlay">
											<div class="card__title">
												<h3>{!! the_title() !!}</h3>
											</div>
										</div>

									</a>
								</article>
							</div>
							@endwhile
						</div>
						<!-- Add Pagination -->
					</div>
					<div class="swiper-pagination"></div>
					<nav class="custom-slider__nav">
						<ul>
							<li><span role="button" aria-label="{!! __('successiva','lifegate') !!}"
										title="{!! __('successiva','lifegate') !!}" class="button button--next"></span></li>
							<li><span role="button" aria-label="{!! __('precedente','lifegate') !!}"
										title="{!! __('precedente','lifegate') !!}" class="button button--prev"></span></li>
						</ul>
					</nav>
				</div>
			</section>
		</div>
	</div>
@endsection
