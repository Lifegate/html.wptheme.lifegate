<div class="fullwidth-updated__newsletter">
	<h3>Iscriviti alla newsletter</h3>
	<form method="POST" action="{!! admin_url( 'admin-ajax.php' ) !!}">
		<div class="newsletter__field">
			<input id="fullwidth-updated-newsletter" class="tbm-email" type="text" name="fullwidth-updated-newsletter" value="" placeholder="Il tuo indirizzo email" required />
			<button type="submit" class="cta cta--icon cta--icon-left cta--enter newsletter__field__submit">iscriviti</button>
		</div>
		<div class="newsletter__block">
			<div class="newsletter__check">
				<div class="rad">
					<fieldset id="radP1a-fieldset">
						<div>
							<input id="radP1aYes" type="radio" value="1"
								   name="trattamento_privacy"
								   data-validation="privacy_radio" required="required" />
							<label for="radP1aYes"><span>Si</span></label>
						</div>
						<div>
							<input id="radP1aNo" type="radio" value="0"
								   name="trattamento_privacy"
								   data-validation="privacy_radio" required="required"/>
							<label for="radP1aNo"><span>No</span></label>
						</div>
					</fieldset>
					<label for="radP1a-fieldset">Ho letto e acconsento l'<a
								href="http://www.greenstyle.it/privacy-policy" rel="nofollow"
								target="_blank">informativa sulla privacy</a></label>
				</div>
			</div>
		</div>

		<div class="newsletter__block">
			<div class="newsletter__check">
				<div class="rad">
					<fieldset id="radP2a-fieldset">
						<div>
							<input id="radP2aYes" type="radio" value="1" name="n_inviadem"
								   data-validation="" />
							<label for="radP2aYes"><span>Si</span></label>
						</div>
						<div>
							<input id="radP2aNo" type="radio" value="0" name="n_inviadem"
								   data-validation="" />
							<label for="radP2aNo"><span>No</span></label>
						</div>
					</fieldset>
					<label for="radP2a-fieldset">Acconsento al trattamento dei dati per
						attività di marketing</label>
				</div>
			</div>
		</div>

	</form>
</div>
