@if ($single_language_switcher)
	<div class="partial-lang">
		<ul>
			@if($single_language_switcher['it'])
				<li class="lang lang--it">
					<a href="{!!$single_language_switcher['it'] !!}" title="{!! __('Leggi in italiano','lifegate') !!}"></a>
				</li>
			@endif
			@if($single_language_switcher['en'])
				<li class="lang lang--en">
					<a href="{!! $single_language_switcher['en'] !!}" title="{!! __('Read in English','lifegate') !!}"></a>
				</li>
			@endif
		</ul>
	</div>
@endif
