<!-- archive.twig -->

@if ( have_posts() )
	@while (have_posts()) @php the_post() @endphp
	@includeFirst(['components.partials.partial-card-'.get_post_type().'-list--k2','components.partials.partial-card-post-list--k2'])
	@endwhile
@endif
