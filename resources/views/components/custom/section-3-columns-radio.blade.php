{{-- section-3-columns.twig --}}
<div class="section-3-columns">
	@if($blocks && method_exists($blocks[0]['posts'],'have_posts') && $blocks[0]['posts']->have_posts() )
		<div class="col-4">
			@include('components.partials.' . $blocks[0]['first_template'])
		</div>
		@while ($blocks[0]['posts']->have_posts()) @php $blocks[0]['posts']->the_post(); $tbm_counter++; @endphp
		<div class="col-4">
			@include('components.partials.' . str_replace('post_type', App::lifegate_get_post_type(), $blocks[0]['template']))
		</div>
		@endwhile

	@endif
</div>
