{{-- social-box--V1.twig --}}
<div class="partial-social partial-social--share">
	<ul>
		<li class="social social--fb {!! isset($layout) && $layout === 'light' ? 'social--fb--light' : '' !!}"><a
					href="https://www.facebook.com/dialog/share?app_id={{ $tbm_fb_app_id }}&display=page&href={!! $share_link !!}&redirect_uri={!! $share_link !!}"
					title="{!! __('Condividi su Facebook','lifegate') !!}" rel="nofollow" target="_blank"></a>
		</li>
		<li class="social social--tw {!! isset($layout) && $layout === 'light' ? 'social--tw--light' : '' !!}"><a
					href="https://twitter.com/share?url={!! $share_link !!}&text={!! rawurlencode( get_the_title() ) !!}"
					title="{!! __('Condividi su Twitter','lifegate') !!}" rel="nofollow" target="_blank"></a>
		</li>
		<li class="social social--ln {!! isset($layout) && $layout === 'light' ? 'social--ln--light' : '' !!}"><a
					href="https://www.linkedin.com/shareArticle?mini=true&url={!! $share_link !!}"
					title="{!! __('Condividi su Linkedin','lifegate') !!}" rel="nofollow" target="_blank"></a>
		</li>
		<li class="social social--pp {!! isset($layout) && $layout === 'light' ? 'social--pp--light' : '' !!}">
			<a href="whatsapp://send?text={!! rawurlencode( get_the_title() ) !!}%20{!! $share_link !!}"
			   title="{!! __('Condividi su Whatsapp','lifegate') !!}"></a>
		</li>
		<li class="social social--tg {!! isset($layout) && $layout === 'light' ? 'social--tg--light' : '' !!}">
			<a href="https://telegram.me/share/url?text={!! rawurlencode( get_the_title() ) !!}&url={!! $share_link !!}"
			   title="{!! __('Condividi su Telegram','lifegate') !!}"></a>
		</li>
	</ul>
</div>
