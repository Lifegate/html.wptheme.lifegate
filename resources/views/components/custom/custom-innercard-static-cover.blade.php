<section>
	<div class="single__featured-image" role="figure" aria-labelledby="featured-image-caption">
		<picture>
			<!--[if IE 9]>
			<video style="display: none;"><![endif]-->
			<source class="lazyload"
					data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(490,490)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(980,980)) !!} 2x"
					media="(max-width: 736px)"/>
			<!--[if IE 9]></video><![endif]-->
			<img class="lazyload"
				 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(980,490)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1960,980)) !!} 2x"
				 alt="{!! the_title() !!}"/>
		</picture>
		<noscript>
			<picture>
				<!--[if IE 9]>
				<video style="display: none;"><![endif]-->
				<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(490,490)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(980,980)) !!} 2x"
						media="(max-width: 736px)"/>
				<!--[if IE 9]></video><![endif]-->
				<img src="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(300,480)) !!}"
					 alt="{!! the_title() !!}"/>
			</picture>
		</noscript>
		<div class="single-card__overlay">
			<p>
				{!! the_excerpt() !!}
			</p>
		</div>
	</div>
	<div class="single-card__footer">
		<div class="post__trends flex">
			@if($related_trends)
				<div>
					@foreach($related_trends as $trend)
						<a href="{!! $trend['link'] !!}">{!! $trend['name'] !!}</a>
					@endforeach
				</div>
			@endif
			@include('components.custom.custom-post-social-box')
		</div>
		<div>
			<a href="{!! $get_nav['next'] !!}" class="cta cta--icon cta--solid cta--icon-right cta--enter">{!! __('Continua', 'lifegate') !!}</a>
		</div>
	</div>
	<div class="single__hero-footer__datas flex">
		{!! lifegate_snippet_post_date() !!},&nbsp;<div class="post__author">{!! $single_post_authors !!}</div>

		@if($single_post_location)
			<span class="post__location">{{ $single_post_location }}</span>
		@endif

		@include('components.custom.custom-post-language-switcher')
	</div>

</section>
