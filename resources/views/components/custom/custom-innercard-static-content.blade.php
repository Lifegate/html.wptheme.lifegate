<section>
	<div class="section sticky-parent">

		<main class="main-content main-content--left">
			@include('components.partials.partial-innercard-static-content')
			<div class="single-card__footer">
				<div class="post__trends flex">
					@if($related_trends)
						<div>
							@foreach($related_trends as $trend)
								<a href="{!! $trend['link'] !!}">{!! $trend['name'] !!}</a>
							@endforeach
						</div>
					@endif
					@include('components.custom.custom-post-social-box')
				</div>
				@if ($paged < $total_cards)
					<div>
						<a href="{!! $get_nav['next'] !!}" class="cta cta--icon cta--solid cta--icon-right cta--enter">{!! __('Continua', 'lifegate') !!}</a>
					</div>
				@endif
			</div>
		</main>
		<aside class="sidebar sidebar--right">
			@include('components.partials.partial-sticky-adv')
		</aside>
	</div>
	<div class="single__hero-footer__datas flex">
		{!! lifegate_snippet_post_date() !!},&nbsp;<div class="post__author">{!! $single_post_authors !!}</div>

		@if($single_post_location)
			<span class="post__location">{{ $single_post_location }}</span>
		@endif

		@include('components.custom.custom-post-language-switcher')
	</div>
</section>
