{{-- social-box--V1.twig --}}
<div class="partial-social partial-social--share partial-social--original">
	<ul>
		<li class="social social--fb"><a
					href="https://www.facebook.com/dialog/share?app_id={{ $tbm_fb_app_id }}&display=page&href={!! $share_link !!}&redirect_uri={!! $share_link !!}"
					title="{!! __('Condividi su Facebook','lifegate') !!}" rel="nofollow" target="_blank"></a>
		</li>
		<li class="social social--tw"><a
					href="https://twitter.com/share?url={!! $share_link !!}&text={!! rawurlencode( get_the_title() ) !!}"
					title="{!! __('Condividi su Twitter','lifegate') !!}" rel="nofollow" target="_blank"></a>
		</li>
		<li class="social social--ln "><a
					href="https://www.linkedin.com/shareArticle?mini=true&url={!! $share_link !!}"
					title="{!! __('Condividi su Linkedin','lifegate') !!}" rel="nofollow" target="_blank"></a>
		</li>
	</ul>
</div>
