<!-- drawer-menu.twig partial -->
<div class="drawer-menu">
	@asset('css/components/partials/drawer-menu.min.css')
	<div class="drawer-menu__menu">
		<header>
			<div class="header__top">
				@if($language_switcher)
					<div class="input-field input-field--select drawer-menu__lang">
						<label for="drawer-menu"></label>
						<select id="drawer-menu" name="drawer-menu" data-placeholder="" data-validation="false"
								required="false"
								class="dropdown ">
							{!! $language_switcher !!}
						</select>
					</div>

				@endif
				<div class="btn btn--close" role="button" aria-label="Chiudi"></div>
			</div>

			<div class="header__search header__search--drawer">
				<form method="get" action="{!! function_exists('pll_home_url') ? pll_home_url() : get_bloginfo('url') !!}">
					<input id="search-checkbox-drawer-search" type="checkbox" hidden/>
					<label for="search-checkbox-drawer-search" role="button"
						   class="header-btn header-btn--search" aria-label="cerca"></label>
					<div class="search__field">
						<input type="text" name="s" value="" autocomplete="off"
							   placeholder="{!!  __('Cerca','lifegate') !!}"/>
						<button type="submit" class="search__field__submit cta cta--icon cta--enter" aria-label="cerca"></button>
					</div>
				</form>
			</div>
		</header>
		<main>
			@if (has_nav_menu('hamburger_navigation'))
				{!! wp_nav_menu($hamburger_menu) !!}
			@endif
			<div class="drawer-menu__block">
			</div>
		</main>
		<div class="drawer-menu__footer">
			<div class="partial-social">
				<ul>
					<li class="social social--fb">
						<a href="https://www.facebook.com/lifegate/" aria-label="facebook" target="_blank" rel="nofollow"></a></li>
					<li class="social social--tw">
						<a href="https://twitter.com/lifegate" aria-label="twitter"
						   target="_blank" rel="nofollow"></a></li>
					<li class="social social--pn">
						<a href="https://www.pinterest.it/lifegate/" aria-label="pinterest" target="_blank" rel="nofollow"></a></li>
					<li class="social social--sp">
						<a href="https://open.spotify.com/user/lifegate" aria-label="spotify" target="_blank" rel="nofollow"></a></li>
					<li class="social social--yt">
						<a href="https://www.youtube.com/channel/UCw6LzVxICKPuUeDrF8JDn0Q"
						   target="_blank" aria-label="youtube" rel="nofollow"></a></li>
					<li class="social social--in">
						<a href="https://www.instagram.com/lifegate/" aria-label="instagram" target="_blank" rel="nofollow"></a></li>
					<li class="social social--ln">
						<a href="https://www.linkedin.com/company/lifegate/" aria-label="linkedin" target="_blank" rel="nofollow"></a></li>
					<li class="social social--tg">
						<a href="https://t.me/lifegate" aria-label="telegram" target="_blank" rel="nofollow"></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="overlay overlay--drawer-menu"></div>
</div>
