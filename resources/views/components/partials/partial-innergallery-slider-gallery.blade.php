<!-- fullwidth-gallery.twig section -->
{{-- use $sc object derived from tbm-common plugin --}}
@isset($sc->gallery)
	<div class="gallery-slider" data-slidesperview="auto">
		<div class="fullwidth-gallery fullwidth-gallery-js">
			@asset('css/components/sections/fullwidth-gallery.min.css')
			<div class="featured-image__box-image featured-image__box-image--V2">
				<div class="single-slider-annuncio__container swiper-container">
					<div class="swiper-wrapper">

						@foreach($sc->gallery as $photo)
							<div class="swiper-slide swiper-slide--small">
								<picture>
									<!--[if IE 9]>
									<video style="display: none;"><![endif]-->
									<source class="lazyload"
											data-srcset="{!! tbm_wp_get_attachment_image_url($photo['ID'],array(360,0)) !!}, {!! tbm_wp_get_attachment_image_url($photo['ID'],array(720,0)) !!} 2x"
											media="(max-width: 736px)"/>
									<!--[if IE 9]></video><![endif]-->
									<img class="lazyload"
										 data-srcset="{!! tbm_wp_get_attachment_image_url($photo['ID'],array(640,0)) !!}, {!! tbm_wp_get_attachment_image_url($photo['ID'],array(1280,0)) !!} 2x"
										 alt="{!! $photo['title'] !!}"/>
								</picture>

								<div class="swiper-slide--small__content">
									@if($photo['caption'])
										<label>{{$photo['caption'] }}</label>
									@endif
								</div>
							</div>
						@endforeach
					</div>
				</div>

				<nav class="single-gallery-slider__nav">
					<ul>
						<!-- <li><span role="button" aria-label="espandi" title="espandi" class="button button--expand"></span></li> -->
						<li><span role="button" aria-label="successiva" title="successiva" class="button button--next"></span></li>
						<li><span href="#" role="button" aria-label="precedente" title="precedente" class="button button--prev"></span></li>
					</ul>
				</nav>


				<div class="swiper-pagination--small"></div>
			</div>

			<div class="featured-image--fullwidth-image" aria-hidden="true" role="slider">
				<div class="featured-image__box-image swiper-container">

					<div class="swiper-wrapper">
						@foreach($sc->gallery as $photo)
							<div class="swiper-slide swiper-slide--big">
								<!-- <picture> -->
								<img data-src="{!! tbm_wp_get_attachment_image_url($photo['ID'],array(1920,0)) !!}"
									 class="swiper-lazy"
									 alt="{!! $photo['title'] !!}"/>
								<!-- </picture> -->
								<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
							</div>
						@endforeach
					</div>

				</div>
				<div class="fullwidth-image__footer">
					<div>
						<div class="swiper-pagination--big"></div>

						<label></label>
					</div>
					<div class="swiper-keyboard-alert"><img src="@asset('images/keyboard-icon.svg')"/></div>
				</div>
				<nav class="single-gallery-slider__nav">
					<ul>
						<li><span role="button" aria-label="espandi" title="espandi"
								  class="button button--collapse"></span>
						</li>
						<li><span data-link="#" role="button" aria-label="successiva" title="successiva"
								  class="button button--next button-slider--next"></span></li>
						<li><span data-link="#" role="button" aria-label="precedente" title="precedente"
								  class="button button--prev button-slider--prev"></span></li>
					</ul>
				</nav>
			</div>
			<div class="fullwidth-gallery__overlay"></div>

		</div>
	</div>
@endisset
