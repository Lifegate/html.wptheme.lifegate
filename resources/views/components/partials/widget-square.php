<?php

$suffix = function_exists( 'pll_current_language' ) && 'en' === pll_current_language() ? '_en' : '';
if(get_field( 'enable_widget_square'. $suffix, 'option' )){
	?>
<!--instagram widget -->
<div class="td-visible-phone lazyload">
	<?php
	echo get_field( 'code_widget_square'. $suffix, 'option' );
	?>
</div>

	<?php
}
