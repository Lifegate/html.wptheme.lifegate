<!-- partial-innerrelated-playervideo--aside.twig -->
@isset($sc)
	<aside class="partial-innerrelated-playervideo--aside">
		<div class="playervideo-wrapper">
			@if ( function_exists( 'tbm_is_amp' ) && tbm_is_amp() )
				{!! html_video_generate_amp_video_player( $sc->id, tbm_get_the_banner( 'AMP_VAST', ' ', ' ', false, true ), $sc, false  ); !!}
			@else
				@if ( function_exists( 'html_video_generate_video_player' ) )
					{!!  html_video_generate_video_player( $sc->id, '', false ) !!}
				@elseif (!empty(get_field( 'tbm_videoid', $sc->id )['url']))
					<video src="{!! get_field( 'tbm_videoid', $sc->id )['url'] !!}" controls
						   controlsList="nodownload"></video>
				@elseif ( is_user_logged_in() && current_user_can( 'edit_posts' ) )
					<div style="padding:10px;background-color: #e85959;color:white">Il video non esiste. <small style="font-size: 70%">Questo testo è visibile solo per editor</small></div>
				@endif
			@endif
		</div>
	</aside>
@endisset
