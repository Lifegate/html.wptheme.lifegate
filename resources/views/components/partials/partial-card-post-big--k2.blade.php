<!-- partial-card-post-big--k2.twig -->
{{-- If breaking news, change layout. Better handle here--}}
@if (get_field('tbm_breaking_news'))
	@include('components.partials.partial-card-breakingnews')
@else
	<article class="partial-card-post-big--k2 @php if(lifegate_post_is_sponsored(get_the_ID())){ echo "partnered"; } @endphp">
		@asset('css/components/partials/partial-card-post-big--k2.min.css')
		@if(has_post_thumbnail(get_the_ID()))
			<div class="card__thumbnail--big">
				<picture data-link="{!! get_permalink() !!}">
					<img class="lazyload"
						 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(280,190)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(560,380)) !!} 2x"
						 alt="{!! the_title() !!}"/>

				</picture>
			</div>
		@endif
		<div class="card__main-container">
			<div class="card__title">
				<a href="{!! get_permalink() !!}"><h3>{!! the_title() !!}</h3></a>
			</div>
			<div class="card__content">
				<p class="abstract">{!! the_excerpt() !!}</p>
			</div>
			<div class="card__footer">

				{!! lifegate_snippet_label("post__story") !!}

				{!! lifegate_snippet_post_date() !!}
			</div>

			{!! lifegate_snippet_post_sponsor_badge(get_the_ID(), "name") !!}

		</div>
	</article>
@endif
