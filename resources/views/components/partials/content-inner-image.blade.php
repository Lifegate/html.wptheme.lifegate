<!-- content-inner-image.twig partial -->

<div class="content-inner-image content-inner-image--left"><!--content-inner-image--left, content-inner-image--right -->
  @asset('css/components/partials/content-inner-image.min.css')
  <figure>
    <img class="lazyload" data-srcset="{{ queryItem.relatedImages[0].srcImg }}, {{ queryItem.relatedImages[0].srcImgx2 }} 2x" alt="{{ queryItem.relatedImages[0].title }}"/>
    <figcaption>{{ queryItem.relatedImages[0].title }}</figcaption>
  </figure>

</div>
