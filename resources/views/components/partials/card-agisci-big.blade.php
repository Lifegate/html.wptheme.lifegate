<!-- card-agisci-big.twig partial -->
<article class="card-agisci-big">
	@asset('css/components/partials/card-agisci-big.min.css')
	<div class="card__title">
		<a href="{!! get_permalink() !!}"><h3>{!! the_title() !!}</h3></a>
	</div>
	<div class="card__content">
		<p class="abstract">{!! the_excerpt() !!}</p>
	</div>
	<div class="card__thumbnail card__thumbnail--big">
		<!-- card__thumbnail--podcast, card__thumbnail--video to add icons-->
		<picture data-link="{!! get_permalink() !!}">
			<img class="lazyload"
				 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(300,200)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(600,400)) !!} 2x"
				 alt="{!! the_title() !!}"/>
		</picture>
		<noscript>
			<picture>
				<!--[if IE 9]>
				<video style="display: none;"><![endif]-->
				<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(300,200)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(600,400)) !!} 2x"
						media="(max-width: 736px)"/>
				<!--[if IE 9]></video><![endif]-->
				<img src="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(300,200)) !!}"
					 alt="{!! the_title() !!}"/>
			</picture>
		</noscript>
	</div>

</article>
