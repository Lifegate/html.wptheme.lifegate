<!-- partial-card-original.twig -->
{{-- If breaking news, change layout. Better handle here--}}
@if (get_field('tbm_breaking_news'))
	@include('components.partials.partial-card-breakingnews')
@else
	<article class="partial-card-original">
		@asset('css/components/partials/partial-card-original.min.css')
		@if(has_post_thumbnail(get_the_ID()))
			<div class="card__thumbnail--big">
				<picture data-link="{!! get_permalink() !!}">
					<img class="lazyload"
						 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(280,190)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(560,380)) !!} 2x"
						 alt="{!! the_title() !!}"/>

				</picture>
			</div>
		@endif
		<div class="card__main-container">
			<div class="card__footer">
				{!! SingleOriginal::original_next_to_the_title(get_the_ID()) !!}
				<div class="date">
					{!! lifegate_snippet_post_date() !!}
				</div>
			</div>
			<div class="card__title">
				<a href="{!! get_permalink() !!}"><h3>{!! the_title() !!}</h3></a>
			</div>

			<div class="card__author">
			<?php echo SingleOriginal::author_card($post); ?>
			</div>
		</div>
	</article>
@endif
