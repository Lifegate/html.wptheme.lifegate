
	<div class="col-6">
		<div class="box">
			@if(has_post_thumbnail(get_the_ID()))
			<a href="{!! get_permalink() !!}" >
				@if(mediacase_video_url(get_the_ID()))
					<svg class="icon icon--video" xmlns="http://www.w3.org/2000/svg" width="84" height="84" viewBox="0 0 84 84">
						<g id="Group_3990" data-name="Group 3990" transform="translate(6 6)">
							<path class="spinner" data-name="Path 2905" d="M36,0A36,36,0,1,0,72,36,36,36,0,0,0,36,0Z" fill="none" stroke="#95c11f" stroke-width="12"/>
							<rect class="Rectangle_226" data-name="Rectangle 226" width="33" height="39" transform="translate(22 17)" fill="#343434"/>
							<path class="Path_2904" data-name="Path 2904" d="M36,0A36,36,0,1,0,72,36,36,36,0,0,0,36,0ZM27,51V21L51,36Z" fill="#fff"/>
						</g>
					</svg>
				@endif
				<img class="lazyload"
					 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(650,310)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1300,620)) !!} 2x"
					 alt="{!! the_title() !!}"/>
			</a>
			@endif
				<div class="box-labels opa up left outdoor">{!! SingleMediacase::mediacase_next_to_the_title(get_the_ID()) !!}</div>
				{!!  lifegate_original_sponsor_snippet(get_the_ID()) !!}
		</div>
		<div class="box-text">
			<a href="{!! get_permalink() !!}" aria-label="">
				<h2 class="article-title">{!! the_title() !!}</h2>
			</a>
		</div>

	</div>

