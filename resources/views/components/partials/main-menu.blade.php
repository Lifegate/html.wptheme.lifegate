<!-- main-menu.twig partial -->
@asset('css/components/partials/main-menu.min.css')
<nav class="main-menu">

	<div class="main-menu__main" aria-label="main-menu">
		<ul>
			@if (has_nav_menu('global_navigation'))
				{!! wp_nav_menu($main_menu) !!}
			@endif
		</ul>
	</div>


	@if (has_nav_menu('magazine_navigation') && (is_page('news') || is_singular('post') || is_tag() ||is_category() || is_tax(array('trend')) ) )
		<div class="main-menu__submenu-magazine" aria-label="magazine-submenu">
			<ul>
				{!! wp_nav_menu($magazine_menu) !!}
			</ul>
		</div>
	@endif

	@if (has_nav_menu('channels_navigation') && ( is_singular('original') || is_tax(array('channel')) ) )
		<div class="main-menu__submenu-magazine" aria-label="magazine-submenu">
			<ul>
				{!! wp_nav_menu($channels_menu) !!}
			</ul>
		</div>
	@endif


	@if (has_nav_menu('casehistory_navigation') && ( is_singular('mediacase') || is_tax(array('casehistory')) ) )
		<div class="main-menu__submenu-magazine" aria-label="magazine-submenu">
			<ul>
				{!! wp_nav_menu($casehistory_menu) !!}
			</ul>
		</div>
	@endif

	@if (has_nav_menu('radio_navigation') && (is_page('radio') || is_singular('show') ) )
		<div class="main-menu__submenu-magazine" aria-label="magazine-submenu">
			<ul>
				{!! wp_nav_menu($radio_menu) !!}
			</ul>
		</div>
	@endif

</nav>
