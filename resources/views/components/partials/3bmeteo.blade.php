<?php
if ( function_exists( 'pll_current_language' ) && 'it' === pll_current_language() ) {
if(get_field("enable_3bmeteo", "options")){
?>
<section class="sticky-parent">
	<script>var trebpath = "@asset('images/3bimg/')";</script>
	<div id="trebmeteo" class="lazyload" data-script="@asset('js/components/partials/3bmeteo.js')">
		<div class="row trebcontainer" style="display: none;">
			@asset('css/components/partials/3bmeteo.min.css')
			<div class="col-4">
				<div>
					<img id="trebimg">
				</div>
			</div>
			<div class="col-8">
				<span id="treblocation"></span>
				<p id="trebline"></p>
				<!-- <p id="trebcta"></p> //-->
			</div>

		</div>
		<div class="row poweredcontainer">
			<div class="col-12">
				<p id="trebpowered"></p>
			</div>
		</div>
	</div>
</section>
<?php
}
}
