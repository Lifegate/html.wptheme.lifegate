<!-- card-post-editoriale.twig partial -->
@if ($hp_pp->have_posts())
	@while ($hp_pp->have_posts()) @php $hp_pp->the_post() @endphp
	<article class="card card-post-editoriale">
		@asset('css/components/partials/partial-card-post-editoriale.min.css')
		<div class="card__main-container">
			<div class="card__title">
				<a href="{!! get_permalink() !!}"><h3>{!! the_title() !!}</h3></a>
			</div>
			<div class="card__footer">
				<div class="post__author">
					{!! lifegate_snippet_card_post_author(get_the_ID(),true,true) !!}
				</div>
			</div>
			<div class="card__content">
				<div class="abstract">{!! tbm_get_paragraph(get_the_ID(), 2) !!}</div>
				<a href="{!! get_permalink() !!}" class="title-link" aria-label="Continua a leggere">Continua a leggere</a>
			</div>
			<div class="card__footer">
				{!! lifegate_snippet_label("post__story") !!}
				{!! lifegate_snippet_post_date() !!}
			</div>
		</div>

	</article>
	@endwhile
@endif
@php wp_reset_postdata(); @endphp
