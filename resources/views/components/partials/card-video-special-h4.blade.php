<!-- card-video-special.twig partial -->
<article class="card-video-special">
	@asset('css/components/partials/card-video-special.css')
	<div class="video-player__container">
		{!! lg_embed_lazy_video_from_id(get_the_ID()) !!}
	</div>
	<div class="card__main-container">
		<div class="card__title">
			<a href="{!! get_permalink() !!}"><h4>{!! the_title() !!}</h4></a>
		</div>
		<div class="card__content">
			<p class="abstract">{!! the_excerpt() !!}</p>
		</div>
		<div class="card__footer">
			{!! lifegate_snippet_label("post__story") !!}
			{!! lifegate_snippet_post_date() !!}
		</div>
	</div>

</article>
