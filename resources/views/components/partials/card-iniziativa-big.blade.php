<!-- card-iniziativa-big.twig partial -->
<article class="card-iniziativa-big">
	@asset('css/components/partials/card-iniziativa-big.min.css')
	<a href="{!! get_permalink() !!}" title="{!! the_title() !!}">
		<div class="card__thumbnail--big">
			<picture>
				<img class="lazyload"
					 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(460,460)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(920,920)) !!} 2x"
					 alt="{!! the_title() !!}"/>
			</picture>
		</div>
		<div class="card__overlay">
			<div class="card__title">
				<h3>{!! the_title() !!}</h3>
			</div>
			<div class="card__content">
				<p class="abstract">{!! the_excerpt() !!}</p>
			</div>
		</div>
	</a>
</article>

