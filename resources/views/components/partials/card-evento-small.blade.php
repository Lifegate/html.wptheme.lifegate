<!-- card-evento-small.twig partial -->

<article class="card-evento-small">
	@asset('css/components/partials/card-evento-small.min.css')
	<div class="card__thumbnail--big">
		<picture data-link="{!! get_permalink() !!}">
			<img class="lazyload"
				 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(180,180)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(360,360)) !!} 2x"
				 alt="{!! the_title() !!}"/>
		</picture>
	</div>
	<div class="card__main-container">
		<label class="event__date"
			   data-day="{{ tbm_get_event_data()['j'] }}"><span>{{ strtolower(tbm_get_event_data()['f']) }}</span></label>
		<div class="card__title">
			<a href="{!! get_permalink() !!}"><h3>{!! the_title() !!}</h3></a>
		</div>
		<div class="card__content">
			<p class="abstract">{!! the_excerpt() !!}</p>
		</div>
		@if(tbm_get_event_data()['location'] || tbm_get_event_data()['tipologia'])
			<div class="card__footer">
				@if(tbm_get_event_data()['location'])
					<label class="event__city">{{ tbm_get_event_data()['location'] }}</label>
					@if(tbm_get_event_data()['tipologia'])
					<label class="post__date"> {{ tbm_get_event_data()['tipologia']}}</label>
					@endif
				@elseif(tbm_get_event_data()['tipologia'])
					<label class="event__city">{{ tbm_get_event_data()['tipologia']}}</label>
				@endif
			</div>
		@endif
	</div>

</article>
