<!-- card-radio-slider.twig partial -->
@asset('css/components/partials/card-radio-slider.min.css')
<article class="card-radio-slider">
	<a href="{!! get_permalink() !!}" title="{!! the_title() !!}">
		<div class="card__thumbnail--big">
			<picture>
				<img class="lazyload"
					 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(450,450)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(900,900)) !!} 2x"
					 alt="{!! the_title() !!}"/>
			</picture>
		</div>
		<div class="card__overlay">
			<div class="card__title">
				<h3>{!! the_title() !!}</h3>
			</div>
		</div>
		<div class="card__footer">
			<button class="play-song-btn" data-tooltip="ascolta da qui" click="javascript:playsong()"></button>
			<iframe title="radio" class="equalizer" src="@asset('images/equalizer-big.html')" alt="equalizer" width="100%" height="60"></iframe>
		</div>
	</a>
</article>
