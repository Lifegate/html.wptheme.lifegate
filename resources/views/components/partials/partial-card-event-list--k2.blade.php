<!-- partial-card-post-list--k2.twig -->
<article class="partial-card-post-list--k2"><!-- add class card--video -->
	@asset('css/components/partials/partial-card-post-list--k2.min.css')
	<div class="card-post-list__figure">
		<div class="card-post-list__image-wrapper">
			<picture data-link="{!! get_permalink() !!}">
				<!--[if IE 9]>
				<video style="display: none;"><![endif]-->
				<source class="lazyload"
						data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(113,75)) !!}"
						media="(max-width: 736px)"/>
				<!--[if IE 9]></video><![endif]-->
				<img class="lazyload"
					 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(200,133)) !!}"
					 alt="{!! the_title() !!}"/>
			</picture>
		</div>
	</div>
	<div class="card-post-list__content">
		<a class="card__title" href="{!! get_permalink() !!}">
			<h3>{!! the_title() !!}</h3>
		</a>
		<p class="abstract">{!! the_excerpt() !!}</p>
		<div class="card__footer">
			<span class="post__story">{{ tbm_get_event_data()['tipologia'] ? tbm_get_event_data()['tipologia'] : 'Evento' }} {{ tbm_get_event_data()['location'] }}</span>
			<label class="post__date post__date--it">{{ tbm_get_event_data()['j'] . ' ' . tbm_get_event_data()['m'] . ' ' . tbm_get_event_data()['y'] }}</label>
		</div>
	</div>
</article>


