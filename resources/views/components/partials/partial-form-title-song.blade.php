<div class="form-title-song lazyload" data-script="@asset('js/components/partials/partial-form-title-song.js')" style="min-width: auto;min-height: auto">
	@asset('css/components/partials/partial-form-title-song.min.css')
	<div>
		<form id="search_song__field__submit">
			<fieldset>
				<div class="input-field">
					<label for="tbm-date">{!! __('Data del brano','lifegate') !!}</label>
					<input id="tbm-date" type="date" name="date" value="" autocomplete="off"
						   placeholder="{!! __('Data del brano','lifegate') !!}"
						   data-validation="true" required="true"/>
				</div>
				<div class="input-field">
					<label for="tbm-hour">{!! __('Ora del brano','lifegate') !!}</label>
					<input id="tbm-hour" type="time" name="hour" data-validation="true"
						   placeholder="{!! __('Ora del brano','lifegate') !!}"
						   required>
				</div>
				<div class="input-field">
					<label for="tbm-email">{!! __('Email','lifegate') !!}</label>
					<input id="tbm-email" class="tbm-email" type="text" name="email" value="" autocomplete="off"
						   placeholder="{!! __('Il tuo indirizzo email','lifegate') !!}"/>
				</div>
				<div class="input-field">
					<div class="newsletter__check">
						<div class="rad">
							<div>
								<input id="tbm-nl" type="checkbox" value="" name="newsletter" data-validation="true"/>
								<label for="tbm-nl"><span>{!! __('Voglio ricevere la Newsletter di LifeGate e accetto la <a style="text-decoration:underline" href="https://www.lifegate.it/privacy">Privacy Policy</a>','lifegate') !!}</span></label>
							</div>
						</div>
					</div>
				</div>
				<div class="input-field">
					<button type="submit" class="cta cta--solid cta--icon cta--icon-right cta--send"
							style="width:200px; margin: 0 auto;">{!! __('Invia','lifegate') !!}
					</button>
				</div>
			</fieldset>
		</form>
		<div class="form__response">
			<span></span>
		</div>
	</div>

</div>
