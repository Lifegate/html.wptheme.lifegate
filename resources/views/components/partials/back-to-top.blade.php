@asset('css/components/partials/back-to-top.min.css')
<style>.back-to-top-btn{display: none;}</style>
<div class="lazyload"  data-script="@asset('js/components/sections/back-to-top.js')">
	<a id="back-to-top" class="back-to-top-btn"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000" xml:space="preserve"><g><path fill="#ef7b10" d="M500,80c231,0,420,189,420,420S731,920,500,920S80,731,80,500S269,80,500,80 M500,10C227,10,10,234,10,500s217,490,490,490s490-217,490-490S773,10,500,10L500,10z"/><path fill="#ef7b10" d="M549,780V402l168,168l63-70L500,220L220,500l63,70l168-168v378H549z"/></g></svg></a>
</div>
