<div class="col-12">
	<div class="article-container">
		<div class="box-image">
			<div class="box">
				@if(has_post_thumbnail(get_the_ID()))
					<a href="{!! get_permalink() !!}" >
						<img class="lazyload"
							 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(800,370)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1600,720)) !!} 2x"
							 alt="{!! the_title() !!}"/>
					</a>
				@endif
				<div class="box-labels opa up left outdoor">{!! SingleOriginal::original_next_to_the_title(get_the_ID()) !!}</div>
				<!-- <div class="box-labels opa up right">{!! lifegate_snippet_post_date() !!}</div> //-->
				{!!  lifegate_original_sponsor_snippet(get_the_ID()) !!}
			</div>
		</div>
		<div class="box-caption">
			<div class="box-text">
				<a href="{!! get_permalink() !!}" aria-label="">
					<h2 class="article-title">{!! the_title() !!}</h2>
					<p>
						{!! the_excerpt() !!}
					</p>
				</a>

			</div>
			<?php echo SingleOriginal::author_box(); ?>
		</div>
	</div>
</div>
