<!-- partial-breadcrumb.twig -->
<div class="partial-breadcrumb">
	@asset('css/components/partials/partial-breadcrumb.min.css')

	<nav aria-label="breadcrumb">
		<ul>
			@if( function_exists( 'bcn_display_list' ) )
				{!!  bcn_display_list() !!}
			@endif
		</ul>
	</nav>
</div>

<?php
if(!is_singular("original") && !is_tax("channel") && !is_singular("mediacase") && !is_tax("casehistory")){
	echo tbm_get_the_banner( 'MASTHEAD_DISPLAY','','',false,false );
}
?>

