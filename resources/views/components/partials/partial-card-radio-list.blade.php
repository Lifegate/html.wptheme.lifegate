<!-- card-radio-list.twig partial -->

<article class="card-radio-list">
	@asset('css/components/partials/partial-card-radio-list.min.css')
	<a href="{!! get_permalink() !!}">
		<div>

			<div class="card__title">
				<h3>{!! the_title() !!}</h3>
			</div>
		</div>
		<span class="title-link"></span>
	</a>
</article>
