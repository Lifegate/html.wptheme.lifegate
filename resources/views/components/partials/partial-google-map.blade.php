<div class="partial-google-map">
	<div id="map">
		<!-- Embed Google Map -->
		<iframe
				width="640"
				height="300"
				frameborder="0" style="border:0"
				src="https://www.google.com/maps/embed/v1/place?key={!! getenv( 'GMAPS_API' )  !!}&amp;q={{ $gmaps['address'] }}"
				allowfullscreen>
		</iframe>
	</div>
</div>
