<!-- card-longform-big.twig partial -->
{{-- IF IS POST
<article class="card-longform-big flex">
	@asset('css/components/partials/card-longform-big.min.css')
	<div class="card__thumbnail--big">
		<picture data-link="{!! get_permalink() !!}">
			<img class="lazyload"
				 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(300,200)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(600,400)) !!} 2x"
				 alt="{!! the_title() !!}"/>
		</picture>
	</div>
	<div class="card__main-container">
		<div class="card__title">
			<a href="{!! get_permalink() !!}"><h3>{!! the_title() !!}</h3></a>
		</div>
		<div class="card__content">
			<p class="abstract">{!! the_excerpt() !!}</p>
		</div>
		<div class="card__footer">
			{!! lifegate_snippet_label("post__story") !!}
			{!! lifegate_snippet_post_date() !!}
			<div class="post__author">
				{!! lifegate_snippet_card_post_author() !!}
			</div>
		</div>
	</div>

</article>
--}}
{{-- IF IS TERM --}}
<article class="card-longform-big flex @php if(lifegate_tax_has_sponsor($hp_cs)){ echo "mag-partnered"; } @endphp">
	@asset('css/components/partials/card-longform-big.min.css')
	@if(lg_term_the_image_id($hp_cs))
		<div class="card__thumbnail--big">
			<picture data-link="{!! lg_term_get_permalink($hp_cs) !!}">
				<img class="lazyload"
					 data-srcset="{!! tbm_wp_get_attachment_image_url(lg_term_the_image_id($hp_cs),array(470, 220)) !!}, {!! tbm_wp_get_attachment_image_url(lg_term_the_image_id($hp_cs),array(840, 440)) !!} 2x"
					 alt="{!! lg_term_the_title($hp_cs) !!}"/>
			</picture>
		</div>
	@endif
	<div class="card__main-container">
		<div class="card__title">
			<a href="{!! lg_term_get_permalink($hp_cs) !!}"><h3>{!! lg_term_the_title($hp_cs) !!}</h3></a>
		</div>
		<div class="card__content">
			<p class="abstract">{!! lg_term_the_excerpt($hp_cs) !!}</p>
		</div>
		<div class="card__footer">
				{!! lifegate_snippet_tax_sponsor_badge($hp_cs, "magazinelink") !!}
		</div>
	</div>

</article>
