<!-- partial-card-video-list--k2.twig -->
<article class="partial-card-post-list--k2">
	@asset('css/components/partials/partial-card-post-list--k2.min.css')
	<div class="card-post-list__content">
		<a class="card__title" target="_blank" href="{!! get_permalink() !!}">
			<h3>{!! the_title() !!}</h3>
		</a>
	</div>

</article>
