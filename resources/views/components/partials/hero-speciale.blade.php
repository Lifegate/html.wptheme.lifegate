<!-- hero-speciale.twig partial -->
<div class="hero-speciale">
	<div class="hero-speciale__partner-container flex">
		@asset('css/components/partials/hero-speciale.min.css')
		@if($sponsor)
			{!! $sponsor !!}
		@endif
		@include('components.custom.custom-post-social-box')
	</div>
	<div class="hero-speciale__featured-image">
		<picture>
			<!--[if IE 9]>
			<video style="display: none;"><![endif]-->
			<source srcset="{!! $image['medium_mobile'] !!}, {!! $image['large_mobile'] !!} 2x"
					media="(max-width: 736px)"/>
			<!--[if IE 9]></video><![endif]-->
			<img class="lazyloaded" srcset="{!! $image['medium_desktop'] !!}, {!! $image['large_desktop'] !!} 2x"
				 alt="{!! the_archive_title() !!}"/>
		</picture>
		<noscript>
			<picture>
				<!--[if IE 9]>
				<video style="display: none;"><![endif]-->
				<source srcset="{!! $image['medium_mobile'] !!}, {!! $image['large_mobile'] !!} 2x"
						media="(max-width: 736px)"/>
				<!--[if IE 9]></video><![endif]-->
				<img src="{!! $image['medium_desktop'] !!}" alt="{!! the_archive_title() !!}"/>
			</picture>
		</noscript>
	</div>

	<div class="hero-speciale__featured-section">
		<div class="col-5">
			@include('components.sections.section-1-columns',array('blocks' => array(
				['template' => 'card-post_type-special','posts' => $special_pp_post]
			)))
		</div>
		<div class="col-3">
			@include('components.sections.section-1-columns',array('blocks' => array(
				['template' => 'partial-card-post_type-big--k2','posts' => $special_sp_post]
			)))
		</div>
		<div class="col-4">
			@include('components.sections.section-1-columns',array('blocks' => array(
				['template' => 'partial-card-post_type-list--k2','posts' => $special_list_posts]
			)))
		</div>
	</div>

</div>
