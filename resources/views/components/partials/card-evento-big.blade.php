<!-- card-evento-big.twig partial -->
@asset('css/components/partials/card-evento-big.min.css')
<article class="card-evento-big">
	<div class="card__thumbnail--big">
		<picture data-link="{!! get_permalink() !!}">
			<img class="lazyload"
				 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(300,300)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(600,600)) !!} 2x"
				 alt="{!! the_title() !!}"/>
		</picture>
	</div>
	<div class="card__main-container">
		<label class="event__date" data-day="{{ tbm_get_event_data()['j'] }}">{{ strtolower(tbm_get_event_data()['f']) }}</label>
		<div class="card__title">
			<a href="{!! get_permalink() !!}"><h3>{!! the_title() !!}</h3></a>
		</div>
		<div class="card__content">
			<p class="abstract">{!! the_excerpt() !!}</p>
		</div>
		@if(tbm_get_event_data()['location'] || tbm_get_event_data()['tipologia'])
			<div class="card__footer">
				@if(tbm_get_event_data()['location'])
					<label class="event__city">{{ tbm_get_event_data()['location'] }}</label>
					@if(tbm_get_event_data()['tipologia'])
						<label class="post__date"> {{ __(tbm_get_event_data()['tipologia'], "lifegate") }}</label>
					@endif
				@elseif(tbm_get_event_data()['tipologia'])
					<label class="event__city">{{ __(tbm_get_event_data()['tipologia'], "lifegate") }}</label>
				@endif
			</div>
		@endif
	</div>
</article>
