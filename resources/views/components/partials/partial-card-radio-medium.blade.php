<!-- card-radio-medium.twig partial -->
<article class="card-radio-medium">
@asset('css/components/partials/partial-card-radio-medium.min.css')
  <div class="card__thumbnail--big">
    <picture data-link="{!! get_permalink() !!}">
      <img class="lazyload" data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(300,200)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(600,400)) !!} 2x" alt="{!! the_title() !!}"/>
      <div class="card-radio__icon-equalizer"></div>
    </picture>
  </div>
  <div class="card__main-container">

    <div class="card__title">
      <a href="{!! get_permalink() !!}"><h3>{!! the_title() !!}</h3></a>
    </div>
  </div>

</article>
