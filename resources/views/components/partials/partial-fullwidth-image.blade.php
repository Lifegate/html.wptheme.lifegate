<!-- fullwidth-image.twig partial -->

<div class="fullwidth-image fullwidth-image-js">
	@asset('css/components/partials/partial-fullwidth-image.css')
	<div class="featured-image--fullwidth-image" aria-hidden="true" role="presentation">
		<div class="featured-image__box-image swiper-container" tabindex="0" aria-label="foto popup viewer"
			 aria-labelledby=".fullwidth-image-caption">
			<div class="swiper-wrapper">
			</div>
		</div>
		<div class="fullwidth-image__footer fullwidth-image-caption">
			<div>
				<label></label>
			</div>
		</div>
		<nav class="single-gallery-slider__nav">
			<ul>
				<li><span role="button" aria-label="espandi" title="espandi" class="button button--collapse"></span>
				</li>
			</ul>
		</nav>
	</div>
</div>
<div class="fullwidth-image__overlay"></div>

<!-- end fullwidth-image modal -->
