<!-- card-longform-slider.twig partial -->
<article class="card-longform-slider">
	@asset('css/components/partials/card-longform-slider.css')
	<a href="{!! get_permalink() !!}" title="{!! the_title() !!}">
		<div class="card__thumbnail--big">
			<picture>
				<img class="lazyload"
					 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(460,460)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(920,920)) !!} 2x"
					 alt="{!! the_title() !!}"/>
			</picture>
		</div>
		<div class="card__overlay">
			<div class="card__title">
				<h3>{!! the_title() !!}</h3>
			</div>
		</div>
	</a>
</article>

