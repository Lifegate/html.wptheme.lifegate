<!-- partial-card-video-big--k2.twig -->

<article class="partial-card-video-big--k2 card--video @php if(lifegate_post_is_sponsored(get_the_ID())){ echo "partnered"; } @endphp">
	@asset('css/components/partials/partial-card-video-big--k2.min.css')
	<div class="card__thumbnail--big">
		<picture data-link="{!! get_permalink() !!}">
			<svg class="icon icon--video" xmlns="http://www.w3.org/2000/svg" width="84" height="84" viewBox="0 0 84 84">
				<g id="Group_3990" data-name="Group 3990" transform="translate(6 6)">
					<path class="spinner" data-name="Path 2905" d="M36,0A36,36,0,1,0,72,36,36,36,0,0,0,36,0Z" fill="none" stroke="#95c11f" stroke-width="12"/>
					<rect class="Rectangle_226" data-name="Rectangle 226" width="33" height="39" transform="translate(22 17)" fill="#343434"/>
					<path class="Path_2904" data-name="Path 2904" d="M36,0A36,36,0,1,0,72,36,36,36,0,0,0,36,0ZM27,51V21L51,36Z" fill="#fff"/>
				</g>
			</svg>
			<img class="lazyload" data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(300,200)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(600,400)) !!} 2x" alt="{!! the_title() !!}"/>
		</picture>
	</div>
	<div class="card__main-container">

	<div class="card__title">
		<a href="{!! get_permalink() !!}"><h3>{!! the_title() !!}</h3></a>
	</div>
	<div class="card__content">
		<p class="abstract">{!! get_the_excerpt() !!}</p>
	</div>
	<div class="card__footer" role="contentinfo">

		{!! lifegate_snippet_label("post__story") !!}

		{!! lifegate_snippet_post_date() !!}

	</div>
	{!! lifegate_snippet_post_sponsor_badge(get_the_ID(), "name") !!}
	</div>
</article>
