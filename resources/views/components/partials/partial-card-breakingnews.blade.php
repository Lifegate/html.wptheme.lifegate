<!-- card-breakingnews.twig partial -->
<article class="card-breakingnews">
	@asset('css/components/partials/partial-card-breakingnews.min.css')
	<div class="card__main-container">
		<div class="card__definition" role="definition">
			<span>Breaking News</span>
		</div>
		<div class="card__title">
			<a href="{!! get_permalink() !!}"><h3>{!! the_title() !!}</h3></a>
		</div>
		<div class="card__content">
			<p class="abstract">{!! the_excerpt() !!}</p>
		</div>
		<div class="card__footer" role="contentinfo">
			{!! lifegate_snippet_label("post__story") !!}
			{!! lifegate_snippet_post_date() !!}
		</div>
	</div>

</article>
