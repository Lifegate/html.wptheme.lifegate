<!-- partial-sticky-adv.twig -->
<div class="partial-sticky-adv" >
	@asset('css/components/partials/partial-sticky-adv.min.css')
	<div class="sticky-element" data-sticky-offset-top="{!! isset($offset) ? $offset : '70' !!}"
		 data-sticky-mediaquery="desktop">
		<div class="sticky-adv">
			@if(empty($banner))
				{!! tbm_get_the_banner( 'BOX_DESKTOP_SIDE_TOP','','',false,false ) !!}
			@else
				{{--  Static adv - Desktop Top --}}
				@if($banner == 'desktop_top')
					{!! tbm_get_the_banner( 'BOX_DESKTOP_SIDE_TOP','','',false,false ) !!}
				@endif

				{{--  Static adv - Mobile Bottom --}}
				@if($banner == 'desktop_bottom')
					{!! tbm_get_the_banner( 'BOX_DESKTOP_SIDE_BOTTOM','','',false,false ) !!}
				@endif

				{{--  Static adv - Mobile Bottom 2--}}
				@if($banner == 'desktop_bottom_2')
					{!! tbm_get_the_banner( 'BOX_DESKTOP_SIDE_BOTTOM_2','','',false,false ) !!}
				@endif


			@endif
		</div>
	</div>
</div>
