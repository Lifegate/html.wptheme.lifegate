<!-- partial-innercard-static-content.twig -->
<div class="partial-innercard-static-content">
	@asset('css/components/partials/partial-innercard-static-content.min.css')
@include('components.partials.partial-single-card-inner-header')

<!-- post content -->
	<div class="editorial">
		{!! $paragraph_array[$card]['html_card_paragrafo'] !!}
	</div>

	<!-- end post content -->
</div>
