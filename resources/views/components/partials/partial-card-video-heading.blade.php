<!-- card-heading.twig partial -->
<div class="swiper-slide" data-video-background="{!! tbm_video_get_file_url() !!}"
	 data-video-poster="{!! tbm_video_get_poster(get_the_ID(),lifegate_get_image_size( 'video', 'poster' ) ) !!}"
	 data-video-poster-media="{!! tbm_video_get_poster(get_the_ID(),lifegate_get_image_size( 'video', 'poster-media' ) ) !!}">
	<article class="card-heading">
		@asset('css/components/partials/card-heading.min.css')
		<div class="single__title flex">
			{!! lifegate_snippet_label('post__story') !!}
			<h3>{!! the_title() !!}</h3>
		</div>
		<p class="abstract">{!! the_excerpt() !!}</p>
		<div><a href="{!! get_permalink() !!}"
				class="cta cta--icon cta--solid cta--ondark cta--icon-right cta--play">{!! __('Riproduci','lifegate') !!}</a>
		</div>
	</article>
</div>
