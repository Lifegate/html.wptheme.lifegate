<!-- partial-head.twig -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, maximum-scale=5, initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
{!! $tbm_critical !!}
<link rel="apple-touch-icon" sizes="180x180" href="@asset('images/apple-touch-icon.png')">
<link rel="icon" type="image/png" sizes="32x32" href="@asset('images/favicon-32x32.png')">
<link rel="icon" type="image/png" sizes="16x16" href="@asset('images/favicon-16x16.png')">
<link rel="mask-icon" color="#5bbad5" href="@asset('images/safari-pinned-tab.svg')">
<link rel="apple-touch-icon" sizes="180x180" href="@asset('images/apple-touch-icon.png')">
<link rel="icon" type="image/png" sizes="32x32" href="@asset('images/favicon-32x32.png')">
<link rel="icon" type="image/png" sizes="194x194" href="@asset('images/favicon-194x194.png')">
<link rel="icon" type="image/png" sizes="192x192" href="@asset('images/android-chrome-192x192.png')">
<link rel="icon" type="image/png" sizes="16x16" href="@asset('images/favicon-16x16.png')">
<link rel="mask-icon" href="@asset('images/safari-pinned-tab.svg')" color="#005039">
<link rel="shortcut icon" href="@asset('images/favicon.ico')">
<meta name="msapplication-TileColor" content="#ffc40d">
<meta name="msapplication-config" content="@asset('images/browserconfig.xml')">
<meta name="theme-color" content="#15907E">
@if(is_front_page() || is_singular('video'))
	<link rel="preconnect" href="https://www.youtube.com">
@endif
<link rel="preconnect" href="https://www.googletagmanager.com">

@php wp_head() @endphp
<style>
	.editorial blockquote cite {
		font-size: 80%;
	}

	{{-- new class for captions --}}
	.lg-mycap-text {
		color: #5f5e5e;
		display: block;
		font-family: CrimsonPro, serif;
		font-size: 14px !important;
		line-height: 18px !important;
		padding: 6px;
		text-align: right;
	}

	.editorial p:last-of-type {
		margin-bottom: 12px;
	}

	{{-- Margin embed --}}
	.editorial .embed-responsive {
		margin-bottom: 12px;
	}

	.fullwidth-gallery .featured-image--fullwidth-image .swiper-container .swiper-wrapper .swiper-slide img {
		border-radius: 0 !important;
	}

	@media all and (max-width: 736px) {
		{{-- move down sponsor in mobile --}}
		.hero-speciale__partner-container .cardpartner {
			margin-bottom: 0;
		}

		{{-- move down map in mobile --}}
		article.card-evento-big .card__main-container .card__footer {
			padding: 4px 0 20px 24px !important;
		}
	}
</style>
@php
	if ( function_exists( 'tbm_get_the_banner' ) ) {
		tbm_get_the_banner( 'HEAD', ' ', ' ', true, true );
		tbm_get_the_banner( 'TRIBOOADV', ' ', ' ', true, true );
	}

	if ( function_exists( 'tbm_head' ) ) {
		tbm_head();
	}
@endphp

<!-- Facebook Pixel Code -->
<script type="text/plain" class="_iub_cs_activate">
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
			'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '1871994926361253');
	fbq('track', 'PageView');
</script>
<!-- End Facebook Pixel Code -->

<!-- linkedin -->
<script type="text/javascript"> _linkedin_partner_id = "3694308"; window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || []; window._linkedin_data_partner_ids.push(_linkedin_partner_id); </script><script type="text/javascript"> (function(l) { if (!l){window.lintrk = function(a,b){window.lintrk.q.push([a,b])}; window.lintrk.q=[]} var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(window.lintrk); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=3694308&fmt=gif" /> </noscript>
<!-- End linkedin -->
