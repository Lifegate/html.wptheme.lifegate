<!-- card-song-big.twig partial -->
<div class="card-song-big lazyload" data-script="@asset('js/components/partials/card-song-big.js')" onclick="javascript: document.location.href='{!! $player_radio !!}';">
	@asset('css/components/partials/card-song-big.min.css')
	<div class="card__thumbnail--big">
		<picture>
			<img class="lazyload"
				 data-srcset="{!! tbm_wp_get_attachment_image_url($song_card_thumbnail["ID"],array(250,260)) !!}, {!! tbm_get_the_post_thumbnail_url($song_card_thumbnail,array(500,520)) !!} 2x"
				 alt="LifeGate Radio"/>
		</picture>
		<iframe title="radio" class="equalizer" src="@asset('images/equalizer-big.html')" alt="equalizer" width="100%" height="60"></iframe>
	</div>
	<div class="card__main-container">
		<div class="card__title">
			<h3>LifeGate Radio</h3>
		</div>
		<div class="card__content">
			<span class="card__artist">LifeGate Radio</span>
		</div>
		<div class="card__footer">
			<a id="radio-card-btn" class="cta cta--icon cta--icon-left cta--play" href="{!! $player_radio !!}">ascolta</a>
		</div>
	</div>
</div>
