<!-- innerrelated-link.twig partial -->

<div class="innerrelated-link">

{% if loop.index <= 1 and innerrelatedlink is not defined or loop.index is not defined and innerrelatedlink is not defined %}

  @asset('css/components/partials/innerrelated-link.min.css')

{% endif %}

</div>
