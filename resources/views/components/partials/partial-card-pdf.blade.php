<!-- card-pdf.twig partial -->

<div class="card-pdf">
	@asset('css/components/partials/partial-card-pdf.min.css')
	<div class="card-pdf__figure">
		<div class="card-pdf__image-wrapper">
			<picture data-link="{!! get_permalink() !!}">
				@if(has_post_thumbnail())
					<img class="lazyload"
						 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(72,148)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(144,296)) !!} 2x"
						 alt="{!! the_title() !!}"/>
				@else
					<img class="lazyload" data-src="@asset('images/icon-pdf.svg')" alt="File PDF"
						 style="background-color: whitesmoke;"/>
				@endif
			</picture>
		</div>
	</div>
	<div class="card-pdf__content">
		<span class="post__story">{!! get_field('tbm_anno_rassegna_stampa',get_the_ID()) !!}</span>
		<a class="card__title" href="{!! get_permalink() !!}">
			<h3>{!! the_title() !!}</h3>
		</a>
		<div class="card__content">
			<p class="abstract">{!! the_excerpt() !!}</p>
		</div>
	</div>

</div>
