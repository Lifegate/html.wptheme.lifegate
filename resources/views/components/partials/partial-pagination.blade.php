<!-- partial-pagination.twig -->
<nav class="partial-pagination">
	@asset('css/components/partials/partial-pagination.min.css')
	<div class="pagination pagination--standard">

		@php
			$pagination_data = (object) wp_parse_args( $pagination, $navigator_data );

		if(function_exists("wp_pagenavi") && is_archive()){

			if($pagination_data->max_num_pages < 100){
				$larger_page_numbers_multiple = 20;
			}else if($pagination_data->max_num_pages < 400){
				$larger_page_numbers_multiple = 50;
			}else{
				$larger_page_numbers_multiple = 100;
			}

		 $nargs = array(
				'wrapper_tag' => 'ul',
				'echo' => false
			);
		 $pagenavi_options = array();
		$pagenavi_options['pages_text'] = "";
		$pagenavi_options['current_text'] = '%PAGE_NUMBER%';
		$pagenavi_options['page_text'] = '%PAGE_NUMBER%';
		$pagenavi_options['first_text'] = '1';
		$pagenavi_options['last_text'] = $pagination_data->max_num_pages;
		$pagenavi_options['next_text'] = '&raquo;';
		$pagenavi_options['prev_text'] = '&laquo;';
		$pagenavi_options['dotright_text'] = '...';
		$pagenavi_options['dotleft_text'] = '...';
		$pagenavi_options['num_pages'] = 10; //continuous block of page numbers
		$pagenavi_options['always_show'] = 0;
		$pagenavi_options['num_larger_page_numbers'] = 10;
		$pagenavi_options['larger_page_numbers_multiple'] = $larger_page_numbers_multiple;
		$pagenavi_options['use_pagenavi_css'] = false;

		 $nargs["options"] = $pagenavi_options;
			 $navi = wp_pagenavi($nargs);
			$navi = str_replace("<ol>", "", $navi);
			 $navi = str_replace("</ol>", "", $navi);
			 echo $navi;
		} else{
				// Get pagination option from controller and parse with defaults ($navigator_data are defaults)
			if($pagination_data->max_num_pages > 1){
			// Define option for custom pagination
			$custom_pagination = array(
				'total'  => $pagination_data->max_num_pages,
				'current' => $pagination_data->page,
				'show_all' => false,
				'end_size' => 1,
				'mid_size' => 3,
				'type' => 'list',
				'next_text' => __(' &rsaquo;', "lifegate"),
				'prev_text' => __('&lsaquo; ', "lifegate")
				);
				echo str_replace($replace_from, $replace_to, paginate_links($custom_pagination));
			}
		}

		@endphp
	</div>
</nav>
