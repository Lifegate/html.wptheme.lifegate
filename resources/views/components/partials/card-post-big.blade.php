<!-- card-radio-big.twig partial -->
@asset('css/components/partials/card-radio-big.min.css')
<article class="card-radio-big">
	<div class="card__category">
		<span>News Musica</span>
	</div>
	<div class="card__thumbnail--big">
		<picture data-link="{!! get_permalink() !!}">
			<img class="lazyload"
				 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(280,190)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(560,380)) !!} 2x"
				 alt="{!! the_title() !!}"/>
		</picture>
	</div>
	<div class="card__main-container">
		<div class="card__title">
			<a href="{!! get_permalink() !!}"><h3>{!! the_title() !!}</h3></a>
		</div>
	</div>

</article>
