<!-- card-radio-big.twig partial -->
@asset('css/components/partials/card-radio-big.min.css')
<article class="card-radio-big">
	<div class="card__category">
		<span>{!! __('Programmi','lifegate') !!}</span>
	</div>
	<div class="card__thumbnail--big">
		<picture data-link="{!! get_permalink() !!}">
			<img class="lazyload"
				 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(300,200)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(600,400)) !!} 2x"
				 alt="{!! the_title() !!}"/>
		</picture>
	</div>
	<div class="card__main-container">
		<div class="card__title">
			<a href="{!! get_permalink() !!}"><h3>{!! the_title() !!}</h3></a>
		</div>
		<div class="card__footer">
			<a class="cta cta--icon-left cta--enter cta--icon"
			   href="{!! get_post_type_archive_link('show') !!}">{!! __('Tutti i programmi','lifegate') !!}</a>
		</div>
	</div>

</article>
