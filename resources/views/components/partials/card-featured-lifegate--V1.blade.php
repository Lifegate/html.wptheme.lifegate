<!-- card-featured-home.twig partial -->
@if ($hp_pp->have_posts())
	@while ($hp_pp->have_posts()) @php $hp_pp->the_post() @endphp

	<article class="card-featured-home">
		@asset('css/components/partials/card-featured-lifegate--V1.css')
		<div class="card__content">
			<div class="card__title">
				<a href="{!! get_permalink() !!}"><h3>{!! the_title() !!}</h3></a>
			</div>
			<div class="card__content">
				<p class="abstract">{!! the_excerpt() !!}</p>
			</div>

			<div class="card__footer">
				{!! lifegate_snippet_label('post__story') !!}
				{!! lifegate_snippet_post_date() !!}
				<div class="post__author">
					{!! lifegate_snippet_card_post_author(get_the_ID()) !!}
				</div>
			</div>

			{!! lifegate_snippet_post_sponsor_badge(get_the_ID(), "name") !!}

		</div>
		<div class="card__thumbnail card__thumbnail--big">
			<!-- card__thumbnail--podcast, card__thumbnail--video to add icons-->
			<picture data-link="{!! get_permalink() !!}">
				<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(400 ,237 )) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(800,474)) !!} 2x"
						media="(max-width: 375px)"/>
				<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(770 ,460 )) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1540,920)) !!} 2x"
						media="(max-width: 1400px)"/>
				<img srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1110,670 )) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(2220,1340)) !!} 2x"
					 alt="{!! the_title() !!}"/>
			</picture>
		</div>

	</article>
	@endwhile
@endif
@php wp_reset_postdata(); @endphp

{!! tbm_get_the_banner( 'BOX_MOBILE_INSIDE_TOP','','',false,false ) !!}
