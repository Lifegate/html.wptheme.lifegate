<!-- partial-innerrelated-post.twig -->
@isset($sc)

	@if (function_exists( 'tbm_is_amp' ) && tbm_is_amp() )
		<div class="partial-amp-innerrelated-post">
			<a href="<?php echo $sc->permalink; ?>"><h3><?php echo $sc->title; ?></h3></a>
		</div>
	@else
		<aside class="inner-related-post partial-innerrelated-post">
			<span>{!! __('Leggi anche','lifegate') !!}</span>
			@asset('css/components/partials/partial-innerrelated-post.css')
			<article class="card innerrelated-post__wrapper" role="article" data-link="{!! $sc->permalink !!}">
				<div class="innerrelated-post__image-wrapper">
					<picture data-link="{!! $sc->permalink !!}">
						<img class="lazyload" width="100" height="100"
							 data-srcset="{!! tbm_get_the_post_thumbnail_url($sc->id,array(100,100)) !!}, {!! tbm_get_the_post_thumbnail_url($sc->id,array(200,200)) !!} 2x"
							 alt="{!! $sc->image_alt !!}"/>
					</picture>
				</div>
				<div>
					<a class="innerrelated-post__title" href="{!! $sc->permalink !!}">
						<h4>{!! $sc->title; !!}</h4>
					</a>

				</div>

			</article>
		</aside>
	@endif
@endisset

