<!-- partial-single-card-inner-header.twig partial -->

<div class="partial-single-card-inner-header">
	@asset('css/components/partials/partial-single-card-inner-header.min.css')
	<div class="static-content__header">
		<a class="header__prev" href="{!! $get_nav['prev'] !!}" title="{!! __('pagina precedente','lifegate') !!}"
		   aria-label="pagina precedente">
			<svg class="icon icon--standard icon--prev" xmlns="http://www.w3.org/2000/svg"
				 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px"
				 viewBox="0 0 24 24" stroke-width="2">
				<g stroke-width="2" transform="translate(0, 0)">
					<line data-cap="butt" data-color="color-2" fill="none" stroke="#343434" stroke-width="2"
						  stroke-miterlimit="10" x1="22" y1="12" x2="2" y2="12" stroke-linejoin="miter"
						  stroke-linecap="butt"></line>
					<polyline fill="none" stroke="#343434" stroke-width="2" stroke-linecap="square"
							  stroke-miterlimit="10" points="9,19 2,12 9,5 " stroke-linejoin="miter"></polyline>
				</g>
			</svg>
		</a>
		<div class="header__nav">
			<span>{!! __('pagina','lifegate') !!}</span>
			<span class="nav__number">{!! $paged !!}</span>
			<span>{!! __('di','lifegate') !!} {!! $total_cards !!}</span>
		</div>
		@if ($paged < $total_cards)
			<a class="header__next" href="{!! $get_nav['next'] !!}" title="{!! __('pagina successiva','lifegate') !!}"
			   aria-label="pagina successiva">
				<svg class="icon icon--standard icon--next" xmlns="http://www.w3.org/2000/svg" width="21.414"
					 height="16.828" viewBox="0 0 21.414 16.828">
					<g transform="translate(0 1.414)">
						<line x2="20" transform="translate(0 7)" fill="none" stroke="#343434" stroke-miterlimit="10"
							  stroke-width="2"/>
						<path d="M2,19l7-7L2,5" transform="translate(11 -5)" fill="none" stroke="#343434"
							  stroke-linecap="square" stroke-miterlimit="10" stroke-width="2"/>
					</g>
				</svg>
			</a>
		@endif
	</div>

</div>
