<!-- card-noimage.twig partial -->
@if ($hp_sp->have_posts())
	@while ($hp_sp->have_posts()) @php $hp_sp->the_post() @endphp
	@if (isset($limit) && $hp_sp->current_post === $limit)
		<article class="card-noimage">
			@asset('css/components/partials/partial-card-post-big--k2.min.css')
			<div class="card__main-container">
				<div class="card__title">
					<a href="{!! get_permalink() !!}"><h3>{!! the_title() !!}</h3></a>
				</div>
				<div class="card__content">
					<p class="abstract">{!! the_excerpt() !!}</p>
				</div>
				<div class="card__footer">

					{!! lifegate_snippet_label("post__story") !!}

					{!! lifegate_snippet_post_date() !!}
				</div>
			</div>

		</article>
	@endif
	@endwhile
@endif
@php wp_reset_postdata(); @endphp
