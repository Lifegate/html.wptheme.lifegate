<!-- card-featured-lifegate--V2.twig partial -->
@asset('css/components/partials/partial-card-featured-lifegate--V2.min.css')
@if ($hp_sp->have_posts())
	@while ($hp_sp->have_posts()) @php $hp_sp->the_post() @endphp
	@if (isset($limit) && $hp_sp->current_post === $limit)
		<article class="card-featured-lifegate--V2">
			<div class="card__main-container">
				<div class="card__title">
					<a href="{!! get_permalink() !!}"><h3>{!! the_title() !!}</h3></a>
				</div>
				<div class="card__content">
					<p class="abstract">{!! the_excerpt() !!}</p>
				</div>
				<div class="card__footer">
					{!! lifegate_snippet_label("post__story") !!}
					{!! lifegate_snippet_post_date() !!}
					<div class="post__author">
						{!! lifegate_snippet_card_post_author(get_the_ID(),false) !!}
					</div>
				</div>
			</div>
			<div class="card__thumbnail--big">
				<picture data-link="{!! get_permalink() !!}">
					<!--[if IE 9]>
					<video style="display: none;"><![endif]-->
					<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(540,540)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(540,540)) !!} 2x"
							media="(max-width: 736px)"/>
					<!--[if IE 9]></video><![endif]-->
					<img class="lazyload"
						 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(540,540)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(540,540)) !!} 2x"
						 alt="{!! the_title() !!}"/>
				</picture>
			</div>

		</article>
	@endif
	@endwhile
@endif
@php wp_reset_postdata(); @endphp
