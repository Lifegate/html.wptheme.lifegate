<?php
$widget_enabled = true;
if(is_singular()){
	// abilito solo per gli ultimi 30 giorni
	$compare = date('Ymd', strtotime("-30 days"));
	$post_date = get_the_date('Ymd');
	if($post_date < $compare) $widget_enabled = false;
}

$suffix = function_exists( 'pll_current_language' ) && 'en' === pll_current_language() ? '_en' : '';

if($widget_enabled && get_field( 'enable_widget_stories'. $suffix, 'option' )){
	?>
<!--instagram widget -->
<div class="td-visible-phone" style="height: 106px">
	<?php
	echo get_field( 'code_widget_stories'. $suffix, 'option' );
	?>
	<style>
		@media  (max-width: 739px){  .trends--section{ display: none !important; }  }
	</style>
</div>

	<?php
}
