@asset('css/components/partials/title-section.min.css')
<!--title-section.twig partial -->
@php $type = !isset($type) ? 'title' : $type; @endphp
@php $class = !isset($class) ? '' : $class; @endphp

@if(!isset($type) || $type === 'title')
	<div class="title-section {!!  ($class) !!}">

		<span class="bottom_line"></span>
		<div>
			@if(isset($text) && $text != "")
				<span>{!!  ($text) !!} </span>
			@endif

			@if(isset($url) && $url != '')
				<a href="{!! $url !!}" class="title-link"  aria-label="{!!  $textlink ? $textlink : 'Leggi tutto' !!}">{!!  $textlink !!} </a>
			@endif
		</div>
	</div>
@endif

@if($type === 'titleSwitchView')
	<div class="title-section title-section--switch title-section-js">
		@if(isset($text) && $text != "")
			<span>{!!  $text !!} </span>
		@endif

		<aside>
			<button class="icon-button icon-button--grid active" title="vista a gliglia"></button>
			<button class="icon-button icon-button--list" title="vista a lista"></button>
		</aside>
	</div>
@endif

@if($type == 'sidebarTitle')
	<div>
		<span class="title title--sidebar">{!!  strtoupper($text)  !!} </span>
	</div>
@endif
