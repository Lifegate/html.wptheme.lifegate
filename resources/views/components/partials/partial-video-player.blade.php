<!-- partial-video-player.twig partial -->
@asset('css/components/partials/partial-video-player.min.css')
<div class="partial-video-player">
	<div class="video-player__container">
		{!! $print_video !!}
	</div>
</div>
