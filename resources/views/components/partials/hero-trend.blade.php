<!-- hero-trend.twig partial -->
<div class="hero-trend">
	@asset('css/components/partials/hero-trend.min.css')
	<picture>
		<!--[if IE 9]>
		<video style="display: none;"><![endif]-->
		<source srcset="{!! $image['medium_mobile'] !!}, {!! $image['large_mobile'] !!} 2x" media="(max-width: 736px)"/>
		<source srcset="{!! $image['medium_tablet'] !!}, {!! $image['large_tablet'] !!} 2x" media="(max-width: 1400px)"/>
		<!--[if IE 9]></video><![endif]-->
		<img class="lazyload" data-srcset="{!! $image['medium'] !!}, {!! $image['large'] !!} 2x"
			 alt="{!! get_the_archive_title() !!}"/>
	</picture>
	<div class="hero-trend__container">
		<div class="single__title">
			<div>
				<span class="post__story">Trend</span>
				<h1>{!! $title !!}</h1>
			</div>
		</div>
		<div class="single__subtitle post__description">
			@if( $trend->description )
				{!! wpautop( $trend->description ) !!}
			@endif
		</div>
	</div>

	<div class="longform-hero__overlay">
		<div class="polygon_1"></div>
		<div class="polygon_2"></div>
	</div>

</div>
