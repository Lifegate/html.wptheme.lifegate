<!-- partial-card-gallery-big--k2.twig -->
@php
	if (has_post_thumbnail(get_the_ID())) {
		$thumbs = array_merge((array) get_post_thumbnail_id(), tbm_get_gallery_thumbs(get_the_ID(),0,2));
	} else {
		$thumbs = tbm_get_gallery_thumbs(get_the_ID(),0,3);
	}
@endphp
<article class="partial-card-gallery-big--k2">
	@asset('css/components/partials/partial-card-gallery-big--k2.min.css')
	<div class="card-gallery-big__figure">
		<div class="card-gallery-big__image-wrapper">
			<picture data-link="{!! get_permalink() !!}">
				<img class="lazyload"
					 data-srcset="{!!tbm_wp_get_attachment_image_url($thumbs[0],array(113,150)) !!}, {!! tbm_wp_get_attachment_image_url($thumbs[0],array(126,300)) !!} 2x"
					 alt="{!! the_title() !!}"/>
			</picture>
		</div>
		<div class="card-gallery-big__image-container">
			<div class="card-gallery-big__image-wrapper">
				<picture data-link="{!! get_permalink() !!}">
					<img class="lazyload"
						 data-srcset="{!! tbm_wp_get_attachment_image_url($thumbs[1],array(113,74)) !!}"
						 alt="{!! the_title() !!}"/>
				</picture>
			</div>
			<div class="card-gallery-big__image-wrapper">
				<span data-link="{!! get_permalink() !!}"
					  class="card-gallery-big__photo-number">+{!! tbm_get_gallery_photo_number(get_the_ID()) !!}</span>
				<picture data-link="{!! get_permalink() !!}">
					<img class="lazyload"
						 data-srcset="{!! tbm_wp_get_attachment_image_url($thumbs[2],array(113,74)) !!}"
						 alt="{!! the_title() !!}"/>
				</picture>
			</div>
		</div>
	</div>
	<div class="card-gallery-big__content">
		<div class="card__title">
			<a href="{!! get_permalink() !!}"><h3>{!! the_title() !!}</h3></a>
		</div>
		<div class="card__content">
			<p class="abstract">{!! the_excerpt() !!}</p>
		</div>
	</div>
	<div class="card__footer">
		{!! lifegate_snippet_label("post__story") !!}
		{!! lifegate_snippet_post_date() !!}
	</div>
</article>
