<!-- card-speciale-big.twig partial -->
@if($ss)
	<article class="card-speciale-big card-speciale-home @php if(lifegate_tax_has_sponsor($ss)){ echo "mag-partnered"; } @endphp">
		@asset('css/components/partials/card-speciale-big.min.css')
		<div class="card__title flex">
			<div>
				<a href="{!! lg_term_get_permalink($ss) !!}"><h3>{!! lg_term_the_title($ss) !!}</h3></a>
				<p class="abstract">{!! lg_term_the_excerpt($ss) !!}</p>
			</div>
		</div>
		@if(lg_term_the_image_id($ss))
			<div class="card__thumbnail--big">
				<picture data-link="{!! lg_term_get_permalink($ss) !!}">
					<img class="lazyload"
						 data-srcset="{!! tbm_wp_get_attachment_image_url(lg_term_the_image_id($ss),array(470,236)) !!}, {!! tbm_wp_get_attachment_image_url(lg_term_the_image_id($ss),array(940,472)) !!} 2x"
						 alt="{!! lg_term_the_title($ss) !!}"/>
				</picture>
			</div>
		@endif
		{!! lifegate_snippet_tax_sponsor_badge($ss, "magazinelink") !!}
	</article>
@endif
