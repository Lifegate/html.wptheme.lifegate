<!-- card-post-special.twig partial -->
<article class="card-post-special @php if(lifegate_post_is_sponsored(get_the_ID())){ echo "partnered"; } @endphp">
	@asset('css/components/partials/card-post-special.min.css')
	<div class="card__thumbnail--big">
		<picture data-link="{!! get_permalink() !!}">
			<svg class="icon icon--best icon--play" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 72 72">
				<rect fill="#343434" width="33" height="39" transform="translate(22 17)"/>
				<path fill="#FFF" d="M36,0A36,36,0,1,0,72,36,36.04,36.04,0,0,0,36,0ZM27,51V21L51,36Z"/>
			</svg>
			<img class="lazyload"
				 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(470,315)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(940,630)) !!} 2x"
				 alt="{!! the_title() !!}"/>
		</picture>
	</div>
	<div class="card__main-container">
		<div class="card__title">
			<a href="{!! get_permalink() !!}"><h3>{!! the_title() !!}</h3></a>
		</div>
		<div class="card__content">
			<p class="abstract">{!! the_excerpt() !!}</p>
		</div>
		<div class="card__footer">
			{!! lifegate_snippet_label('post__story') !!}
			{!! lifegate_snippet_post_date() !!}
			<div class="post__author">
				{!! lifegate_snippet_card_post_author() !!}
			</div>
		</div>
		{!! lifegate_snippet_post_sponsor_badge(get_the_ID(), "name") !!}

	</div>

</article>

