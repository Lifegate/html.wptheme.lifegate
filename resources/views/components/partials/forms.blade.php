<!-- forms.twig macros -->
{% macro input(id, name, value, type, placeholder, dataValidation, required, css) %}
  {% if css|default(false) %}
      @asset('css/components/partials/forms.min.css')
  {% endif %}
      <input id="{{ id }}" type="{{ type|default('text') }}" name="{{ name }}" value="{{ value|e }}" autocomplete="off" placeholder="{{ placeholder }}" data-validation="{{ dataValidation }}" required="{{ required }}" />
{% endmacro %}

{% macro inputField(id, name, value, type, placeholder, dataValidation, required, label, css) %}
  {% if css|default(false) %}
      @asset('css/components/partials/forms.min.css')
  {% endif %}
    <div class="input-field">
        <label for="{{ id }}">{{ label }}</label>
        <input id="{{ id }}" type="{{ type|default('text') }}" name="{{ name }}" value="{{ value|e }}" autocomplete="off" placeholder="{{ placeholder }}" data-validation="{{ dataValidation }}" required="{{ required }}" />
    </div>
{% endmacro %}

{% macro textAreaField(id, name, value, placeholder, dataValidation, required, label, css) %}
  {% if css|default(false) %}
      @asset('css/components/partials/forms.min.css')
  {% endif %}
    <div class="input-field input-field--textarea">
        <label for="{{ id }}">{{ label }}</label>
        <textarea id="{{ id }}" name="{{ name }}" value="{{ value }}" placeholder="{{ placeholder }}" data-validation="{{ dataValidation }}" required="{{ required }}"></textarea>
    </div>
{% endmacro %}

{% macro selectField(id, label, name, placeholder, options, dataValidation, required, css, class) %}
  {% if css|default(false) %}
      @asset('css/components/partials/forms.min.css')
  {% endif %}
    <div class="input-field input-field--select {{ class }}">
        <label for="{{ id }}">{{ label }}</label>
        <select id="{{ id }}" name="{{ name }}" data-placeholder="{{ placeholder }}" data-validation="{{ dataValidation }}" required="{{ required }}"  class="dropdown chosen-select">
            {% if placeholder != "" %}
              <option selected disabled>{{ placeholder }}</option>
            {% endif %}
            {% for option in options %}
                <option value="{{ option.value }}">{{ option.label }}</option>
            {% endfor %}
        </select>
    </div>
{% endmacro %}

{% macro radiocouple(label1, label2, id, id1, id2, groupName, description, dataValidation, required, css) %}
  {% if css|default(false) %}
      @asset('css/components/partials/forms.min.css')
  {% endif %}
    <div class="newsletter__check">
        <div class="rad">
            <fieldset id="{{ id }}">
                <div>
                    <input id="{{ id1 }}" type="radio" value="1" name="{{ groupName }}" data-validation="{{ dataValidation }}" required="{{ required }}" />
                    <label for="{{ id1 }}"><span>{{ label1 }}</span></label>
                </div>
                <div>
                    <input id="{{ id2 }}" type="radio" value="0" name="{{ groupName }}" data-validation="{{ dataValidation }}" required="{{ required }}" />
                    <label for="{{ id2 }}"><span>{{ label2 }}</span></label>
                </div>
            </fieldset>
            <label for="{{ id }}">{{ description }}</label>
        </div>
    </div>
{% endmacro %}

{% macro checkbox(label, value, checked, css) %}
  {% if css|default(false) %}
      @asset('css/components/partials/forms.min.css')
  {% endif %}

  <div class="input-field input-field--checkbox">
      <input id="{{ value }}" type="checkbox" class="input-field__checkbox" value="{{ value }}"></button>
      <label for="{{ value }}">{{ label }}</label>
  </div>
{% endmacro %}

{% macro newsletter(id, name, value, type, placeholder, dataValidation, required, extended, css) %}
  {% if css|default(false) %}
      @asset('css/components/partials/forms.min.css')
  {% endif %}
    <div class="newsletter__field">
        <input id="{{ id }}" type="{{ type|default('text') }}" name="{{ name }}" value="{{ value|e }}" autocomplete="off" placeholder="{{ placeholder }}" data-validation="{{ dataValidation }}" required="{{ required }}" />
        <button type="submit" class="cta cta--icon cta--icon-left cta--enter newsletter__field__submit">iscriviti</button>
    </div>
    {% if extended|default(false) %}
      <div class="newsletter__block">
        <div class="newsletter__check">
          <div class="rad">
            <fieldset id="radP1a-fieldset">
              <div>
                <input id="radP1aYes" type="radio" value="1"
                     name="trattamento_privacy"
                     data-validation="privacy_radio" required="required" />
                <label for="radP1aYes"><span>Si</span></label>
              </div>
              <div>
                <input id="radP1aNo" type="radio" value="0"
                     name="trattamento_privacy"
                     data-validation="privacy_radio" required="required"/>
                <label for="radP1aNo"><span>No</span></label>
              </div>
            </fieldset>
            <label for="radP1a-fieldset">Ho letto e acconsento l'<a
                href="http://www.greenstyle.it/privacy-policy" rel="nofollow"
                target="_blank">informativa sulla privacy</a></label>
          </div>
        </div>
      </div>

      <div class="newsletter__block">
        <div class="newsletter__check">
          <div class="rad">
            <fieldset id="radP2a-fieldset">
              <div>
                <input id="radP2aYes" type="radio" value="1" name="n_inviadem"
                     data-validation="" />
                <label for="radP2aYes"><span>Si</span></label>
              </div>
              <div>
                <input id="radP2aNo" type="radio" value="0" name="n_inviadem"
                     data-validation="" />
                <label for="radP2aNo"><span>No</span></label>
              </div>
            </fieldset>
            <label for="radP2a-fieldset">Acconsento al trattamento dei dati per
              attività di marketing</label>
          </div>
        </div>
      </div>
    {% endif %}
{% endmacro %}

{% macro search(name, value, type, placeholder, css) %}
  {% if css|default(false) %}
      @asset('css/components/partials/forms.min.css')
  {% endif %}
    <div class="search__field">
        <input type="{{ type|default('text') }}" name="{{ name }}" value="{{ value|e }}" autocomplete="off" placeholder="{{ placeholder }}" />
        <button type="submit" class="search__field__submit">cerca</button>
    </div>
{% endmacro %}

{% macro pagination(name, value, type, min, max, placeholder, css) %}
  {% if css|default(false) %}
      @asset('css/components/partials/forms.min.css')
  {% endif %}
    <div class="pagination__field">
        <input type="{{ type|default('number') }}" name="{{ name }}" value="{{ value|e }}" min="{{ min }}" max="{{ max }}" autocomplete="off" placeholder="{{ placeholder }}" />
        <button type="submit" class="pagination__field__submit">vai</button>
    </div>
{% endmacro %}

{% macro contacts(css) %}
  <div class="contact-form">
    <div>
      {% if css|default(false) %}
        @asset('css/components/partials/forms.min.css')
      {% endif %}
      <div class="input-field">
        <input id="name" type="text" name="name" value="" autocomplete="off" placeholder="Nome..." data-validation="true" required=false />
      </div>
      <div class="input-field">
        <input id="lastname" type="text" name="lastname" value="" autocomplete="off" placeholder="Cognome..." data-validation="true" required=false />
      </div>
      <div class="input-field">
        <input id="phone" type="text" name="phone" value="" autocomplete="off" placeholder="Telefono..." data-validation="true" required=false />
      </div>
      <div class="input-field">
        <input id="email" type="text" name="email" value="" autocomplete="off" placeholder="email..." data-validation="true" required=false />
      </div>
    </div>
    <div>
      <div class="input-field input-field--textarea">
        <textarea id="message" name="message" value="" placeholder="Messaggio..." data-validation="true" required=false></textarea>
      </div>
      <button type="submit" class="cta cta-icon cta--solid cta--icon-right cta--send" style="width:100%">invia</button>
    </div>
  </div>
{% endmacro %}
