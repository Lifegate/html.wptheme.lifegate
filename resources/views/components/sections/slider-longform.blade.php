<!-- slider-iniziative.twig section -->
<div class="slider slider--longform">
	@asset('css/components/sections/slider-longform.css')
	<div class="swiper-container swiper-slider-longform" tabindex="0" aria-label="slider longform">
		<div class="swiper-wrapper">
			@include('components.sections.section-1-columns',array('blocks' => array(
				['template' => 'card-longform-slider','posts' => $hp_lf, 'slider' => true]
				)))
		</div>
	</div>
	<!-- </div> -->
	@if(property_exists($hp_lf,'found_posts') && $hp_lf->found_posts > 3)
		<div class="swiper-slider__nav swiper-slider__next swiper-slider-longform__next"></div>
		<div class="swiper-slider__nav swiper-slider__prev swiper-slider-longform__prev"></div>
	@endif
</div>
