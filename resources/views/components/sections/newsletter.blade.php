<?php
// controllo in quale categoria / tag mi trovo, per personalizzare l'iscrizione

$nl["settimanale"]=array("titolo" => "Iscriviti alla newsletter settimanale", "testo" => "Per rimanere aggiornato sulle notizie dal mondo della sostenibilità", "cta" => "Iscriviti", "link" => home_url("newsletters-lifegate?checked=1"));
$nl["climatariano"]=array("titolo" => "Iscriviti al Climatariano", "testo" => "Per un punto di vista “metabolizzato” sulla crisi climatica", "cta" => "Iscriviti alla newsletter", "link" => home_url("newsletters-lifegate?checked=2"));
$nl["mediterranea"]=array("titolo" => "Iscriviti a Mediterranea", "testo" => "Perché sul piatto non c’è solo il gusto", "cta" => "Iscriviti alla newsletter", "link" => home_url("newsletters-lifegate?checked=3"));
$nl["imprese"]=array("titolo" => "Iscriviti a Pratica", "testo" => "Risposte e soluzioni sostenibili per il mondo del business", "cta" => "Iscriviti alla newsletter", "link" => home_url("newsletters-lifegate?checked=5"));
$nl["evoluta"]=array("titolo" => "Iscriviti a Evoluta", "testo" => "Perché tutte le specie hanno pari diritti", "cta" => "Iscriviti alla newsletter", "link" => home_url("newsletters-lifegate?checked=4"));
$nl["all"]=array("titolo" => "Iscriviti alle newsletter Lifegate", "testo" => "Per rimanere sempre aggiornato", "cta" => "Iscriviti alla newsletter", "link" => home_url("newsletters-lifegate"));

// default per tutti
$nltoshow = $nl["settimanale"];

$ambiente_child = get_term_children( $term_id, $taxonomy_name );


if(is_singular()){
	if(is_page_template("views/page-survey-interna.blade.php")){
		$nltoshow = $nl["imprese"];
	} else if(is_page_template("views/page-list-newsletter.blade.php")){
		$nltoshow = $nl["all"];
	} else if(in_category("ambiente") || in_category("cambiamenti-climatici")){
		$nltoshow = $nl["climatariano"];
	}else if(in_category("alimentazione") || in_category("ricette")){
		$nltoshow = $nl["mediterranea"];
	}else if(in_category("economia") || (has_term("best-practice", "trend"))){
		$nltoshow = $nl["imprese"];
	}else if(in_category("animali") || in_category("animali-domestici") || in_category("biodiversita")){
		$nltoshow = $nl["evoluta"];
	}
}else if(is_category("ambiente") || is_category("cambiamenti-climatici") ){
	$nltoshow = $nl["climatariano"];
}else if(is_category("alimentazione") || is_category("ricette")){
	$nltoshow = $nl["mediterranea"];
}else if(is_category("economia") || is_tax("trend", "best-practice")){
	$nltoshow = $nl["imprese"];
}else if(is_category("animali") || is_category("animali-domestici") || is_category("biodiversita")){
	$nltoshow = $nl["evoluta"];
}



?>
<div class="footer__newsletter">
	<div class="wrapper">
		<div class="container">
			<span class="footer_heading"><?php echo $nltoshow["titolo"] ?></span>
			<span><?php echo $nltoshow["testo"] ?></span>

			<div class="input-field" style="margin-bottom:0px;">
				<a class="cta cta--solid cta--icon cta--icon-right cta--go" href="<?php echo $nltoshow["link"] ?>" style="font-size: 14px; font-weight: 700;">
					<?php echo $nltoshow["cta"] ?> </a>
			</div>

		</div>
	</div>
</div>
