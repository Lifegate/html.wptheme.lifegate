	<div class="site inner-site--noskin"><div class="wrapper"><div class="container"><div class="post__content editorial editorial--longform">

					<blockquote class="lifegate-blockquote">
						<p class="quotation-mark">
						</p>
						<p class="quote-text">{!!  get_field( 'longform_blockquote_testo_citazione' ) !!}</p>
						<p class="autore"><i>{!!  get_field( 'longform_blockquote_autore_citazione' ) !!}</i></p>
					</blockquote>

	</div></div></div></div>
