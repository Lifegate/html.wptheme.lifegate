<?php if(is_singular("longform")){ ?>
<div class="site inner-site--noskin">
	<div class="wrapper">
		<div class="container">
			<div class="post__content editorial editorial--longform">
				<?php } ?>
				<div class="lazyload"  data-script="@asset('js/components/sections/block-indice.js')">
					<div class="submenu-sticky sticky-element" data-sticky-offset-top="60" style="display: flow-root;">
						<button class="submenu-control">Indice</button>
						<div class="submenu-content">
							<h2><strong>{!! __('Indice','lifegate') !!}</strong></h2>
							<div class="<?php if(get_field("longform_indice_numero_colonne") == 1) echo "col-12"; else if(get_field("longform_indice_numero_colonne") == 2) echo "col-6";  ?>">
								<ul class="submenu-nav">
									<?php
									$c=0;
									foreach (get_field("longform_indice_indice_colonna_1") as $item){
										$c++;
										echo '<li><a href="'.$item["link_indice"].'">'.$item["voce_indice"].'</a></li>';
									}
									?>
								</ul>
							</div>
							<?php if(get_field("longform_indice_numero_colonne") > 1){ ?>
							<div class="col-6">
								<ul class="submenu-nav">
									<?php
									foreach (get_field("longform_indice_indice_colonna_2") as $item){
										$c++;
										echo '<li><a href="'.$item["link_indice"].'">'.$item["voce_indice"].'</a></li>';
									}
									?>
								</ul>
							</div>
							<?php
							}
							?>
						</div>
					</div>
				</div>
				<?php if(is_singular("longform")){ ?>
			</div>
		</div>
	</div>
</div>
<?php } ?>
