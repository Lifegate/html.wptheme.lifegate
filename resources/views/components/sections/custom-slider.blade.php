{{-- custom-slider.twig section --}}
@asset('css/components/sections/custom-slider.min.css')
@foreach ($blocks as $block)
	@if(method_exists($block['posts'],'have_posts'))
		<div class="custom-slider custom-slider-js">
			<div class="swiper-container">
				<div class="swiper-wrapper">
					@while ($block['posts']->have_posts()) @php $block['posts']->the_post() @endphp
					<div class="swiper-slide">
						@includeFirst(['components.partials.' . str_replace('post_type', App::lifegate_get_post_type(), $block['template']),'components.partials.' . str_replace('post_type', 'post',  $block['template'])])
					</div>
					@endwhile
				</div>
				<!-- Add Pagination -->
			</div>
			<div class="swiper-pagination"></div>
			<nav class="custom-slider__nav">
				<ul>
					<li><span role="button" aria-label="{!! __('successiva','lifegate') !!}"
							  title="{!! __('successiva','lifegate') !!}" class="button button--next"></span></li>
					<li><span role="button" aria-label="{!! __('precedente','lifegate') !!}"
							  title="{!! __('precedente','lifegate') !!}" class="button button--prev"></span></li>
				</ul>
			</nav>
		</div>
		@php wp_reset_postdata(); @endphp
	@endif
@endforeach
