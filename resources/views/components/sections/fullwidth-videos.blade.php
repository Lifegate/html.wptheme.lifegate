<!-- fullwidth-videos.twig section -->
<section class="fullwidth-videos">
	@asset('css/components/sections/fullwidth-videos.min.css')
	<div class="wrapper">
		<div class="container">
			@include('components.partials.title-section',array('text' => __('Video in evidenza','lifegate'),'textlink' => __('Tutti i video','lifegate'),'url' =>  get_post_type_archive_link(HTML_VIDEO_POST_TYPE)))
			@include('components.sections.section-2-columns',array('blocks' => array(
				['template' => 'card-video-special','posts' => $hp_vp,'post_number' => 1],
				['template' => 'partial-card-video-list--k2','posts' => $hp_vs,'post_number' => 4]
			)))
		</div>
	</div>
</section>
