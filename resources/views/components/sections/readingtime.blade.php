<div class="site inner-site--noskin">
	<div class="wrapper">
		<div class="container">
			<div class="readingtime">
				{!! __('Tempo di lettura','lifegate') !!}: <?php echo lg_reading_time(); ?> min.
			</div>
		</div>
	</div>
</div>
