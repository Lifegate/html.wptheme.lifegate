{{-- section-3-columns.twig --}}
<div class="section-3-columns">
	@if($blocks)
		@foreach ($blocks as $block)
			@if(method_exists($block['posts'],'have_posts'))
				<div class="col-4">
					@while ($block['posts']->have_posts()) @php $block['posts']->the_post(); $tbm_counter++; @endphp
					@if(isset($block['first_template']) && $tbm_counter === 1 )
						@include('components.partials.' . $block['first_template'])
					@else
						{{-- @include('components.partials.' . str_replace('post_type', get_post_type(), $block['template']), array('lazy' => $block['lazy'])) --}}
						@includeFirst(['components.partials.' . str_replace('post_type', App::lifegate_get_post_type(), $block['template']),'components.partials.' . str_replace('post_type', 'post',  $block['template'])], array('lazy' => $block['lazy']))
					@endif
					@if($block['post_number'] > 1 && $tbm_counter < 3)
				</div>
				<div class="col-4 count-{{$tbm_counter}}">
					@endif
					@endwhile
				</div>
				@php wp_reset_postdata(); @endphp
			@else
				<div class="col-4">
					@php $params = ( is_array( $block['params'] ) ) ? ['params' => $block['params'] ] : []; @endphp
					{{-- @include('components.partials.' . str_replace('post_type', get_post_type(), $block['template']), $params) --}}
					@includeFirst(['components.partials.' . str_replace('post_type', App::lifegate_get_post_type(), $block['template']),'components.partials.' . str_replace('post_type', 'post',  $block['template'])], $params)
				</div>
			@endif
		@endforeach
	@endif
</div>
