<div class="site inner-site--noskin"><div class="wrapper"><div class="container"><div class="post__content editorial editorial--longform">

<div class="sponsor-logo">
	<strong>{!!  get_field( 'longform_sponsor_label' ) !!} &nbsp; </strong>
	<a href="{!!  get_field( 'longform_sponsor_link' ) !!}" target="_blank" style="z-index:999;">
		<img src="{!!  wp_get_attachment_image_url( get_field( 'longform_sponsor_logo' ),  "full") !!}" alt="{!!  tbm_get_the_post_thumbnail_alt( get_field( 'longform_sponsor_logo' ) ) !!}"/>
	</a>
</div>

</div></div></div></div>
