<!-- featured-section-home--lifegate-V2.twig section -->
@asset('css/components/sections/section-featured-home--lifegate-V2.min.css')
<div class="featured-section featured-home--lifegate-V2">
	<div class="large-block">
		<div class="container">
			<div class="col-4">
				{{-- query: posts.items.children[1]  --}}
				@include('components.partials.partial-card-post-editoriale')
				{{-- query: posts.items.children[2]  --}}
				@include('components.partials.partial-card-noimage', array( 'limit' => 1 ))
			</div>
			<div class="col-5 order-first">
				{{-- query: posts.items.children[0]  --}}
				@include('components.partials.partial-card-featured-lifegate--V2',array( 'limit' => 0 ))
				{{-- query: posts.items.children[3]  --}}
				@include('components.partials.partial-card-noimage',array( 'limit' => 2 ))
			</div>
			<div class="col-3">
				@include('components.partials.title-section',array("text" => "Agisci ora","textlink"=>"","url" =>$iniziative_hp,"css"=>true))
				@include('components.sections.section-1-columns',array('blocks' => array(
					['template' => 'card-agisci-big','posts' => $hp_ao]
				)))
			</div>
		</div>
	</div>

</div>
