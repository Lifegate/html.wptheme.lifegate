@if(get_field('longform_mutation_text'))
	<div class="background-mutation lazyload" data-script="@asset('js/components/sections/background-mutation.js')"
		 data-text="{!! get_field('longform_mutation_text_color') !!}"
		 data-background="{!! get_field('longform_mutation_background_color') !!}">
		<div class="site inner-site--noskin">
			<div class="wrapper">
				<div class="container">
					<div class="post__content editorial editorial--longform">
						{!! get_field('longform_mutation_text') !!}
					</div>
				</div>
			</div>
		</div>
	</div>
@endif
