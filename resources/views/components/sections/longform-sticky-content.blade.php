<!-- sticky-content-section.twig section -->
@if(get_field( 'longform_sticky_content' ))

	<div class="sticky-section lazyload {!! get_field( 'longform_sticky_content_layout' ) === 'fullwidth' ? 'sticky-section--fullwidth' : '' !!} sticky-parent"
		 data-script="@asset('js/components/sections/sticky-content-section.js')">
		@asset('css/components/sections/sticky-content-section.min.css')
		{{-- query: stickyslider.items.children[0]  --}}
		<div class="sticky-section-images__container sticky-element" data-sticky-offset-top="60">
			<div class="picture-container">
				@foreach(get_field( 'longform_sticky_content' ) as $field)
					{{-- Set the content in array and implode --}}
					@php
						$title = !empty($field['titolo']) ? '<h2>'.$field["titolo"].'</h2>' : '';
						$content = !empty($field['contenuto']) ? $field['contenuto'] : '';

						$paragraphs[] = '<div class="slider-paragraph">'.$title.' '.$content.'</div>';
					@endphp
					<picture class="visible">
						<!--[if IE 9]>
						<video style="display: none;"><![endif]-->
						<source srcset="{!!  tbm_wp_get_attachment_image_url( $field['immagine'], array(
						960,
						960
				) ) !!}, {!!  tbm_wp_get_attachment_image_url( $field['immagine'], array(
						1920,
						1080
				) ) !!} 2x" media="(max-width: 736px)"/>
						<!--[if IE 9]></video><![endif]-->
						<img height="100vh" class="lazyload" data-srcset="{!!  tbm_wp_get_attachment_image_url( $field['immagine'], array(
						960,
						960
				) ) !!}, {!!  tbm_wp_get_attachment_image_url( $field['immagine'], array(
						1920,
						1080
				) ) !!} 2x" alt="{!!  tbm_get_the_post_thumbnail_alt( $field['immagine'] ) !!}"/>
					</picture>
				@endforeach
			</div>
			<div class="dot-container">
				<div class="dot-container__dots"></div>
			</div>
		</div>
		<div class="sticky-section-content__container editorial">
			{!! implode('',$paragraphs) !!}
		</div>
	</div>

@endif
