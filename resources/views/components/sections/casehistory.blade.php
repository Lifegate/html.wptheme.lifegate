<!--trends.twig section -->

@if (has_nav_menu('casehistory_navigation') )


	<section class="trends--section swiper trends-js">
		@asset('css/components/sections/trends.min.css')
		<div class="trends-section__container swiper-container">
			<div class="swiper-wrapper">
				{!! wp_nav_menu($casehistory_navigation) !!}
			</div>
		</div>
		<div class="trendsSlider-nav trendsSlider__next swiper-slider__next"></div>
		<div class="trendsSlider-nav trendsSlider__prev swiper-slider__prev"></div>
	</section>
@endif
