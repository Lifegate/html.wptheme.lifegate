<!-- sticky-content-section.twig section -->

<div class="sticky-section sticky-section-js sticky-parent"><!-- sticky-section--fullwidth -->

{% if loop.index <= 1 and stickycontentsection is not defined or loop.index is not defined and stickycontentsection is not defined %}

  @asset('css/components/sections/sticky-content-section.min.css')

{% endif %}
{{-- query: stickyslider.items.children[0]  --}}
  <div class="sticky-section-images__container sticky-element" data-sticky-offset-top="60">
    <div class="picture-container">
      <picture>
        <!--[if IE 9]><video style="display: none;"><![endif]-->
        <source srcset="undefined, undefined 2x" media="(max-width: 736px)"/>
        <!--[if IE 9]></video><![endif]-->
        <img class="lazyload" data-srcset="undefined, undefined 2x" alt="Sticky-slider-image"/>
      </picture>
      <picture>
        <!--[if IE 9]><video style="display: none;"><![endif]-->
        <source srcset="undefined, undefined 2x" media="(max-width: 736px)"/>
        <!--[if IE 9]></video><![endif]-->
        <img class="lazyload" data-srcset="undefined, undefined 2x" alt="Sticky-slider-image"/>
      </picture>
      <picture>
        <!--[if IE 9]><video style="display: none;"><![endif]-->
        <source srcset="undefined, undefined 2x" media="(max-width: 736px)"/>
        <!--[if IE 9]></video><![endif]-->
        <img class="lazyload" data-srcset="undefined, undefined 2x" alt="Sticky-slider-image"/>
      </picture>
      <picture>
        <!--[if IE 9]><video style="display: none;"><![endif]-->
        <source srcset="undefined, undefined 2x" media="(max-width: 736px)"/>
        <!--[if IE 9]></video><![endif]-->
        <img class="lazyload" data-srcset="undefined, undefined 2x" alt="Sticky-slider-image"/>
      </picture>
    </div>
    <div class="dot-container">
      <div class="dot-container__dots"></div>
    </div>
  </div>
  <div class="sticky-section-content__container editorial">
    {{ queryItem.content }}
  </div>

</div>
