<!-- fullwidth-sentences.twig section -->
@if(get_field('longform_sentences'))
	<div class="fullwidth-sentences lazyload" data-script="@asset('js/components/sections/fullwidth-sentences.js')">
		@asset('css/components/sections/fullwidth-sentences.min.css')
		<ul>
			@foreach(get_field('longform_sentences') as $block)
				<li><span data-fontsize="{!!  $block['dimensione_font'] !!}">{!! $block['contenuto'] !!}</span></li>
			@endforeach
		</ul>
	</div>
@endif
