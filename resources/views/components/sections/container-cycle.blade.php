<!-- container-cycle.twig -->
@if($blocks)
	<div class="container-cycle-{!! $space !!}-{!! $colNumber !!}">
		@foreach ($blocks as $block)
			@if(method_exists($block['posts'],'have_posts'))
				@while ($block['posts']->have_posts()) @php $block['posts']->the_post() @endphp
				@includeFirst(['components.partials.' . str_replace('post_type', App::lifegate_get_post_type(), $block['template']),'components.partials.' . str_replace('post_type', 'post',  $block['template'])])
				@endwhile
				@php wp_reset_postdata(); @endphp
			@endif
		@endforeach
	</div>
@endif
