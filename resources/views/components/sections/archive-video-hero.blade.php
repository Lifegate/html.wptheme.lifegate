<!-- archive-video-hero.twig section -->
<section class="archive-video-hero section sticky-parent">
	@asset('css/components/sections/archive-video-hero.min.css')
	<div class="archive-video-hero__container sticky-element" data-sticky-mediaquery="desktop"
		 data-sticky-offset-top="60">

		@if($featured_video)
			<div class="video-player__container">
				{!! $featured_video !!}
				<div class="overlay overlay--dotted"></div>
			</div>
		@endif
		@if($featured_image)
			<div class="img-background__container">
				{!! $featured_image !!}
				<div class="overlay overlay--dotted"></div>
			</div>
		@endif
		<div class="archive-video-hero__left-block">

			<!--div class="spy-menu spy-menu-js">
				@asset('css/components/partials/spy-menu.min.css')

				<ul role="nav">
					<li><a href="#" class="spy-menu__item spy-menu__item-1">interviste</a></li>
					<li><a href="#" class="spy-menu__item spy-menu__item-2">tour</a></li>
					<li><a href="#" class="spy-menu__item spy-menu__item-3">eventi</a></li>
					<li><a href="#" class="spy-menu__item spy-menu__item-4">tutorial</a></li>
				</ul>
				<span class="spy-menu__marker"></span>
			</div-->

			<div class="archive-video-hero__slider slider custom-slider-js" data-slider-slidesperview="1"
				 data-slider-loop="false" data-slider-space="0">
				<div class="swiper-container hidden">
					<div class="swiper-wrapper">
						@include('components.sections.section-1-columns',array('blocks' => array(
							['template' => 'partial-card-video-heading','posts' => $featured_videos]
						)))

					</div>
					<!-- Add Pagination -->
					<div class="swiper-pagination"></div>
				</div>
				<nav class="custom-slider__nav">
					<ul>
						<!-- <li><span role="button" aria-label="espandi" title="espandi" class="button button--expand"></span></li> -->
						<li><span role="button" aria-label="successiva" title="successiva"
								  class="button swiper-slider__nav swiper-slider__next button--next"></span></li>
						<li><span role="button" aria-label="precedente" title="precedente"
								  class="button swiper-slider__nav swiper-slider__prev button--prev"></span></li>
					</ul>
				</nav>
			</div>

		</div>

		<div class="longform-hero__icon-mouse"></div>
	</div>

	<div class="col-4 archive-video-hero__archive">
		@include('components.partials.title-section',array('text' => 'In evidenza','textlink' => null,'url' =>  null,'css' =>  true))
		<div class="container-cycle-col-2">
			@include('components.sections.section-1-columns',array('blocks' => array(
				['template' => 'partial-card-video-big--k2','posts' => $best_videos]
			)))
		</div>
	</div>

</section>
