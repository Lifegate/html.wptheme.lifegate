{{--  section-1-columns.twig --}}
@foreach ($blocks as $block)
	@php $block['offset'] = isset($block['offset']) ? $block['offset'] : 0; @endphp
	@if(method_exists($block['posts'],'have_posts'))
		@php $block['posts']->rewind_posts(); @endphp
		@while ($block['posts']->have_posts()) @php $block['posts']->the_post() @endphp
		@if(isset($block['slider']) && $block['slider'])
			<div class="swiper-slide">
				@endif
				@if (isset($block['offset']) && $block['offset'])
					@continue($block['posts']->current_post + 1 <= $block['offset'])
				@endif
				@includeFirst(['components.partials.' . str_replace('post_type', App::lifegate_get_post_type(), $block['template']),'components.partials.' . str_replace('post_type', 'post',  $block['template'])])
				@if (isset($block['post_number']) &&  $block['post_number'])
					@break($block['posts']->current_post + 1 >= ($block['post_number'] + $block['offset']))
				@endif
				@if(isset($block['slider']) && $block['slider'])
			</div>
		@endif
		@endwhile
		@php wp_reset_postdata(); @endphp
	@endif
@endforeach
