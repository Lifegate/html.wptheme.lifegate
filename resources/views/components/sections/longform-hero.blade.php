<!-- longform-hero.twig section -->

<section class="longform-hero longform-hero-js"  data-script="@asset('js/components/sections/longform-hero.js')">
	@asset('css/components/sections/longform-hero.css')
	<div class="longform-hero__container">
		<div class="longform-hero__container__bg">
			<picture>
				<!--[if IE 9]>
				<video style="display: none;"><![endif]-->
				<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(414,812)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(828,1624)) !!} 2x"
						media="(max-width: 736px)"/>
				<!--[if IE 9]></video><![endif]-->
				<img class="lazyloaded" srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1920,1080)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(3840,1160)) !!} 2x"
					 alt="{!! the_title() !!}"/>
			</picture>
			<noscript>
				<picture>
					<!--[if IE 9]>
					<video style="display: none;"><![endif]-->
					<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(414,812)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(828,1624)) !!} 2x"
							media="(max-width: 736px)"/>
					<!--[if IE 9]></video><![endif]-->
					<img src="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1920,1280)) !!}"
						 alt="{!! the_title() !!}"/>
				</picture>
			</noscript>

			<div class="longform-hero__overlay">
				<div class="shadow-top"></div>
				<div class="polygon_1"></div>
				<div class="polygon_2"></div>
			</div>

		</div>
		<div class="longform-hero__container__body">
			<div class="single__title single__title--longform">
				<h1>{!! the_title() !!}</h1>
				<span class="title-divider"></span>
				<div class="single__subtitle single__subtitle--longform">
					<p>{!! the_excerpt() !!}</p>
				</div>
			</div>
			<div class="single__hero-footer">
				<div class="single__hero-footer__trends">
					{!! lifegate_snippet_label("post__story") !!}
				</div>
				<div class="single__hero-footer__datas">
					{!! lifegate_snippet_post_date() !!}
					<div class="post__author">{!! $single_post_authors !!}</div>
					@if($single_post_location)
						<span class="post__location">{{ $single_post_location }}</span>
					@endif
				</div>
				<div>
					@include('components.custom.custom-post-social-box',array('layout'=>'light'))
				</div>
			</div>
			<div class="longform-hero__icon-mouse"></div>
		</div>
	</div>

</section>
