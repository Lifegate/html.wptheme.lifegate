{{--  section-2-columns.twig --}}
<div class="section-2-columns">
	@foreach ($blocks as $block)
		@if(!empty($block['posts']) && method_exists($block['posts'],'have_posts'))
			<div class="col-6">
				@while ($block['posts']->have_posts()) @php $block['posts']->the_post(); $tbm_counter++; @endphp
				@includeFirst(['components.partials.' . str_replace('post_type', App::lifegate_get_post_type(), $block['template']),'components.partials.' . str_replace('post_type', 'post', $block['template'])])
				@if($block['post_number'] > 1 && $tbm_counter < 2)
			</div>
			<div class="col-6 count-{{$tbm_counter}}">
				@endif
				@endwhile
			</div>
			@php wp_reset_postdata(); @endphp
		@endif
		@if(isset($block['cta']) && is_array($block['cta']))
			<div class="col-6">
				@include('components.partials.'.$block['template'], ['params' => $block['cta']])
			</div>
		@endif
	@endforeach
</div>

