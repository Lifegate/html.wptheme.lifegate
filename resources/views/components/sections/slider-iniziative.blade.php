<!-- slider-iniziative.twig section -->
<div class="slider slider--iniziative">
	@asset('css/components/sections/slider-iniziative.min.css')
	<div class="swiper-container swiper-slider-iniziative" tabindex="0" aria-label="slider delle iniziative">
		<div class="swiper-wrapper">
			@include('components.sections.section-1-columns',array('blocks' => array(
				['template' => 'card-iniziativa-big','posts' => $hp_iz, 'slider' => true]
				)))
		</div>
	</div>
	<!-- </div> -->
	@if(property_exists($hp_iz,'found_posts') && $hp_iz->found_posts > 3)
		<div class="swiper-slider__nav swiper-slider__next swiper-slider-iniziative__next"></div>
		<div class="swiper-slider__nav swiper-slider__prev swiper-slider-iniziative__prev"></div>
	@endif
</div>
