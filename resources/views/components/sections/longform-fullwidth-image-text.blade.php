@if( get_field( 'longform_immagine_testo_img' ))
	<section class="image-text-full">
		<div class="row">
			<div class="col-6 col-image">
				<div class="card-image">
				<img class="lazyload" data-srcset="{!!  tbm_wp_get_attachment_image_url( get_field( 'longform_immagine_testo_img' ), array(
						640,
						320
				) ) !!}, {!!  tbm_wp_get_attachment_image_url( get_field( 'longform_immagine_testo_img' ), array(
						1280,
						640
				) ) !!} 2x" alt="{!!  tbm_get_the_post_thumbnail_alt( get_field( 'longform_immagine_testo_img' ) ) !!}"/>
				</div>
			</div>
			<div class="col-6 col-text">
				<p>{!!  get_field( 'longform_immagine_testo_txt' ) !!}</p>
			</div>
		</div>
	</section>
@endif
