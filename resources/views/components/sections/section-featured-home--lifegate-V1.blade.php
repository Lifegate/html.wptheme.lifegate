<section class="featured-section featured-home--lifegate-V1">
	@asset('css/components/sections/section-featured-home--lifegate-V1.min.css')
	<div class="large-block">
		{{-- Primo piano --}}
		<div class="container">
			@include('components.partials.card-featured-lifegate--V1')
		</div>

		<div class="container large-block__featured-bottom flex">
			{{-- Secondo piano --}}
			@if($hp_sp->have_posts() )
				<div class="featured-bottom col-7">
					@include('components.sections.section-3-columns',array('blocks' => array(
						['template' => 'partial-card-post_type-big--k2','posts' => $hp_sp,'post_number' => 3, 'lazy' => 'false']
					)))
				</div>
			@endif

			{{-- Agisci ora --}}
			@if($hp_ao->have_posts())
				<div class="act-now-section col-5">
					@include('components.partials.title-section',array('text' => __('Agisci ora','lifegate'),'textlink' => __('Vedi tutti i servizi','lifegate'),'url' =>  $iniziative_hp))
					@include('components.sections.container-cycle',array(
						'space' => 'col',
						'colNumber' => '2',
						'blocks' => array( [
						'template' => 'card-agisci-big',
						'posts' => $hp_ao,
						])))
				</div>
			@endif
		</div>
	</div>
</section>
