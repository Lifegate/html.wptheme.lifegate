@if( get_field( 'longform_immagine_cta_img' ))
	<section class="row section-img-cta" >

		<div class="col-4 block-img" >
			<img class="img"  src="{!! get_field( 'longform_immagine_cta_img' ) !!}" alt="{!!  get_field( 'longform_immagine_cta_titolo' ) !!}"/>
		</div>

		<div class="col-8 block-cta">
			<h3>{!!  get_field( 'longform_immagine_cta_titolo' ) !!}</h3>
			<h4>{!!  get_field( 'longform_immagine_cta_sottotitolo' ) !!}</h4>
			<a href="{!!  get_field( 'longform_immagine_cta_link' ) !!}" class="cta cta--solid">{!!  get_field( 'longform_immagine_cta_label' ) !!}</a>
			@if( get_field( 'longform_immagine_cta_sponsor' ))
				<img class="loghi" src="{!!  get_field( 'longform_immagine_cta_sponsor' ) !!}" />
			@endif
		</div>

	</section>
@endif
