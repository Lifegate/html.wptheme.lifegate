{{-- section-3-columns.twig --}}

@if($blocks)
	<div class="section-4-columns">
		@foreach ($blocks as $block)
			@if(method_exists($block['posts'],'have_posts'))
				<div class="col-3">
					@while ($block['posts']->have_posts()) @php $block['posts']->the_post(); $tbm_counter++; @endphp
					@include('components.partials.' . str_replace('post_type', get_post_type(), $block['template']))
					@if($block['post_number'] > 1 && $tbm_counter < 4)
				</div>
				<div class="col-3 count-{{$tbm_counter}}">
					@endif
					@endwhile
				</div>
				@php wp_reset_postdata(); @endphp
			@else
				<div class="col-3">
					@php $params = ( is_array( $block['params'] ) ) ? ['params' => $block['params'] ] : []; @endphp
					@include('components.partials.' . str_replace('post_type', get_post_type(), $block['template']), $params)
				</div>
			@endif
		@endforeach
	</div>
@endif
