{{--  section-horizontal-iteration.twig section --}}
@asset('css/components/sections/section-horizontal-iteration.min.css')

@php $totalMargins =  $query['gutter'] * $query['colNumber'] @endphp
@php $totalSpace = ($query['colNumber'] * $query['gutter']) - $query['gutter'] @endphp
@php $cardScrollerWidth = ($query['componentNumber']  * 240) + (($query['componentNumber'] * $query['gutter']) - $query['gutter']) @endphp

@php $colMobileNumber = @isset($query['colMobileNumber']) ? $query['colMobileNumber'] : 1 @endphp
@php $sectionhorizontaliteration = @isset($query['sectionhorizontaliteration']) ? true : false @endphp

<style>
	.{{ $query['class'] }} .horizontal-iteration-list > div {
		width: calc((100% - {{ $totalSpace }}px) / {{ $query['colNumber'] }} );
		margin-right: {{ $query['gutter'] }}px;
	}

	.{{ $query['class'] }} .card-scroller .horizontal-iteration-list > div {
		width: calc((100% - {{ $totalSpace }}px) / {{ $query['colNumber'] }} );
		margin-right: {{ $query['gutter'] }}px;
	}

	.{{ $query['class'] }} .horizontal-iteration-list > div[class*="col-"]:nth-of-type({{ $query['colNumber'] }}n) {
		margin-right: 0;
	}

	.{{ $query['class'] }} .horizontal-iteration-list > div:nth-of-type({{ $query['colNumber']}}n + 1) {
		clear: left;
	}

	.{{ $query['class'] }} .horizontal-iteration-list > div:last-of-type {
		margin-right: 0;
	}

	@media screen and (max-width: 767px) {

		.{{ $query['class'] }} .card-scroller .horizontal-iteration-list {
			width: {{ $cardScrollerWidth }}px;
		}

		.{{ $query['class'] }} .horizontal-iteration-list > div {
			width: 100% !important;
			margin-right: 0;
		}

		.{{ $query['class'] }} .card-scroller .horizontal-iteration-list > div, .{{ $query['class'] }} .horizontal-iteration-list > div[class*="col-"]:nth-of-type({{ $query['colNumber']}}n) {
			margin-right: {{ $query['gutter'] }}px;
		}

		.{{ $query['class'] }} .horizontal-iteration-list > div[class*="col-"]:nth-of-type({{ $query['componentNumber']}}n) {
			margin-right: 0 !important;
		}

	}

</style>

<div class="horizontal-iteration-list">
	{{--  section-1-columns.twig --}}
	@if(method_exists($tbmposts,'have_posts'))
		@while ($tbmposts->have_posts()) @php $tbmposts->the_post() @endphp
		<div class="col-{{12 / $query['colNumber']}} col-mobile-{{(12 / $colMobileNumber)}}">
			@includeFirst([ 'components.partials.' . str_replace('post_type', App::lifegate_get_post_type(), $query['component']),'components.partials.' . str_replace('post_type','post', $query['component'])])
		</div>
		@endwhile
		@php wp_reset_postdata(); @endphp
	@endif
</div>
