@if( get_field( 'longform_3_colonne' ))
	<section class="video-image-cta">

		@foreach ( get_field( 'longform_3_colonne' ) as $partial )

			@php $acf_fc_layout = isset( $partial["acf_fc_layout"] ) ? $partial["acf_fc_layout"] : null; @endphp

			@if ( $acf_fc_layout === 'video')
				@php
				$url_video = $partial[ 'youtube_video' ] ? str_replace("https://www.youtube.com/watch?v=", "https://www.youtube-nocookie.com/embed/", $partial[ 'youtube_video' ]) : wp_get_attachment_url($partial["video"]);
				if( $partial[ 'youtube_video_autoplay' ]) $url_video.= "?autoplay=1&mute=1&loop=1";
				@endphp
				@if ($url_video)
				<div class="video-player__container span-4">
					<iframe class="lazyload" width="100%" data-src="{!! $url_video !!}"
							frameborder="0"
							allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
							allowfullscreen=""></iframe>
				</div>
				@endif

			@endif

			@if ( $acf_fc_layout === 'immagine' )
				<div class="card__thumbnail--big span-4">
					<figure><img class="lazyload" data-srcset="{!!  tbm_wp_get_attachment_image_url( $partial['immagine'], array(
						640,
						320
				) ) !!}, {!!  tbm_wp_get_attachment_image_url( $partial['immagine'], array(
						1280,
						640
				) ) !!} 2x" alt="{!!  tbm_get_the_post_thumbnail_alt( $partial['immagine'] ) !!}"/>
						<figcaption>{!!  wp_get_attachment_caption( $partial['immagine'] ) !!}</figcaption>
					</figure>
				</div>
			@endif

			@if ( $acf_fc_layout === 'cta' )
				<div class="inner-cta span-4">
					<h3>{!!  $partial['titolo'] !!}</h3>
					<p>{!!  $partial['contenuto'] !!}</p>
					<a href="{!!  $partial['link_cta'] !!}" class="cta cta--solid cta--icon-right cta--act-now">{!!
				$partial['cta'] !!}</a>
				</div>
			@endif

		@endforeach
		@endif
	</section>

