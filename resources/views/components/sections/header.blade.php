<!--header.twig section -->

<header class="header"><!-- header--dark -->
	<div class="header__block">
		<div class="header__block__left">
			<a
			   title="{!! __('Apri il menu principale','lifegate') !!}"
			   class="header-btn header-btn--hamburger"></a>
			@if(pll_current_language() == "it")
			<a href="https://store.lifegate.com/" class="header-btn header-btn--shop"
			   target="_blank">{!! __('Store','lifegate') !!}</a>
			<a href="{!! get_page_link( get_page_by_path( "newsletters-lifegate" ) ); !!}"
			   class="header-btn header-btn--newsletter" target="_blank">{!! __('Newsletter','lifegate') !!}</a>
			@endif
			<div class="header__search">
				<form id="mainsearchform" method="get"
					  action="{!! function_exists('pll_home_url') ? pll_home_url() : get_bloginfo('url') !!}">
					<input id="search-checkbox-main-search" aria-haspopup="true" aria-controls="search__field"
						   type="checkbox" hidden/>
					<label for="search-checkbox-main-search" aria-label="{!! __('Cerca','lifegate') !!}" role="button"
						   class="header-btn header-btn--search">{!! __('Cerca','lifegate') !!} </label>

					<div class="search__field">
						<label for="search-checkbox-main-search" role="button" class="btn btn--close"></label>
						<input type="text" name="s" value="" autocomplete="off"
							   placeholder="{!! __('Cerca','lifegate') !!}"/>
						<button type="submit"
								class="search__field__submit cta cta--icon cta--icon-left cta--enter"></button>

					</div>
				</form>
			</div>
		</div>
		<div class="header__block__middle">
			<a href="/" class="logo logo--green" title="LifeGate">
				<img src="@asset('images/logo-lifegate.svg')" width="130" height="27" alt="{!! __('LifeGate','lifegate') !!}"
					 title="{!! __('LifeGate','lifegate') !!}"/></a>
			<a href="/" class="logo logo--light" title="LifeGate">
				<img src="@asset('images/logo-lifegate-light.svg')" width="130" height="27" alt="{!! __('LifeGate','lifegate') !!}"
					 title="{!! __('LifeGate','lifegate') !!}"/>
			</a>
			@if(defined( 'WP_ENV' ) && WP_ENV !== 'production')
				<h1>{!! $theme_version !!}</h1>
			@endif
		</div>
		<div class="header__block__right">
			<a href="{!! $player_radio !!}">
			<div class="radio-panel flex flex--center" style="position:relative; z-index:1">
				<div class="transparent"></div>
				<div class="equalizer-container">
					<iframe title="radio" class="equalizer" src="@asset('images/equalizer.html')" alt="equalizer" width="56"
							height="56"></iframe>
				</div>
				<div id="radio-panel-btn" aria-label="{!! __('Ascolta la radio di LifeGate','lifegate') !!}"
					 aria-haspopup="true" role="button"
					 tabindex="0" class="flex flex--column">
					<label class="radio-panel__title">{!! __('LifeGate Radio','lifegate') !!}</label>
					<label class="radio-panel__artist">{!! __('LifeGate Radio','lifegate') !!}</label>
				</div>
			</div>
			</a>
		</div>
	</div>
</header>
<!-- end header -->
