<!-- fullwidth-gallery.twig section -->
@if(get_field('longform_gallery_fullwidth'))
	<section class="gallery-slider" data-slidesperview="3">
		<div class="fullwidth-gallery fullwidth-gallery-js">
			@asset('css/components/sections/fullwidth-gallery.min.css')
			<div class="featured-image__box-image featured-image__box-image--V2">
				<div class="single-slider-annuncio__container swiper-container" tabindex="0"
					 aria-label="fotogallery slider">
					<div class="swiper-wrapper">
						@foreach(get_field('longform_gallery_fullwidth') as $photo)
							<div class="swiper-slide swiper-slide--small">
								<picture>
									<!--[if IE 9]>
									<video style="display: none;"><![endif]-->
									<source srcset="{!! tbm_wp_get_attachment_image_url($photo['ID'],array(148,190)) !!}, {!! tbm_wp_get_attachment_image_url($photo['ID'],array(296,380)) !!} 2x"
											media="(max-width: 736px)"/>
									<!--[if IE 9]></video><![endif]-->
									<img class="lazyload"
										 data-srcset="{!! tbm_wp_get_attachment_image_url($photo['ID'],array(640,0)) !!}, {!! tbm_wp_get_attachment_image_url($photo['ID'],array(1280,0)) !!} 2x"
										 alt="{!! isset($photo['title']) ? $photo['title'] : 'Foto'!!}"/>
								</picture>

								<div class="swiper-slide--small__content">
									@if($photo['title'])
										<h4>{!! $photo['title'] !!}</h4>
									@endif
									@if($photo['caption'])
										<label>{{$photo['caption'] }}</label>
									@endif
								</div>

							</div>
						@endforeach
					</div>
				</div>

				<nav class="single-gallery-slider__nav">
					<ul>
						<!-- <li><span role="button" aria-label="espandi" title="espandi" class="button button--expand"></span></li> -->
						<li><span role="button" aria-label="successiva" title="successiva"
								  class="button button--next"></span></li>
						<li><span href="#" role="button" aria-label="precedente" title="precedente"
								  class="button button--prev"></span></li>
					</ul>
				</nav>

				<div class="swiper-pagination--small"></div>
			</div>

			<div class="featured-image--fullwidth-image" aria-hidden="true" role="presentation">
				<div class="featured-image__box-image swiper-container" tabindex="0"
					 aria-label="fotogallery popup slider" aria-labelledby=".fullwidth-image-caption">

					<div class="swiper-wrapper">
						@foreach(get_field('longform_gallery_fullwidth') as $photo)
							<div class="swiper-slide swiper-slide--big">
								<!-- <picture> -->
								<img data-src="{!! tbm_wp_get_attachment_image_url($photo['ID'],array(1920,1080 )) !!}"
									 class="swiper-lazy"
									 alt="{!! isset($photo['title']) ? $photo['title'] : 'Foto'!!}"/>
								<!-- </picture> -->
								<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
							</div>
						@endforeach
					</div>

				</div>

				<div class="fullwidth-image__footer fullwidth-image-caption">
					<div>
						<div class="swiper-pagination--big"></div>
						<label></label>
					</div>
					<div class="swiper-keyboard-alert"><img src="@asset('images/keyboard-icon.svg')"/></div>
				</div>
				<nav class="single-gallery-slider__nav">
					<ul>
						<li><span role="button" aria-label="espandi" title="espandi"
								  class="button button--collapse"></span></li>
						<li><span role="button" aria-label="successiva" title="successiva"
								  class="button button--next button-slider--next"></span></li>
						<li><span role="button" aria-label="precedente" title="precedente"
								  class="button button--prev button-slider--prev"></span></li>
					</ul>
				</nav>
			</div>
			<div class="fullwidth-gallery__overlay"></div>

		</div>
	</section>
@endif
