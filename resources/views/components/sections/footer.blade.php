<!--footer.twig section -->
<footer class="footer">
	@if(!is_page('newsletter') && !is_page('lista-newsletter') && !is_page_template("views/page-newsletter-multiple.blade.php") && !is_page_template("views/page-newsletter-manuale.blade.php") && !is_singular("post") && (pll_current_language() == "it") && !is_singular("mediacase") && !is_tax("casehistory") && !is_post_type_archive("mediacase"))
		@include('components.sections.newsletter')
	@endif
	<div class="footer__bottom">
		<div class="wrapper">
			<div class="container">
				<div class="col-6">
					<div class="footer__bottom__menu col-6">
						@php dynamic_sidebar('footer_sidebar1') @endphp
					</div>
					<div class="footer__bottom__menu col-6">
						@php dynamic_sidebar('footer_sidebar2') @endphp
					</div>
				</div>
				<div class="col-6">
					<a href="/" class="logo" title="LifeGate">
						<img src="@asset('images/logo-lifegate-light.svg')" width="151" height="35" alt="LifeGate" title="LifeGate | Versione {!! $theme_version !!}"/>
					</a>
					@php dynamic_sidebar('footer_sidebar3') @endphp


					<div class="footer__bottom__social">
						<div class="partial-social" style="--socialnumber: 7">
							<ul>
								<li class="social social--fb social--fb--light">
									<a href="https://www.facebook.com/lifegate/" aria-label="facebook" target="_blank" rel="nofollow"></a>
								</li>
								<li class="social social--tw social--tw--light">
									<a href="https://twitter.com/lifegate" aria-label="twitter"
											target="_blank" rel="nofollow"></a></li>
								<li class="social social--pn social--pn--light">
									<a href="https://www.pinterest.it/lifegate/" aria-label="pinterest" target="_blank" rel="nofollow"></a>
								</li>
								<li class="social social--sp social--sp--light">
									<a href="https://open.spotify.com/user/lifegate" aria-label="spotify" target="_blank" rel="nofollow"></a>
								</li>
								<li class="social social--yt social--yt--light">
									<a href="https://www.youtube.com/channel/UCw6LzVxICKPuUeDrF8JDn0Q"
											aria-label="youtube" target="_blank" rel="nofollow"></a></li>
								<li class="social social--in social--in--light">
									<a href="https://www.instagram.com/lifegate/" aria-label="instagram" target="_blank" rel="nofollow"></a>
								</li>
								<li class="social social--ln social--ln--light">
									<a href="https://www.linkedin.com/company/lifegate/" aria-label="linkedin" target="_blank"
									   rel="nofollow"></a></li>
								<li class="social social--tg social--tg--light">
									<a href="https://t.me/lifegate" target="_blank" aria-label="telegram" rel="nofollow"></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
