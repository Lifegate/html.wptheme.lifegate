<!-- video-fullwidth.twig section -->
@if( get_field( 'longform_video_fullwidth' ) || get_field( 'youtube_video_fullwidth' ))
	<div id="fullwidh-video-container" class="longform-video-fullwidth lazyload sticky-parent" data-script="@asset('js/components/sections/video-fullwidth.js')">
		<div id="fullwidth-video" class="video-player__container sticky-element" data-sticky-offset-top="60">

			@if( get_field( 'youtube_video_fullwidth' ))
				<div class="embed-responsive embed-responsive-16by9">

					<div class="entry-content-asset">
						<iframe class="lazyload"  data-src="@php
							echo str_replace("https://www.youtube.com/watch?v=", "https://www.youtube-nocookie.com/embed/", get_field( 'youtube_video_fullwidth' ));
							if( get_field( 'youtube_video_autoplay' ))
								echo "?autoplay=1&mute=1&loop=1";
							@endphp" frameborder="0"
								allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
								allowfullscreen></iframe>
					</div>
				</div>
			@else
			<video controls="false" loop muted="muted" preload="none">
				<source src="{!! get_field( 'longform_video_fullwidth' ) !!}" type="video/mp4">
			</video>
			<div class="overlay overlay--dotted"></div>
			@endif
		</div>
		@if( get_field( 'titolo_video_fullwidth' ) && get_field( 'contenuto_video_fullwidth' ))
		<div class="video-fullwidth__content">
			@if( get_field( 'titolo_video_fullwidth' ))
				<h2>{!!  get_field( 'titolo_video_fullwidth' ) !!}  </h2>
			@endif
			@if( get_field( 'contenuto_video_fullwidth' ))
				<p>{!! get_field( 'contenuto_video_fullwidth' ) !!}  </p>
			@endif
		</div>
		@endif
	</div>
@endif
