<!-- faq.twig section -->

<div class="faq">

{% if loop.index <= 1 and faq is not defined or loop.index is not defined and faq is not defined %}

@asset('css/components/sections/faq.min.css')

{% endif %}

  <h2>FAQ</h2>
  <p>Le parole del presidente del centro di deradicalizzazione trovano conferma in quelle di un ex guerrigliero jihadista: Abdulay Tidjani. L’uomo è un ex soldato di Boko Haram, ha trascorso tre anni della sua vita tra le fila dei guerriglieri islamisti e oggi lavora facendo il falegname.</p>
  <div class="question-answer">
    <span>"Domanda numero uno?"</span>
    <p>”Il terrorismo della setta jihadista Boko Haram ha approfittato di questa situazione di crisi per penetrare nella regione del Lago, fare proselitismo, reclutare nuovi combattenti e nuovi affiliati ed espandere i confini del Califfato in Africa Occidentale”. A parlare è Ahmat Yacoub, presidente e fondatore del Centro di studi per lo sviluppo.</p>
  </div>
  <div class="question-answer">
    <span>"Domanda numero due?"</span>
    <p>”Il terrorismo della setta jihadista Boko Haram ha approfittato di questa situazione di crisi per penetrare nella regione del Lago, fare proselitismo, reclutare nuovi combattenti e nuovi affiliati ed espandere i confini del Califfato in Africa Occidentale”. A parlare è Ahmat Yacoub, presidente e fondatore del Centro di studi per lo sviluppo.</p>
  </div>
  <div class="question-answer">
    <span>"Domanda numero tre.....?"</span>
    <p>”Il terrorismo della setta jihadista Boko Haram ha approfittato di questa situazione di crisi per penetrare nella regione del Lago, fare proselitismo, reclutare nuovi combattenti e nuovi affiliati ed espandere i confini del Califfato in Africa Occidentale”. A parlare è Ahmat Yacoub, presidente e fondatore del Centro di studi per lo sviluppo.</p>
  </div>
  <div class="question-answer">
    <span>"Domanda numero quattro.....?"</span>
    <p>”Il terrorismo della setta jihadista Boko Haram ha approfittato di questa situazione di crisi per penetrare nella regione del Lago, fare proselitismo, reclutare nuovi combattenti e nuovi affiliati ed espandere i confini del Califfato in Africa Occidentale”. A parlare è Ahmat Yacoub, presidente e fondatore del Centro di studi per lo sviluppo.</p>
  </div>

</div>
