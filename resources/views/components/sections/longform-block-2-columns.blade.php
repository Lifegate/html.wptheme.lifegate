@if(get_field('longform_2_cols_boxed'))
	<div class="site inner-site--noskin"><div class="wrapper"><div class="container"><div class="post__content editorial editorial--longform">
 @endif
	<section class="block-2-columns">
		<div class="row @if(get_field('longform_2_cols_boxed')) boxed @else full @endif">
			<div class="col-6 col-first-column">
				{!!  get_field( 'longform_2_cols_first_col' ) !!}
			</div>
			<div class="col-6 col-second-column">
				{!!  get_field( 'longform_2_cols_second_col' ) !!}
			</div>
		</div>
	</section>
@if(get_field('longform_2_cols_boxed'))
	</div></div></div></div>
@endif
