@asset('css/components/sections/events.min.css')
<div class="section-events">
	@if($hp_ep->have_posts())
	<div class="events__last @php if(!$hp_es->have_posts()) echo "events_alone"; @endphp">
		@include('components.sections.section-1-columns',array('blocks' => array(
			['template' => 'card-evento-big','posts' => $hp_ep]
		)))
		@include('components.partials.title-section',array('text' => null,'textlink' => __('Guarda tutti gli eventi','lifegate'),'url' => get_post_type_archive_link(HTML_EVENT_POST_TYPE)))
	</div>
	@endif

	@if($hp_es->have_posts())
	<div class="events__others @php if(!$hp_ep->have_posts()) echo "events_alone"; @endphp">
		@include('components.sections.section-1-columns',array('blocks' => array(
			['template' => 'card-evento-small','posts' => $hp_es]
		)))
	</div>
	@endif

</div>
