<!-- iniziativa-hero.twig section -->
<section class="iniziativa-hero">
	@asset('css/components/sections/iniziativa-hero.min.css')
	<div class="longform-hero__container">
		<div class="longform-hero__container__bg">
			<picture data-link="{!! get_permalink() !!}">
				<!--[if IE 9]>
				<video style="display: none;"><![endif]-->
				<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(414,812)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(828,1624)) !!} 2x"
						media="(max-width: 736px)"/>
				<!--[if IE 9]></video><![endif]-->
				<img class="lazyload"
					 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1920,1080)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(3840,1160)) !!} 2x"
					 alt="{!! the_title() !!}"/>
			</picture>
			<noscript>
				<picture>
					<!--[if IE 9]>
					<video style="display: none;"><![endif]-->
					<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(414,812)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(828,1624)) !!} 2x"
							media="(max-width: 736px)"/>
					<!--[if IE 9]></video><![endif]-->
					<img src="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1920,1280)) !!}"
						 alt="{!! the_title() !!}"/>
				</picture>
			</noscript>

			<div class="longform-hero__overlay">
				<div class="shadow-top"></div>
				<div class="polygon_1"></div>
				<div class="polygon_2"></div>
			</div>
		</div>
		<div class="longform-hero__container__body">
			<div class="single__title single__title--longform">
				<div>
					<a class="post__story"
					   href="{!! get_post_type_archive_link('iniziative') !!}">{!! __('Iniziative','lifegate') !!}</a>
					<h1>{!! the_title() !!}</h1>
				</div>
				<div class="single__subtitle single__subtitle--longform">
					<p>{!! the_excerpt() !!}</p>
				</div>
				@include('components.custom.custom-post-social-box',['layout'=>'light'])
			</div>

			<div class="single__hero-footer sticky-element" data-sticky-offset-top="60"
				 data-sticky-meadiaquery="desktop">
				@include('components.partials.spy-menu')
				@if($single_iniziative_act_now)
					<div><a href="{!! $single_iniziative_act_now !!}"
							class="cta cta--icon cta--solid cta--ondark cta--icon-right cta--act-now">{!! __('agisci ora!','lifegate') !!}</a>
					</div>
				@endif
			</div>

			<div class="longform-hero__icon-mouse"></div>
		</div>
	</div>

</section>
