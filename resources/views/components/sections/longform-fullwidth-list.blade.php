<!-- fullwidth-list.twig section -->
@asset('css/components/sections/fullwidth-list.min.css')
@if(get_field('longform_lista_fullwidth'))
	<div class="fullwidth-list">
		<div class="fullwidth-list__content">
			<h2>{!! get_field('longform_lista_fullwidth_title') !!}</h2>
			<ol>
				@foreach(get_field('longform_lista_fullwidth') as $block)
					<li>{!! $block['contenuto'] !!}</li>
				@endforeach
			</ol>
		</div>
	</div>
@endif
