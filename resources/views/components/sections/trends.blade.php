<!--trends.twig section -->
@if (has_nav_menu('trends_navigation') )
	<section class="trends--section swiper trends-js">
		@asset('css/components/sections/trends.min.css')
		<div class="trends-section__container swiper-container">
			<div class="swiper-wrapper">
				{!! wp_nav_menu($trends_menu) !!}
			</div>
		</div>
		<div class="trendsSlider-nav trendsSlider__next swiper-slider__next"></div>
		<div class="trendsSlider-nav trendsSlider__prev swiper-slider__prev"></div>
	</section>
@endif

<?php
if(!is_post_type_archive("video"))
	echo tbm_get_the_banner( 'SKIN','','',false,false );

// stampo intestitial solo se inizia con queste lettere:
//global $wp;
//echo  "<!-- debug req=".$wp->request." //-->";
//$enabled = array("a", "b", "c", "d", "e", "f", "g", "h", "i");
//if(in_array(substr($wp->request, 0, 1), $enabled))

echo tbm_get_the_banner( 'INTERSTITIAL','','',false,false );


if(!is_post_type_archive("video"))
	echo tbm_get_the_banner( 'HEADOFPAGE','','',false,false );

?>
