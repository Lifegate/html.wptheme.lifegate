<?php
if ( function_exists( 'pll_current_language' ) && 'en' !== pll_current_language() ) {
if(get_field("original_enable_in_news", "options")){
?>
@asset('css/components/sections/original-home.min.css')
<div class="wrapper-full">
	<?php
	if(get_field("original_open_type", "options") == "singola"){
	$img = get_field("original_image", "options");

	?>
	<section class="original_home_image_full">
		<img src="{!!tbm_wp_get_attachment_image_url($img["ID"],array(1440,700)) !!}" alt="Original">
		<div class="caption">
			<p class="text-white"><?php echo get_field("original_presentazione", "options"); ?></p>
			<marquee behavior="scroll" scrollamount="10" direction="left">
				<span>Original</span> <span>Original</span> <span>Original</span> <span>Original</span> <span>Original</span> <span>Original</span> <span>Original</span> <span>Original</span> <span>Original</span>
			</marquee>
			<a href="<?php echo get_post_type_archive_link("original"); ?>" class="cta-utils" title="scopri Tutti gli articoli" aria-label="scopri Tutti gli articoli">
				Scopri tutti gli articoli
			</a>
		</div>
	</section>
	<?php
	}else if(get_field("original_open_type", "options") == "slider"){
	?>
	<section id="new_carousel" class="original-home original-home-js lazyload" data-script="@asset('js/components/sections/original-home.js')">
		<div class="carousel-container">
			<div class="owl-carousel">
				<?php
				$cards = get_field("original_cards", "options");
				foreach ($cards as $card) {
				?>
				<div class="item-owl-cont">
					<img src="{!!tbm_wp_get_attachment_image_url($card["immagine"]["ID"],array(1440,700)) !!}">
					<div class="play">
						<?php
						if($card["is_video"]){
						?>
						<div class="play-icon"><a href="<?php echo $card["link"]; ?>" title="<?php echo esc_attr($card["testo"]); ?>" aria-label="<?php echo esc_attr($card["testo"]); ?>">
								<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 490 490" style="enable-background:new 0 0 490 490;" xml:space="preserve"><path style="fill:#fff" d="M472.981,245.008L17.019,0v490L472.981,245.008z M47.634,51.214l360.68,193.794L47.634,438.786V51.214z"/></svg>
							</a>
						</div>
						<p><a href="<?php echo $card["link"]; ?>" title="<?php echo esc_attr($card["testo"]); ?>" aria-label="<?php echo esc_attr($card["testo"]); ?>"><?php echo $card["testo"]; ?></a></p>
						<?php
						}else{
							?>
							<a href="<?php echo $card["link"]; ?>" title="<?php echo esc_attr($card["testo"]); ?>" aria-label="<?php echo esc_attr($card["testo"]); ?>"><?php echo $card["testo"]; ?></a>
							<?php
						}
						?>
					</div>
				</div>
				<?php
				}
				?>
			</div>
			<div class="section-caption-utils">
				<div class="section-caption">
					<?php echo get_field("original_presentazione", "options"); ?>
				</div>
				<div class="section-caption">
					<a href="<?php echo get_post_type_archive_link("original"); ?>" class="cta-utils" title="scopri Tutti gli articoli" aria-label="scopri Tutti gli articoli">
						Scopri tutti gli articoli
					</a>
				</div>
			</div>
		</div>
		<marquee behavior="scroll" scrollamount="10" direction="left">
			<span>Original</span> <span>Original</span> <span>Original</span> <span>Original</span> <span>Original</span> <span>Original</span> <span>Original</span> <span>Original</span> <span>Original</span>
		</marquee>
	</section>
	<?php
	}
	?>
</div>
<?php
}
}
?>
