<div class="full-image">
	<figure>
		<img src="{!!  wp_get_attachment_image_url( get_field( 'longform_image' ),  "full") !!}" alt="{!!  tbm_get_the_post_thumbnail_alt( get_field( 'longform_image' ) ) !!}"/>
	</figure>
</div>
