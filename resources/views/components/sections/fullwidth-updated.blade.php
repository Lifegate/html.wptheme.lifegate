<!-- fullwidth-updated.twig section -->
<section class="fullwidth-updated flex lazyload" data-bg="{!! $get_strip['image'] !!}">
	@asset('css/components/sections/fullwidth-updated.min.css')
	<div class="fullwidth-updated__social-container">
		<h3>{!! $get_strip['title_sx'] !!}</h3>{!! $get_strip['sx'] !!}
	</div>
	<div class="fullwidth-updated__newsletter">
		@include('components.custom.custom-strip-newsletter')
	</div>
	<div class="fullwidth-updated__inner-cta inner-cta">
		<h3>{!! $get_strip['title_dx'] !!}</h3>{!! $get_strip['dx'] !!}
	</div>
	<span class="overlay"></span>

</section>
