@extends('base')
@section('content')

	{{-- query: posts.items.children[0]  --}}

	<div class="single single-card">

		<div class="site">

			@include('components.partials.widget-stories')

			@asset('css/single-card.min.css')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
				<div class="container">
					@while(have_posts()) @php the_post(); @endphp
					@include('components.partials.partial-breadcrumb')
					<div class="single__title flex">
						@if($related_trend)
							<a class="post__story"
											  href="{!! $related_trend['link'] !!}">{!! $related_trend['name'] !!}</a>
						@endif
						<h1>{!! the_title() !!}</h1>
					</div>

					{{-- Static innercard cover - just first page --}}
					@if($paged < 2)
						@include('components.custom.custom-innercard-static-cover')
					@endif

					{{-- Static innercard content --}}
					@if($paged > 1)
						@include('components.custom.custom-innercard-static-content')
					@endif

					@endwhile
				</div>
			</div>
		</div>
	</div>

@endsection
