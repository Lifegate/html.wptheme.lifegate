@extends('base')
@if(is_paged() || $special_type === 'V0')
	@include('taxonomy-trend--loop')
@elseif($special_type === 'V1')
	@include('taxonomy-trend--trend')
@else
	@include('taxonomy-trend--loop')
@endif
