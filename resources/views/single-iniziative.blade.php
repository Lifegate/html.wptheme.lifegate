@extends('base')
@section('content')

	<!--single-iniziativa.twig page -->

	{{-- query: initiatives.items.children[0]  --}}
	<div class="single single-iniziativa">
		@while (have_posts()) @php the_post() @endphp
		@include('components.sections.iniziativa-hero')
		<div class="site site--noskin">
			@asset('css/components/partials/partial-sticky-adv.min.css')
			<div class="wrapper">
				<div class="container sticky-parent">

					<main class="main-content main-content--left">
						<div class="post__content editorial editorial--longform">
							{!! the_content() !!}
						</div>
					</main>

					<aside class="sidebar sidebar--right">
						@include('components.partials.partial-sticky-adv',
							['banner' => 'desktop_top', 'offset' => '170', 'mediaquery' => 'desktop'])
					</aside>

				</div>
			</div>
		</div>

		@if($get_strip)
			@include('components.sections.fullwidth-updated')
		@endif

		<div class="site site--noskin">
			<div class="wrapper">
				<div class="container sticky-parent">

					<main class="main-content main-content--left">
						@if($get_contacts)

							<section>
								<div class="post__content editorial editorial--longform">
									{!! $get_contacts !!}
								</div>
							</section>
						@endif
						@if($get_footer)
							<section>
								<div class="post__content editorial editorial--longform">
									{!! $get_footer !!}
								</div>
							</section>
						@endif
					</main>

					<aside class="sidebar sidebar--right">
						@include('components.partials.partial-sticky-adv',
							['banner' => 'desktop_bottom', 'offset' => '170', 'mediaquery' => 'desktop'])
					</aside>

				</div>
			</div>
		</div>
		@endwhile
	</div>
@endsection
