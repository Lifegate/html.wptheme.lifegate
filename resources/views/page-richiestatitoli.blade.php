{{-- /* Template Name: Richiesta titoli */ --}}
@extends('base')
@section('content')
	<!--page-richiestatitoli.twig page -->
	<div class="page-richiestatitoli">
		@asset('css/components/sections/longform-hero.min.css')
		@asset('css/page-newsletter.min.css')
		<section class="longform-hero">
			<div class="longform-hero__container">
				<div class="longform-hero__container__bg">
					<picture>
						<!--[if IE 9]>
						<video style="display: none;"><![endif]-->
						<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(414,812)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(828,1624)) !!} 2x"
								media="(max-width: 736px)"/>
						<!--[if IE 9]></video><![endif]-->
						<img class="lazyload"
							 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1920,1080)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(3840,1160)) !!} 2x"
							 alt="{!! the_title() !!}"/>
					</picture>
					<noscript>
						<picture>
							<!--[if IE 9]>
							<video style="display: none;"><![endif]-->
							<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,320)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1280,640)) !!} 2x"
									media="(max-width: 736px)"/>
							<!--[if IE 9]></video><![endif]-->
							<img src="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,320)) !!}"
								 alt="{!! the_title() !!}"/>
						</picture>
					</noscript>

					<div class="longform-hero__overlay">
						<div class="shadow-top"></div>
						<div class="polygon_1"></div>
						<div class="polygon_2"></div>
					</div>
				</div>
				<div class="longform-hero__container__body">
					<div class="single__title single__title--longform span-6">
						<div>
							<h1>{!! __('Cerca il titolo della tua canzone','lifegate') !!}</h1>
							<div class="single__subtitle single__subtitle--longform">
								<p>{!! __('Per chiedere il titolo di una canzone andata in onda su LifeGate Radio, compila il
									form qui accanto','lifegate') !!}</p>
							</div>
						</div>
					</div>
					<div class="span-6">
						@include('components.partials.partial-form-title-song')
					</div>
				</div>
			</div>

		</section>
	</div>
@endsection

