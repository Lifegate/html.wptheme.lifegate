@extends('base')
@section('head')
	{!! $adk_domination !!}
@endsection

@section('content')


	<!--single-post.twig page -->
	<div class="single @if($is_liveblog_post) single-liveblog @else single-post @endif">
		<div class="site">

			@include('components.partials.widget-stories')

		@if($is_liveblog_post)
				@asset('css/single-liveblog.min.css')
				@asset('css/components/partials/partial-liveblog-entry.min.css')
			@else
				@asset('css/single-post.min.css')
			@endif
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
				@while (have_posts()) @php the_post() @endphp
				<div class="container">
					@include('components.partials.partial-breadcrumb')
					<div class="single__title flex">
						@if($next_to_the_title)
							{!! $next_to_the_title !!}
						@endif
						<h1>{!! the_title() !!}</h1>
					</div>
					@if($post_thumb_id)
						<section class="hide-mobile">
							<div class="single__featured-image" role="figure" aria-labelledby="featured-image-caption-desktop">
								<picture>
									<!--[if IE 9]>
									<video style="display: none;"><![endif]-->
									<source srcset="{!! tbm_wp_get_attachment_image_url($post_thumb_id,array(360,180)) !!}, {!! tbm_wp_get_attachment_image_url($post_thumb_id,array(720,360)) !!} 2x"
											media="(max-width: 736px)"/>
									<!--[if IE 9]></video><![endif]-->
									<img srcset="{!! tbm_wp_get_attachment_image_url($post_thumb_id,array(980,490)) !!}, {!! tbm_wp_get_attachment_image_url($post_thumb_id,array(1960,980)) !!} 2x"
										 alt="{!! the_title() !!}" width="980" height="490"/>
								</picture>
								<span id="featured-image-caption-desktop"
									  class="figcaption">{{ wp_get_attachment_caption(get_post_thumbnail_id()) }}</span>
							</div>
						</section>
					@endif
					<div class="section sticky-offset-element nomargin"  style="display:flex">

						<main class="main-content main-content--left">
							<section>
								<div class="single__hero-footer__box">
									<div class="single__hero-footer__datas">
										<?php
										$date = lifegate_snippet_post_date();
										if($date)
											echo $date.",&nbsp;";
										?>
										<div class="post__author">{!! $single_post_authors !!}</div>
										@if($single_post_location)
											<span class="post__location">{{ $single_post_location }}</span>
										@endif

										@include('components.custom.custom-post-language-switcher')

									</div>
									@if($sponsor)
										{!! $sponsor !!}
									@endif

								</div>
								{!! tbm_get_the_banner( 'BOX_MOBILE_INSIDE_TOP','','',false,false ) !!}
								<div class="post__description editorial">
									<p>{!! the_excerpt() !!}</p>
								</div>
								<div class="post__trends flex">
									@if($related_trends)
										<div>
											@foreach($related_trends as $trend)
												<a href="{!! $trend['link'] !!}">{!! $trend['name'] !!}</a>
											@endforeach
										</div>
									@endif
									@include('components.custom.custom-post-social-box')

								</div>

								{!! tbm_get_the_banner( 'BOX_MOBILE_SPECIAL','','',false,false ) !!}

								@if($post_thumb_id)
									<section class="hide-pc">
										<div class="single__featured-image" role="figure" aria-labelledby="featured-image-caption">
											<picture>
												<!--[if IE 9]>
												<video style="display: none;"><![endif]-->
												<source srcset="{!! tbm_wp_get_attachment_image_url($post_thumb_id,array(360,180)) !!}, {!! tbm_wp_get_attachment_image_url($post_thumb_id,array(720,360)) !!} 2x"
														media="(max-width: 736px)"/>
												<!--[if IE 9]></video><![endif]-->
												<img srcset="{!! tbm_wp_get_attachment_image_url($post_thumb_id,array(980,490)) !!}, {!! tbm_wp_get_attachment_image_url($post_thumb_id,array(1960,980)) !!} 2x"
														alt="{!! the_title() !!}" width="980" height="490"/>
											</picture>
											<span id="featured-image-caption"
													class="figcaption">{{ wp_get_attachment_caption(get_post_thumbnail_id()) }}</span>
										</div>
									</section>
								@endif

								<div class="post__content editorial">
									{!! the_content() !!}
									@if($sponsor)
										<div class="single__hero-footer__box">
											{!! $sponsor !!}
										</div>
									@endif

										<?php
									if(get_field("abilita_live_blog")){
										$listnews = get_field("lista_live");
										?>
									<script type="application/ld+json">
										{
											"@context": "https://schema.org",
											"@type": "LiveBlogPosting",
											"headline": "<?php echo addslashes(get_the_title()); ?>",
											"datePublished": "<?php echo date("Y-m-d\TH:i:sP", get_post_time('U')); ?>",
											"dateModified": "<?php echo date("Y-m-d\TH:i:sP", get_post_modified_time('U')); ?>",
											"liveBlogUpdate": [
											<?php
											$first = true;
											$italianMonths = ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno",
												"Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"];
											$englishMonths = ["January", "February", "March", "April", "May", "June",
												"July", "August", "September", "October", "November", "December"];

											foreach ($listnews as $item) {
												if (!$first) {
													echo ",";
												}
												$first = false;

												// Convertire i nomi dei mesi italiani in inglese
												$italianDate = $item["data_e_ora"];
												$formattedDate = str_replace($italianMonths, $englishMonths, $italianDate);
												$formattedDate = str_replace(',', '', $formattedDate); // Rimuove la virgola

												// Creazione di un oggetto DateTime dal formato specifico
												$date = DateTime::createFromFormat('G:i j F Y', $formattedDate, new DateTimeZone('Europe/Rome'));
												if ($date) {
													$isoDate = $date->format('c');  // Formattazione ISO 8601
													// Sanificazione dell'headline
													$headline = htmlspecialchars($item["titolo"], ENT_QUOTES, 'UTF-8');
													?>
												{
													"@type": "BlogPosting",
													"headline": "<?php echo $headline; ?>",
													"datePublished": "<?php echo $isoDate; ?>",
													"articleBody": "<?php echo addslashes(strip_tags($item["testo"])); ?>"
												}
													<?php
												}
											}
											?>
										],
											<?php
											// Funzione di conversione della data
											function lgconvertToDateTime($dateString, $italianMonths, $englishMonths) {
												$formattedDate = str_replace($italianMonths, $englishMonths, $dateString);
												$formattedDate = str_replace(',', '', $formattedDate); // Rimuove la virgola
												return DateTime::createFromFormat('G:i j F Y', $formattedDate, new DateTimeZone('Europe/Rome'));
											}

											// Trova la data di inizio e di fine
											$dates = array_map(function($item) use ($italianMonths, $englishMonths) {
												return lgconvertToDateTime($item["data_e_ora"], $italianMonths, $englishMonths);
											}, $listnews);

											usort($dates, function($a, $b) {
												return $a <=> $b;
											});

											$startDate = $dates[0]->format('c');
											$endDate = end($dates)->modify('+1 day')->format('c');
											?>

										"coverageStartTime": "<?php echo $startDate; ?>",
										"coverageEndTime": "<?php echo $endDate; ?>"
									}
									</script>
										<?php
									foreach ($listnews as $item) {
										?>
									<div class="liveblog-item">
										<div class="liveblog-header">
												<?php echo $item["data_e_ora"]; ?>
										</div>
										<div class="liveblog-content">
											<h2 class="liveblog-title"><?php echo $item["titolo"]; ?></h2>
												<?php echo wpautop($item["testo"]); ?>
										</div>
									</div>
										<?php
									}
									}
										?>



									<p class="disclaimer" style="margin-bottom: 0px;">
										<i>Siamo anche su WhatsApp.
										<a href="https://whatsapp.com/channel/0029VaLy7oE2v1Ivm5K4NF3E" target="_blank"> Segui il canale ufficiale LifeGate per restare aggiornata, aggiornato</a> sulle ultime notizie e sulle nostre attività.
										</i>
									</p>

									<p class="licence" style="font-size: 16px; line-height: 16px;"><a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Licenza Creative Commons" width="88" height="31" style="border-width:0; float:left; margin-top:28px; padding-right:10px;" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br />Quest'opera è distribuita con Licenza <a style="font-size: 16px; line-height: 16px;" rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribuzione - Non commerciale - Non opere derivate 4.0 Internazionale</a>.</p>

								</div>
								@if($single_post_iniziatives)
									<div class="slider-act-now slider" data-slider-slidesperview="auto"
										 data-slider-loop="false" data-slider-space="40">
										@include('components.partials.title-section',array('text' => __('Agisci ora!','lifegate'),'textlink' => __('Vedi tutti i servizi','lifegate'),'url' =>  'http://www.lifegate.it','css' =>  true))
										@include('components.sections.custom-slider',array('blocks' => array(
											['template' => 'card-agisci-big','posts' => $single_post_iniziatives]
										)))
									</div>
								@endif

								{!! $lg_post_tag  !!}


								<div class="post__trends flex">
									@include('components.custom.custom-post-social-box')
								</div>

								{!! $blockchain_footer	 !!}
								{!! tbm_get_the_banner( 'BOX_MOBILE_INSIDE_BOTTOM','','',false,false ) !!}
								<div class="section">
									@include('components.custom.custom-post-footer')
								</div>
							</section>
						</main>
						<aside class="sidebar sidebar--right" style="min-height: 100%">
							@include('components.partials.3bmeteo')

							<section class="section-sticky-rail sticky-parent">
								@include('components.partials.partial-sticky-adv')
							</section>
							<section class="section-sticky-rail sticky-parent">
								@include('components.partials.partial-sticky-adv',['banner' => 'desktop_bottom'])
							</section>
							<section class="section-sticky-rail sticky-parent">
								@include('components.partials.partial-sticky-adv',['banner' => 'desktop_bottom_2'])
							</section>
							<script>
								document.addEventListener('DOMContentLoaded', function(){
										var tbm_s=document.getElementsByClassName("sidebar--right"),tbm_r=document.getElementsByClassName("section-sticky-rail");if(tbm_s.length){var tbm_o=document.getElementsByClassName("sidebar--right")[0].offsetHeight,tbm_h=tbm_o/3;if(tbm_r.length)for(i=0;i<tbm_r.length;i++)tbm_r[i].style.height=tbm_h+"px"};
									setTimeout(function(){
										var tbm_s=document.getElementsByClassName("post__content"),tbm_r=document.getElementsByClassName("section-sticky-rail");if(tbm_s.length){var tbm_o=document.getElementsByClassName("post__content")[0].offsetHeight,tbm_h=tbm_o/3;if(tbm_r.length)for(i=0;i<tbm_r.length;i++)tbm_r[i].style.height=tbm_h+"px"};
									},2000);
								}, false);
							</script>
						</aside>

					</div>

					{!! tbm_get_the_banner( 'HEADOFPAGE_BTF','','',false,false ) !!}

					@if(pll_current_language() == "it")
					<div class="footer">
						@include('components.sections.newsletter')
					</div>
					@endif

					@include('components.partials.widget-square')

					<div class="wrapper">

						@if($act_now_box)
							@include('components.partials.card-act-now')
						@endif

							<section class="lasts-from">
							@include('components.partials.title-section',array('text' => __('Articoli correlati','lifegate'),'textlink' => '','url' =>  $post_hp))
							<div class="section-2-column flex">
								<div class="col-6">
									{{-- query: posts.items.children[1]  --}}
									@include('components.sections.section-1-columns',array('blocks' => array(
										['template' => 'card-post_type-special-h4','posts' => $single_post_relateds_special]
									)))
								</div>
								<div class="col-6">
									@include('components.sections.section-horizontal-iteration',['tbmposts' => $single_post_relateds,'query' => [
											'class' =>  'lasts-from',
											'colNumber' =>  '2',
											'gutter' =>  '40',
											'componentNumber' =>  '4',
											'type' =>  'posts',
											'component' =>  'partial-card-post-big--h4'
										]])
								</div>
							</div>
						</section>
					</div>
				</div>
				@include('components.partials.partial-fullwidth-image')
				@endwhile
			</div>
		</div> <!-- end site -->
	</div>
@endsection
