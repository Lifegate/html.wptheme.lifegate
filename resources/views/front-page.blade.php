@extends('base')
{{-- HEAD --}}
@section('content')




	<!--front-page.twig page -->
	<div class="page front-page">
		@asset('css/front-page.min.css')
		<div class="site">

			@include('components.partials.widget-stories')

			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			{{-- Trends --}}
			@include('components.sections.trends')
			@if ($atf_v === 'V2')
				@include('components.sections.section-featured-home--lifegate-V2')
			@else
				@include('components.sections.section-featured-home--lifegate-V1')
			@endif

			{!! tbm_get_the_banner( 'HOME_TOP','','',false,false ) !!}

			@include('components.sections.original-home')

			<div class="wrapper">
				<div class="container">
					{{-- Magazine --}}
					@if($hp_ss || $hp_cs || $hp_pp2)
						<section class="magazine-lifegate">
							@include('components.partials.title-section',array('text' => __('I nostri magazine','lifegate')))


							@if($hp_cs)
								@include('components.partials.card-longform-big',array($hp_cs))
							@endif

							@if($hp_ss)
								<div class="slider slider--magazine">
									@asset('css/components/sections/slider-magazine.css')

									<div class="swiper-container swiper-slider-magazine" tabindex="0" aria-label="slider magazine">
										<div class="swiper-wrapper">
											@foreach($hp_ss as $ss)
												<div class="swiper-slide">
													@include('components.partials.card-speciale-home',array($ss))
												</div>
											@endforeach
										</div>
									</div>
									@if(count($hp_ss) > 3)
										<div class="swiper-slider__nav swiper-slider__next swiper-slider-magazine__next"></div>
										<div class="swiper-slider__nav swiper-slider__prev swiper-slider-magazine__prev"></div>
									@endif
								</div>
							@endif


							@if($hp_pp2)
								<div class="section-2-column flex magazine-second-line">
									@foreach($hp_pp2 as $ss)
										<div class="col-6">
											@include('components.partials.card-speciale-home',array($ss))
										</div>
									@endforeach
								</div>
							@endif


						</section>
					@endif



					{{-- LongForm --}}
					@if($hp_lf->post_count > 2)
						<section class="longform">
							@include('components.partials.title-section',array('text' => __('I nostri approfondimenti','lifegate'),'textlink' => __('Leggi tutti i longform','lifegate'),'url' =>  $longform_hp))
							@include('components.sections.slider-longform')
						</section>
					@endif


					{{-- Ultime dal magazine --}}
					<section class="last-from-magazine sticky-parent">
						@if($hp_mp->have_posts() && $hp_ms->have_posts())
							@include('components.partials.title-section',array('text' => __('Ultime news','lifegate'),'textlink' => __('Tutte le news','lifegate'),'url' => $magazine_hp))
							<div class="section-2-column flex">

								{{-- Magazine Primo Piano --}}
								<div class="col-6">
									@include('components.sections.section-1-columns',array('blocks' => array(
										['template' => 'card-post_type-special','posts' => $hp_mp]
									)))
								</div>

								{{-- Magazine Secondo Piano --}}
								<div class="col-6">
									<div class="container-cycle-col-2">
										@include('components.sections.section-1-columns',array('blocks' => array(
												['template' => 'partial-card-post_type-big--k2','posts' => $hp_ms]
											)))

									</div>
								</div>
							</div>
						@endif

							{{-- Contenuti speciali --}}
							@if($hp_man_a || $hp_man_b)
									@include('components.partials.title-section',array('text' => __('Contenuti speciali','lifegate')))
									@if($hp_man_a)
										{!! $hp_man_a !!}
									@endif
									@if($hp_man_b)
										{!! $hp_man_b !!}
									@endif

								@include('components.partials.title-section',array('text' => __('Altre news','lifegate'),'textlink' => __('Tutte le news','lifegate'),'url' => $magazine_hp))

							@endif


						@if($hp_xp->have_posts() && $hp_xs->have_posts())
							<div class="container sticky-parent">
								<aside class="sidebar sidebar--left">
									@include('components.partials.partial-sticky-adv')
								</aside>

								<main class="main-content main-content--right">
									{{-- Taglio Medio Primo piano --}}
									<div>
										<!-- Containerr cycle 2 -->
										@include('components.sections.container-cycle',array(
											'space' => 'col',
											'componentNumber' =>  '2',
											'colNumber' => '2',
											'blocks' => array(
											['template' => 'partial-card-post_type-big--k2','posts' => $hp_xp,'post_number' => 2]
										)))
									</div>

									{{-- Taglio Medio Secondo piano --}}
									<div class="others-from-magazine">
										<div class="archive">
											@include('components.sections.section-1-columns',array('blocks' => array(
												['template' => 'partial-card-post_type-list--k2','posts' => $hp_xs]
												)))
										</div>
									</div>

								</main>
							</div>
						@endif
					</section>

					{{-- Iniziative --}}
					@if($hp_iz->have_posts())
						<section class="iniziative">
							@include('components.partials.title-section',array('text' => __('Le iniziative di LifeGate','lifegate'),'textlink' => __('Leggi tutte le iniziative','lifegate'),'url' =>  $iniziative_hp))
							@include('components.sections.slider-iniziative')
						</section>
					@endif



					{{-- Eventi Primo piano e Eventi secondo piano --}}
					@if($hp_ep->have_posts() || $hp_es->have_posts())
						<section class="events">
							@include('components.partials.title-section',array('text' => __('Eventi di LifeGate','lifegate')))
							@include('components.sections.section-event')
						</section>
					@endif
				</div>
			</div>
		</div>

		@if ($hp_vp->have_posts() && $hp_vs->have_posts())
			{{-- Video Primo piano e Video Secondo piano  --}}
			<div class="videos lazyload" data-bg="{{ $video_background }}">
				@include('components.sections.fullwidth-videos')
				<div class="overlay"></div>
			</div>
		@endif

		{{-- Programmi / Taglio Basso  --}}
		<div class="site">
			<div class="wrapper">
				@if($hp_pr->have_posts() )
					<div class="container">
						<section class="radio">
							{{-- Programmi  --}}
							@include('components.partials.title-section',array('text' => __('La radio di LifeGate','lifegate'),'textlink' => __('Tutto sulla radio','lifegate'),'url' => $radio_hp))
							@include('components.custom.section-3-columns-radio', array('blocks' => array([
								'first_template' => 'card-song-big',
								'template'    => 'card-post_type-big',
								'posts'       => $hp_pr,
								'post_number' => 3
							])))

							@if($hp_fp->have_posts())
								{{-- Taglio Basso primo piano  --}}
								<span class="divider" style="clear: both"></span>
								@include('components.sections.section-4-columns',array('blocks' => array([
									'template'    => 'partial-card-post-big--k2',
									'posts'       => $hp_fp,
									'post_number' => 4
								])))
							@endif
						</section>
					</div>
				@endif

				<div class="container sticky-parent">
					@if($hp_fs->have_posts())
						<main class="main-content main-content--left secondo">
							<div>
								{{-- Taglio Basso secondo piano  --}}
								@include('components.sections.section-1-columns',array('blocks' => array(
								['template' => 'partial-card-post-list--k2','posts' => $hp_fs,'post_number' => 5]
								)))
							</div>
						</main>
						<aside class="sidebar sidebar--right">
							@include('components.partials.partial-sticky-adv',array('banner'=>'desktop_bottom'))
						</aside>
					@endif
				</div>
			</div>
		</div>
	</div>
	<!-- end site -->

@endsection
