@extends('base')
@section('content')
	<style>
		.custom-slider .swiper-container {
			min-height: 540px;
		}

		.custom-slider .swiper-slide {
			display: none;
			min-width: 460px;
		}
	</style>
	@asset('css/archive-iniziative.min.css')
	<!--archive-iniziative.twig page -->
	<div class="archive archive-iniziative">
		<div class="site">

			@include('components.partials.widget-stories')

			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.partial-breadcrumb')
					<div class="page-title">
						<h1>{!! __('Iniziative','lifegate') !!}</h1>
					</div>
				</div>
			</div>
			<section class="featured-slider slider"data-slider-effect="coverflow" data-slider-slidesperview="auto" data-slider-loop="true" data-slider-space="0" data-slider-centered="true">
				@include('components.sections.custom-slider',array('blocks' => array(
					['template' => 'card-iniziativa-big','posts' => $all_iniziatives]
				)))
			</section>
		</div>
	</div>
@endsection
