@extends('base')
@section('content')

	<!--archive-autore.twig page -->
	{{-- query: authors.items.children[0]  --}}

	<div class="archive archive-autore">
		<div class="site">
			@asset('css/archive-autore.min.css')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
				<div class="container sticky-parent">
					@while (have_posts()) @php the_post() @endphp
					<aside class="sidebar sidebar--left">
						<div class="single__title">
							<h1>{!! the_title() !!}</h1>
							<h2>{!! $single_author_role !!}</h2>
						</div>
						<div class="author-image">
							<picture>
								<!--[if IE 9]>
								<video style="display: none;"><![endif]-->
								<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(300,300)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(600,600)) !!} 2x"
										media="(max-width: 736px)"/>
								<!--[if IE 9]></video><![endif]-->
								<img class="lazyload"
									 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(300,300)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(600,600)) !!} 2x"
									 alt="{!! the_title() !!}"/>
							</picture>
							<noscript>
								<picture>
									<!--[if IE 9]>
									<video style="display: none;"><![endif]-->
									<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(296,296)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(592,592)) !!} 2x"
											media="(max-width: 736px)"/>
									<!--[if IE 9]></video><![endif]-->
									<img src="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(148,148)) !!}"
										 alt="{!! the_title() !!}"/>
								</picture>
							</noscript>
						</div>
						@include('components.partials.partial-sticky-adv')
					</aside>

					<main class="main-content main-content--right">
						<div>

							<div class="single__title">
								<p class="has-h1">{!! the_title() !!}</p>
								<p class="has-h2">{!! $single_author_role !!}</p>
							</div>

							@unless($author_page > 1)
								<div class="author-bio">
								{!! $single_author_bio !!}
								{!! $single_author_social !!}
								</div>


							@endunless

							<div class="author-archive">

								@if ($single_author_last_originals->have_posts())
									<div>
										@include('components.partials.title-section',array('text' => __('I suoi Original' ,'lifegate'),'textlink' => null,'url' =>  null,'class' =>  "title-original"))
										@include('components.sections.section-1-columns',array('blocks' => array(
											['template' => 'partial-card-post_type-list--k2','posts' => $single_author_last_originals]
										)))
									</div>

								@endif

								@unless($author_page > 1)
									@if ($single_author_last_post->have_posts())
										@include('components.partials.title-section',array('text' => __('Il suo ultimo articolo','lifegate'),'textlink' => null,'url' =>  null,'css' =>  true))
										{{-- query: posts.items.children[0]  --}}
										@include('components.sections.section-1-columns',array('blocks' => array(
											['template' => 'partial-card-author-big--k2','posts' => $single_author_last_post]
										)))
									@endif
								@endunless

								@if ($single_author_last_posts->have_posts())
									<div>
										@if($author_page > 1)
											@include('components.partials.title-section',array('text' => __('Tutti i suoi articoli' ,'lifegate').'<span style="font-size:60%"> - '. $author_page .'</span>','textlink' => null,'url' =>  null))
										@else
											@include('components.partials.title-section',array('text' => __('Tutti i suoi articoli' ,'lifegate'),'textlink' => null,'url' =>  null))
										@endif
										@include('components.sections.section-1-columns',array('blocks' => array(
											['template' => 'partial-card-post_type-list--k2','posts' => $single_author_last_posts]
										)))
									</div>
									@include('components.partials.partial-pagination')
								@endif



							</div>

						</div>
					</main>
					@endwhile
				</div>
			</div>
		</div>
	</div>
@endsection
