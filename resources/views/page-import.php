<?php
/* Template Name: Lifegate import raw */
if ( WP_ENV === 'production' ) {
	require_once( '/var/www/www_lifegate_it/current/web/wp/wp-admin/includes/file.php' );
} else {
	require_once( '/var/www/dev.lifegate.it/current/web/wp/wp-admin/includes/file.php' );
}

header( "Cache-Control: private" ); //HTTP 1.1
header( "Pragma: no-cache" ); //HTTP 1.0

function tbm_save_show_cover_image( $post_id ) {
	if ( 'show' !== get_post_type( $post_id ) ) {
		return;
	}

	if ( get_field( 'lg_show_image', $post_id ) ) {
		return;
	}

	/**
	 * Get the show id
	 */
	$lg_show_spreaker_id = get_field( 'lg_show_spreaker_id', $post_id );
	if ( ! $lg_show_spreaker_id ) {
		return;
	}

	/**
	 * Require the show data  from spreaker endpoint
	 */
	$response = wp_remote_get( 'https://api.spreaker.com/v2/shows/' . $lg_show_spreaker_id, array(
			'timeout'   => 60,
			'sslverify' => false
	) );

	/**
	 * Check the response. If error, return empty string
	 */
	if ( is_wp_error( $response ) ) {
		return '';
	}

	/**
	 * Check the response. If false, return empty string
	 */
	if ( ! is_array( $response ) || 200 != wp_remote_retrieve_response_code( $response ) ) {
		return '';
	}

	/**
	 * Retrieve the body
	 */
	$body = $response['body'];

	/**
	 * Check the body. If false, return empty string
	 */
	if ( $body == "false" ) {
		return '';
	}

	$response_array = json_decode( $body, true );

	if ( ! empty( $response_array['response']['show']['cover_image_url'] ) ) {
		update_field( 'lg_show_image', $response_array['response']['show']['cover_image_url'], $post_id );
	}

	return true;
}

/**
 * Get the google maps coordinates from address
 *
 * @param string $s The address
 *
 * @return mixed
 */
function tbm_my_get_address( $s = '' ) {

	if ( empty( $s ) ) {
		return false;
	}

	$url = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyALEao8BnIoIMpAVASgVk0J66WqyF4pMfU&address=' . urlencode( $s );

	$response = wp_remote_get( $url, array( 'timeout' => 5, 'httpversion' => '1.1' ) );

	if ( 200 == wp_remote_retrieve_response_code( $response ) ) {
		$body = $response['body'];
		if ( $body ) {
			$results = json_decode( $body, true );

			if ( isset( $results['results'] ) ) {
				$res = $results['results'][0];

				$out['address'] = $res['formatted_address'];
				$out['lat']     = $res['geometry']['location']['lat'];
				$out['lng']     = $res['geometry']['location']['lng'];
				$out['zoom']    = 7;

				return $out;
			}


		}
	}
}

/**
 * Get the images attached to gallery
 *
 * @param $post_id
 *
 * @return array
 */
function tbm_get_gallery_images( $post_id ) {

	$out = array();

	$args        = array(
			'orderby'        => 'menu_order ID',
			'order'          => 'ASC',
			'post_type'      => 'attachment',
			'post_parent'    => $post_id,
			'post_mime_type' => 'image',
			'post_status'    => null,
			'numberposts'    => - 1
	);
	$attachments = get_posts( $args );

	if ( ! $attachments ) {
		return false;
	}

	foreach ( $attachments as $attachment ) {
		$out[] = $attachment->ID;
	}

	return $out;
}

/**
 * Get the images attached to gallery
 *
 * @param $post_id
 *
 * @return array
 */
function tbm_set_image_lang( $post_id, $lang ) {

	$out = array();

	$args        = array(
			'post_type'      => 'attachment',
			'post_parent'    => $post_id,
			'lang'           => array( 'it' ),
			'post_mime_type' => 'image',
			'fields'         => 'ids',
			'numberposts'    => - 1
	);
	$attachments = get_posts( $args );

	if ( ! $attachments ) {
		return false;
	}

	foreach ( $attachments as $attachment ) {
		$out = pll_set_post_language( $attachment, $lang );
	}

	return true;
}

/**
 * Get the value of a settings field
 *
 * @param string $option settings field name
 * @param string $section the section name this field belongs to
 * @param string $default default text if it's not found
 *
 * @return mixed
 */
function getAeria( $section, $option, $default = '' ) {
	static $options = null;
	if ( $options === null ) {
		$options = [];
	}
	if ( false == isset( $options[ $section ] ) ) {
		$options[ $section ] = get_option( $section );
	}
	if ( isset( $options[ $section ][ $option ] ) ) {
		return $options[ $section ][ $option ];
	} else {
		return $default;
	}
}

/**
 * Download and import images
 *
 * @param null $url
 * @param string $title
 *
 * @return int|void|WP_Error
 */
function tbm_my_get_image( $url = null, $title = 'Immagine video' ) {
	if ( ! $url ) {
		return;
	}
	$timeout_seconds = 20;
	$temp_file       = \download_url( $url, $timeout_seconds );

	if ( ! is_wp_error( $temp_file ) ) {
		$file = array(
				'name'     => basename( $url ),
				'tmp_name' => $temp_file,
				'size'     => filesize( $temp_file ),
		);

		$overrides = array(
				'test_form' => false,
				'test_size' => true,
		);
		$file      = wp_handle_sideload( $file, $overrides );

		if ( ! empty( $file['error'] ) ) {
		} else {
			$attachment = array(
					'post_mime_type' => $file['type'],
					'post_title'     => $title,
					'post_content'   => '',
					'post_status'    => 'inherit',
			);
			$attach_id  = wp_insert_attachment( $attachment, $file['file'] );
		}

		return $attach_id;

	}
}

/**
 * Dies if nothing
 */
if ( ! isset( $_GET ) ) {
	die( 'define an action' );
}

/**
 * Convert events
 */
if ( $_GET['call'] === 'event' ) {
	$args = array(
			'post_type'      => 'event',
			'posts_per_page' => 10,
			'offset'         => $_GET['offset'],
			'meta_key'       => 'address',
			'meta_value'     => '',
			'meta_compare'   => 'NOT EXISTS'
	);

	$posts = get_posts( $args );

	if ( ! $posts ) {
		die( 'fine' );

	}

	foreach ( $posts as $post ) {
		$post_id = $post->ID;

		$meta  = get_post_meta( $post_id );
		$data  = ( isset( $meta['data_evento'] ) ) ? $meta['data_evento'][0] : '';
		$luogo = ( isset( $meta['luogo_evento'] ) ) ? $meta['luogo_evento'][0] : '';
		$url   = ( isset( $meta['link_evento'] ) ) ? trim( strip_tags( $meta['link_evento'][0] ) ) : '';

		if ( $data ) {
			$date = DateTime::createFromFormat( 'd/m/Y H:i', $data );
			if ( $date ) {
				update_field( 'dateFrom', $date->format( 'Ymd' ), $post_id );
			}
		}

		if ( $url ) {
			if ( filter_var( $url, FILTER_VALIDATE_URL ) !== false ) {
				update_field( 'url', $url, $post_id );
			}
		}

		if ( $luogo ) {
			$l = tbm_my_get_address( $luogo );

			if ( $l ) {
				$update = update_field( 'address', $l, $post_id );

				echo '<a target="_blank" href="http://dev.lifegate.it/wp/wp-admin/post.php?action=edit&post=' . $post_id . '">' . $post_id . '</a></code> Aggiornamento <span style="background-color: darkgreen;color:whitesmoke">Riuscito</span><br/>' . PHP_EOL;
			} else {
				echo '<code><a target="_blank" href="http://dev.lifegate.it/wp/wp-admin/post.php?action=edit&post=' . $post_id . '">' . $post_id . '</a></code> Aggiornamento ' . $luogo . ' <span style="background-color: darkred;color: whitesmoke">Fallito</span> <br/>' . PHP_EOL;
			}
		}


	}

}

/**
 * Check missing images
 */
if ( $_GET['call'] === 'missing' ) {
	$args = array(
			'post_type'      => HTML_GALLERY_POST_TYPE,
			'posts_per_page' => 10,
			'fields'         => 'ids',
			'lang'           => array( 'en' ),
			'offset'         => $_GET['offset'],
			'date_query'     => array(
					array(
							'before' => 'May 31, 2020',
					)
			),
	);

	$posts   = get_posts( $args );
	$failing = array();

	if ( ! $posts ) {
		die( 'fine' );
	}

	foreach ( $posts as $post ) {
		$post_id = $post;
		$out     = array();

		$out[] = '<br><span style="background-color: yellow;color: black">Analizzo <a target="_blank" href="http://www.lifegate.it/wp/wp-admin/post.php?action=edit&post=' . $post_id . '">' . $post_id . '</a></span>';

		$images = get_field( 'tbm_gallery', $post_id );

		if ( empty( $images ) ) {
			$out[] = '<span style="background-color: darkred;color: whitesmoke">IMMAGINI MANCANTI</span>';
		} else {
			$basedir = wp_get_upload_dir()['basedir'];

			foreach ( $images as $image ) {
				$file = '/var/www/www_lifegate_it/current/web/app/uploads/2020/06/' . $image['filename'];
				if ( ! file_exists( $file ) ) {
					$out[] = '<span style="background-color: darkred;color: whitesmoke" title="' . $file . '">F</span>';
				}
			}
		}
		$results = implode( ' ', $out );
		echo $results . '<br>' . PHP_EOL;
	}
	echo implode( ' ', $failing ) . '<br>' . PHP_EOL;
}

/**
 * Convert galleries
 */
if ( $_GET['call'] === 'gallery' ) {

	$args = array(
			'post_type'      => HTML_GALLERY_POST_TYPE,
			'post_status'    => array( 'pending', 'draft', 'future', 'publish' ),
			'offset'         => $_GET['offset'],
			'fields'         => 'ids',
			'lang'           => array( 'it', 'en' ),

			'meta_query' => array(
					'relation' => 'OR',
					array(
							'key'     => 'tbm_gallery',
							'compare' => 'NOT EXISTS', // works!
							'value'   => '' // This is ignored, but is necessary...
					),
					array(
							'key'   => 'tbm_gallery',
							'value' => ''
					)
			)
	);

	$posts = get_posts( $args );


	if ( ! $posts ) {
		die( 'fine' );

	}


	foreach ( $posts as $post ) {
		$post_id = $post;

		$images = tbm_get_gallery_images( $post_id );

		if ( empty( $images ) ) {
			echo '<code><a target="_blank" href="http://www.lifegate.it/wp/wp-admin/post.php?action=edit&post=' . $post_id . '">' . $post_id . '</a></code> Aggiornamento <span style="background-color: darkred;color: whitesmoke">No Images</span> <br/>' . PHP_EOL;
			continue;
		}

		$field = update_field( 'tbm_gallery', $images, $post_id );

		if ( $field ) {
			echo '<a target="_blank" href="http://www.lifegate.it/wp/wp-admin/post.php?action=edit&post=' . $post_id . '">' . $post_id . '</a></code> Aggiornamento <span style="background-color: darkgreen;color:whitesmoke">Riuscito</span>' . implode( ' => ', $images ) . '<br/>' . PHP_EOL;
		} else {
			echo '<code><a target="_blank" href="http://www.lifegate.it/wp/wp-admin/post.php?action=edit&post=' . $post_id . '">' . $post_id . '</a></code> Aggiornamento <span style="background-color: darkred;color: whitesmoke">ACF</span> <br/>' . PHP_EOL;
		}

		echo '<code><a target="_blank" href="http://www.lifegate.it/wp/wp-admin/post.php?action=edit&post=' . $post_id . '">' . $post_id . '</a></code> Aggiornamento <span style="background-color: darkred;color: whitesmoke">Boh</span> <br/>' . PHP_EOL;

	}

}

if ( $_GET['call'] === 'media' ) {
	$args = array(
			'post_type'      => array( HTML_GALLERY_POST_TYPE, 'post' ),
			'posts_per_page' => 10,
			'lang'           => array( 'en' ),
			'offset'         => $_GET['offset'],
			'fields'         => 'ids',
			'date_query'     => array(
					array(
							'before' => 'May 31, 2020',
					)
			),
	);

	$posts = get_posts( $args );

	if ( ! $posts ) {
		die( 'fine' );

	}

	foreach ( $posts as $post ) {
		$post_id = $post;

		$images = tbm_set_image_lang( $post_id, 'en' );

		if ( empty( $images ) ) {
			echo '<code><a target="_blank" href="http://dev.lifegate.it/wp/wp-admin/post.php?action=edit&post=' . $post_id . '">' . $post_id . '</a></code> Aggiornamento <span style="background-color: darkred;color: whitesmoke">Fallito</span> <br/>' . PHP_EOL;
			continue;
		} else {
			echo '<code><a target="_blank" href="http://dev.lifegate.it/wp/wp-admin/post.php?action=edit&post=' . $post_id . '">' . $post_id . '</a></code> Aggiornamento <span style="background-color: darkred;color: whitesmoke">Fallito</span> <br/>' . PHP_EOL;
		}

	}

}

/**
 * Convert shows
 */
if ( $_GET['call'] === 'show' ) {
	$args = array(
			'post_type'      => 'show',
			'posts_per_page' => 1,
			'offset'         => $_GET['offset'],
			'meta_key'       => 'lg_show_spreaker_id',
			'meta_compare'   => 'NOT EXISTS'
	);

	$posts = get_posts( $args );

	if ( ! $posts ) {
		die( 'fine' );
	}

	foreach ( $posts as $post ) {
		$post_id = $post->ID;

		$showid = preg_match( '/show_id=([0-9]+)/', $post->post_content, $matches );

		if ( $matches[1] ) {
			$result = update_field( 'lg_show_spreaker_id', $matches[1], $post->ID );
			tbm_save_show_cover_image( $post->ID );
		}

		if ( ! is_wp_error( $result ) && $result ) {
			echo '<a target="_blank" href="http://tbm.lifegate.it/wp/wp-admin/post.php?action=edit&post=' . $post_id . '">' . $post_id . '</a></code> Aggiornamento <span style="background-color: darkgreen;color:whitesmoke">Riuscito</span><br/>' . PHP_EOL;
		} else {
			echo '<code><a target="_blank" href="http://tbm.lifegate.it/wp/wp-admin/post.php?action=edit&post=' . $post_id . '">' . $post_id . '</a></code> Aggiornamento <span style="background-color: darkred;color: whitesmoke">Fallito</span> <br/>' . PHP_EOL;
		}

	}

}

/**
 * Convert rassegna stampa
 */
if ( $_GET['call'] === 'rassegna' ) {

	$args = array(
			'post_type'      => 'rassegna_stampa',
			'posts_per_page' => 10,
			'offset'         => $_GET['offset'],
			'meta_key'       => 'tbm_allegato_rassegna_stampa',
			'meta_compare'   => 'NOT EXISTS'
	);

	$posts = get_posts( $args );

	if ( ! $posts ) {
		die( 'fine' );
	}

	foreach ( $posts as $post ) {
		$post_id = $post->ID;

		$out   = array();
		$out[] = '<a href="' . get_edit_post_link( $post ) . '" target="_blank">' . $post_id . '</a>';

		$anno     = get_post_meta( $post_id, 'rassegna_stampa_year', true );
		$original = get_post_meta( $post_id, 'attachments', true );
		$file     = json_decode( $original );

		if ( $anno ) {
			$out[] = 'Assegnato anno';
			update_field( 'tbm_anno_rassegna_stampa', $anno, $post_id );
		}

		if ( $file ) {
			$out[] = 'Assegnato file';
			update_field( 'tbm_allegato_rassegna_stampa', $file->item_attachments[0]->id, $post_id );
		}

		echo implode( ', ', $out ) . '<br>' . PHP_EOL;

	}

}

/**
 * Assign imported authors to imported english content
 */
if ( $_GET['call'] === 'author' ) {
	$args = array(
			'post_type'      => 'any',
			'posts_per_page' => 10,
			'offset'         => $_GET['offset'],
			'fields'         => 'ids',
			'lang'           => 'en',
			'meta_query'     => array(
					array(
							'key'     => 'autore_news',
							'compare' => 'EXISTS',
					)
			),
	);

	$posts = get_posts( $args );

	if ( ! $posts ) {
		die( 'fine' );
	}

	foreach ( $posts as $post ) {
		$out   = array();
		$out[] = '<a href="' . get_edit_post_link( $post ) . '" target="_blank">' . $post . '</a>';

		$autore_id = get_post_meta( $post, 'autore_news', true );
		$out[]     = 'autore id ' . $autore_id;

		$translate_id = get_post_meta( $post, 'traduttore_news', true );
		$out[]        = 'traduttore id ' . $translate_id;

		$authors = get_posts(
				array(
						'post_type'      => 'autori',
						'lang'           => 'en',
						'posts_per_page' => - 1,
						'fields'         => 'ids',
						'meta_key'       => 'old_id',
						'meta_value'     => $autore_id
				)
		);
		if ( $authors ) {
			$update = update_field( 'autore_news', $authors, $post );
			if ( $update ) {
				$out[] = 'aggiornato ' . get_the_title( $post ) . ' con autore_news ' . call_user_func_array( function ( $arg ) {
							return get_the_title( $arg );
						}, $authors );
			}
		}

		$translator = get_posts(
				array(
						'post_type'      => 'autori',
						'lang'           => 'en',
						'posts_per_page' => - 1,
						'fields'         => 'ids',
						'meta_key'       => 'old_id',
						'meta_value'     => $translate_id
				)
		);
		if ( $translator ) {
			$update = update_field( 'traduttore_news', $translator, $post );
			if ( $update ) {
				$out[] = 'aggiornato ' . get_the_title( $post ) . ' con traduttore_news ' . call_user_func_array( function ( $arg ) {
							return get_the_title( $arg );
						}, $translator );
			}
		}

		echo implode( ', ', $out ) . '<br>' . PHP_EOL;

	}

}

/**
 * Match authors translations
 */
if ( $_GET['call'] === 'authors_map' ) {
	$authors = get_posts(
			array(
					'post_type'      => 'autori',
					'lang'           => 'en',
					'posts_per_page' => 10,
					'offset'         => $_GET['offset'],
					'fields'         => 'ids',
			)
	);

	if ( ! $authors ) {
		die( 'fine' );
	}

	foreach ( $authors as $author ) {

		$out   = array();
		$out[] = '<a href="' . get_edit_post_link( $author ) . '" target="_blank">' . $author . '</a>';

		$new_authors = get_posts(
				array(
						'post_type'      => 'autori',
						'lang'           => 'it',
						'posts_per_page' => - 1,
						'fields'         => 'ids',
						's'              => get_the_title( $author )
				)
		);

		if ( $new_authors ) {
			$new_author = reset( $new_authors );
			$out[]      = 'aggiorno ' . get_the_title( $author ) . ' con ' . get_the_title( $new_author );
			pll_save_post_translations( [
					'en' => $author,
					'it' => $new_author
			] );

		} else {
			$out[] = '<span style="color:red">non aggiornato</span>';
		}


		echo implode( ', ', $out ) . '<br>' . PHP_EOL;

	}
}

/**
 * Map articles language
 */
if ( $_GET['call'] === 'posts_map' ) {
	$posts = get_posts(
			array(
					'post_type'      => 'post',
					'lang'           => 'en',
					'posts_per_page' => 10,
					'offset'         => $_GET['offset'],
					'fields'         => 'ids',
			)
	);

	if ( ! $posts ) {
		die( 'fine' );
	}


	foreach ( $posts as $post ) {

		$out   = array();
		$out[] = '<a href="' . get_edit_post_link( $post ) . '" target="_blank">' . $post . '</a>';

		$translations = pll_get_post_translations( $post );
		if ( count( $translations ) > 1 ) {
			$out[] = '<span style="color:orange">already</span>';
			$out[] = implode( '|', array_keys( $translations ) );
			echo implode( ', ', $out ) . '<br>' . PHP_EOL;
			continue;
		}

		$old_slug = get_post_meta( $post, 'old_slug', true );

		$new_posts = get_posts(
				array(
						'post_type'      => 'post',
						'lang'           => 'it',
						'posts_per_page' => - 1,
						'fields'         => 'ids',
						'meta_query'     => array(
								array(
										'key'     => 'link_translate',
										'value'   => array(
												'https://www.lifegate.com' . $old_slug,
												'http://www.lifegate.com' . $old_slug
										),
										'compare' => 'IN',
								),
						)
				)
		);

		if ( $new_posts && count( $new_posts ) === 1 ) {
			$new_post = reset( $new_posts );
			$out[]    = '<span style="color:green">ok</span>';
			$out[]    = get_the_title( $post );
			$out[]    = get_the_title( $new_post );
			pll_save_post_translations( [
					'en' => $post,
					'it' => $new_post
			] );

		} else {
			$out[] = '<span style="color:red">ko</span>';
			$out[] = get_the_title( $post );
			$out[] = '-';
		}


		echo implode( ', ', $out ) . '<br>' . PHP_EOL;

	}
}

/**
 * Match terms translation
 */
if ( $_GET['call'] === 'terms_match' ) {

	$terms = get_terms( array(
			'taxonomy'   => array( 'tag_en', 'category_en' ),
			'offset'     => $_GET['offset'],
			'number'     => 10,
			'hide_empty' => false,
			'lang'       => 'en'
	) );

	if ( ! $terms || is_wp_error( $terms ) ) {
		die( 'fine' );
	}


	foreach ( $terms as $term ) {

		$out = array();

		$out[] = '<a href="' . get_edit_term_link( $term->term_id ) . '" target="_blank">' . $term->term_id . '</a>';

		$term_count = $term->count;
		if ( $term_count < 1 ) {
			$out[] = '<span style="color:red">cancellato</span>';
			wp_delete_term( $term->term_id, $term->taxonomy );
		}

		/**
		 * Get the italian translation
		 */
		$terms_it = get_terms( array(
				'taxonomy' => $term->taxonomy,
				'number'   => 1,
				'lang'     => 'it',
				'name'     => $term->name
		) );

		/**
		 * Save languages
		 */
		if ( $terms_it && ! is_wp_error( $terms_it ) ) {
			foreach ( $terms_it as $term_it ) {
				$out[] = '<span style="color:green">ok</span> <a href="' . get_edit_term_link( $term->term_id ) . '" target="_blank">' . $term->name . '</a> [en] => <a href="' . get_edit_term_link( $terms_it->term_id ) . '" target="_blank">' . $term_it->name . '</a> [it]';
				$out[] = '<span style="font-family: monospace">pll_save_term_translations(array(\'it\' => ' . $term_it->term_id . ', \'en\' => ' . $term->term_id . ' ));</span> ';
			}
		} else {
			$out[] = '<span style="color:black">-</span> Nessuna traduzione per <em>' . $term->name . '</em>';
		}

		echo implode( ', ', $out ) . '<br>' . PHP_EOL;


	}
}

/**
 * Convert sponsors to sponsor tax
 */
if ( $_GET['call'] === 'sponsor' ) {
	$posts = get_posts(
			array(
					'post_type'      => 'sponsor',
					'posts_per_page' => 10,
					'offset'         => $_GET['offset'],
			)
	);

	if ( ! $posts ) {
		die( 'fine' );
	}


	foreach ( $posts as $post ) {
		$terms          = '';
		$out            = array();
		$sponsors_posts = array();
		$ss             = array();

		$out[] = '<a href="' . get_edit_post_link( $post ) . '" target="_blank">' . $post->ID . '</a>';

		$term = wp_insert_term( $post->post_title, 'sponsor', array(
				'description' => $post->post_content,
		) );

		if ( is_wp_error( $term ) || empty( $term ) ) {
			$out[] = '<span style="color:red">termine</span>';
			echo implode( ', ', $out ) . '<br>' . PHP_EOL;

			continue;
		} else {
			$out[] = '<span style="color:green">termine</span>';
			/**
			 * Imposto la lingua
			 */
			pll_set_term_language( $term['term_id'], 'it' );

			$acf_tax_id = 'sponsor_' . $term['term_id'];

			$out[] = ' <a href="' . get_edit_term_link( $term['term_id'], 'sponsor' ) . '">link</a>';

			$sponsor_logo = get_post_thumbnail_id( $post );

			if ( $sponsor_logo ) {
				$out[] = '<span style="color:green">sponsor_img</span>';
				update_field( 'sponsor_img', $sponsor_logo, $acf_tax_id );
			}

			$website_sponsor = get_post_meta( $post->ID, 'website_sponsor', true );
			if ( $website_sponsor ) {
				$out[] = '<span style="color:green">sponsor_landing</span>';
				update_field( 'sponsor_landing', $website_sponsor, $acf_tax_id );
			}

			/**
			 * Setto gli sponsor
			 */
			$sponsors = getAeria( 'sponsor_news', 'sponsorizzazioni' );

			foreach ( $sponsors as $sponsor ) {
				if ( isset( $sponsor['sponsors'] ) && $sponsor['sponsors'] == $post->ID ) {
					$sponsors_posts = $sponsor['articoli_news'];
					if ( $sponsors_posts ) {
						foreach ( $sponsors_posts as $sponsors_post ) {
							$ss = array_merge( $ss, explode( ',', $sponsors_post ) );
						}
					}
				}
			}

			if ( ! empty( $ss ) ) {
				foreach ( $ss as $s ) {
					$term_taxonomy_term = wp_set_object_terms( $s, $term['term_id'], 'sponsor', true );
					$term_taxonomy_acf  = update_field( 'post_sponsor', $term['term_id'], $s );
					if ( ! is_wp_error( $term_taxonomy_term ) && ! empty( $term_taxonomy_acf ) ) {
						$out[] = '<span style="color:green"><a href="' . get_edit_post_link( $s ) . '" target="_blank">' . $s . '</a></span>';
					} else {
						$out[] = '<span style="color:red"><a href="' . get_edit_post_link( $s ) . '" target="_blank">' . $s . '</a></span>';
					}
				}
			}
		}


		echo implode( ', ', $out ) . '<br>' . PHP_EOL;

	}
}

/**
 * Convert sponsors to magazine
 */
if ( $_GET['call'] === 'magazine' ) {
	$posts = get_posts(
			array(
					'post_type'      => 'sponsor',
					'posts_per_page' => 10,
					'offset'         => $_GET['offset'],
			)
	);

	if ( ! $posts ) {
		die( 'fine' );
	}


	foreach ( $posts as $post ) {
		$terms          = '';
		$image_id       = '';
		$term           = '';
		$out            = array();
		$sponsors_posts = array();
		$ss             = array();

		$out[] = '<a href="' . get_edit_post_link( $post ) . '" target="_blank">' . $post->ID . '</a>';

		//Create the term in trend taxonomy
		$term = wp_insert_term( $post->post_title, 'magazine', array(
				'description' => $post->post_excerpt,
				'slug'        => $post->post_name
		) );

		if ( is_wp_error( $term ) || empty( $term ) ) {
			$out[] = '<span style="color:red">termine</span>';
			echo implode( ', ', $out ) . '<br>' . PHP_EOL;

			continue;
		} else {
			$out[] = '<span style="color:green">termine</span>';
			/**
			 * Imposto la lingua
			 */
			pll_set_term_language( $term['term_id'], 'it' );

			/**
			 * Imposto i custom field
			 */
			$acf_tax_id = 'magazine_' . $term['term_id'];
			$image_url  = get_post_meta( $post->ID, 'header_image', true );

			$out[] = ' <a href="' . get_edit_term_link( $term['term_id'], 'magazine' ) . '">link</a>';

			/**
			 * Image
			 */
			if ( $image_url ) {
				$image_id = tbm_my_get_image( 'https://www.lifegate.it/' . $image_url, $post->post_title );
				if ( $image_id ) {
					$out[] = '<span style="color:green">tbm_special_featured_img</span>';
					update_field( 'tbm_special_featured_img', $image_id, $acf_tax_id );
				}
			}

			/**
			 * Content
			 */
			if ( $post->post_content ) {
				$out[] = '<span style="color:green">tbm_magazine_enrichment</span>';
				update_field( 'tbm_magazine_enrichment', $post->post_content, $acf_tax_id );
			}

			/**
			 * Post sponsor connection
			 */
			$sponsor_term = get_term_by( 'name', $post->post_title, 'sponsor' );
			if ( $sponsor_term ) {
				$out[] = '<span style="color:green">post_sponsor</span>';
				update_field( 'post_sponsor', $sponsor_term->term_id, $acf_tax_id );
			}

			/**
			 * Setto gli sponsor
			 */
			$sponsors = getAeria( 'sponsor_news', 'sponsorizzazioni' );

			foreach ( $sponsors as $sponsor ) {
				if ( isset( $sponsor['sponsors'] ) && $sponsor['sponsors'] == $post->ID ) {
					$sponsors_posts = $sponsor['articoli_news'];
					if ( $sponsors_posts ) {
						foreach ( $sponsors_posts as $sponsors_post ) {
							$ss = array_merge( $ss, explode( ',', $sponsors_post ) );
						}
					}
				}
			}

			// Cycle the arrays of post related to this sponsor
			if ( ! empty( $ss ) ) {
				foreach ( $ss as $s ) {
					$term_taxonomy_term = wp_set_object_terms( $s, $term['term_id'], 'magazine', true );
					if ( ! is_wp_error( $term_taxonomy_term ) && ! empty( $term_taxonomy_acf ) ) {
						$out[] = '<span style="color:green"><a href="' . get_edit_post_link( $s ) . '" target="_blank">' . $s . '</a></span>';
					} else {
						$out[] = '<span style="color:red"><a href="' . get_edit_post_link( $s ) . '" target="_blank">' . $s . '</a></span>';
					}
				}
			}
		}


		echo implode( ', ', $out ) . '<br>' . PHP_EOL;

	}
}

/**
 * Assign language to english trends
 */
if ( $_GET['call'] === 'trends_en' ) {
	$terms = array(
			'pollution',
			'design',
			'travel-news',
			'architecture',
			'coronavirus',
			'activism',
			'climate-conferences',
			'forests',
			'oceans',
			'animal-rights',
			'gender',
			'indigenous-peoples',
			'migration'
	);

	$out = array();

	foreach ( $terms as $term ) {

		$out[] = $term;

		$term_object = get_term_by( 'slug', $term, 'trend' );

		if ( $term_object ) {
			$out[] = 'ok';
			pll_set_term_language( $term_object->term_id, 'en' );
		}

		echo implode( ', ', $out ) . '<br>' . PHP_EOL;

	}
}

if ( $_GET['call'] === 'delete_empty_terms' ) {

	$taxonomies = array( 'category', 'category_en', 'post_tag', 'tag_en' );

	foreach ( $taxonomies as $taxonomy ) {
		$terms = get_terms( array(
				'taxonomy'   => $taxonomy,
				'hide_empty' => false,
				'lang'       => 'en'
		) );
		if ( $terms && ! is_wp_error( $terms ) ) {
			foreach ( $terms as $term ) {
				$out[] = '<span style="color:green">' . $term->ID . '</span>';
				if ( $term->count < 1 ) {
					$del = wp_delete_term( $term->term_id, $taxonomy );
					if ( $del ) {
						$out[] = '<span style="color:green">cancellato</span>';
					} else {
						$out[] = '<span style="color:red">errore</span>';
					}
				}
			}

		}

		echo implode( ', ', $out ) . '<br>' . PHP_EOL;
	}

}

if ( ! isset( $_GET['call'] ) ) : ?>
	<html>
	<div id="div1"></div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script>
		function ajaxCall(offset, call) {
			$.ajax({
				url: "<?php echo get_bloginfo( 'url' ) . '/import'; ?>",
				data: {
					call: call,
					offset: parseInt(offset)
				},
				success: function (result) {
					$("#div1").append(result);
					if (result !== 'fine') {
						setTimeout(ajaxCall(parseInt(offset) + 10, call), 3000);
					}
				}
			});
		}

	</script>
	</html>
<?php
endif;
