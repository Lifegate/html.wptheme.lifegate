{{-- /* Template Name: Chi siamo */ --}}
@extends('base')
@section('content')
	@asset('css/page-chisiamo.min.css')
	<!--page-chisiamo.twig page -->
	<div class="page page-chisiamo">
		<section class="longform-hero">
			<div class="longform-hero__container">
				<div class="longform-hero__container__bg">
					<picture>
						<!--[if IE 9]>
						<video style="display: none;"><![endif]-->
						<source class="lazyload"
								data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,320)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1280,640)) !!} 2x"
								media="(max-width: 736px)"/>
						<!--[if IE 9]></video><![endif]-->
						<img class="lazyload"
							 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,320)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1280,640)) !!} 2x"
							 alt="{!! the_title() !!}"/>
					</picture>
					<noscript>
						<picture>
							<!--[if IE 9]>
							<video style="display: none;"><![endif]-->
							<source class="lazyload"
									data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,320)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1280,640)) !!} 2x"
									media="(max-width: 736px)"/>
							<!--[if IE 9]></video><![endif]-->
							<img class="lazyload"
								 data-src="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,320)) !!}"
								 alt="{!! the_title() !!}"/>
						</picture>
					</noscript>
				</div>
				<!-- <div class="longform-hero__container__body">

				</div> -->
			</div>
		</section>


		<div class="popup-container" style="display:none">
			@foreach($tbm_authors as $tbm_author)
				<input name="inputtoggle" id="person--{!! $loop->iteration !!}" type="radio" class="toggle"
					   hidden/>
				<div class="person-popup">
					<header class="person-popup__header">
						<label for="person--none" class="btn btn--close btn--close--dark"></label>
						<div class="person-popup__header__thumb">
							<picture>
								<!--[if IE 9]>
								<video style="display: none;"><![endif]-->
								<source class="lazyload"
										data-srcset="{!! tbm_get_the_post_thumbnail_url($tbm_author,array(280,280)) !!}, {!! tbm_get_the_post_thumbnail_url($tbm_author,array(560,560)) !!} 2x"
										media="(max-width: 736px)"/>
								<!--[if IE 9]></video><![endif]-->
								<img class="lazyload"
									 data-srcset="{!! tbm_get_the_post_thumbnail_url($tbm_author,array(280,280)) !!}, {!! tbm_get_the_post_thumbnail_url($tbm_author,array(560,560)) !!} 2x"
									 alt="{!! get_the_title($tbm_author) !!}"/>
							</picture>
							<noscript>
								<picture>
									<!--[if IE 9]>
									<video style="display: none;"><![endif]-->
									<source class="lazyload"
											data-srcset="{!! tbm_get_the_post_thumbnail_url($tbm_author,array(280,280)) !!}, {!! tbm_get_the_post_thumbnail_url($tbm_author,array(560,560)) !!} 2x"
											media="(max-width: 736px)"/>
									<!--[if IE 9]></video><![endif]-->
									<img class="lazyload"
										 data-src="{!! tbm_get_the_post_thumbnail_url($tbm_author,array(280,280)) !!}"
										 alt="{!! get_the_title($tbm_author) !!}"/>
								</picture>
							</noscript>
						</div>
						<div class="single__title">
							@if(get_field( 'email_autore', $tbm_author ))
								<a href="mailto:{!! get_field( 'email_autore', $tbm_author ) !!}">
									<span class="h1">{!! get_the_title($tbm_author) !!}</span></a>
							@else
								<span class="h1">{!! get_the_title($tbm_author) !!}</span>
							@endif
							@if(get_field( 'ruolo_autore', $tbm_author ))
								<span class="h2">{!! get_field( 'ruolo_autore', $tbm_author ) !!}</span>
							@endif

						</div>
					</header>
					@if(get_field( 'esperienza_autore', $tbm_author ))
						<main class="person-popup__main">
							<div class="editorial">
								{!! get_field( 'esperienza_autore', $tbm_author ) !!}
							</div>
						</main>
					@endif
				</div>
			@endforeach
		</div>


		@php wp_reset_postdata() @endphp
		<section class="chisiamo__content">
			<div class="chisiamo__people">
				<div class="single__title single__title--longform">
					<div>
						<h1>{!! get_the_title() !!}</h1>
					</div>
				</div>

				<div class="chisiamo__people__thumbnails">
					<input name="inputtoggle" id="person--none" type="radio" class="toggle" hidden/>
					@foreach($tbm_authors as $tbm_author)
						<div class="person" role="button">
							<label for="person--{!! $loop->iteration !!}">
								<div class="chisiamo__people__thumbnails__thumb">
									<picture>
										<img class="lazyload"
											 data-srcset="{!! tbm_get_the_post_thumbnail_url($tbm_author,array(156,156)) !!}, {!! tbm_get_the_post_thumbnail_url($tbm_author,array(312,312)) !!} 2x"
											 alt="{!! get_the_title($tbm_author) !!}"/>
									</picture>
								</div>
								<div class="person__overlay"><span>{!! get_the_title($tbm_author) !!}</span></div>
							</label>
						</div>
					@endforeach
					@php wp_reset_postdata() @endphp
				</div>
			</div>

			<div class="wrapper">
				<div class="container mobile-switch-container">
					<div class="col-12 editorial">
						{!! the_content() !!}
					</div>
				</div>
			</div>
		</section>
	</div>
	<script>
		(function () {
			var popup = document.getElementsByClassName("popup-container");
			popup[0].style.display = 'block';
		})();
	</script>
@endsection

