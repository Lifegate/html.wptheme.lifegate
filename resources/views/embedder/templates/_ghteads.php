<?php
/**
 * Template for the HTML Embedder plugin
 *
 * Created by PhpStorm.
 * User: francesco
 * Date: 26/02/2018
 * Time: 22:55
 */

if ( function_exists( 'tbm_is_amp' ) && tbm_is_amp() ) {
	tbm_get_the_banner( 'TEADS', '', '', true, false );
} else {
	tbm_get_the_banner( 'AMP_TEADS', '', '', true, false );
}

