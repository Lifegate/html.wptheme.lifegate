<?php
/**
 * Template for the HTML Embedder plugin
 *
 * Created by PhpStorm.
 * User: francesco
 * Date: 26/02/2018
 * Time: 22:55
 */


global $post;
$ad_code = '';
$related = array();

// Se non ho disabilitato la visualizzazione del video, eseguo la funzione
if ( ! get_field( 'video_single_disabled', $post->ID ) ) {
	_tbm_print_random_video( $post );
}

/**
 * Print a random video related to $post
 *
 * @param $post Post object
 */
function _tbm_print_random_video( $post ) {


	$related = tbm_get_custom_related_posts( $post, 1, null, 'listino', 'video' );

	if ( $related && isset( $related[0] ) && ! empty( $related[0]->ID ) ) {

		$post_id = $related[0]->ID;

		if ( function_exists( 'tbm_is_amp' ) && tbm_is_amp() ) {

			$amp_vast = tbm_get_the_banner( 'AMP_VAST', ' ', ' ', false, true );
			html_video_generate_amp_video_player( $related[0]->ID, $amp_vast, '', true );

		} else {

			if ( function_exists( 'html_video_generate_video_player' ) && get_field( 'video_cnvid_id', 'option' ) ) {
				$ad_code = html_video_generate_video_player( $post_id, '', false );
			} else if ( get_field( 'tbm_videoid', $post_id ) && filter_var( get_field( 'tbm_videoid', $post_id )['url'], FILTER_VALIDATE_URL ) ) {
				$ad_code = '<video src="' . get_field( 'tbm_videoid', $post_id )['url'] . '" controls controlsList="nodownload"></video>';
			}

			if ( $ad_code ) {
				echo '<div><div class="video-post video-post--aside">' . $ad_code . '</div><h3 style="margin-top: 20px">' . get_the_title( $post_id ) . '</h3></div>';
			}

		}

	}
}




