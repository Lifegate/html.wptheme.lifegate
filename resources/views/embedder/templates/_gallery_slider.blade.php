@if ( function_exists( 'tbm_is_amp' ) && tbm_is_amp() )
	@include('components.partials.partial-innerrelated-post')
@else
	@include('components.partials.partial-innergallery-slider-gallery')
@endif
