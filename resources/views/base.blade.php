<!doctype html>
<html {!! get_language_attributes() !!} class="no-js">
<head>
	{{-- Head --}}
	@include('components.partials.partial-head')
	@yield('head')
</head>
<body class="sticky-parent">

<!-- Facebook Pixel Code -->
<noscript><img height="1" width="1" style="display:none"
			   src="https://www.facebook.com/tr?id=1871994926361253&ev=PageView&noscript=1"
	/></noscript>
<!-- End Facebook Pixel Code -->

<!-- base.twig -->
<div class="base sticky-kit chosen-js main-lifegate-js">
	@include('components.partials.drawer-menu')
	@include('components.sections.header')
	@php if(!is_post_type_archive("video")) echo tbm_get_the_banner( 'AFTER_HEADER','','',false,false ) @endphp

	{{-- Block content --}}
	@yield('content')

	@include('components.sections.footer')
</div>
	@php if(!is_post_type_archive("iniziative")) echo tbm_get_the_banner( 'AFTER_FOOTER','','',false,false ); @endphp

@php wp_footer() @endphp
<script>
	tbm.respoinsesongtitle = "Ci dispiace, ma non abbiamo trovato nessun risultato utile. Prova ad impostare un altro orario.";
	tbm.photolabels = ['Foto ', ' di '];
</script>
@php $ver = wp_get_theme()->get('Version'); @endphp

@asset('css/afterall.min.css?ver='.$ver)

</body>
</html>
