{{-- /* Template Name: Iframe Radio Player */ --}}
		<!doctype html>
<html lang="it" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, maximum-scale=5, initial-scale=1.0">
	<meta name="format-detection" content="telephone=no">

	<link rel="apple-touch-icon" sizes="180x180" href="@asset('images/apple-touch-icon.png')">
	<link rel="icon" type="image/png" sizes="32x32" href="@asset('images/favicon-32x32.png')">
	<link rel="icon" type="image/png" sizes="16x16" href="@asset('images/favicon-16x16.png')">
	<link rel="mask-icon" color="#5bbad5" href="@asset('images/safari-pinned-tab.svg')">
	<link rel="apple-touch-icon" sizes="180x180" href="@asset('images/apple-touch-icon.png')">
	<link rel="icon" type="image/png" sizes="32x32" href="@asset('images/favicon-32x32.png')">
	<link rel="icon" type="image/png" sizes="194x194" href="@asset('images/favicon-194x194.png')">
	<link rel="icon" type="image/png" sizes="192x192" href="@asset('images/android-chrome-192x192.png')">
	<link rel="icon" type="image/png" sizes="16x16" href="@asset('images/favicon-16x16.png')">
	<link rel="mask-icon" href="@asset('images/safari-pinned-tab.svg')" color="#005039">
	<link rel="shortcut icon" href="@asset('images/favicon.ico')">
	<meta name="msapplication-TileColor" content="#ffc40d">
	<meta name="theme-color" content="#15907E">


	@php
		// Embed GTM
		if ( get_field( 'tbm_common_enable_gtm', 'option' ) && ! empty( get_field( 'tbm_common_code_gtm', 'option' ) ) ) {
			\Tbm_Plugins\TBM_tms::gtm_script_tag( get_field( 'tbm_common_code_gtm', 'option' ) );
		}

		if ( get_field( 'tbm_common_enable_tealium', 'option' ) && ! empty( get_field( 'tbm_common_code_tealium', 'option' ) ) ) {
			\Tbm_Plugins\TBM_tms::tealium_tag( get_field( 'tbm_common_code_tealium', 'option' ) );
		}

	wp_head()
	@endphp

	@php
		if ( function_exists( 'tbm_get_the_banner' ) ) {
			tbm_get_the_banner( 'HEAD', ' ', ' ', true, true );
		}
	@endphp

</head>
<style>
	body {
		overflow: hidden;
		margin: 0px;
		padding:0px;
	}

	.lg_player {
		display:block;
		height: 100vh;
 		width: 100%;
	}

	.hasads .lg_player {
		height: calc(100vh - 184px);
	}

</style>
<body <?php if($_GET["debug"] == 1) echo " class='hasads' " ?>>
{!! tbm_get_the_banner( 'RADIO_PLAYER_HEADER','','',false,false ) !!}
<!--page-radioplayer.twig page -->
<div class="page-radioplayer"><iframe class="lg_player"
			src="https://play.xdevel.com/12650"
			frameborder="no" scrolling="0" allowfullscreen="allowfullscreen"></iframe></div>
{!! tbm_get_the_banner( 'RADIO_PLAYER_FOOTER','','',false,false ) !!}
</body>


</html>
