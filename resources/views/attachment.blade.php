@extends('base')
@section('content')
	<!--single-post.twig page -->
	<div class="single single-post">
		<div class="site">

			@include('components.partials.widget-stories')

			@asset('css/single-post.min.css')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
				@while (have_posts()) @php the_post() @endphp
				<div class="container">
					@include('components.partials.partial-breadcrumb')
					<div class="section sticky-parent">
						<main class="main-content main-content--left" style="width: 100%">

							<section>
								<div class="single__featured-image" role="figure"
									 aria-labelledby="featured-image-caption">
									<picture>
										<!--[if IE 9]>
										<video style="display: none;"><![endif]-->
										<source srcset="{!! tbm_wp_get_attachment_image_url(get_the_ID(),array(980,551)) !!}, {!! tbm_wp_get_attachment_image_url(get_the_ID(),array(1960,1102)) !!} 2x"
												media="(max-width: 736px)"/>
										<!--[if IE 9]></video><![endif]-->
										<img class="lazyload"
											 data-srcset="{!! tbm_wp_get_attachment_image_url(get_the_ID(),array(980,551)) !!}, {!! tbm_wp_get_attachment_image_url(get_the_ID(),array(1960,1102)) !!} 2x"
											 alt="{!! the_title() !!}"/>
									</picture>
									<span id="featured-image-caption" role=""
										  class="figcaption">{{ tbm_wp_get_attachment_image_url(get_post_thumbnail_id()) }}</span>
								</div>
							</section>

							<section>
								<div class="post__content editorial">
									{!! the_content() !!}
								</div>
								<div class="post__trends flex">
									@include('components.custom.custom-post-social-box')
								</div>
							</section>
						</main>
					</div>
				</div>
				@endwhile
			</div>
		</div> <!-- end site -->
	</div>
@endsection
