{{-- /* Template Name: Homepage News */ --}}
@extends('base')
@section('content')
	@asset('css/frontpage-magazine.min.css')
	<!--frontpage-magazine.twig page -->
	<div class="page frontpage-magazine">
		<div class="site">

			@include('components.partials.widget-stories')

			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
				<div class="container">
					<div class="single__title">
						<h1>{!! get_the_title() !!}</h1>
					</div>

					<section class="featured-section">
						<div class="col-6">
							{{-- query: posts.items.children[1]  --}}
							@include('components.sections.section-1-columns',array('blocks' => array(
								['template' => 'card-post-special','posts' => $mg_hp_pp,'post_number' => 1]
							)))
						</div>
						<div class="col-6">
							<div class="container-cycle-col-2">
								@include('components.sections.section-1-columns',array('blocks' => array(
									['template' => 'partial-card-post_type-big--k2','posts' => $mg_hp_sp,'post_number' => 4]
								)))
							</div>
						</div>
						<!-- end last from magazine -->
					</section>

					<div class="section sticky-parent">

						<main class="main-content main-content--right">

							{{-- Blocco verticale --}}
							@if(isset($mg_hp_blocks[0]))
								<section>
									@include('components.partials.title-section',array('text' => $mg_hp_blocks[0]['name'],'textlink' => __('Leggi tutto su','lifegate') . ' '.$mg_hp_blocks[0]['name'],'url' =>  $mg_hp_blocks[0]['url'],'css' =>  true))
									<div class="section">
										<div class="col-5">
											{{-- query: posts.items.children[1]  --}}
											@include('components.sections.section-1-columns',array('blocks' => array(
												['template' => 'partial-card-post_type-big--k2','posts' => $mg_hp_blocks[0]['query'],'post_number' => 1]
												)))
										</div>
										<div class="col-7">
											@include('components.sections.section-1-columns',array('blocks' => array(
											['template' => 'partial-card-post_type-list--k2','posts' =>  $mg_hp_blocks[0]['query'],'post_number' => 3,'offset' => 1]
										)))
										</div>
									</div>
									<div class="section archive">
										@include('components.sections.section-1-columns',array('blocks' => array(
											['template' => 'partial-card-post_type-list--k2','posts' =>  $mg_hp_blocks[0]['query'],'post_number' => 5,'offset' => 4]
											)))
									</div>
								</section>
							@endif

							{{-- Blocco verticale --}}
							@if(isset($mg_hp_blocks[1]))
								<section>
									@include('components.partials.title-section',array('text' => $mg_hp_blocks[1]['name'],'textlink' => __('Leggi tutto su','lifegate') . ' '.$mg_hp_blocks[1]['name'],'url' =>  $mg_hp_blocks[1]['url'],'css' =>  true))
									<div class="section">
										<div class="col-5">
											{{-- query: posts.items.children[1]  --}}
											@include('components.sections.section-1-columns',array('blocks' => array(
												['template' => 'partial-card-post_type-big--k2','posts' => $mg_hp_blocks[1]['query'],'post_number' => 1]
												)))
										</div>
										<div class="col-7">
											@include('components.sections.section-1-columns',array('blocks' => array(
											['template' => 'partial-card-post_type-list--k2','posts' =>  $mg_hp_blocks[1]['query'],'post_number' => 3,'offset' => 1]
										)))
										</div>
									</div>
									<div class="section archive">
										@include('components.sections.section-1-columns',array('blocks' => array(
											['template' => 'partial-card-post_type-list--k2','posts' =>  $mg_hp_blocks[1]['query'],'post_number' => 5,'offset' => 4]
											)))
									</div>
								</section>
							@endif

						</main>
						<aside class="sidebar sidebar--left">
							@include('components.partials.partial-sticky-adv')
						</aside>
					</div>


					{{-- Blocco orizzontale --}}
					@if(isset($mg_hp_blocks[2]))
						<section>
							@include('components.partials.title-section',array('text' => $mg_hp_blocks[2]['name'],'textlink' => __('Leggi tutto su','lifegate') . ' '.$mg_hp_blocks[2]['name'],'url' =>  $mg_hp_blocks[2]['url'],'css' =>  true))
							<div class="col-7">
								<div class="container-cycle-col-2">
									@include('components.sections.section-1-columns',array('blocks' => array(
										['template' => 'partial-card-post_type-big--k2','posts' => $mg_hp_blocks[2]['query'],'post_number' => 2]
										)))
								</div>
								@include('components.sections.section-1-columns',array('blocks' => array(
								['template' => 'partial-card-post_type-list--k2','posts' => $mg_hp_blocks[2]['query'],'post_number' => 2,'offset' => 2]
								)))
							</div>
							<div class="col-5">
								@include('components.sections.section-1-columns',array('blocks' => array(
								['template' => 'partial-card-post_type-list--k2','posts' => $mg_hp_blocks[2]['query'],'post_number' => 5,'offset' => 4]
								)))
							</div>
						</section>
					@endif

					<div class="section sticky-parent">

						<main class="main-content main-content--right">

							{{-- Blocco verticale --}}
							@if(isset($mg_hp_blocks[3]))
								<section>
									@include('components.partials.title-section',array('text' => $mg_hp_blocks[3]['name'],'textlink' => __('Leggi tutto su','lifegate') . ' '.$mg_hp_blocks[3]['name'],'url' =>  $mg_hp_blocks[3]['url'],'css' =>  true))
									<div class="section">
										<div class="col-5">
											{{-- query: posts.items.children[1]  --}}
											@include('components.sections.section-1-columns',array('blocks' => array(
												['template' => 'partial-card-post_type-big--k2','posts' => $mg_hp_blocks[3]['query'],'post_number' => 1]
												)))
										</div>
										<div class="col-7">
											@include('components.sections.section-1-columns',array('blocks' => array(
											['template' => 'partial-card-post_type-list--k2','posts' =>  $mg_hp_blocks[3]['query'],'post_number' => 3,'offset' => 1]
										)))
										</div>
									</div>
									<div class="section archive">
										@include('components.sections.section-1-columns',array('blocks' => array(
											['template' => 'partial-card-post_type-list--k2','posts' =>  $mg_hp_blocks[3]['query'],'post_number' => 5,'offset' => 4]
											)))
									</div>
								</section>
							@endif

							{{-- Blocco verticale --}}
							@if(isset($mg_hp_blocks[4]))
								<section>
									@include('components.partials.title-section',array('text' => $mg_hp_blocks[4]['name'],'textlink' => __('Leggi tutto su','lifegate') . ' '.$mg_hp_blocks[4]['name'],'url' =>  $mg_hp_blocks[4]['url'],'css' =>  true))
									<div class="section">
										<div class="col-5">
											{{-- query: posts.items.children[1]  --}}
											@include('components.sections.section-1-columns',array('blocks' => array(
												['template' => 'partial-card-post_type-big--k2','posts' => $mg_hp_blocks[4]['query'],'post_number' => 1]
												)))
										</div>
										<div class="col-7">
											@include('components.sections.section-1-columns',array('blocks' => array(
											['template' => 'partial-card-post_type-list--k2','posts' =>  $mg_hp_blocks[4]['query'],'post_number' => 3,'offset' => 1]
										)))
										</div>
									</div>
									<div class="section archive">
										@include('components.sections.section-1-columns',array('blocks' => array(
											['template' => 'partial-card-post_type-list--k2','posts' =>  $mg_hp_blocks[4]['query'],'post_number' => 5,'offset' => 4]
											)))
									</div>
								</section>
							@endif

						</main>
						<aside class="sidebar sidebar--left">
							@include('components.partials.partial-sticky-adv')
						</aside>
					</div>

					{{-- Blocco orizzontale --}}
					@if(isset($mg_hp_blocks[5]))
						<section>
							@include('components.partials.title-section',array('text' => $mg_hp_blocks[5]['name'],'textlink' => __('Leggi tutto su','lifegate') . ' '.$mg_hp_blocks[5]['name'],'url' =>  $mg_hp_blocks[5]['url'],'css' =>  true))
							<div class="col-7">
								<div class="container-cycle-col-2">
									@include('components.sections.section-1-columns',array('blocks' => array(
										['template' => 'partial-card-post_type-big--k2','posts' => $mg_hp_blocks[5]['query'],'post_number' => 2]
										)))
								</div>
								@include('components.sections.section-1-columns',array('blocks' => array(
								['template' => 'partial-card-post_type-list--k2','posts' => $mg_hp_blocks[5]['query'],'post_number' => 2,'offset' => 2]
								)))
							</div>
							<div class="col-5">
								@include('components.sections.section-1-columns',array('blocks' => array(
								['template' => 'partial-card-post_type-list--k2','posts' => $mg_hp_blocks[5]['query'],'post_number' => 5,'offset' => 4]
								)))
							</div>
						</section>
					@endif

				</div>
			</div>
		</div>
	</div>
@endsection
