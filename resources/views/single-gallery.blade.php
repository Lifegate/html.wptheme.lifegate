@extends('base')
@section('content')
	<div class="single single-gallery">
		@while (have_posts()) @php the_post() @endphp
		<div class="site">

			@include('components.partials.widget-stories')

			<style>
				@media screen and (min-width: 737px) {
					.fullwidth-gallery .featured-image__box-image.featured-image__box-image--V2 picture {
						padding-bottom: 56.25% !important;
					}
				}
			</style>
			@asset('css/single-gallery.min.css')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.partial-breadcrumb')
					<div class="single__title flex">
						@if($related_trend)
							<a class="post__story"
							   href="{!! $related_trend['link'] !!}">{!! $related_trend['name'] !!}</a>
						@endif
						<h1>{!! the_title() !!}</h1>
					</div>
					<div class="section sticky-parent">

						<main class="main-content main-content--left">
							<section>
								<div class="single-gallery-slider gallery-slider" data-slidesperview="1">
									@include('components.sections.fullwidth-gallery')
								</div>

								<div class="single__hero-footer__datas">
									{!! lifegate_snippet_post_date() !!},&nbsp;<div
											class="post__author">{!! $single_post_authors !!}</div>
									@include('components.custom.custom-post-social-box')
									@include('components.custom.custom-post-language-switcher')
								</div>

								@if(get_the_content())
									<div class="post__content editorial">{!! the_content() !!}</div>
								@elseif(get_the_excerpt())
									<div class="post__content editorial">{!! the_excerpt() !!}</div>
								@endif



							</section>
						</main>
						<aside class="sidebar sidebar--right">

							@include('components.partials.partial-sticky-adv')

						</aside>
					</div>
				</div>
			</div>
		</div><!-- end site -->
		@endwhile
	</div>
@endsection
