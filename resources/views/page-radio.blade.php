{{-- /* Template Name: Homepage Radio  */ --}}
@extends('base')
@section('content')
	<!--frontpage-radio.twig page -->
	<div class="page frontpage-radio">
		<div class="site">
			@asset('css/frontpage-radio.min.css')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
				@while (have_posts()) @php the_post() @endphp
				<div class="container">
					<div class="single__title">
						<h1>{!! get_the_title() !!}</h1>
					</div>
					<section class="featured-section">
						@include('components.custom.section-3-columns-radio', array('blocks' => array([
							'first_template' => 'card-song-big',
							'template'    => 'card-post_type-big',
							'posts'       => $hp_pr,
							'post_number' => 3
						])))
					</section>

					<div class="section sticky-parent">

						<main class="main-content main-content--right">

							{{-- Blocco verticale --}}
							@if(isset($radio_hp_blocks[0]))
								<section>
									@include('components.partials.title-section',array('text' => $radio_hp_blocks[0]['name'],'textlink' => __('Leggi tutto su','lifegate').' '.$radio_hp_blocks[0]['name'],'url' =>  $radio_hp_blocks[0]['name'],'css' =>  true))
									<div class="section">
										<div class="col-5">
											{{-- query: posts.items.children[1]  --}}
											@include('components.sections.section-1-columns',array('blocks' => array(
												['template' => 'partial-card-post_type-big--k2','posts' => $radio_hp_blocks[0]['query'],'post_number' => 1]
												)))
										</div>
										<div class="col-7">
											@include('components.sections.section-1-columns',array('blocks' => array(
											['template' => 'partial-card-post_type-list--k2','posts' =>  $radio_hp_blocks[0]['query'],'post_number' => 3,'offset' => 1]
										)))
										</div>
									</div>
									<div class="section archive">
										@include('components.sections.section-1-columns',array('blocks' => array(
											['template' => 'partial-card-post_type-list--k2','posts' =>  $radio_hp_blocks[0]['query'],'post_number' => 5,'offset' => 4]
											)))
									</div>
								</section>
							@endif

						</main>
						<aside class="sidebar sidebar--left">
							@include('components.partials.partial-sticky-adv')
						</aside>
					</div>


				</div>
				@endwhile
			</div>
		</div><!-- end site -->
	</div>
@endsection
