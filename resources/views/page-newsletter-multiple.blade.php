{{-- /* Template Name: Newsletter Multiple */ --}}
@extends('base')
@section('head')
<style>
.newsletters-block .col-4{
position:relative;
border: solid 1px #666;
padding:20px 20px 10px 20px;
margin-bottom:40px;
}

.newsletters-block input[type=checkbox]{
position:absolute;
top:20px;
right:20px;
}

.newsletters-block .nl-tumb{
margin-bottom:20px;
}

.newsletters-block .nl-titolo{
margin-bottom:20px;
}

.newsletters-block p{
line-height:22px;
}

.newsletters-block .nl-archive{
color:#666;
font-size:18px;
}

.nl-fields button[type=submit] {
    background-image: url(/app/themes/lifegate-2020/dist/images/icons.svg);
    background-color: #006454!important;
    background-position: right 13px top -1170px;
    width: 240px;
    border: 1px solid transparent;
    color: #fff;
    padding: 20px 0px 14px 0px;
    text-transform: uppercase;
    font-size: 18px;
    background-repeat: no-repeat;
    background-size: 34px 2700px;
    display: flex;
    justify-content: center;
		margin-bottom:40px;
}

		input[type=checkbox] {
    transform: scale(1.5);
}

.nl-fields label{
font-weight:bold;
}

.nl-fields{
padding-bottom:40px;
}
.nl-fields .optionals {
padding-top:30px;
}
.nl-fields .optionals label{
font-weight:normal;
}

.nl-fields .optionals input[type=checkbox] {
margin-right:10px;
}

#pratica_fields {
padding-top:20px;
}
#pratica_fields .pratica-titolo{
display:inline;
    vertical-align: middle;

height:24px;
margin-bottom:4px;
line-height:200px;
}

.nl-grey, .nl-grey p{
color:#444 !important;
}

.newsletters-block .col-4 p{
color:#444 !important;
}

select,	input[type=text] {
margin-bottom:10px;
}

.nl-small, .nl-small a, .nl-small p, .nl-small p a{
font-size:14px !important;
font-family:Nunito;
}
.nl-large{
font-size:22px !important;
line-height: 32px !important
}
</style>
@endsection
@section('content')
@asset('css/page-html.min.css')
@if ( have_posts() )
@while (have_posts()) @php the_post() @endphp
<div class="site page page-html">
	<div class="wrapper">
		<div class="container">
			@include('components.partials.main-menu')
		</div>
	</div>
	@include('components.sections.trends')
	<div class="wrapper">
		<section class="page-hero-none">
			<div class="page-hero__container-none container">
				<div class="page-hero__container__body-none editorial">
					@include('components.partials.partial-breadcrumb')
					<h1>{!! the_title() !!}</h1>
					{!! the_content() !!}
					<form action="https://c4f0i.emailsp.com/frontend/subscribe.aspx">

					<div class="section-3-columns newsletters-block">

					<div class="col-4">
							<label>
							<img class="nl-tumb" src="@asset('images/newsletter/Tumb_lanewsletter.jpg')" />
							<input name="list" type="checkbox"
										value="1">
 							<img class="nl-titolo" src="@asset('images/newsletter/titolo_lanewsletter.jpg')" />
							<p>Notizie della settimana dal mondo della sostenibilit&agrave; </p>
							<a target="_blank" href="https://c4f0i.emailsp.com/frontend/newsletters.aspx?idlist=1&hashlista=76eda201-574f-4ad3-a1ce-d61c59aa1421&pv=1" class="nl-archive"> > ARCHIVIO</a>
							</label>
						</div>

						<div class="col-4">
							<label>
							<img class="nl-tumb" src="@asset('images/newsletter/Tumb_ilclimatariano.jpg')" />
							<input name="list" type="checkbox"
										value="13">
 							<img class="nl-titolo" src="@asset('images/newsletter/titolo_il-climatariano.jpg')" />
							<p>Il punto di vista &ldquo;metabolizzato&rdquo; sulla crisi climatica </p>
							<a target="_blank" href="https://c4f0i.emailsp.com/frontend/newsletters.aspx?idlist=13&hashlista=89a5902e-1ac3-40eb-bd06-ff6e6850555f&pv=1" class="nl-archive"> > ARCHIVIO</a>
							</label>
						</div>
						<div class="col-4">
							<label>
							<img class="nl-tumb" src="@asset('images/newsletter/Tumb_mediterranea.jpg')" />
							<input name="list" type="checkbox"
										value="14">
 							<img class="nl-titolo" src="@asset('images/newsletter/titolo_mediterranea.jpg')" />
							<p>Perch&eacute; sul piatto non c&rsquo;&egrave; solo il gusto </p>
							<a target="_blank" href="https://c4f0i.emailsp.com/frontend/newsletters.aspx?idlist=14&hashlista=834a0b1f-b520-4da2-83bd-16a1884d9282&pv=1" class="nl-archive"> > ARCHIVIO</a>
							</label>
						</div>

					</div>
					<div class="section-3-columns newsletters-block">
					<div class="col-4">
							<label>
							<img class="nl-tumb" src="@asset('images/newsletter/Tumb_evoluta.jpg')" />
							<input name="list" type="checkbox"
										value="18">
 							<img class="nl-titolo" src="@asset('images/newsletter/titolo_evoluta.jpg')" />
							<p>Perch&eacute; tutte le specie hanno pari diritti </p>
							<a target="_blank" href="https://c4f0i.emailsp.com/frontend/newsletters.aspx?idlist=18&hashlista=f6f0071d-0014-46cb-a421-1585667b91f1&pv=1" class="nl-archive"> > ARCHIVIO</a>
							</label>
						</div>
						<div class="col-4">
							<label>
							<img class="nl-tumb" src="@asset('images/newsletter/Tumb_pratica.jpg')" />
							<input name="list" type="checkbox"
							 onchange='handleChangePratica(this);'
										value="3">
 							<img class="nl-titolo" src="@asset('images/newsletter/titolo_pratica.jpg')" />
							<p>Risposte e soluzioni sostenibili per il business </p>
							<a target="_blank" href="https://c4f0i.emailsp.com/frontend/newsletters.aspx?idlist=3&hashlista=c74fa49b-3a64-4319-bbc8-ccd4a8a92b00&pv=1" class="nl-archive"> > ARCHIVIO</a>
							</label>
						</div>


					</div>
						<div class="col-12 nl-fields">
						<div class="">
							<label for="0">Nome</label><br><input id="campo1" maxlength="200" name="campo1"
									required="required" type="text" placeholder="">
						</div>
						<div class="">
							<label for="1">Cognome</label><br><input id="campo2" maxlength="200" name="campo2"
									required="required" type="text" placeholder="">
						</div>
						<div class="">
							<label for="2">Email</label><br><input id="email" name="email" required="required"
									type="email" placeholder="Inserisci la tua email">
						</div>
						<div class="" id="pratica_fields">
								<p class="nl-large">Per l'iscrizione a <b>pratica_</b> ti chiediamo qualche dato in più. Solo così possiamo essere sicuri di darti sempre le risposte giuste e le soluzioni migliori per cambiare il tuo business in chiave sostenibile.</p>
						<div><label for="4">Ruolo in azienda</label> <small>(facoltativo)</small><br/> <select id="campo17" name="campo17">
								<option selected="selected" value=""></option>
								<option value="DE">Direzione Generale</option>
								<option value="MA">Management</option>
								<option value="VE">Vendite</option>
								<option value="AC">Acquisti</option>
								<option value="Mk">Marketing</option>
								<option value="Co">Comunicazione</option>
								<option value="Re">Risorse Umane</option>
								<option value="Lo">Logistica</option>
								<option value="AF">Amministrazione e finanza</option>
								<option value="Cs">CSR</option>
								<option value="RS">Ricerca e Sviluppo</option>
								<option value="PQ">Produzione e Qualità</option>
								<option value="REE">Relazioni Esterne</option>
								<option value="Cu">Consulente</option>
							</select></div>
						<div><label for="5">Azienda o Libero professionista</label> <small>(facoltativo)</small><br/> <input id="campo3"
									maxlength="200" name="campo3" type="text" placeholder=""/></div>
						<div><label for="6">Settore merceologico</label> <small>(facoltativo)</small><br/> <input id="campo22" maxlength="200"
									name="campo22" type="text" placeholder=""/></div>
						<div><label for="7">Città</label> <small>(facoltativo)</small><br/> <input id="campo4" maxlength="200" name="campo4"
									 type="text" placeholder=""/></div>
						<div><label
									for="8">Interessi in materia di sostenibilità</label> <small>(facoltativo)</small><br/> <input id="campo16"
									maxlength="200" name="campo16" type="text" placeholder=""/>
									</div>

						</div>
						<p class="optionals"><b>Desidero rimanere aggiornato sui servizi e prodotti di LifeGate S.p.A.*</b><br>
						<label class="nl-grey nl-small"><br><input
										name="list" type="checkbox"
										value="15">Autorizzo LifeGate S.p.A. al trattamento dei miei dati di contatto (informazioni relative al nome, cognome, luogo e data di nascita, indirizzo, numero di telefono, numero di cellulare, indirizzo email) per finalit&agrave; di marketing e comunicazione pubblicitaria diretta ad informarLa su iniziative promozionali di vendita, realizzate mediante modalit&agrave; automatizzate di contatto (posta elettronica).<br></label><br><b>Desidero rimanere aggiornato sui servizi e prodotti di societ&agrave; del Gruppo LifeGate e di terze parti*</b><br>
						<label class="nl-grey nl-small"><br><input
										name="list" type="checkbox"
										value="16">Autorizzo LifeGate S.p.A. al trattamento dei miei dati di contatto (informazioni relative al nome, cognome, luogo e data di nascita, indirizzo, numero di telefono, numero di cellulare, indirizzo email) per attivit&agrave; di marketing su prodotti e servizi di societ&agrave; del Gruppo LifeGate ed anche di terzi (appartenenti prevalentemente ai settori dell&rsquo;editoria, della finanza, dell&rsquo;economia, dell&rsquo;industria, del lusso, dei servizi, delle telecomunicazioni, dell&rsquo;Ict, delle assicurazioni e del no profit), realizzate mediante modalit&agrave; automatizzate di contatto (posta elettronica).<br></label>
						</p>
						<div class="nl-grey nl-small">
							<p>* I consensi sono facoltativi: posso in ogni momento revocare le mie dichiarazioni di consenso alla Societ&agrave; inviando una e-mail all&rsquo;indirizzo:
								<a href="mailto:privacy@lifegate.com">privacy@lifegate.com</a>. Se hai meno di 18 anni, questo modulo deve essere sottoscritto da un genitore esercente la responsabilit&agrave; genitoriale o dal tuo tutore.
							</p>
						</div>
						<p class="nl-grey">Presa visione dell&rsquo;<a target="_blank" href="https://www.lifegate.it/privacy">informativa privacy</a>.<br>
										Se prima di iscriverti vuoi avere un&rsquo;idea sulle nostre newsletter <a
										href="https://c4f0i.emailsp.com/frontend/nl_catalog.aspx" target="_blank"
										rel="noopener">clicca qui</a>.</p>
						<p><input id="apgroup" name="apgroup" type="hidden" value="244"></p>
						<div>
							<button name="submit" type="submit" value="true"> Iscriviti</button>
						</div>
						<div>
							<strong>Dopo esserti iscritto, controlla la posta in arrivo per confermare l&rsquo;iscrizione</strong>
						</div>
					</div>
					</form>
				<script type="text/javascript">
				var x = document.getElementById("pratica_fields");
				x.style.display = "none";

				const urlParams = new URLSearchParams(window.location.search);
				const checked = urlParams.get('checked');
				if(checked){
					var checkboxes = document.querySelectorAll('input[type="checkbox"]');
					for (var i = 0; i < checkboxes.length; i++) {
						if(parseInt(i) == (parseInt(checked) + 1)){
							checkboxes[i].checked = true;
							if(parseInt(checked) == 5){
					 			x.style.display = "block";
							}
						}
					}
				}
				const forms = document.querySelectorAll("form");
				for (let form of forms) {
					form.addEventListener("submit", function(evt) {
						var checkboxes = document.querySelectorAll('input[type="checkbox"]');
						var checkedOne = Array.prototype.slice.call(checkboxes).some(x => x.checked);
						if(!checkedOne){
							evt.preventDefault();
							alert("Seleziona almeno una newsletter");
						}
					});
				}
				function handleChangePratica(checkbox) {
					var x = document.getElementById("pratica_fields");
					if(checkbox.checked == true){
						 x.style.display = "block";
					}else{
						 x.style.display = "none";
				   }
				}

				</script>
				</div>
			</div>
		</section>
	</div>
</div>
@endwhile
@endif
@endsection
