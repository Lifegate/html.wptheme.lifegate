<!doctype html>
<html {!! get_language_attributes() !!} class="no-js">
<head>
	{{-- Head --}}
	@include('components.partials.partial-head')
</head>
<body class="sticky-parent">

<!-- Facebook Pixel Code -->
<noscript><img height="1" width="1" style="display:none"
			   src="https://www.facebook.com/tr?id=1871994926361253&ev=PageView&noscript=1"
	/></noscript>
<!-- End Facebook Pixel Code -->

<!-- base.twig -->
<div class="base sticky-kit chosen-js main-lifegate-js">
	@include('components.partials.drawer-menu')
	@include('components.sections.header')
	@php if(!is_post_type_archive("video")) echo tbm_get_the_banner( 'AFTER_HEADER','','',false,false ) @endphp

	{{-- Block content --}}
	@yield('content')

	@include('components.sections.footer')
</div>
{!! tbm_get_the_banner( 'RADIO_PLAYER_FOOTER','','',false,false ) !!}
@php wp_footer() @endphp

@php $ver = wp_get_theme()->get('Version'); @endphp

@asset('css/afterall.min.css?ver='.$ver)

</body>
</html>
