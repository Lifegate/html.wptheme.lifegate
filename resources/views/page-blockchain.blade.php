{{-- /* Template Name: Pagina Blockchain  */ --}}
<?php
if(!$_GET["bid"])
	wp_redirect(home_url());

if(!is_numeric($_GET["bid"]))
	wp_redirect(home_url());

$post = get_post($_GET["bid"]);
if(!$post)
	wp_redirect(home_url());

// controllo se l'id passato è effettivamente di un post in blockchain
$blockchain_main_id = get_field("blockchain_main_id", $_GET["bid"]);
if(!$blockchain_main_id){
	wp_redirect(home_url());
}

// controllo se c'è già settato il campo hash_eth
$blockchain_hash_eth = get_field("blockchain_hash_eth", $_GET["bid"]);
if(!$blockchain_hash_eth){
	// controllo se è stato pubblicato e in caso aggiorno il dato su wp
	$blockchain_hash_eth =  lifegate_get_hash_eth($post);
}


$blockchain_data = get_field("blockchain_data", $_GET["bid"]);
$datas = json_decode($blockchain_data, true);

?>
@extends('base')
@section('content')
	@asset('css/page-html.min.css')
	@if ( have_posts() )
		@while (have_posts()) @php the_post() @endphp
		<div class="site page page-html">
			@include('components.partials.widget-stories')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			<div class="wrapper">
				<section class="page-hero-none">
					<div class="page-hero__container-none container">
						<div class="page-hero__container__body-none editorial">
							@include('components.partials.partial-breadcrumb')
							<h1>{!! the_title() !!}</h1>

							{!! the_content() !!}

							<?php if($blockchain_hash_eth){ ?>
							<p>
								Per verificare direttamente clicca qui:  <a href="https://algoexplorer.io/tx/<?php echo $blockchain_hash_eth; ?>">https://algoexplorer.io/tx/<?php echo $blockchain_hash_eth; ?></a> .
							</p>
							<?php } ?>
							<h3>Come verificare in autonomia</h3>
							<ol style="background-color: #fff; margin-top: 0px;padding-top: 0px;margin-bottom: 0px;padding-bottom: 0px;">
								<li>copia il parametro <b>ID Transizione</b></li>
								<li> Vai su <a href="https://algoexplorer.io/">algoexplorer.io</a></li>
								<li>incolla il parametro <b>ID Transizione</b> nella stringa di ricerca di <a href="https://algoexplorer.io/">algoexplorer.io</a></li>
							</ol>



							<div class="col-12 blockchain-data">
								<h3>Titolo dell'articolo</h3>
								<h5><?php echo $post->post_title; ?></h5>
								<p>
								<h3><?php _e("Testo dell'articolo"); ?></h3>

								<textarea id="content-blockchain" readonly><?php
									if($post)
										echo wpautop($post->post_content);
									?></textarea>
								<!-- Trigger -->
								<button class="btn-clipboard btn-blockchain" data-clipboard-action="copy" data-clipboard-target="#content-blockchain">
									<?php _e("Seleziona e copia negli appunti"); ?>
								</button>
								</p>

								<p>
								<h5><?php _e("<b>Hash</b>, è l’impronta digitale di un articolo. Ogni articolo ha un hash univoco."); ?></h5>
								<!-- Target -->
								<input id="hash-blockchain" value="<?php echo $datas["certificate_hash"] ?>">

								<!-- Trigger -->
								<button class="btn-clipboard btn-blockchain" data-clipboard-target="#hash-blockchain">
									<?php _e("Seleziona e copia negli appunti"); ?>
								</button>
								</p>
								<p>
								<h5><?php _e("<b>ID Transazione</b>, è la registrazione – con autore e data – di un dato in blockchain. È immutabile."); ?></h5>
								<?php
								if($blockchain_hash_eth){
								?>
								<input id="id-blockchain_hash_eth" value="<?php echo $blockchain_hash_eth; ?>">

								<button class="btn-clipboard btn-blockchain" data-clipboard-target="#id-blockchain_hash_eth">
									<?php _e("Seleziona e copia negli appunti"); ?>
								</button>
								<?php
								}else{
									echo "ID Transazione ancora non disponibile. Riprova tra qualche minuto.";

								}
								?>
								</p>

							</div>

							<p>&nbsp;</p>
							<script src="@asset('js/libs/clipboard/clipboard.min.js')" ></script>
							<script>
								new ClipboardJS('.btn-clipboard');
							</script>
							<style>
								.blockchain-data{
									background-color: #dbe1dd;
									padding: 20px;
									margin-top:20px;
									word-wrap: break-word;

								}

								.blockchain-data textarea{
									background-color: #fff;
									margin-bottom:10px;
								}

								.blockchain-data input{
									padding:10px;
									width: 400px;
									max-width: 100%;
									margin-right:10px;
									margin-bottom: 10px;
								}
								.btn-clipboard{
									border: solid 1px #006454;
									border-radius: 4px;
									padding:10px;
									color: #006454;
									margin-bottom: 10px;
								}

							</style>
						</div>
					</div>
				</section>
			</div>
		</div>
		@endwhile
	@endif
@endsection
