@extends('base-original')
@section('content')


	<section id="nav">
		@asset('css/archive-original.min.css')
		<nav class="seco-menu">
			<div>
				<ul>
					@if (has_nav_menu('global_navigation'))
						{!! wp_nav_menu($main_menu) !!}
					@endif
				</ul>
			</div>
			<div class="channel-submenu" aria-label="magazine-submenu">
				<ul>
					{!! wp_nav_menu($channels_menu) !!}
				</ul>
			</div>
			<div class="separator">
				<hr>
			</div>
		</nav>
	</section>


	<?php
	$or_hp_pp = ArchiveOriginal::or_hp_pp();
//	$or_hp_sp = ArchiveOriginal::or_hp_sp();
	?>
	<section class="apertura-landing lazyload"  data-script="@asset('js/components/partials/archive-original.js')">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="owl-apertura owl-carousel">
						<?php
							$c=0;
							foreach ($or_hp_pp as $idpost){
								$post = get_post($idpost);
								$c++;
								?>
							<div class="owl-inner-item" id-slide="<?php echo $c; ?>">
								<img src="{!! tbm_get_the_post_thumbnail_url($post->ID,array(1440,800)) !!}"
									 alt="{!! get_the_title($post) !!}"/>
								<div class="caption-apertura text-white">
									<div class="spnsorizzato">
										<small>{!!  lifegate_original_sponsor_snippet($post->ID, false) !!}</small>
									</div>
									<a href="{!! get_the_permalink($post) !!}" aria-label="{!! get_the_title($post) !!}">
									<h1>{!! get_the_title($post) !!}</h1>
									</a>
									<small><?php echo SingleOriginal::original_next_to_the_title($post->ID); ?> | {!! lifegate_snippet_post_date($post) !!}</small>
								</div>
							</div>
						<?php
							}
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="site">
			<div class="container">
				<div class="row">
					<?php
					$c=0;
					foreach ($or_hp_pp as $idpost){
					$post = get_post($idpost);
						$c++;
						?>
						<div class="col-3 owl-posts <?php if($c == 1) echo "current-post"; else if($c == 2) echo "next-post"; ?>" id-div-slide="<?php echo $c; ?>">
							<div class="info-header">
								<small><?php echo SingleOriginal::original_next_to_the_title($post->ID); ?></small>
								<small>{!! lifegate_snippet_post_date($post) !!}</small>
							</div>
							<a href="{!! get_the_permalink($post) !!}" aria-label="{!! get_the_title($post) !!}">
							<h2>{!! get_the_title($post) !!}</h2>
							</a>
						</div>

					<?php
					}
					?>
				</div>
			</div>
		</div>
	</section>

	<?php
	$or_hp_sp = ArchiveOriginal::or_hp_sp();
	?>
	<div class="site">
		<section id="under_apertura">
			<div class="container">
				<div class="row titolo-sezione-row">
					<div class="col-12">
						<h2 class="titolo-sezione">In evidenza</h2>
					</div>
				</div>
				<div class="row">
					<?php
					$c=0;
					foreach ($or_hp_sp as $idpost){
						$post = get_post($idpost);
						$c++;

						?>
					<div class="col-6">
						<div class="box">
							<a href="{!! get_the_permalink($post) !!}" aria-label="{!! get_the_title($post) !!}">
								<img class="lazyload"
									 data-srcset="{!! tbm_get_the_post_thumbnail_url($post->ID,array(650,310)) !!}, {!! tbm_get_the_post_thumbnail_url($post->ID,array(1300,620)) !!} 2x"
									 alt="{!! the_title() !!}"/>
							</a>
							<div class="box-labels opa up left outdoor"><?php echo SingleOriginal::original_next_to_the_title($post->ID); ?></div>
							<!--<div class="box-labels opa up right">{!! lifegate_snippet_post_date($post) !!}</div> /-->
							{!!  lifegate_original_sponsor_snippet($post->ID) !!}
						</div>
						<div class="box-text">
							<a href="{!! get_the_permalink($post) !!}" aria-label="{!! get_the_title($post) !!}">
								<h2 class="article-title">{!! get_the_title($post) !!}</h2>
							</a>
						</div>
						<?php echo SingleOriginal::author_box($post); ?>
					</div>
						<?php
						}
						?>
				</div>
			</div>
		</section>
	</div>

	<?php
	$canale_1_original = get_field("canale_1_original", "options");
	if(is_object($canale_1_original)){


	$args = array(
			'post_type' => 'original',
			"posts_per_page" => 4,
			'tax_query' => array(
					array(
							'taxonomy' => 'channel',
							'terms' => $canale_1_original->slug,
							'field' => 'slug'
					),
			),
	);
	$custom_query = new WP_Query($args);
	?>
	<section id="offset-articles" class="bg-green mb-30">
		<div class="site">

			<div class="container">
				<div class="row offset titolo-sezione-row">
					<div class="col-6">
						<h2 class="titolo-sezione"><?php echo $canale_1_original->name; ?></h2>
					</div>
					<div class="col-6 cta-titolo-row">
						<a href="<?php echo get_term_link($canale_1_original); ?>" class="cta-utils" title="scopri Tutti gli articoli" aria-label="scopri Tutti gli articoli">
							Scopri tutti gli articoli
						</a>
					</div>
				</div>

				<div class="row offset">
					<?php
					$counter = 0;
					while ( $custom_query->have_posts() ) {
					$custom_query->the_post();
					$counter++;
					if($counter == 1){ ?>
					@include('components.partials.partial-card-original-large')
					<?php
					}
					}
					?>
				</div>
			</div>
			<div class="container gotocarousel">
				<div class="row offset">
					<?php
					$counter = 0;
					while ( $custom_query->have_posts() ) {
					$custom_query->the_post();
					$counter++;
					if($counter > 1){ ?>
					@include('components.partials.partial-card-original-small')
					<?php
					}
					}
					?>
				</div>
			</div>
		</div>
	</section>
	<?php
	}
	?>

		<?php
		$canale_2_original = get_field("canale_2_original", "options");
		if(is_object($canale_2_original)){


	$args = array(
			'post_type' => 'original',
			"posts_per_page" => 3,
			'tax_query' => array(
					array(
							'taxonomy' => 'channel',
							'terms' => $canale_2_original->slug,
							'field' => 'slug'
					),
			),
	);
	$custom_query = new WP_Query($args);

	?>
	<div class="site">
		<section id="offset-articles2">
			<div class="container">
				<div class="row offset titolo-sezione-row titolo-sezione-row-white">
					<div class="col-6">
						<h2 class="titolo-sezione"><?php echo $canale_2_original->name; ?></h2>
					</div>
					<div class="col-6 cta-titolo-row">
						<a href="<?php echo get_term_link($canale_2_original); ?>" class="cta-utils" title="scopri Tutti gli articoli" aria-label="scopri Tutti gli articoli">
							Scopri tutti gli articoli
						</a>
					</div>
				</div>
				<div class="row offset">
					<?php
					$counter = 0;
					while ( $custom_query->have_posts() ) {
					$custom_query->the_post();
					$counter++;
					if($counter > 1){ ?>
					@include('components.partials.partial-card-original-large')
					<?php
					}
					}
					?>
				</div>
			</div>
		</section>

	</div>
	<?php
	}
	?>


	<?php
		$canale_3_original = get_field("canale_3_original", "options");
		if(is_object($canale_3_original)){


	$args = array(
			'post_type' => 'original',
			"posts_per_page" => 4,
			'tax_query' => array(
					array(
							'taxonomy' => 'channel',
							'terms' => $canale_3_original->slug,
							'field' => 'slug'
					),
			),
	);
	$custom_query = new WP_Query($args);
	?>
	<section id="offset-articles" class="bg-green">
		<div class="site">

			<div class="container">
				<div class="row offset titolo-sezione-row">
					<div class="col-6">
						<h2 class="titolo-sezione"><?php echo $canale_3_original->name; ?></h2>
					</div>
					<div class="col-6 cta-titolo-row">
						<a href="<?php echo get_term_link($canale_3_original); ?>" class="cta-utils" title="scopri Tutti gli articoli" aria-label="scopri Tutti gli articoli">
							Scopri tutti gli articoli
						</a>
					</div>
				</div>

				<div class="row offset">
					<?php
					$counter = 0;
					while ( $custom_query->have_posts() ) {
						$custom_query->the_post();
						$counter++;
						if($counter == 1){ ?>
						@include('components.partials.partial-card-original-large')
						<?php
						}
					}
					?>
				</div>
			</div>
			<div class="container gotocarousel">
				<div class="row offset">
					<?php
					$counter = 0;
					while ( $custom_query->have_posts() ) {
					$custom_query->the_post();
					$counter++;
					if($counter > 1){ ?>
					@include('components.partials.partial-card-original-small')
					<?php
					}
					}
					?>
				</div>
			</div>
		</div>
	</section>
	<?php
	}
	?>

@endsection
