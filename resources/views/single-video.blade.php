@extends('base')
@section('content')

	<div class="single single-gallery">
		<div class="site">

			@include('components.partials.widget-stories')

			@asset('css/single-video.min.css')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
				@while (have_posts()) @php the_post() @endphp
				<div class="container">
					@include('components.partials.partial-breadcrumb')
					<div class="single__title flex">
						@if($related_trend)
							<a class="post__story"
							   href="{!! $related_trend['link'] !!}">{!! $related_trend['name'] !!}</a>@endif
						<h1>{!! the_title() !!}</h1>
					</div>
					<div class="section sticky-parent nomargin">
						<main class="main-content main-content--left">
							<section>
								<style>
									@media screen and (max-width: 736px) {
										.single-video__container {
											margin-left: -16px;
											width: calc(100% + 32px);
										}
									}

								</style>
								<div class="single-video__container">
									@include('components.partials.partial-video-player')
								</div>

								<div class="single__hero-footer__datas">
									{!! lifegate_snippet_post_date() !!},&nbsp;<div class="post__author">{!! $single_post_authors !!}</div>
									@if($single_post_location)
										<span class="post__location">{{ $single_post_location }}</span>
									@endif

									@include('components.custom.custom-post-language-switcher')

									<div class="post__trends  flex no-border">
										@include('components.custom.custom-post-social-box')
									</div>
									@if($sponsor)
									<div class="single__hero-footer__box">
										{!! $sponsor !!}
									</div>
									@endif

								</div>
								<div class="post__content editorial">
								@if(get_post_field('post_content'))
										{!! the_content() !!}
								@elseif(get_the_excerpt())
										{!! wpautop(get_the_excerpt()) !!}
								@endif


								</div>

								@if($sponsor)
								<div class="single__hero-footer__datas">
									<div class="single__hero-footer__box">
										{!! $sponsor !!}
									</div>
								</div>
								@endif

								<div class="post__trends flex">
									@include('components.custom.custom-post-social-box')
								</div>
							</section>
						</main>
						<aside class="sidebar sidebar--right">
							@include('components.partials.partial-sticky-adv')
						</aside>
					</div>

					{!! tbm_get_the_banner( 'HEADOFPAGE_BTF','','',false,false ) !!}


					<div class="wrapper">
						<section class="lasts-from">
							@include('components.partials.title-section',array('text' => __('Video correlati','lifegate'),'textlink' => '','url' =>  $video_hp))
							<div class="section-2-column flex">
								<div class="col-6">
									{{-- query: posts.items.children[1]  --}}
									@include('components.sections.section-1-columns',array('blocks' => array(
										['template' => 'card-post_type-special','posts' => $single_video_relateds_special]
									)))
								</div>
								<div class="col-6">
									@include('components.sections.section-horizontal-iteration',['tbmposts' => $single_video_relateds,'query' => [
											'class' =>  'lasts-from',
											'colNumber' =>  '2',
											'gutter' =>  '40',
											'componentNumber' =>  '4',
											'type' =>  'posts',
											'component' =>  'partial-card-post-big--k2'
										]])
								</div>
							</div>
						</section>
					</div>

				</div>
				@endwhile
			</div>
		</div><!-- end site -->
	</div>
@endsection
