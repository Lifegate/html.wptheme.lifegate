@extends('base')
@section('content')
	<!--archive.twig page -->
	<div class="archive">
		<div class="site">

			@include('components.partials.widget-stories')

			@asset('css/archive.min.css')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
				<div class="container sticky-parent">
					@include('components.partials.partial-breadcrumb')
					<div class="single__title">
						<h1>{!! $tbm_custom_tax_title !!}</h1>
						@unless(is_paged() || (is_post_type_archive() && !is_post_type_archive("longform")))
							<div class="editorial">
								<p>{!! get_the_archive_description() !!}</p>
								@if($tbm_related_category)
									<p>{!! __('Trend correlati','lifegate') !!}: {!! $tbm_related_category !!}.</p>
								@endif
							</div>
						@endunless
					</div>
					<main class="archive main-content main-content--left">
						<div class="container-cycle-col-1">
							@if ( have_posts() )
								@while (have_posts()) @php the_post() @endphp
								@includeFirst(['components.partials.partial-card-'.get_post_type().'-list--k2','components.partials.partial-card-post-list--k2'])
								@endwhile
							@endif
						</div>
						@include('components.partials.partial-pagination')
					</main>

					<aside class="sidebar sidebar--right">
						@include('components.partials.partial-sticky-adv')
					</aside>
				</div>
			</div>
		</div>
	</div>
@endsection
