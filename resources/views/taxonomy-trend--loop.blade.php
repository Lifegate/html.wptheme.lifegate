@section('content')
	<!--archive.twig page -->
	<div class="archive">
		<div class="site">

			@include('components.partials.widget-stories')

			@asset('css/archive.min.css')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
				<div class="container sticky-parent">
					@include('components.partials.partial-breadcrumb')
					<div class="single__title">
						<h1>{!! get_the_archive_title() !!}</h1>
						@unless(is_paged())
							<p>{!! term_description() !!}</p>
						@endunless
					</div>
					<main class="archive main-content main-content--left">
						<!-- container-cycle.twig -->
						<div class="container-cycle-col-1">
							@if ( have_posts() )
								@while (have_posts()) @php the_post() @endphp
								@includeFirst(['components.partials.partial-card-'.get_post_type().'-list--k2','components.partials.partial-card-post-list--k2'])
								@endwhile
							@endif
						</div>
						@if($navigator_data['is_paginable'])
							@include('components.partials.partial-pagination')
						@endif
					</main>
					<aside class="sidebar sidebar--right">
						@include('components.partials.partial-sticky-adv')
					</aside>
				</div>
			</div>
		</div>
	</div>
@endsection
