@extends('base')
@section('content')

	<!--single-evento.twig page -->
	<div class="single single-evento">
		<div class="site">

			@include('components.partials.widget-stories')

			@asset('css/single-evento.min.css')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
				@while (have_posts()) @php the_post() @endphp
				<div class="container">
					@include('components.partials.partial-breadcrumb')
					<div class="single__title flex">
						<h1>{!! the_title() !!}</h1>
					</div>
					<div class="single__hero">
						@include('components.partials.card-evento-big')
					</div>

					<div class="section sticky-parent" @if(!$gmaps) style="margin-top: 40px;" @endif>

						<main class="main-content main-content--left">
							<section>
								@if($gmaps)
									@include('components.partials.partial-google-map')
								@endif
								<div class="post__trends flex">
									@if($related_trend)
										<div>
											<a class="post__story"
											   href="{!! $related_trend['link'] !!}">{!! $related_trend['name'] !!}</a>
										</div>
									@endif
									@include('components.custom.custom-post-social-box')
									@include('components.custom.custom-post-language-switcher')
								</div>

								<div class="table table--event">
									{!! $event_section !!}
								</div>
								<div class="post__content editorial">
									{!! the_content() !!}
								</div>

								<div class="post__trends flex">
									@include('components.custom.custom-post-social-box')
								</div>
								<div class="section">
									@include('components.custom.custom-post-footer')
								</div>
							</section>
						</main>
						<aside class="sidebar sidebar--right">
							@include('components.partials.partial-sticky-adv')
						</aside>
					</div>

					{!! tbm_get_the_banner( 'HEADOFPAGE_BTF','','',false,false ) !!}

					<div class="wrapper">
						@php dynamic_sidebar( 'post_footer' ); @endphp
						<section class="lasts-from">
							@include('components.partials.title-section',array('text' => __('Articoli correlati','lifegate'),'textlink' => '','url' =>  'http://www.lifegate.it'))
							<div class="section-2-column flex">
								<div class="col-6">
									{{-- query: posts.items.children[1]  --}}
									@include('components.sections.section-1-columns',array('blocks' => array(
										['template' => 'card-post_type-special','posts' => $single_post_relateds_special]
									)))
								</div>
								<div class="col-6">
									@include('components.sections.section-horizontal-iteration',['tbmposts' => $single_post_relateds,'query' => [
											'class' =>  'lasts-from',
											'colNumber' =>  '2',
											'gutter' =>  '40',
											'componentNumber' =>  '4',
											'type' =>  'posts',
											'component' =>  'partial-card-post-big--k2'
										]])
								</div>
							</div>
						</section>
					</div>


				</div>
				@endwhile
			</div>
		</div> <!-- end site -->

	</div>
@endsection
