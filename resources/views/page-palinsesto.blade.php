{{-- /* Template Name: Palinsesto */ --}}
@extends('base')
@section('content')

	<!--page-palinsesto.twig page -->

	<div class="page page-palinsesto">
		<div class="site">
			@asset('css/page-palinsesto.min.css')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
				<div class="container">
					<div class="page-title">
						<h1>Palinsesto</h1>
					</div>
					<div class="page-palinsesto__spymenu sticky-element" data-sticky-offset-top="60">
						<div class="wrapper">
							<div class="container">
								<div class="spy-menu spy-menu-js">
									@asset('css/components/partials/spy-menu.min.css')
									<ul role="nav">
										@if(isset($show_times[1]))
											<li><a href="#" class="spy-menu__item" data-anchor-offset="170"
												   data-anchor="#paragraph-1">Lunedì</a></li>
										@endif
										@if(isset($show_times[2]))
											<li><a href="#" class="spy-menu__item" data-anchor-offset="170"
												   data-anchor="#paragraph-2">Martedì</a></li>
										@endif
										@if(isset($show_times[3]))
											<li><a href="#" class="spy-menu__item" data-anchor-offset="170"
												   data-anchor="#paragraph-3">Mercoledì</a></li>
										@endif
										@if(isset($show_times[4]))
											<li><a href="#" class="spy-menu__item" data-anchor-offset="170"
												   data-anchor="#paragraph-4">Giovedì</a></li>
										@endif
										@if(isset($show_times[5]))
											<li><a href="#" class="spy-menu__item" data-anchor-offset="170"
												   data-anchor="#paragraph-5">Venerdì</a></li>
										@endif
										@if(isset($show_times[6]))
											<li><a href="#" class="spy-menu__item" data-anchor-offset="170"
												   data-anchor="#paragraph-6">Sabato</a></li>
										@endif
										@if(isset($show_times[7]))
											<li><a href="#" class="spy-menu__item" data-anchor-offset="170"
												   data-anchor="#paragraph-7">Domenica</a></li>
										@endif
									</ul>
									<span class="spy-menu__marker"></span>
								</div>
							</div>
						</div>
					</div>
					<div class="section sticky-parent">

						<main class="main-content main-content--left">

							<section>
								@for ($i = 1; $i <= 7; $i++)
									@if(isset($show_times[$i]))
										<div id="paragraph-{!! $i !!}" class="spyparagraph"
											 data-menu-item="spy-menu__item-{!! $i !!}">
											@include('components.partials.title-section',array('text' => ucfirst( lg_get_the_day($i)),'textlink' =>  null,'url' =>  null,'css' =>  true))
											@foreach($show_times[$i] as $show_time)
												<article class="card-radio-list">
													@asset('css/components/partials/partial-card-radio-list.min.css')
													<a href="{!! $show_time['permalink'] !!}">
														<div>
															<span class="card__hour">{!! $show_time['hour'] !!}</span>
															<div class="card__title">
																<h3>{!! $show_time['title'] !!}</h3>
															</div>
														</div>
														<span class="title-link"></span>
													</a>
												</article>
											@endforeach
										</div>
									@endif
								@endfor
							</section>
						</main>
						<aside class="sidebar sidebar--right">

							<div class="partial-sticky-adv">
								@asset('css/components/partials/partial-sticky-adv.min.css')
								<div class="sticky-element" data-sticky-offset-top="134"
									 data-sticky-mediaquery="desktop">
									<div class="sticky-adv">
										<!-- banner here -->
									</div>
								</div>
							</div>

						</aside>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
