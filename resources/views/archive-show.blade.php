@extends('base')

@section('content')
	<style>
		.custom-slider .swiper-wrapper {
			min-height: 540px;
		}

		.swiper-slide {
			max-height: 80vw;
		}

		@media screen and (max-width: 736px) {
			.custom-slider .swiper-wrapper {
				min-height: 320px;
			}
		}
	</style>
	<!--page-programmi.twig page -->

	<div class="page page-programmi">
		<div class="site">

			@include('components.partials.widget-stories')

			@asset('css/page-programmi.min.css')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.partial-breadcrumb')
					<div class="page-title">
						<h1>{!! __('Podcast','lifegate') !!}</h1>
					</div>
				</div>
			</div>
			<section class="featured-slider slider" data-slider-effect="coverflow" data-slider-loop="true"
					 data-slider-slidesperview="auto" data-slider-space="0" data-slider-centered="true">
				@include('components.sections.custom-slider',array('blocks' => array(
						['template' => 'card-radio-slider','posts' => $all_shows]
					)))
			</section>
		</div>
	</div>
@endsection
