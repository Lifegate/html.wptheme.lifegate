@extends('base')
@section('content')
	<!--single-programma.twig page -->
	<div class="single single-programma">
		<div class="site">

			@include('components.partials.widget-stories')

			@asset('css/single-programma.min.css')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
				@while (have_posts()) @php the_post() @endphp
					<div class="container">
					@include('components.partials.partial-breadcrumb')
					<div class="single__title">
						<h1>{!! the_title() !!}</h1>
					</div>
					<div class="post__description editorial">
						<p>{!! the_excerpt() !!}</p>
					</div>
					@if($lg_show_spotify_id)
						<section>
							<div class="single-program__container">
								<iframe src="https://open.spotify.com/embed/playlist/{!! $lg_show_spotify_id !!}"
										width="100%" height="400px" frameborder="0" allowtransparency="true"
										allow="encrypted-media"></iframe>
							</div>
						</section>
					@elseif($lg_show_spreaker_id)
						<section>
							<div class="single-program__container">
								<iframe src="https://widget.spreaker.com/player?show_id={!! $lg_show_spreaker_id !!}&amp;theme=light&amp;autoplay=false&amp;playlist=show&amp;cover_image_url={{$lg_show_image}}"
										width="100%" height="400px" frameborder="0"></iframe>
							</div>
						</section>
					@endif
					<div class="section sticky-parent">

						<main class="main-content main-content--left">
							<section>

								<div class="post__trends flex">
									@include('components.custom.custom-post-social-box')
								</div>
								<div class="post__content editorial">
									{!! the_content() !!}
								</div>

								<div class="post__trends flex">
									@include('components.custom.custom-post-social-box')
								</div>
							</section>
						</main>
						<aside class="sidebar sidebar--right">
							@include('components.partials.partial-sticky-adv')
							<div>
								@include('components.partials.title-section',array('text' => 'Cerca un titolo','textlink' =>  null,'url' =>  null))
								@include('components.partials.partial-form-title-song')
							</div>
						</aside>
					</div>


					<div class="wrapper">
						@php dynamic_sidebar( 'post_footer' ); @endphp
						<section class="others">
							@include('components.partials.title-section',array('text' => __('Altri programmi e podcast','lifegate'),'textlink' => 'Tutti i podcast','url' =>  $show_archive))
							<div class="section-2-column flex">
								<div class="col-5">
									@include('components.sections.section-1-columns',array('blocks' => array(
										['template' => 'partial-card-radio-medium','posts' => $single_post_relateds_special]
									)))
								</div>
								<div class="col-7">
									@include('components.sections.container-cycle',array(
										'space' => 'col',
										'componentNumber' =>  '4',
										'colNumber' => '1',
										'blocks' => array( [
										'template' => 'partial-card-radio-list',
										'posts' => $single_post_relateds,
									])))
								</div>
							</div>
						</section>
					</div>
				</div>
				@endwhile
			</div>
		</div> <!-- end site -->
	</div>
@endsection
