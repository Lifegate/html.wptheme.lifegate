<!doctype html>
<html {!! get_language_attributes() !!} class="no-js">
<head>
	{{-- Head --}}
	@include('components.partials.partial-head')
	{{-- HEAD --}}
</head>
<body>
<!-- base.twig -->
<div class="base base-modal sticky-kit">
	<div class="wrapper">
		<div class="container">

			{{-- Block content --}}
			@yield('content')

		</div>
	</div>
	@include('components.sections.footer')
</div>
</div>@php wp_footer() @endphp

</body>
</html>
