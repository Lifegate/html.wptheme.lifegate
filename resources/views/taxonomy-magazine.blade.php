@extends('base')
@section('content')
	<div class="page page-speciale">
		@asset('css/page-speciale.min.css')
		<div class="site">

			@include('components.partials.widget-stories')

			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<section class="featured-section">
				<div class="wrapper">
					<div class="container">
						<div class="single__title flex">
							<span class="post__story">Magazine</span>
							<h1>{!! the_archive_title() !!}</h1>
						</div>
						<div class="single__subtitle post__description">
							{!! term_description() !!}
						</div>
					</div>
				</div>
				@unless(is_paged())
					<div class="large-block">
						<div class="container">
							@include('components.partials.hero-speciale')
						</div>
					</div>
				@endunless
			</section>
			<div class="wrapper">
				<div class="container">
					{{-- Content --}}
					@unless(is_paged())
						@if($tbm_magazine_enrichment)
							<div class="section sticky-parent">
								<main class="main-content main-content--left">
									<section>
										<div class="post__content editorial">
											{!! $tbm_magazine_enrichment !!}
										</div>
									</section>
								</main>
								<aside class="sidebar sidebar--right">
									@include('components.partials.partial-sticky-adv')
								</aside>
							</div>
						@endif
					@endunless

					{{-- Featured post --}}
					@unless(is_paged())
						<section class="trends-evidence">
							@include('components.sections.section-2-columns',array('blocks' => array(
								['template' => 'card-post-special','posts' => $featured_posts,'post_number' => 2]
							)))
						</section>
					@endunless

					<div class="section sticky-parent">
						<aside class="sidebar sidebar--left">
							@include('components.partials.partial-sticky-adv')
						</aside>

						<main class="main-content main-content--right">
							<section class="trend-archive">
								@while(have_posts()) @php the_post(); @endphp
								@include('components.partials.partial-card-post-list--k2')
								@endwhile
							</section>
							@include('components.partials.partial-pagination')
						</main>

					</div>
				</div>
			</div>

		</div>
	</div>
@endsection
