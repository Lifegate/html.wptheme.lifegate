@extends('base-mediacase')
@section('content')

	<!--single-post.twig page -->
	<div class="single single-original">
		<div class="site">
				@asset('css/single-original.min.css')
			<div class="wrapper wrapper-menu">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>

			<div class="wrapper wrapper-content">
				@while (have_posts()) @php the_post() @endphp
				<div class="container">
					@include('components.partials.partial-breadcrumb')
					@if($mediacase_next_to_the_title)
						<div class="single__title flex">
						{!! $mediacase_next_to_the_title !!}
						</div>
					@endif
					<div class="single__title flex">
						<h1>{!! the_title() !!}</h1>
					</div>

					@if($post_thumb_id && !mediacase_video_url(get_the_ID()))
						<section style="margin:0px;">
							<div class="single__featured-image" role="figure" aria-labelledby="featured-image-caption">
								<picture>
									<img srcset="{!! tbm_wp_get_attachment_image_url($post_thumb_id,array(1200,600)) !!}, {!! tbm_wp_get_attachment_image_url($post_thumb_id,array(2400,1200)) !!} 2x"
										 alt="{!! the_title() !!}"/>
								</picture>
							</div>
						</section>
					@endif


					<div class="section" >
						<main class="main-content-original">
							<section>
								<div class="post__content editorial post__content__original">
									{!! mediacase_video_url(get_the_ID()) !!}


									{!! the_content() !!}
								</div>
							</section>
						</main>
					</div>


					<div class="wrapper">

							<section class="lasts-from">
							@include('components.partials.title-section',array('text' => __('Leggi anche','lifegate'),'textlink' => '','url' =>  $mediacase_hp))

							@include('components.sections.section-horizontal-iteration',['tbmposts' => $single_mediacase_relateds,'query' => [
									'class' =>  'lasts-from',
									'colNumber' =>  '3',
									'gutter' =>  '40',
									'componentNumber' =>  '4',
									'type' =>  'posts',
									'component' =>  'partial-card-mediacase'
								]])

							</section>
					</div>
				</div>
				@endwhile
			</div>
		</div> <!-- end site -->
	</div>
@endsection
