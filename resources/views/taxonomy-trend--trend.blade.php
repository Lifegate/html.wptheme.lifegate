@section('content')
	<div class="single single-trend">
		<div class="site">

			@include('components.partials.widget-stories')

			@asset('css/archive-trend.min.css')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.partial-breadcrumb')
				</div>
			</div>

		@unless(is_paged())
				<section class="featured-section">
					<div class="large-block">
						<div class="container">
							@include('components.partials.hero-trend')
						</div>
					</div>
				</section>
			@endunless
			<div class="wrapper">
				<div class="container">

					{{-- Content --}}
					@unless(is_paged())
						@if($content)
							<div class="section sticky-parent">
								<main class="main-content main-content--left">
									<section>
										<div class="post__content editorial">
											{!! $content !!}
										</div>
									</section>
								</main>
								<aside class="sidebar sidebar--right">
									@include('components.partials.partial-sticky-adv')
								</aside>
							</div>
						@endif
					@endunless

					{{-- Featured post --}}
					@unless(is_paged())
						<section class="trends-evidence">
							@include('components.sections.section-2-columns',array('blocks' => array(
								['template' => 'card-post-special','posts' => $featured_posts,'post_number' => 2]
							)))
						</section>
					@endunless

					<div class="section sticky-parent">
						<aside class="sidebar sidebar--left">
							@include('components.partials.partial-sticky-adv')
						</aside>

						<main class="main-content main-content--right">
							<section class="trend-archive">
								@while(have_posts()) @php the_post(); @endphp
								@include('components.partials.partial-card-post-list--k2')
								@endwhile
							</section>
							@include('components.partials.partial-pagination')
						</main>

					</div>
				</div>
			</div>

		</div>
	</div>
@endsection
