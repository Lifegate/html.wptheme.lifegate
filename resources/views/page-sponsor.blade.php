{{-- /* Template Name: Pagina Sponsor  */ --}}
@extends('base')
@section('content')
	<style>
		.custom-slider .swiper-container {
			min-height: 540px;
		}

		.custom-slider .swiper-slide {
			display: none;
			min-width: 460px;
		}
	</style>
	<!--archive-iniziative.twig page -->
	<div class="archive archive-iniziative">
		<div class="site">
			@asset('css/archive-iniziative.min.css')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
				<div class="container">
					<div class="page-title">
						<h1>{!! get_the_title() !!}</h1>
					</div>
				</div>
			</div>
			<section class="featured-slider slider" data-slider-effect="coverflow" data-slider-slidesperview="auto"
					 data-slider-loop="true" data-slider-space="0" data-slider-centered="true">
				<div class="custom-slider custom-slider-js">
					<div class="swiper-container">
						<div class="swiper-wrapper">
							@foreach($last_partners as $term)
								<div class="swiper-slide">
									<!-- card-iniziativa-big.twig partial -->
									<article class="card-iniziativa-big">
										@asset('css/components/partials/card-iniziativa-big.min.css')
										<a href="{!! get_term_link($term) !!}" title="{!! $term->name !!}">
											<div class="card__thumbnail--big">
												<picture>
													<img class="lazyload"
														 data-srcset="{!! tbm_wp_get_attachment_image_url(\App\Controllers\PagePartner::get_term_image($term),array(460,460)) !!}, {!! tbm_wp_get_attachment_image_url(\App\Controllers\PagePartner::get_term_image($term),array(920,920)) !!} 2x"
														 alt="{!! $term->name !!}"/>
												</picture>
											</div>
											<div class="card__overlay">
												<div class="card__title">
													<h3>{!! $term->name !!}</h3>
												</div>
												<div class="card__content">
													<p class="abstract">{!! wp_strip_all_tags(wp_trim_words(term_description($term->term_id),10))  !!}</p>
												</div>
											</div>
										</a>
									</article>
								</div>
							@endforeach
						</div>
						<!-- Add Pagination -->
					</div>
					<div class="swiper-pagination"></div>
					<nav class="custom-slider__nav">
						<ul>
							<li><span role="button" aria-label="{!! __('successiva','lifegate') !!}"
									  title="{!! __('successiva','lifegate') !!}"
									  class="button button--next"></span></li>
							<li><span role="button" aria-label="{!! __('precedente','lifegate') !!}"
									  title="{!! __('precedente','lifegate') !!}"
									  class="button button--prev"></span></li>
						</ul>
					</nav>
				</div>
			</section>
		</div>
	</div>
@endsection
