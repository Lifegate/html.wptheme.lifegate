{{-- /* Template Name: Survey Embed */ --}}
@extends('base')
@section('content')
	@asset('css/page-chisiamo.min.css')
	<style>
		.page-chisiamo .chisiamo__content .chisiamo__people {
			margin: -100vh auto 0;
		}
		@media only screen and (min-width: 1200px) {
			.page-chisiamo .chisiamo__content .chisiamo__people {
				margin: -120vh auto 0 !important;
			}
		}

		.editorial img{
			max-width:100% !important;
			width:400px !important;
			min-width:auto;
			margin:auto;
			margin-top:40px;
		}

		.single__title h1{
			color:#000 !important;
			text-shadow: 0px 0px !important; !important;
			font-size:70px;
		}

		.single__title h1 span{
			font-size:26px;
			font-family:Nunito regular;
			color:#000;
		}
			.chisiamo__content .wrapper{
				background: rgb(169,196,143);
				background: linear-gradient(90deg, rgba(169,196,143,1) 0%, rgba(208,221,195,1) 100%);
				padding-bottom:0px !important;
				padding-top:0px !important;
			}
		.chisiamo__content .wrapper p{
			font-family:Nunito regular;
		}

		.editorial img.size-full{
			width: auto !important;
			margin-top:10px;
			margin-bottom:20px;
		}


	</style>
	<!--page-chisiamo.twig page -->
	<div class="page page-chisiamo">
		<section class="longform-hero">
			<div class="longform-hero__container">
				<div class="longform-hero__container__bg">
					<picture>
						<!--[if IE 9]>
						<video style="display: none;"><![endif]-->
						<source class="lazyload"
								data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,320)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1280,640)) !!} 2x"
								media="(max-width: 736px)"/>
						<!--[if IE 9]></video><![endif]-->
						<img class="lazyload"
							 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,320)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1280,640)) !!} 2x"
							 alt="{!! the_title() !!}"/>
					</picture>
					<noscript>
						<picture>
							<!--[if IE 9]>
							<video style="display: none;"><![endif]-->
							<source class="lazyload"
									data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,320)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1280,640)) !!} 2x"
									media="(max-width: 736px)"/>
							<!--[if IE 9]></video><![endif]-->
							<img class="lazyload"
								 data-src="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,320)) !!}"
								 alt="{!! the_title() !!}"/>
						</picture>
					</noscript>
				</div>
				<!-- <div class="longform-hero__container__body">

				</div> -->
			</div>
		</section>


		<section class="chisiamo__content">
			<div class="chisiamo__people">
				<div class="single__title single__title--longform">
					<div>
						<h1>{!! get_the_title() !!}</h1>
					</div>
				</div>

			</div>

			<div class="wrapper">
				<div class="container mobile-switch-container">
					<div class="col-12 editorial">
						{!! the_content() !!}
					</div>
				</div>
			</div>
		</section>
	</div>

@endsection

