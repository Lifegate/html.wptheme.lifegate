{{-- /* Template Name: Pagina 3BMeteo  */ --}}
<?php

?>
@extends('base')

@section('head')
	{!! $adk_domination !!}
@endsection

@section('content')
	@asset('css/page-html.min.css')
	@if ( have_posts() )
		@while (have_posts()) @php the_post() @endphp
		<div class="site page page-html">
			@include('components.partials.widget-stories')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper" id="dynamicdata" style="display: none;">
				<section class="page-hero-none">
					<div class="page-hero__container-none container">
						<div class="page-hero__container__body-none editorial">
							@include('components.partials.partial-breadcrumb')
							<h1>{!! the_title() !!}</h1>

							{!! the_content() !!}

							<script>var trebpath = "@asset('images/3bimg/')";</script>

							<div>
								<div class="row" >
									<span class="divider"></span>

									<div class="col-12">
										<div id="trebmeteo" class="trebmeteoinpage">
											<div class="row trebcontainer">
												@asset('css/components/partials/3bmeteo.min.css')
												<div class="col-4">
													<img id="trebimg">
												</div>
												<div class="col-8">
													<span id="treblocation" class="trebtitle"></span>
													<p id="trebline"></p>
												</div>
											</div>

										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12">
										<h2>Oggi</h2>
										<table id="tableoggi"></table>
										<h2>Domani</h2>
										<table id="tabledomani"></table>
										<div id="data"></div>
									</div>
								</div>
								<script>
									var trebshowed = false;

									if (navigator.geolocation) {
										var options = {enableHighAccuracy: false,timeout: 10000,maximumAge: 0,desiredAccuracy: 0, frequency: 1 };
										watchID = navigator.geolocation.watchPosition(geo_onSuccess, geo_onError, options);
									}

									function geo_onSuccess(position){
										if(!trebshowed) {
											jQuery.ajax({
												type: "GET",
												url: window.tbm.ajaxurl,
												data: {
													action: "trebmeteo",
													lat: position.coords.latitude,
													long: position.coords.longitude,
												},
												success: function (data) {
													if (data.status == "success") {
														jQuery("#dynamicdata").show();

														var obj = jQuery.parseJSON(data.data);
														jQuery("#trebline").html("Qualità dell'aria: <spam id=\"trebquality\" class=\"treb" + obj.localita.previsione_giorno[0].qualita_aria + "\">" + obj.localita.previsione_giorno[0].qualita_aria + "</spam>");
														jQuery("#treblocation").html(obj.localita.localita);
														jQuery("#trebimg").attr("src", trebpath + obj.localita.previsione_giorno[0].qualita_aria + ".png");

														tableGenerator('#tableoggi', obj.localita.previsione_giorno[0].inquinanti);
														tableGenerator('#tabledomani', obj.localita.previsione_giorno[0].inquinanti);

														trebshowed = true;
													}else{
														console.log("3bmeteo fail request");
														jQuery("#dynamicdata").hide();
													}
												},
												error: function (error) {
													// Errore chiamata
													console.log(error);
													jQuery("#dynamicdata").hide();
												}
											});
										}
									}


									function geo_onError(error){
										console.log(error);
										jQuery("#dynamicdata").hide();
									}



									function tableGenerator(selector, data) { // data is an array
										var keys = Object.keys(Object.assign({}, ...data));// Get the keys to make the header
										// Add header
										var head = '<thead><tr>';
										keys.forEach(function(key) {
											var printkey = key.replace("_", " ");
											head += '<th>'+printkey+'</th>';
										});
										$(selector).append(head+'</tr></thead>');
										// Add body
										var body = '<tbody>';
										data.forEach(function(obj) { // For each row
											var row = '<tr>';
											keys.forEach(function(key) { // For each column
												row += '<td style="text-align:center">';
												if (obj.hasOwnProperty(key)) { // If the obj doesnt has a certain key, add a blank space.
													if(obj[key] != null)
														row += obj[key];
													else
														row += "n/a"
												}
												row += '</td>';
											});
											body += row+'<tr>';
										})
										$(selector).append(body+'</tbody>');
									}

								</script>
							</div>
						</div>
				</section>
			</div>

			<div class="wrapper">
				<div class="container">

				<section class="lasts-from">
					@include('components.partials.title-section',array('text' => __('Qualità dell\'aria','lifegate'),'textlink' => '','url' =>  ''))
					<div class="section-2-column flex">
						<div class="col-6">
							<?php
							$related = new WP_Query(array(
									'posts_per_page' => 3,
									'tax_query' => array(
											array(
													'taxonomy' => 'post_tag',
													'field' => 'slug',
													'terms' => "qualita-dellaria",
											)
									)
							));
							wp_reset_postdata();
							?>
							@include('components.sections.section-1-columns',array('blocks' => array(
								['template' => 'card-post_type-special','posts' => $related]
							)))
						</div>
						<div class="col-6">
							<?php
							$relatedsponsor = new WP_Query(array(
									'posts_per_page' => 6,
									'tax_query' => array(
											array(
													'taxonomy' => 'category',
													'field' => 'slug',
													'terms' => "ambiente",
											)
									)
							));
							wp_reset_postdata();
							?>
							@include('components.sections.section-horizontal-iteration',['tbmposts' => $relatedsponsor,'query' => [
									'class' =>  'lasts-from',
									'colNumber' =>  '2',
									'gutter' =>  '40',
									'componentNumber' =>  '4',
									'type' =>  'posts',
									'component' =>  'partial-card-post-big--k2'
								]])
						</div>
					</div>
				</section>
				</div>
			</div>
		</div>
		@endwhile
	@endif
@endsection
