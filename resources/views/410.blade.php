@extends('base')
@section('content')
	<div class="page page-404 page-410">
		@asset('css/page-404.min.css')
		@asset('css/page-newsletter.min.css')
		<section class="page-hero">
			<div class="page-hero__container">
				<div class="page-hero__container__body">
					<h1>{!! __('Ops... qualcosa è andato storto!','lifegate') !!}</h1>
					<p>{!! __('La pagina che stavi cercando non esiste.','lifegate') !!}</br>{!! __('Usa il nostro motore di ricerca.','lifegate') !!}</p>
					<div class="search__field">
						<form method="get" action="{!!  home_url( '/' ) !!}" role="search"
							  style="display: flex;width: 80%;margin: 24px auto 0;">
							<input id="mainsearch" type="text" name="s" value="{{get_search_query()}}"
								   autocomplete="off"
								   placeholder="{!! __('Cerca','lifegate') !!}" required/>
							<button type="submit"
									class="search__field__submit cta cta--icon cta--icon-left cta--enter"></button>
						</form>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection
