@extends('base')

@section('content')
	@if(is_paged())
		<div class="archive">
			<div class="site">

				@include('components.partials.widget-stories')

				@asset('css/archive.min.css')
				<div class="wrapper">
					<div class="container">
						@include('components.partials.main-menu')
					</div>
				</div>
				@include('components.sections.trends')
				<div class="wrapper">
					<div class="container sticky-parent">
						<div class="single__title">
							<h1>{!! get_the_archive_title() !!}</h1>
						</div>
						<main class="archive main-content main-content--left">
							<div class="container-cycle-col-1">
								@if ( have_posts() )
									@while (have_posts()) @php the_post() @endphp
									@includeFirst(['components.partials.partial-card-'.get_post_type().'-list--k2','components.partials.partial-card-post-list--k2'])
									@endwhile
								@endif
							</div>
							@include('components.partials.partial-pagination')
						</main>

						<aside class="sidebar sidebar--right">
							@include('components.partials.partial-sticky-adv')
						</aside>
					</div>
				</div>
			</div>
		</div>
	@else
		@asset('css/archive-video.min.css')
		@asset('css/archive.min.css')
		<!--archive-video.twig page -->
		<div class="archive archive-video">

			@include('components.sections.archive-video-hero')
			<div class="wrapper">
				<div class="container sticky-parent">

					<main class="video-archive main-content main-content--left">
						@include('components.partials.title-section',array('text' => 'Tutti i video','textlink' => null,'url' =>  null,'css' =>  true))
						@include('components.sections.section-horizontal-iteration',['tbmposts' => $all_videos,'query' => [
								'class' =>  'video-archive',
								'colNumber' =>  '2',
								'gutter' =>  '40',
								'componentNumber' =>  '8',
								'component' =>  'partial-card-video-big--k2'
							]])
						@include('components.partials.partial-pagination')
					</main>

					<aside class="sidebar sidebar--right">
						@include('components.partials.partial-sticky-adv')
					</aside>

				</div>
			</div>
		</div>
	@endif
@endsection
