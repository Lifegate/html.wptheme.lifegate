@extends('base')
@section('head')
	{!! $adk_domination !!}
@endsection
@section('content')
	@asset('css/page-html.min.css')
	@if ( have_posts() )
		@while (have_posts()) @php the_post() @endphp
		<div class="site page page-html">
			@include('components.partials.widget-stories')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
			<section class="page-hero-none">
				<div class="page-hero__container-none container">
					<div class="page-hero__container__body-none editorial">
						@include('components.partials.partial-breadcrumb')
						<h1>{!! the_title() !!}</h1>
						{!! the_content() !!}
					</div>
				</div>
			</section>
			</div>
		</div>
		@endwhile
	@endif
@endsection
