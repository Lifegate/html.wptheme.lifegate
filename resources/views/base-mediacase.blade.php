<!doctype html>
<html {!! get_language_attributes() !!} class="no-js">
<head>
	{{-- Head --}}
	@include('components.partials.partial-head')
</head>
<body class="sticky-parent original-body">

<!-- Facebook Pixel Code -->
<noscript><img height="1" width="1" style="display:none"
			   src="https://www.facebook.com/tr?id=1871994926361253&ev=PageView&noscript=1"
	/></noscript>
<!-- End Facebook Pixel Code -->

<!-- base.twig -->
<div class="base sticky-kit chosen-js main-lifegate-js">

	@include('components.partials.drawer-menu')
	@include('components.sections.header')

	{{-- Block content --}}
	@yield('content')

	@include('components.sections.footer')
</div>

@php wp_footer() @endphp

@php $ver = wp_get_theme()->get('Version'); @endphp

</body>
</html>
