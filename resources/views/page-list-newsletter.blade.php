{{-- /* Template Name: Pagina Lista Newsletter  */ --}}

@extends('base')
@section('content')
	@asset('css/page-html.min.css')
		<style>
		.container-archive-content{
			float:left;
			height:auto;
			margin-bottom:20px;
			padding-top:20px;
			border-top:solid 1px #ccc;
			padding-left:20px;
			padding-right:20px;
		width: 100%;
		}

.container-archive-title{
float:left;
padding-bottom:20px;
}
.container-archive-title .titolo-archivio-newsletter{
text-transform:uppercase;
color:#999;
font-size:22px;
font-family: Nunito, Arial, sans-serif;
}

.titolo-newsletter{
font-family: Nunito, Arial, sans-serif;
font-weight:bold;
}


.container-archive-title
.sottotitolo-archivio-newsletter{
font-weight:bold;
font-size:22px;
font-family: Nunito, Arial, sans-serif;
}


.container-archive-content .data{
 display: flex;
text-align:center;
  justify-content: center;
  align-items: center;
height:140px;
color:#666
}

@media screen and (max-width: 736px){

.container-archive-title .titolo-archivio-newsletter{

  padding-top:16px;

}

.container-archive-content .data{
height:auto;
padding-bottom:14px;
  justify-content: left;

}

}

		</style>
	@if ( have_posts() )
		@while (have_posts()) @php the_post() @endphp
		<div class="site page page-html">
			@include('components.partials.widget-stories')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			<div class="wrapper">

				<section class="page-hero-none">
					<div class="page-hero__container-none container">
						<div class="page-hero__container__body-none editorial">
							@include('components.partials.partial-breadcrumb')
							<?php
							$titolo_newsletter = get_field("titolo_newsletter");
							$immagine_quadrata = get_field("immagine_quadrata");
							$logo_newsletter = get_field("logo_newsletter");
							$sottotitolo_newsletter = get_field("sottotitolo_newsletter");
							$newsletters = get_field("newsletters");
							?>

						<div class="container-archive-title">
							<div class="col-2"> &nbsp; </div>
							<div class="col-3" ><img src="<?php echo  tbm_wp_get_attachment_image_url( $immagine_quadrata["id"], [ 500, 500 ] );  ?>"></div>
							<div class="col-7">
								<p class="titolo-archivio-newsletter"><?php echo ($titolo_newsletter); ?></p>
 								<p class="immagine-archivio-newsletter"><img src="<?php echo  tbm_wp_get_attachment_image_url( $logo_newsletter["id"], [720, 110] );  ?>"></p>
								<p class="sottotitolo-archivio-newsletter"><?php echo ($sottotitolo_newsletter); ?></p>
							</div>
						</div>

						</div>
					</div>
				</section>

		<?php
			foreach ($newsletters as $newsletter){
		?>
			<div class="container-archive-content">
				<div class="col-2 data"><?php echo $newsletter["data"]; ?></div>
				<div class="col-7 corpo-newsletter">
					<h3 class="titolo-newsletter"><a target="_blank" href="<?php echo $newsletter["link_dettaglio_newsletter"]; ?>"><?php echo $newsletter["titolo"]; ?></a></h3>
					<p class="testo-newsletter"><?php echo $newsletter["testo"]; ?></p>
				</div>
				<div class="col-3" ><a target="_blank" href="<?php echo $newsletter["link_dettaglio_newsletter"]; ?>"><img src="<?php echo  tbm_wp_get_attachment_image_url( $newsletter["immagine"]["id"], [ 500, 400 ] );  ?>"></a></div>
			</div>
		<?php
		}
		?>

		</div>

	</div>

		@endwhile
	@endif
@endsection
