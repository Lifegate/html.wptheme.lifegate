{{-- /* Template Name: Player in Pagina */ --}}
@extends('base-radio')
@section('content')
	@asset('css/page-html.min.css')
	<style>
		.lg_player{
			height:620px;
			width:100%;
			margin-top:20px !important;
		}
	</style>
	@if ( have_posts() )
		@while (have_posts()) @php the_post() @endphp
		<div class="site page page-html">
			@include('components.partials.widget-stories')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
			<section class="page-hero-none">
				<div class="page-hero__container-none container">
					<div class="page-hero__container__body-none editorial">
						@include('components.partials.partial-breadcrumb')
						<?php
						echo tbm_get_the_banner( 'BEFORE_TITLE','','',false,false );
						?>
						<h1>{!! the_title() !!}</h1>
						{!! the_content() !!}

						<div class="page-radioplayer">
							<iframe class="lg_player" src="https://play.xdevel.com/12650/?widthToOpenSidebar=979" frameborder="no" scrolling="0" allowfullscreen="allowfullscreen"></iframe>
						</div>
					</div>
				</div>
			</section>
			</div>
		</div>
		@endwhile
	@endif
@endsection
