{{-- /* Template Name: Come ascoltare */ --}}
@extends('base')
@section('content')
	@asset('css/page-comeascoltare.min.css')

	<div class="page page-comeascoltare sticky-parent">
		<div class="site">

			@include('components.partials.widget-stories')

			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
				<div class="container">
					<div class="single__title flex">
						<h1>{!! the_title() !!}</h1>
					</div>
				</div>
			</div>
		</div>
		<div class="page-comeascoltare__content">
			<div class="sticky-element lazyload bg-comeascoltare" data-sticky-offset-top="60">
				<picture>
					<!--[if IE 9]>
					<video style="display: none;"><![endif]-->
					<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,320)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1280,640)) !!} 2x"
							media="(max-width: 736px)"/>
					<!--[if IE 9]></video><![endif]-->
					<img class="lazyload"
						 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,320)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1280,640)) !!} 2x"
						 alt="{!! the_title() !!}"/>
				</picture>
				<noscript>
					<picture>
						<!--[if IE 9]>
						<video style="display: none;"><![endif]-->
						<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,320)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1280,640)) !!} 2x"
								media="(max-width: 736px)"/>
						<!--[if IE 9]></video><![endif]-->
						<img src="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,320)) !!}"
							 alt="{!! the_title() !!}"/>
					</picture>
				</noscript>

				<div class="longform-hero__overlay">
					<div class="polygon_1"></div>
					<div class="polygon_2"></div>
				</div>
			</div>
			<div class="site">
				<main class="main-content main-content--left">
					@if($comeascoltare_repeater)
						<section>
							<div class="page-comeascoltare__items">
								<ul>
									@foreach($comeascoltare_repeater as $block)
										<li>
											<div>
												<a href="{!! $block['url'] !!}" title="vai a...">
													<picture
															class="icon-radio icon-radio--{!! $block['icon'] !!}"></picture>
													<div>
														<p>{!! $block['text'] !!}</p>
														<div class="cta cta--icon cta--icon-left cta--enter"></div>
													</div>
												</a>
											</div>
										</li>
									@endforeach
								</ul>
							</div>
						</section>
					@endif
				</main>
				<aside class="sidebar sidebar--right">
					<div class="partial-sticky-adv sticky-element" data-sticky-offset-top="60"
						 data-sticky-mediaquery="desktop">
						@include('components.partials.partial-form-title-song')
					</div>
				</aside>
			</div>
		</div>
	</div>
@endsection
