{{-- /* Template Name: Iscrizione newsletter */ --}}
@extends('base')
@section('content')

	<!--page-richiestatitoli.twig page -->
	<div class="page page-newsletter">
		@asset('css/components/sections/longform-hero.min.css')
		@asset('css/page-newsletter.min.css')
		<section class="longform-hero">
			<div class="longform-hero__container">
				<div class="longform-hero__container__bg">
					<picture>
						<!--[if IE 9]>
						<video style="display: none;"><![endif]-->
						<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(414,812)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(828,1624)) !!} 2x"
								media="(max-width: 736px)"/>
						<!--[if IE 9]></video><![endif]-->
						<img class="lazyload"
							 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1920,1080)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(3840,1160)) !!} 2x"
							 alt="{!! the_title() !!}"/>
					</picture>
					<noscript>
						<picture>
							<!--[if IE 9]>
							<video style="display: none;"><![endif]-->
							<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,320)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1280,640)) !!} 2x"
									media="(max-width: 736px)"/>
							<!--[if IE 9]></video><![endif]-->
							<img src="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,320)) !!}"
								 alt="{!! the_title() !!}"/>
						</picture>
					</noscript>

					<div class="longform-hero__overlay">
						<div class="shadow-top"></div>
						<div class="polygon_1"></div>
						<div class="polygon_2"></div>
					</div>
				</div>
				<div class="longform-hero__container__body">
					<div class="single__title single__title--longform span-6">
						<div>
							<h1>{!! __('Iscriviti alla newsletter di LifeGate','lifegate') !!}</h1>
							<div class="single__subtitle single__subtitle--longform">
								<p>{!! __('Per ricevere informazioni sulle nostre pubblicazioni iscriviti alla newsletter di
									LifeGate.','lifegate') !!}</p>
							</div>
						</div>
					</div>
					<div class="span-6">
						<div class="page-newsletter__form">
							<div id="newsletter_div" class="newsletter__pass-a">
								<form id="newsletterform" name="newsletterform">
									<input type="hidden" name="action" value="newsletter_subscribe">
									<div class="newsletter__block">
										<div class="input-field">
											<label for="mail">Email</label>
											<input id="mail" type="email" name="n_email" class="tbm-email" value=""
												   autocomplete="off"
												   placeholder="{!! __('Il tuo indirizzo email','lifegate') !!}"
												   data-validation="email"
												   required="required"/>
										</div>
									</div>
									<div class="newsletter__block">
										<div class="newsletter__field flex flex--around">
											<button type="submit"
													class="cta cta--icon cta--solid cta--icon-right cta--send newsletter__tbmsubmit">{!! __('Iscriviti','lifegate') !!}
											</button>
										</div>
									</div>
								</form>
							</div>

							<!-- newsletter notice -->
							<div class="newsletter__response notice" style="color: black"></div>
						</div>
					</div>
				</div>
			</div>

		</section>
	</div>
@endsection
