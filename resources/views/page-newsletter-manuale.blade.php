{{-- /* Template Name: Iscrizione Newsletter Manuale */ --}}
@extends('base')
@section('content')

	<!--page-richiestatitoli.twig page -->
	<div class="page page-newsletter page-newsletter-manuale">
		@asset('css/components/sections/longform-hero.min.css')
		@asset('css/page-newsletter.min.css')
		<style>
			.page-newsletter-manuale #newsletter_div button[type=submit]{
				background-image: url(/app/themes/lifegate-2020/dist/images/icons.svg);
				background-color: #006454!important;
				background-position: right 13px top -1170px;
				width:240px;
				border: 1px solid transparent;
				color: #fff;
				padding: 0px 0px 14px 0px;
				text-transform: uppercase;
				font-size:18px;
				background-repeat: no-repeat;
				background-size: 34px 2700px;
				margin:auto;
				display: flex;
				justify-content: center;
				align-items: center;
			}

			.page-newsletter-manuale .page-newsletter__form{
				max-width:800px;
				margin:auto;
				padding-top:60px;
				padding-bottom:40px;

			}
			.page-newsletter-manuale p{
				color:#444;
			}
			.page-newsletter-manuale p, .page-newsletter-manuale p a{
				font-family:Nunito;
				font-size:16px;
				line-height:20px;
			}
			.page-newsletter-manuale p a{
				color:#006454;
				font-weight:bold;
			}



			.longform-hero__container__body{padding-bottom:20px !important;}


		</style>
		@if ( have_posts() )
			@while (have_posts()) @php the_post() @endphp
		<section class="longform-hero">
			<div class="longform-hero__container">
				<div class="longform-hero__container__bg">
					<picture>
						<!--[if IE 9]>
						<video style="display: none;"><![endif]-->
						<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(414,812)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(828,1624)) !!} 2x"
								media="(max-width: 736px)"/>
						<!--[if IE 9]></video><![endif]-->
						<img class="lazyload"
							 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1920,1080)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(3840,1160)) !!} 2x"
							 alt="{!! the_title() !!}"/>
					</picture>
					<noscript>
						<picture>
							<!--[if IE 9]>
							<video style="display: none;"><![endif]-->
							<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,320)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1280,640)) !!} 2x"
									media="(max-width: 736px)"/>
							<!--[if IE 9]></video><![endif]-->
							<img src="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,320)) !!}"
								 alt="{!! the_title() !!}"/>
						</picture>
					</noscript>

					<div class="longform-hero__overlay">
						<div class="shadow-top"></div>
						<div class="polygon_1"></div>
						<div class="polygon_2"></div>
					</div>
				</div>

				<div class="longform-hero__container__body">
					<div class="single__title single__title--longform span-6">
						<div>
							<h1>{!! the_title() !!}</h1>
							<div class="single__subtitle single__subtitle--longform">
								<p>{!! the_excerpt() !!}</p>
							</div>
						</div>
					</div>
					<div class="span-6">
						<div class="page-newsletter__form">
							<div id="newsletter_div" class="newsletter__pass-a">
								{!! the_content() !!}
							</div>
						</div>
					</div>
				</div>
				<?php
					if(get_field("titolo_blocco")){
				?>
				<div class="longform-hero__container__body" style="padding-top: 0px;">
					<div class="span-6">
						<div class="page-newsletter__form" style="padding: 0px;">
							<div class="footer">

								<div class="footer__newsletter">
									<div class="wrapper">
										<div class="container">
											<span class="footer_heading"><?php echo get_field("titolo_blocco"); ?></span>
											<span><?php echo get_field("testo_blocco"); ?></span>
											<div class="input-field">
												<a class="cta cta--solid cta--icon cta--icon-right cta--go" href="<?php echo get_field("link_blocco"); ?>">
													<?php echo get_field("cta_blocco"); ?>
												</a>
											</div>

										</div>
								</div>

							</div>
						</div>
					</div>
				</div>
					<?php
					}
					?>

			</div>

		</section>

<script>
	const urlParams = new URLSearchParams(window.location.search);
	const checked = urlParams.get('checked');
	if(checked){
		var checkboxes = document.querySelectorAll('input[type="checkbox"]');
		for (var i = 0; i < checkboxes.length; i++) {
			if(parseInt(i) == (parseInt(checked) + 1))
				checkboxes[i].checked = true;
		}
	}
	const forms = document.querySelectorAll("form");
	for (let form of forms) {
		form.addEventListener("submit", function(evt) {
			var checkboxes = document.querySelectorAll('input[type="checkbox"]');
			var checkedOne = Array.prototype.slice.call(checkboxes).some(x => x.checked);
			if(!checkedOne){
				evt.preventDefault();
				alert("Seleziona almeno una newsletter");
			}
		});
	}

</script>

			@endwhile
		@endif
	</div>
@endsection
