@extends('base')
@section('content')

	@include('components.partials.back-to-top')

	<div class="single single-longform" style="background: rgba(0, 0, 0, 0) none repeat scroll 0% 0%;">
		@while (have_posts()) @php the_post() @endphp
		@include('components.sections.longform-hero')
		@include('components.sections.readingtime')
		{!! the_content() !!}
		@endwhile
	</div>
@endsection
