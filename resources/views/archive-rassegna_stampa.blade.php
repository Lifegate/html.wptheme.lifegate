@extends('base')
@section('content')
	<!--archive-rassegnastampa.twig page -->
	<div class="archive archive-rassegnastampa">
		@asset('css/archive-rassegnastampa.min.css')
		<div class="site">

			@include('components.partials.widget-stories')

			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
				<div class="container">
					<div class="single__title">
						<h1>{!! post_type_archive_title() !!}</h1>
					</div>

					<section class="sticky-parent">

						<main class="main-content main-content--left">

							<!--div class="title-section title-section--post__century">
								<span class="bottom_line"></span>
								<span>2019</span>
							</div-->
							@if ( have_posts() )
								@while (have_posts()) @php the_post() @endphp
								@include('components.partials.partial-card-pdf')
								@endwhile
							@endif
							@include('components.partials.partial-pagination')
						</main>
						<aside class="sidebar sidebar--right">
							@include('components.partials.partial-sticky-adv')
						</aside>
					</section>
				</div>
			</div>
		</div>
	</div>
@endsection
