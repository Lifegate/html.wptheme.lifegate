{{-- /* Template Name: Survey Interna */ --}}
@extends('base')
@section('content')
	<style>
		.page-survey, .wrapper{
			background-color:#124E3F;
			color:#fff;
		}

		.wrapper .survey-title {
			background-color:#124E3F;
			padding-top:40px;
			padding-bottom:20px;
			padding-left:10px;
			padding-right:10px;
		}

		.wrapper .survey-container{
			background-color:#124E3F;
			padding-left:10px;
			padding-right:10px;
			padding-bottom:40px;
		}
		.survey-container p, .survey-container a{
			color:#fff;
			font-size:24px;
			line-height: 32px;
		}



	</style>
	<!--page-chisiamo.twig page -->
	<div class="page page-survey">
		<section class="survey">
			<div class="wrapper">
				<div class="survey-title">
					<div class="single__title single__title--longform">
						<div>
							<h1>{!! get_the_title() !!}</h1>
						</div>
					</div>
				</div>

				<div class="container survey-container">
					<div class="col-12">
						{!! the_content() !!}
					</div>
				</div>
			</div>
		</section>
	</div>

@endsection

