@extends('base')
@section('content')
	<!--archive.twig page -->
	<div class="archive">
		<div class="site">
			@asset('css/archive.min.css')
			<div class="wrapper">
				<div class="container">
					@include('components.partials.main-menu')
				</div>
			</div>
			@include('components.sections.trends')
			<div class="wrapper">
				<div class="container sticky-parent">
					<div class="single__title">
						<h1 class="title-search-big__title">{!! __('Risultati della ricerca','lifegate') !!}</h1>
						@if (!have_posts())
							<p class="title-search-big__abstract abstract">{!! __('Hai cercato','lifegate') !!}
								<strong>{!! get_search_query(); !!}</strong>. {!! __('Siamo spiacenti, non abbiamo trovato
								nulla. Prova ad utilizzare parole più generiche o a modificare la ricerca','lifegate') !!}.</p>
						@else
							<p class="title-search-big__abstract abstract">{!! __('Hai cercato','lifegate') !!}
								<strong>{!! get_search_query(); !!}</strong>. {!! __('La tua ricerca ha prodotto','lifegate') !!}
								<strong>{!! App::tbm_count_posts() !!}</strong> {!! __('risultati','lifegate') !!}.</p>
						@endif
						<section>
							<form method="get" action="{!! function_exists('pll_home_url') ? pll_home_url() : get_bloginfo('url') !!}">
								<div class="slide-search__input__field">
									<input class="search-bar__field__input" type="text" name="s" id="main_search"
										   placeholder="cerca..." value="{!! get_search_query(); !!}" required/>
									<button type="submit" class="search__field__submit cta cta--icon cta--icon-left cta--enter"></button>
								</div>
							</form>
						</section>
					</div>

					<main class="archive main-content main-content--left">
						<div class="container-cycle-col-1">
							@if ( have_posts() )
								@while (have_posts()) @php the_post() @endphp
								@includeFirst(['components.partials.partial-card-'.get_post_type().'-list--k2','components.partials.partial-card-post-list--k2'])
								@endwhile
							@endif
						</div>
						@include('components.partials.partial-pagination')
					</main>

					<aside class="sidebar sidebar--right">
						@include('components.partials.partial-sticky-adv')
					</aside>
				</div>
			</div>
		</div>
	</div>
@endsection
