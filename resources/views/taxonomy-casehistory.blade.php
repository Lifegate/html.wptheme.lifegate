@extends('base-mediacase')
@section('content')

	<section id="nav">
		@asset('css/taxonomy-channel.min.css')
		<nav class="seco-menu">
			<div>
				<ul>
					@if (has_nav_menu('global_navigation'))
						{!! wp_nav_menu($main_menu) !!}
					@endif
				</ul>
			</div>
			<div class="channel-submenu" aria-label="magazine-submenu">
				<ul>
					{!! wp_nav_menu($casehistory_menu) !!}
				</ul>
			</div>
			<div class="separator">
				<hr>
			</div>
		</nav>
	</section>

	<section class="apertura-category" >
		<div class="container">
			<div class="row">
				<div class="col-12">
					<?php $immagine = get_field("immagine", get_queried_object()); ?>
					<img class="lazyload" srcset="{!! tbm_wp_get_attachment_image_url($immagine["ID"],array(1200,600)) !!}, {!! tbm_wp_get_attachment_image_url($immagine["ID"],array(2400,1200)) !!} 2x"/>
					<div class="caption-apertura text-white">
						<h1>{!! the_archive_title() !!}</h1>
						<p class="text-white">{!! strip_tags(term_description()) !!}</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="site">
		<section id="under_apertura" class="lazyload"  data-script="@asset('js/components/sections/taxonomy-channel.js')">
			<div class="container">
				<div class="row">
					<?php
					$counter = 0;
					while (have_posts()) :
					the_post();
					$counter++;

					if($counter <= 2){
					?>
					@include('components.partials.partial-card-mediacase-half')
					<?php
					}
					endwhile;
					?>
				</div>
			</div>
		</section>
		<section id="offset-articles">
			<div class="container">
				<div class="row offset">
					<?php
					$counter = 0;
					while (have_posts()) :
					the_post();
					$counter++;
					if($counter == 3 || $counter == 4){ ?>
					@include('components.partials.partial-card-mediacase-large')
					<?php
					}
					endwhile;
					?>
				</div>
			</div>
		</section>
		<section id="articles-grid">
			<div class="container">
				<div class="row offset">
					<?php
					$counter = 0;
					while (have_posts()) :
					the_post();
					$counter++;
					if($counter == 5 || $counter == 6 || $counter == 7){
					?>
					@include('components.partials.partial-card-mediacase-small')
					<?php
					}

					endwhile;
					?>
				</div>
				<div class="row offset">
					<?php
					$counter = 0;
					while (have_posts()) :
					the_post();
					$counter++;
					if($counter == 8 || $counter == 9 || $counter == 10){
					?>
					@include('components.partials.partial-card-mediacase-small')
					<?php
					}
					endwhile;
					?>
				</div>
			</div>


			@include('components.partials.partial-pagination')


		</section>

	</div>
@endsection
