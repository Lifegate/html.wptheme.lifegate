<?php
/**
 * Add custom post type to author, tag, feed archive
 *
 * @param $query
 *
 * @return mixed
 */
add_filter( 'pre_get_posts', function ( $query ) {

	if ( is_admin() ) {
		return;
	}

	if ( get_query_var( 'post_type' ) ) {
		return;
	}

	$post_types = array(
		HTML_VIDEO_POST_TYPE,
		HTML_GALLERY_POST_TYPE,
		HTML_CARD_POST_TYPE,
		HTML_EVENT_POST_TYPE,
		'show',
		'post',
	);

	// Author archive
	if ( is_author() && $query->is_main_query() ) {
		$query->set( 'post_type', $post_types );
	}

	// Category and tag Archive
	if ( ( is_category() || is_tag() ) && $query->is_main_query() ) {

		// Handle post_type:  paginated results
		$query->set( 'post_type', $post_types );
	}

	// Feed
	if ( is_feed() && $query->is_main_query() ) {
		$query->set( 'post_type', $post_types );
		$query->set( 'posts_per_page', 20 );
	}

	if ( is_search() && $query->is_main_query() ) {
		$query->set( 'post_type', $post_types );
	}

	return;
} );

/**
 * Order show by name
 */
add_action( 'pre_get_posts', function ( $query ) {

	if ( is_admin() ) {
		return;
	}

	if ( $query->is_main_query() && is_post_type_archive( 'show' ) ) {
		$query->set( 'posts_per_page', - 1 );
	//	$query->set( 'orderby', 'name' );
	//	$query->set( 'order', 'ASC' );
		$query->set( 'no_found_rows', true );
	}

} );

/**
 * Order Event
 */
add_action( 'pre_get_posts', function ( $query ) {

	if ( is_admin() ) {
		return;
	}

	if ( $query->is_main_query() && is_post_type_archive( HTML_EVENT_POST_TYPE ) ) {
		$query->set( 'meta_query', array(
			'date_event' => array(
				'key'     => 'dateFrom',
				'compare' => 'EXISTS',
			),
		) );
		$query->set( 'orderby', 'date_event' );
		$query->set( 'order', 'DESC' );
	}
} );

/**
 * Remove featured posts from Magazine Homepage
 */
add_action( 'pre_get_posts', function ( $query ) {

	if ( is_admin() ) {
		return;
	}

	if ( $query->is_main_query() && ( is_tax( 'magazine' ) ) ) {

		$magazine_01 = get_field( 'tbm_magazine_high_posts' ) ?: array();
		$magazine_02 = get_field( 'tbm_magazine_featured_posts' ) ?: array();

		$magazines = array_merge( $magazine_01, $magazine_02 );

		if ( $magazines ) {
			$query->set( 'post__not_in', $magazines );
		}

	}
} );

/**
 * Remove featured posts from Trend Homepage
 */
add_action( 'pre_get_posts', function ( $query ) {

	if ( is_admin() ) {
		return;
	}

	if ( $query->is_main_query() && ( is_tax( 'trend' ) ) ) {

		$trend_01 = get_field( 'tbm_special_high_posts' ) ?: array();
		$trend_02 = get_field( 'tbm_special_featured_posts' ) ?: array();

		$trends = array_merge( $trend_01, $trend_02 );

		if ( $trends ) {
			$query->set( 'post__not_in', $trends );
		}

	}
} );
