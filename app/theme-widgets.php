<?php

namespace App;

/*
 * The widget that ACT NOW banner
 */

class Act_Now extends \WP_Widget {
	/**
	 * Register widget with WordPress.
	 */
	public function __construct() {
		parent::__construct(
			'act_now_widget', // Base ID
			esc_html__( '[LG] Agisci ora', 'lifegate' ), // Name
			array( 'description' => esc_html__( 'Widget "Agisci ora"', 'lifegate' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @param array $args Widget arguments.
	 * @param array $instance Saved values from database.
	 *
	 * @see WP_Widget::widget()
	 *
	 */
	public function widget( $args, $instance ) {

		if ( is_single() && get_field( 'tbm_post_initiative' ) ) {
			return '';
		}

		$id = $args['widget_id'];

		$title      = get_field( 'tbm_act_now_title', 'widget_' . $id );
		$subtitle   = get_field( 'tbm_act_now_subtitle', 'widget_' . $id );
		$cta        = get_field( 'tbm_act_now_cta', 'widget_' . $id );
		$link       = get_field( 'tbm_act_now_url', 'widget_' . $id );
		$image      = get_field( 'tbm_act_now_background', 'widget_' . $id ) ? tbm_wp_get_attachment_image_url( get_field( 'tbm_act_now_background', 'widget_' . $id ), array(
			980,
			280
		) ) : '';
		$font_color = get_field( 'tbm_act_now_font_color', 'widget_' . $id );

		if ( $title && $subtitle && $image ) {
			wp_enqueue_style( 'act-now' );

			$code = "<style>div.card-act-now--widget h3, div.card-act-now--widget .inner-cta p {color:%s}</style><div class=\"card-act-now card-act-now--widget\"><div class=\"lazyload\" data-bg=\"%s\"><div class=\"inner-cta\">
			<h3>%s</h3><div class=\"inner-cta__content\"><p>%s</p><a href=\"%s\" class=\"cta cta--icon cta--solid cta--icon-right cta--act-now\">%s</a></div></div></div></div>";

			$mobile_cta = '<a href="%s" class="cta cta--sticky cta--icon" data-tooltip="Agisci ora"></a>';

			echo sprintf( $code, $font_color, $image, $title, $subtitle, $link, $cta );
			echo sprintf( $mobile_cta, $link );
		}
	}

	/**
	 * Back-end widget form.
	 *
	 * @param array $instance Previously saved values from database.
	 *
	 * @see WP_Widget::form()
	 *
	 */
	public function form( $instance ) {

	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 * @see WP_Widget::update()
	 *
	 */
	public function update( $new_instance, $old_instance ) {
		$instance           = array();
		$instance['trends'] = ( ! empty( $new_instance['trends'] ) ) ? sanitize_text_field( $new_instance['trends'] ) : '';

		return $instance;
	}

}
