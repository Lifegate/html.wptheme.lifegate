<?php
/**
 * Theme assets
 */

use function App\asset_path;

/**
 * Remove and dequeue unuseful assets
 */
add_action( 'wp_print_styles', function () {
	if ( defined( 'HTML_QA_QUESTION_POST_TYPE' ) && ! is_singular( HTML_QA_QUESTION_POST_TYPE ) ) {
		wp_dequeue_style( 'gdrts-font' );
		wp_dequeue_style( 'wp-block-library' );
		wp_dequeue_style( 'gdrts-gridism' );
		wp_dequeue_style( 'gdrts-rating' );
	}
}, 100 );

/**
 * Add and remove
 */
add_action( 'wp_enqueue_scripts', function () {
	global $post;

	$ver = wp_get_theme()->get( 'Version' );


	/**
	 * Dequeue
	 */
	if ( ! is_singular( 'longform' ) ) {
		wp_dequeue_style( 'wp-block-library' );
	}

	/**
	 * Enqueue our jQuery
	 */
	wp_deregister_script( 'jquery' );
	if ( is_singular( 'iniziative' ) ) {
		wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), null, true );
	}
	wp_enqueue_script( 'scriptloader', asset_path( 'js/tbm.min.js' ), array(), $ver, true );
	wp_localize_script( 'scriptloader', 'tbm', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'songurl' => 'https://servizi.lifegate.it/titoli/titoli.json'
	) );

	/**
	 * Register styles for later use
	 */
	wp_register_style( 'act-now', asset_path( 'css/components/partials/card-act-now.min.css', false ), array( 'custom' ), $ver );

	/**
	 * Stiles global
	 */
	wp_enqueue_style( 'tbm-forms', asset_path( 'css/components/partials/forms.min.css', false ), array(  ), $ver );

} );


/**
 * Hook the action to include Newsletter CSS
 */
add_action( 'tbm_newsletter_enqueue_style', function () {
	$ver = wp_get_theme()->get( 'Version' );
	wp_enqueue_style( 'tbm-lg-newsletter', asset_path( 'css/components/partials/partial-newsletter.min.css', false ), array( 'custom' ), $ver );
} );
