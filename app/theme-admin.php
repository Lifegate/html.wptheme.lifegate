<?php

/**
 * Move admin bar to bottom
 */
add_action( 'wp_head', function () {
	if ( is_admin() ) {
		return;
	}

	echo "<style>div#wpadminbar{top:auto;bottom:0;position:fixed}.ab-sub-wrapper{bottom:32px}html[lang]{margin-top:0!important;margin-bottom:32px!important}@media screen and (max-width:782px){.ab-sub-wrapper{bottom:46px}html[lang]{margin-bottom:46px!important}}</style>";

}, 100 );

/**
 * Expand term taxonomy-trend editing page
 */
add_action( 'admin_head', function () {
	echo "<style>.taxonomy-trend #edittag {max-width: 80%;margin: 0 auto}</style>";
	echo "<style>.taxonomy-sponsor #edittag {max-width: 80%;margin: 0 auto}</style>";
	echo "<style>.taxonomy-magazine #edittag {max-width: 80%;margin: 0 auto}</style>";
}, 100 );


/**
 * Add custom styles to the block editor
 */
add_action('enqueue_block_editor_assets', function () {
	// Registra uno stile placeholder (se non ne hai già uno da utilizzare)
	wp_register_style('lifegate-editor-placeholder-style', false);
	wp_enqueue_style('lifegate-editor-placeholder-style');

	// Definisci il tuo CSS
	$custom_css = ".wp-block { max-width: 840px;  margin-left: auto; margin-right: auto; } ";

	// Aggiungi il CSS inline allo stile
	wp_add_inline_style('lifegate-editor-placeholder-style', $custom_css);
});

