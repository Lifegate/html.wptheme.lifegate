<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;

class LG_Purge_Cache extends Tbm_Plugins\WP_Async_Request {

	/**
	 * @var string
	 */
	protected $action = 'purge_cache';

	/**
	 * Handle the $_POST data
	 */
	protected function handle() {
		$id = $_POST['postid'];

		/**
		 * get the post language
		 */
		$lang = pll_get_post_language( $id ) ? pll_get_post_language( $id ) : 'it';

		/**
		 * Sleep two seconds
		 */
		sleep(2);

		/**
		 * Purge AMP
		 */
		$amp_url = $this->get_amp_urls( $id );
		$amp     = $this->tbm_amp_purge_cache( $amp_url );

		/**
		 * Purge Cloudflare (on .com we don't have cache)
		 */
		if ( $lang === 'it' ) {
			$cloudflare_urls = $this->get_cloudflare_urls( $id );
			$cloudflare      = $this->tbm_cf_purge_cache( $cloudflare_urls );
		}

	}

	protected function get_amp_urls( $id ) {
		$urls = array();

		$urls[] = function_exists( 'amp_get_permalink' ) ? amp_get_permalink( $id ) : '';

		return $urls;
	}

	protected function get_cloudflare_urls( $id ) {
		$urls = array();

		$urls[] = get_permalink( $id );
		$urls[] = get_permalink( $id )."?amp";

		/**
		 * Get the categories
		 */
		$categories = get_the_terms( $id, 'category' );
		if ( $categories && ! is_wp_error( $categories ) ) {
			foreach ( $categories as $category ) {
				$urls[] = get_term_link( $category );
			}
		}

		/**
		 * Get the trends
		 */
		$trends = get_the_terms( $id, 'trend' );
		if ( $trends && ! is_wp_error( $trends ) ) {
			foreach ( $trends as $trend ) {
				$urls[] = get_term_link( $trend );
			}
		}

		/**
		 * Get the others urls
		 */
		$defaults = array(
			'https://www.lifegate.it/google_news-sitemap.xml',
			'https://www.lifegate.it/',
			'https://www.lifegate.it/news'
		);

		$urls = array_merge( $urls, $defaults );

		return $urls;

	}

	protected function tbm_amp_purge_cache( $urls = array() ) {
		if ( empty( $urls ) ) {
			return false;
		}

		if ( ! is_array( $urls ) ) {
			$urls = (array) $urls;
		}

		$private_amp_key = new TbmCommon\System\AmpUpdate( ROOT_DIR . '/amp-cache-private-key.pem' );

		$status = $private_amp_key->purgeAll( $urls );

		return $status;
	}

	protected function tbm_cf_purge_cache( $urls = array() ) {
		if ( empty( $urls ) ) {
			return false;
		}

		if ( ! is_array( $urls ) ) {
			$urls = (array) $urls;
		}

		$files = implode( '","', $urls );

		$curl = curl_init();

		curl_setopt_array( $curl, array(
			CURLOPT_URL            => "https://api.cloudflare.com/client/v4/zones/745170572ec1e7602748c35397171f24/purge_cache",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING       => "",
			CURLOPT_CONNECTTIMEOUT => 2,
			CURLOPT_MAXREDIRS      => 5,
			CURLOPT_TIMEOUT        => 5,
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST  => "POST",
			CURLOPT_POSTFIELDS     => "{\"files\": [\"{$files}\"]}",
			CURLOPT_HTTPHEADER     => array(
				"Accept-Encoding: gzip, deflate",
				"Authorization: Bearer v0QcsD1Yyu5_8Di7R5Pik9FDmCsv5it_-Skx4iYa",
				"Content-Type: application/json",
			),
		) );

		$response = curl_exec( $curl );
		$err      = curl_error( $curl );

		curl_close( $curl );

		if ( $err ) {
			//echo "cURL Error #:" . $err;
			return false;
		}

		$response_array = json_decode( $response );

		if ( $response_array->success === false ) {
			return false;
		}

		return $response;
	}


}

/**
 * Instantiate class to make handle function always available
 */
add_action( 'init', function () {
	new LG_Purge_Cache();
} );
