<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 03/04/2019
 * Time: 17:10
 */

/**
 * Make ACF label translated
 */
if ( function_exists( 'pll_register_string' ) ) {
	foreach ( acf_get_field_groups() as $group ) {
		$fields = acf_get_fields( $group );
		if ( is_array( $fields ) && count( $fields ) ) {
			foreach ( $fields as &$field ) {
				pll_register_string( 'acf_tbm_' . $group['ID'] . '_label_' . $field['name'], $field['label'], 'acf_form_fields' );
				pll_register_string( 'acf_tbm_' . $group['ID'] . '_instructions_' . $field['name'], $field['instructions'], 'acf_form_fields' );
			}
		}
	}
}


add_filter( 'acf/prepare_field', function ( $field ) {
	if ( ! function_exists( 'pll__' ) ) {
		return;
	}
	$field['label'] = pll_translate_string( $field['label'], pll_current_language() );

	return $field;
}, 10, 1 );


/**
 * Select sponsor and partners
 *
 * @return array
 */
function tbm_acf_sponsor_hp_select() {
	$out = array();

	$terms = get_terms( array(
		'taxonomy'   => 'sponsor',
		'hide_empty' => false,
	) );

	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
		foreach ( $terms as $term ) {
			$out[ $term->term_id ] = $term->name;
		}
	}

	return $out;

}

/**
 * Select authors post types
 *
 * @return array
 */
function tbm_acf_get_author_post_types() {
	$out = array();

	$lang = function_exists( 'pll_current_language' ) ? pll_current_language() : '';


	$authors = get_posts( array(
		'post_type'      => 'autori',
		'posts_per_page' => - 1,
		'lang'           => $lang,
		'fields'         => 'ids',
		'post_status'    => 'publish',
	) );

	if ( $authors ) {
		foreach ( $authors as $choice ) {
			$lang           = function_exists( 'pll_get_post_language' ) ? pll_get_post_language( $choice ) : '';
			$out[ $choice ] = get_the_title( $choice ) . ' [' . $lang . ']';
		}
	}

	return $out;
}

/**
 * Select Iniziative post types
 *
 * @return array
 */
function tbm_acf_get_initiative_post_types() {
	$out = array();

	$lang = function_exists( 'pll_current_language' ) ? pll_current_language() : '';

	$initiatives = get_posts( array(
		'post_type'      => 'iniziative',
		'posts_per_page' => - 1,
		'lang'           => $lang,
		'fields'         => 'ids',
		'post_status'    => 'publish',
	) );

	if ( $initiatives ) {
		foreach ( $initiatives as $choice ) {
			$lang           = function_exists( 'pll_get_post_language' ) ? pll_get_post_language( $choice ) : '';
			$out[ $choice ] = get_the_title( $choice ) . ' [' . $lang . ']';
		}
	}

	return $out;
}

/**
 * Get terms from magazine and trend taxonomy
 *
 * @return array
 */
function tbm_get_trend_magazine_terms() {
	$terms = get_terms( array(
		'taxonomy'   => array( 'magazine', 'trend' ),
		'hide_empty' => false,
	) );

	if ( empty( $terms ) || is_wp_error( $terms ) ) {
		return array();
	}

	foreach ( $terms as $term ) {
		$out[ $term->term_id ] = $term->name . ' [' . $term->taxonomy . ']';
	}

	return $out;
}

/**
 * Select post types
 *
 * @return array
 */
function tbm_acf_get_select_post_types() {
	$out = array();

	$choices = get_post_types( array( 'public' => true ) );

	if ( is_array( $choices ) ) {
		foreach ( $choices as $choice ) {
			$out[ $choice ] = get_post_type_object( $choice )->labels->singular_name;
		}
	}

	return $out;
}

/**
 * Get selected post types
 *
 * @return array
 */
function tbm_acf_get_active_post_types( $option_name ) {
	//tbm_log( get_field( $option_name . '_pt' ) );
}

/**
 * Create all the options fields for the Homepage Option
 *
 * @param string $prefix The prefix of the functions
 * @param string $name The content name (usd in labels)
 * @param int $max The maximum numbers of posts
 *
 * @return array
 */
function tbm_acf_create_hp_fields( $prefix = '', $name = 'Contenuti', $max = 10, $lang = 'it' ) {

	if ( empty( $prefix ) ) {
		return array();
	}

	$current_lang = function_exists( 'pll_current_language' ) ? pll_current_language() : 'it';

	if ( $current_lang && $lang !== $current_lang ) {
		return array();
	}

	return array(
		array(
			'key'               => $prefix . '_00',
			'label'             => $name,
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'placement'         => 'left',
			'endpoint'          => 0,
		),
		array(
			'key'           => $prefix . '_pt',
			'label'         => 'Quali contenuti attivare per quest\'area?',
			'name'          => $prefix . '_posttype',
			'type'          => 'select',
			'instructions'  => 'Seleziona i post type da mostrare in questa zona',
			'required'      => 0,
			'wrapper'       => array(
				'width' => '70',
				'class' => '',
				'id'    => '',
			),
			'choices'       => tbm_acf_post_type_select(),
			'default_value' => array(),
			'allow_null'    => 1,
			'multiple'      => 1,
			'ui'            => 1,
			'ajax'          => 0,
			'return_format' => 'value',
			'placeholder'   => '',
		),
		array(
			'key'          => $prefix . '_0C',
			'label'        => 'Contenuti casuali',
			'name'         => $prefix . '_casuali',
			'type'         => 'true_false',
			'instructions' => 'Pubblica contenuto casuale',
			'required'     => 0,

			'wrapper'       => array(
				'width' => '30',
				'class' => '',
				'id'    => '',
			),
			'message'       => '',
			'default_value' => 0,
			'ui'            => 1,
			'ui_on_text'    => 'Sì',
			'ui_off_text'   => 'No',
		),
		array(
			'key'               => $prefix . '_01',
			'label'             => 'Aperture Automatiche',
			'name'              => $prefix . '_automatiche',
			'type'              => 'true_false',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					array(
						'field'    => $prefix . '_02',
						'operator' => '!=',
						'value'    => '1',
					),
				),
			),
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'message'           => 'Seleziona per automatizzare la gestione degli articoli nell\'area ' . $name . ' (in ordine cronologico inverso sulle categorie selezionate)',
			'default_value'     => 0,
			'ui'                => 1,
			'ui_on_text'        => 'Sì',
			'ui_off_text'       => 'No',
		),
		array(
			'key'               => $prefix . '_02',
			'label'             => 'Contenuti specifici',
			'name'              => $prefix . '_singular',
			'type'              => 'true_false',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					array(
						'field'    => $prefix . '_01',
						'operator' => '!=',
						'value'    => '1',
					),
				),
			),
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'message'           => 'Seleziona per scegliere articoli specifici',
			'default_value'     => 0,
			'ui'                => 1,
			'ui_on_text'        => 'Sì',
			'ui_off_text'       => 'No',
		),
		array(
			'key'               => $prefix . '_03',
			'label'             => 'Aperture In evidenza',
			'name'              => $prefix . '_featured',
			'type'              => 'true_false',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					array(
						'field'    => $prefix . '_01',
						'operator' => '==',
						'value'    => '1',
					),
				),
			),
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'message'           => 'Seleziona se vuoi che gli articoli nell\'area ' . $name . ' siano solo quelli "In evidenza"',
			'default_value'     => 0,
			'ui'                => 1,
			'ui_on_text'        => 'Sì',
			'ui_off_text'       => 'No',
		),
		array(
			'key'               => $prefix . '_04',
			'label'             => 'Categorie',
			'name'              => $prefix . '_categories',
			'type'              => 'taxonomy',
			'instructions'      => 'Istruzioni',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					array(
						'field'    => $prefix . '_01',
						'operator' => '==',
						'value'    => '1',
					),
				),
			),
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'taxonomy'          => 'category',
			'field_type'        => 'checkbox',
			'allow_null'        => 0,
			'add_term'          => 0,
			'save_terms'        => 0,
			'load_terms'        => 0,
			'return_format'     => 'id',
			'multiple'          => 0,
		),
		array(
			'key'               => $prefix . '_05',
			'label'             => $name,
			'name'              => $prefix . '_single',
			'type'              => 'relationship',
			'instructions'      => 'Seleziona gli articoli da utilizzare nell\'area ' . $name . '. Massimo: ' . $max . '.',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					array(
						'field'    => $prefix . '_02',
						'operator' => '==',
						'value'    => '1',
					),
				),
			),
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'post_type'         => array(
				'post',
				HTML_GALLERY_POST_TYPE,
				HTML_VIDEO_POST_TYPE,
				HTML_EVENT_POST_TYPE,
				HTML_CARD_POST_TYPE,
				'longform',
				'iniziative',
				'original'
			),
			'taxonomy'          => array(),
			'filters'           => array(
				0 => 'search',
				1 => 'post_type',
			),
			'elements'          => '',
			'min'               => 0,
			'max'               => $max,
			'return_format'     => 'id',
		),
	);
}

/**
 * Create Pages
 */
if ( function_exists( 'acf_add_options_page' ) ) {

	$args = array(
		'page_title' => 'LifeGate',
		'menu_slug'  => 'tbm_config',
		'icon_url'   => 'dashicons-list-view',
		'capability' => 'activate_plugins',
	);
	acf_add_options_page( $args );

	// add sub page
	acf_add_options_sub_page( array(
		'page_title'  => 'Configurazione HP',
		'menu_title'  => 'Configurazione HP',
		'parent_slug' => 'tbm_config'
	) );

	// add sub page
	acf_add_options_sub_page( array(
		'page_title'  => 'Configurazione HP Magazine',
		'menu_title'  => 'Configurazione HP Magazine',
		'parent_slug' => 'tbm_config'
	) );


	// add sub page
	acf_add_options_sub_page( array(
		'page_title'  => 'Configurazione HP Original',
		'menu_title'  => 'Configurazione HP Original',
		'parent_slug' => 'tbm_config'
	) );


	// add sub page
	acf_add_options_sub_page( array(
		'page_title'  => 'Configurazione HP Mediacase',
		'menu_title'  => 'Configurazione HP Mediacase',
		'parent_slug' => 'tbm_config'
	) );

	// add sub page
	acf_add_options_sub_page( array(
		'page_title'  => 'Configurazione HP Contenuti Speciali',
		'menu_title'  => 'Configurazione HP Contenuti Speciali',
		'parent_slug' => 'tbm_config'
	) );

	// add sub page
	acf_add_options_sub_page( array(
		'page_title'  => 'Configurazione Agisci Ora',
		'menu_title'  => 'Conf Agisci Ora',
		'parent_slug' => 'tbm_config'
	) );


	// add sub page
	acf_add_options_sub_page( array(
		'page_title'  => 'Configurazione Widget Stories',
		'menu_title'  => 'Conf Widget Stories',
		'parent_slug' => 'tbm_config'
	) );

	// add sub page
	acf_add_options_sub_page( array(
		'page_title'  => 'Configurazione 3bmeteo',
		'menu_title'  => '3BMeteo',
		'parent_slug' => 'tbm_config'
	) );




}

function tbm_acf_get_hp_fields( $fields = array(), $lang = 'it' ) {
	if ( empty( $fields ) ) {
		return array();
	}

	$current_lang = function_exists( 'pll_current_language' ) ? pll_current_language() : 'it';

	if ( $current_lang && $lang !== $current_lang ) {
		return array();
	}

	return $fields;
}

function lifegate_field_groups() {
	if ( ! function_exists( 'acf_add_local_field_group' ) ) {
		return '';
	}

	/**
	 * Background video fields
	 */
	$homepage_video = array(
		array(
			'key'               => 'bv_00',
			'label'             => 'Background area video',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           =>
				array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
			'placement'         => 'left',
			'endpoint'          => 0,
		),
		array(
			'key'               => 'field_5e57942afc3d3',
			'label'             => 'Immagine background Area Video',
			'name'              => 'tbm_hp_video_bg',
			'type'              => 'image',
			'instructions'      => 'Carica l\'immagine che sarà usata come sfondo della sezione video. Deve avere dimensione full hd (1920x1280)',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'return_format'     => 'array',
			'preview_size'      => 'medium',
			'library'           => 'all',
			'min_width'         => 1920,
			'min_height'        => 1280,
			'min_size'          => 0,
			'max_width'         => 1920,
			'max_height'        => 1280,
			'max_size'          => 1,
			'mime_types'        => '',

		)
	);

	/**
	 * Background song card
	 */
	$background_song_card = array(
		array(
			'key'               => 'bsc_00',
			'label'             => 'Background card canzone',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           =>
				array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
			'placement'         => 'left',
			'endpoint'          => 0,
		),
		array(
			'key'               => 'field_5e57942afc4r4',
			'label'             => 'Immagine background Card Canzone',
			'name'              => 'tbm_hp_song_card',
			'type'              => 'image',
			'instructions'      => 'Carica l\'immagine che sarà usata come thumbnail della card "Canzone". Deve avere dimensioni minime di 1024x576',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'return_format'     => 'array',
			'preview_size'      => 'medium',
			'library'           => 'all',
			'min_width'         => 1024,
			'min_height'        => 576,
			'min_size'          => 0,
			'max_width'         => 0,
			'max_height'        => 0,
			'max_size'          => 1,
			'mime_types'        => '',

		)
	);

	/**
	 * Sponsored Magazine fields
	 */
	$sponsored_trend_pp = array(
		array(
			'key'               => 'field_5e5e949ac87uyt',
			'label'             => 'Un Magazine o Trend Primo Piano [it]',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           =>
				array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
			'placement'         => 'left',
			'endpoint'          => 0,
		),
		array(
			'key'               => 'field_5e5e949dhd76',
			'label'             => 'Quale Magazine o Trend mostrare in quest\'area?',
			'name'              => 'hp_cs',
			'instructions'      => 'Seleziona un Magazine o un Trend da pubblicare nell\'area. Ricorda che deve avere una formattazione adatta al widget',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'choices'           => tbm_get_trend_magazine_terms(),
			'default_value'     => array(),
			'allow_null'        => 1,
			'multiple'          => 0,
			'ui'                => 1,
			'ajax'              => 0,
			'return_format'     => 'value',
			'translations'      => 'copy_once',
			'placeholder'       => '',
			'type'              => 'select',
		)
	);


	/**
	 * Sponsored magazine/trend fields
	 */
	$sponsored_trend_pp2 = array(
		array(
			'key'               => 'field_1222e949ac347856',
			'label'             => 'Magazine e Trend Secondo Piano  [it]',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           =>
				array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
			'placement'         => 'left',
			'endpoint'          => 0,
		),
		array(
			'key'               => 'field_5e5e949acewewb',
			'label'             => 'Quali Speciali o Trend mostrare in quest\'area?',
			'name'              => 'hp_pp2',
			'type'              => 'select',
			'instructions'      => 'NB: Seleziona <b>2 elementi</b> Magazine o Trend da pubblicare nell\'area',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'choices'           => tbm_get_trend_magazine_terms(),
			'default_value'     => array(),
			'allow_null'        => 1,
			'multiple'          => 1,
			'ui'                => 1,
			'ajax'              => 0,
			'return_format'     => 'value',
			'translations'      => 'copy_once',
			'placeholder'       => '',
			'field_type'        => 'multi_select',
		)
	);

	/**
	 * Sponsored magazine/trend fields
	 */
	$sponsored_trend_sp = array(
		array(
			'key'               => 'field_5e5e949ac347856',
			'label'             => 'Magazine e Trend in slider [it]',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           =>
				array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
			'placement'         => 'left',
			'endpoint'          => 0,
		),
		array(
			'key'               => 'field_5e5e949ac692b',
			'label'             => 'Quali Speciali o Trend mostrare in quest\'area?',
			'name'              => 'hp_ss',
			'type'              => 'select',
			'instructions'      => 'Seleziona Magazine o Trend da pubblicare nell\'area',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'choices'           => tbm_get_trend_magazine_terms(),
			'default_value'     => array(),
			'allow_null'        => 0,
			'multiple'          => 1,
			'ui'                => 1,
			'ajax'              => 0,
			'return_format'     => 'value',
			'translations'      => 'copy_once',
			'placeholder'       => '',
			'field_type'        => 'multi_select',
		)
	);

	/**
	 * Sponsored Magazine fields
	 */
	$sponsored_trend_pp_en = array(
		array(
			'key'               => 'field_5e5e9wsdc87uyt',
			'label'             => 'Un Magazine o Trend Primo Piano [en]',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           =>
				array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
			'placement'         => 'left',
			'endpoint'          => 0,
		),
		array(
			'key'               => 'field_5e6y749dhd76',
			'label'             => 'Quale Magazine o Trend mostrare in quest\'area?',
			'name'              => 'hp_cs_en',
			'instructions'      => 'Seleziona un Magazine o un Trend da pubblicare nell\'area. Ricorda che deve avere una formattazione adatta al widget',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'choices'           => tbm_get_trend_magazine_terms(),
			'default_value'     => array(),
			'allow_null'        => 1,
			'multiple'          => 0,
			'ui'                => 1,
			'ajax'              => 0,
			'return_format'     => 'value',
			'translations'      => 'copy_once',
			'placeholder'       => '',
			'type'              => 'select',
		)
	);

	/**
	 * Sponsored magazine/trend fields
	 */
	$sponsored_trend_sp_en = array(
		array(
			'key'               => 'field_5e8uy49ac347856',
			'label'             => 'Magazine e Trend Secondo Piano [en]',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           =>
				array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
			'placement'         => 'left',
			'endpoint'          => 0,
		),
		array(
			'key'               => 'field_5e8uish4ac692b',
			'label'             => 'Quali Speciali o Trend mostrare in quest\'area?',
			'name'              => 'hp_ss_en',
			'type'              => 'select',
			'instructions'      => 'Seleziona Magazine o Trend da pubblicare nell\'area',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'choices'           => tbm_get_trend_magazine_terms(),
			'default_value'     => array(),
			'allow_null'        => 0,
			'multiple'          => 1,
			'ui'                => 1,
			'ajax'              => 0,
			'return_format'     => 'value',
			'translations'      => 'copy_once',
			'placeholder'       => '',
			'field_type'        => 'multi_select',
		)
	);

	/**
	 * ATF
	 */
	$atf_field = array(
		array(
			'key'               => 'field_5e8dfa40dd0db',
			'label'             => 'Above The Fold Homepage',
			'name'              => 'tbm_hp_atf',
			'type'              => 'radio',
			'instructions'      => 'Scegliere il layout per la parte superiore della Homepage',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'choices'           => array(
				'V1' => 'Versione 1',
				'V2' => 'Versione 2',
			),
			'allow_null'        => 0,
			'other_choice'      => 0,
			'default_value'     => '',
			'layout'            => 'horizontal',
			'return_format'     => 'value',
			'save_other_choice' => 0,
		),

	);

	/**
	 * Homepage fields
	 */
	$fields_hp = array_merge(
		tbm_acf_create_hp_fields( 'pp', 'Primo piano [it]', 1, 'it' ),
		tbm_acf_create_hp_fields( 'pp_en', 'Primo piano [en]', 1, 'en' ),
		tbm_acf_create_hp_fields( 'sp', 'Secondo piano [it]', 3, 'it' ),
		tbm_acf_create_hp_fields( 'sp_en', 'Secondo piano [en]', 3, 'en' ),
		tbm_acf_create_hp_fields( 'ao', 'Agisci ora [it]', 3, 'it' ),
		tbm_acf_create_hp_fields( 'ao_en', 'Agisci ora [en]', 3, 'en' ),
		tbm_acf_get_hp_fields( $sponsored_trend_pp, 'it' ),
		tbm_acf_get_hp_fields( $sponsored_trend_sp, 'it' ),
		tbm_acf_get_hp_fields( $sponsored_trend_pp2, 'it' ),

		tbm_acf_get_hp_fields( $sponsored_trend_pp_en, 'en' ),
		tbm_acf_get_hp_fields( $sponsored_trend_sp_en, 'en' ),
		tbm_acf_create_hp_fields( 'mp', 'News Primo Piano', 1, 'it' ),
		tbm_acf_create_hp_fields( 'ms', 'News Secondo piano', 4, 'it' ),
		tbm_acf_create_hp_fields( 'xp', 'Taglio Medio Primo piano', 2, 'it' ),
		tbm_acf_create_hp_fields( 'xs', 'Taglio Medio Secondo piano', 10, 'it' ),
		tbm_acf_create_hp_fields( 'iz', 'Iniziative', 3, 'it' ),

		tbm_acf_create_hp_fields( 'lf', 'LongForm [it]', 10, 'it' ),
		tbm_acf_create_hp_fields( 'lf_en', 'LongForm [en]', 10, 'en' ),


		tbm_acf_create_hp_fields( 'ep', 'Eventi Primo piano', 1, 'it' ),
		tbm_acf_create_hp_fields( 'es', 'Eventi Secondo piano', 4, 'it' ),
		$homepage_video,
		tbm_acf_create_hp_fields( 'vp', 'Video Primo piano', 1, 'it' ),
		tbm_acf_create_hp_fields( 'vs', 'Video Secondo piano', 4, 'it' ),
		$background_song_card,
		tbm_acf_create_hp_fields( 'pr_center', 'Programmi centro', 1, 'it' ),
		tbm_acf_create_hp_fields( 'pr_dx', 'Programmi destra', 1, 'it' ),
		tbm_acf_create_hp_fields( 'fp', 'Taglio Basso primo piano', 4, 'it' ),
		tbm_acf_create_hp_fields( 'fs', 'Taglio Basso secondo piano', 10, 'it' )
	);

	$atf_fields = array_merge(
		$atf_field
	);

	/**
	 * ACF for ATF Configuration
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5b451s3sf88226',
		'title'                 => 'Homepage: configurazione above the fold',
		'fields'                => $atf_fields,
		'location'              => array(
			array(
				array(
					'param'    => 'options_page',
					'operator' => '==',
					'value'    => 'acf-options-configurazione-hp',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
		'recaptcha'             => 0,
	) );

	/**
	 * ACF for Homepage Configuration
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5b451f5f87394',
		'title'                 => 'Homepage: selezione contenuti',
		'fields'                => $fields_hp,
		'location'              => array(
			array(
				array(
					'param'    => 'options_page',
					'operator' => '==',
					'value'    => 'acf-options-configurazione-hp',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
		'recaptcha'             => 0,
	) );

	/**
	 * Magazine HP Blocks
	 */
	$blocks_magazine    = array(
		array(
			'key'               => 'field_5e57942ert625',
			'label'             => 'Ordine categorie HP Magazine',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           =>
				array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
			'placement'         => 'left',
			'endpoint'          => 0,
		),
		array(
			'key'           => 'field_5e57942afx2e4',
			'label'         => 'Ordine delle categorie in HP Magazine',
			'name'          => 'tbm_blocks_magazine',
			'type'          => 'taxonomy',
			'taxonomy'      => 'category',
			'field_type'    => 'multi_select',
			'instructions'  => 'Seleziona le categorie da mostrare in questa zona e impostane l\'ordine di visualizzazione',
			'required'      => 0,
			'wrapper'       => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'choices'       => '',
			'default_value' => array(),
			'allow_null'    => 1,
			'multiple'      => 1,
			'ui'            => 1,
			'ajax'          => 0,
			'return_format' => 'object',
			'placeholder'   => '',
		),
	);
	$blocks_magazine_en = array(
		array(
			'key'               => 'field_5e57942ert478',
			'label'             => 'Ordine categorie HP Magazine [en]',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           =>
				array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
			'placement'         => 'left',
			'endpoint'          => 0,
		),
		array(
			'key'           => 'field_5e57942afx4e2',
			'label'         => 'Ordine delle categorie in HP Magazine [en]',
			'name'          => 'tbm_blocks_magazine_en',
			'type'          => 'taxonomy',
			'taxonomy'      => 'category',
			'field_type'    => 'multi_select',
			'instructions'  => 'Seleziona le categorie da mostrare in questa zona e impostane l\'ordine di visualizzazione',
			'required'      => 0,
			'wrapper'       => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'choices'       => '',
			'default_value' => array(),
			'allow_null'    => 1,
			'multiple'      => 1,
			'ui'            => 1,
			'ajax'          => 0,
			'return_format' => 'object',
			'placeholder'   => '',
		),
	);
	$blocks_original    = array(
		array(
			'key'               => 'field_43357942ert625',
			'label'             => 'Lancio in Home News',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           =>
				array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
			'placement'         => 'left',
			'endpoint'          => 0,
		),
		array(
			'key' => 'field_6074ba1b4f44a',
			'label' => 'Abilita Original in Home News',
			'name' => 'original_enable_in_news',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 0,
			'ui' => 1,
			'ui_on_text' => '',
			'ui_off_text' => '',
			'translations' => 'copy_once',
		),
		array(
			'key' => 'field_6074c0a34f44c',
			'label' => 'Tipo di apertura',
			'name' => 'original_open_type',
			'type' => 'radio',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'singola' => 'singola',
				'slider' => 'slider',
			),
			'allow_null' => 0,
			'other_choice' => 0,
			'default_value' => '',
			'layout' => 'horizontal',
			'return_format' => 'value',
			'translations' => 'copy_once',
			'save_other_choice' => 0,
		),
		array(
			'key' => 'field_6074c25d718bd',
			'label' => 'Immagine',
			'name' => 'original_image',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_6074c0a34f44c',
						'operator' => '==',
						'value' => 'singola',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => '1536x1536',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
			'translations' => 'copy_once',
		),
		array(
			'key' => 'field_6074c3314efc6',
			'label' => 'Cards',
			'name' => 'original_cards',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_6074c0a34f44c',
						'operator' => '==',
						'value' => 'slider',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => '',
			'min' => 0,
			'max' => 0,
			'layout' => 'row',
			'button_label' => 'Aggiungi Card',
			'sub_fields' => array(
				array(
					'key' => 'field_6074c3884efc7',
					'label' => 'Immagine',
					'name' => 'immagine',
					'type' => 'image',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'array',
					'preview_size' => '1536x1536',
					'library' => 'all',
					'min_width' => '',
					'min_height' => '',
					'min_size' => '',
					'max_width' => '',
					'max_height' => '',
					'max_size' => '',
					'mime_types' => '',
					'translations' => 'copy_once',
				),
				array(
					'key' => 'field_6074c45693424',
					'label' => 'Testo',
					'name' => 'testo',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'translations' => 'translate',
				),
				array(
					'key' => 'field_6074c3b24efc8',
					'label' => 'è un Video',
					'name' => 'is_video',
					'type' => 'true_false',
					'instructions' => 'Aggiunge icona del player',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'default_value' => 0,
					'ui' => 0,
					'translations' => 'copy_once',
					'ui_on_text' => '',
					'ui_off_text' => '',
				),
				array(
					'key' => 'field_6074c3d64efc9',
					'label' => 'Link card',
					'name' => 'link',
					'type' => 'url',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'translations' => 'copy_once',
				),
			),
		),
		array(
			'key' => 'field_6074c146718bc',
			'label' => 'Presentazione',
			'name' => 'original_presentazione',
			'type' => 'textarea',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'maxlength' => '',
			'rows' => '',
			'new_lines' => '',
			'translations' => 'translate',
		),
	);

	$blocks_original_pp    = array(
		array(
			'key'               => 'field_77357942ert625',
			'label'             => 'Primo Piano',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           =>
				array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
			'placement'         => 'left',
			'endpoint'          => 0,
		),
		array(
			'key' => 'field_60800c327d777',
			'label' => 'Primo Piano Original',
			'name' => 'primo_piano_original',
			'type' => 'relationship',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'post_type' => array(
				0 => 'original',
			),
			'taxonomy' => '',
			'filters' => array(
				0 => 'search',
			),
			'elements' => '',
			'min' => 4,
			'max' => 4,
			'return_format' => 'id',
			'translations' => 'copy_once',
		),
	);
	$blocks_original_sp    = array(
		array(
			'key'               => 'field_773579543rt625',
			'label'             => 'Secondo Piano',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           =>
				array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
			'placement'         => 'left',
			'endpoint'          => 0,
		),
		array(
			'key' => 'field_60800c697d778',
			'label' => 'Secondo Piano Original',
			'name' => 'secondo_piano_original',
			'type' => 'relationship',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'post_type' => array(
				0 => 'original',
			),
			'taxonomy' => '',
			'filters' => array(
				0 => 'search',
			),
			'elements' => '',
			'min' => 2,
			'max' => 2,
			'return_format' => 'id',
			'translations' => 'copy_once',
		),
	);
	$blocks_original_canali    = array(
		array(
			'key'               => 'field_762879543rt625',
			'label'             => 'Canali',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           =>
				array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
			'placement'         => 'left',
			'endpoint'          => 0,
		),
		array(
			'key' => 'field_60808e19ec326',
			'label' => 'Canale prima Fascia',
			'name' => 'canale_1_original',
			'type' => 'taxonomy',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'taxonomy' => 'channel',
			'field_type' => 'select',
			'allow_null' => 1,
			'add_term' => 0,
			'save_terms' => 0,
			'load_terms' => 0,
			'return_format' => 'object',
			'translations' => 'copy_once',
			'multiple' => 0,
		),
		array(
			'key' => 'field_60808edbec327',
			'label' => 'Canale seconda Fascia',
			'name' => 'canale_2_original',
			'type' => 'taxonomy',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'taxonomy' => 'channel',
			'field_type' => 'select',
			'allow_null' => 1,
			'add_term' => 0,
			'save_terms' => 0,
			'load_terms' => 0,
			'return_format' => 'object',
			'translations' => 'copy_once',
			'multiple' => 0,
		),
		array(
			'key' => 'field_60808eecec328',
			'label' => 'Canale terza Fascia',
			'name' => 'canale_3_original',
			'type' => 'taxonomy',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'taxonomy' => 'channel',
			'field_type' => 'select',
			'allow_null' => 1,
			'add_term' => 0,
			'save_terms' => 0,
			'load_terms' => 0,
			'return_format' => 'object',
			'translations' => 'copy_once',
			'multiple' => 0,
		),
	);

	$blocks_mediacase_pp    = array(
		array(
			'key'               => 'field_543579543ert625',
			'label'             => 'Primo Piano',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           =>
				array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
			'placement'         => 'left',
			'endpoint'          => 0,
		),
		array(
			'key' => 'field_6085436c327d777',
			'label' => 'Primo Piano Mediacase',
			'name' => 'primo_piano_mediacase',
			'type' => 'relationship',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'post_type' => array(
				0 => 'mediacase',
			),
			'taxonomy' => '',
			'filters' => array(
				0 => 'search',
			),
			'elements' => '',
			'min' => 4,
			'max' => 4,
			'return_format' => 'id',
			'translations' => 'copy_once',
		),
	);
	$blocks_mediacase_sp    = array(
		array(
			'key'               => 'field_3435379543rt625',
			'label'             => 'Secondo Piano',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           =>
				array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
			'placement'         => 'left',
			'endpoint'          => 0,
		),
		array(
			'key' => 'field_658050c697d778',
			'label' => 'Secondo Piano Media Case',
			'name' => 'secondo_piano_mediacase',
			'type' => 'relationship',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'post_type' => array(
				0 => 'mediacase',
			),
			'taxonomy' => '',
			'filters' => array(
				0 => 'search',
			),
			'elements' => '',
			'min' => 2,
			'max' => 2,
			'return_format' => 'id',
			'translations' => 'copy_once',
		),
	);
	$blocks_mediacase_casehistory    = array(
		array(
			'key'               => 'field_7624239543rt625',
			'label'             => 'Sezioni',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           =>
				array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
			'placement'         => 'left',
			'endpoint'          => 0,
		),
		array(
			'key' => 'field_60658e19ec326',
			'label' => 'Case History prima Fascia',
			'name' => 'casehistory_1_original',
			'type' => 'taxonomy',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'taxonomy' => 'casehistory',
			'field_type' => 'select',
			'allow_null' => 1,
			'add_term' => 0,
			'save_terms' => 0,
			'load_terms' => 0,
			'return_format' => 'object',
			'translations' => 'copy_once',
			'multiple' => 0,
		),
		array(
			'key' => 'field_65808edbec327',
			'label' => 'Cae History seconda Fascia',
			'name' => 'casehistory_2_original',
			'type' => 'taxonomy',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'taxonomy' => 'casehistory',
			'field_type' => 'select',
			'allow_null' => 1,
			'add_term' => 0,
			'save_terms' => 0,
			'load_terms' => 0,
			'return_format' => 'object',
			'translations' => 'copy_once',
			'multiple' => 0,
		),
		array(
			'key' => 'field_87808eecec328',
			'label' => 'Case History terza Fascia',
			'name' => 'casehistory_3_original',
			'type' => 'taxonomy',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'taxonomy' => 'casehistory',
			'field_type' => 'select',
			'allow_null' => 1,
			'add_term' => 0,
			'save_terms' => 0,
			'load_terms' => 0,
			'return_format' => 'object',
			'translations' => 'copy_once',
			'multiple' => 0,
		),
	);

	/**
	 * Magazine HP fields
	 */
	$fields_magazine = array_merge(
		tbm_acf_create_hp_fields( 'mpp', 'Magazine Primo piano [it]', 1 ),
		tbm_acf_create_hp_fields( 'msp', 'Magazine Secondo piano [it]', 4 ),
		tbm_acf_create_hp_fields( 'mpp_en', 'Magazine Primo piano [en]', 1, 'en' ),
		tbm_acf_create_hp_fields( 'msp_en', 'Magazine Secondo piano [en]', 4, 'en' ),
		tbm_acf_get_hp_fields( $blocks_magazine, 'it' ),
		tbm_acf_get_hp_fields( $blocks_magazine_en, 'en' )
	);

	/**
	 * Original HP fields
	 */
	$fields_original = array_merge(
		tbm_acf_get_hp_fields( $blocks_original, 'it' ),
		tbm_acf_get_hp_fields( $blocks_original_pp, 'it' ),
		tbm_acf_get_hp_fields( $blocks_original_sp, 'it' ),
		tbm_acf_get_hp_fields( $blocks_original_canali, 'it' )
	);

	/**
	 * Mediacase HP fields
	 */
	$fields_mediacase = array_merge(
		tbm_acf_get_hp_fields( $blocks_mediacase_pp, 'it' ),
		tbm_acf_get_hp_fields( $blocks_mediacase_sp, 'it' ),
		tbm_acf_get_hp_fields( $blocks_mediacase_casehistory, 'it' )
	);


	/**
	 * ACF for Magazine HP Configuration
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5b451f5f92864',
		'title'                 => 'Magazine: selezione contenuti',
		'fields'                => $fields_magazine,
		'location'              => array(
			array(
				array(
					'param'    => 'options_page',
					'operator' => '==',
					'value'    => 'acf-options-configurazione-hp-magazine',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
		'recaptcha'             => 0,
	) );


	/**
	 * ACF for Original HP Configuration
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5b499f5f92864',
		'title'                 => 'Original: selezione contenuti',
		'fields'                => $fields_original,
		'location'              => array(
			array(
				array(
					'param'    => 'options_page',
					'operator' => '==',
					'value'    => 'acf-options-configurazione-hp-original',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
		'recaptcha'             => 0,
	) );



	/**
	 * ACF for Mediacase HP Configuration
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5b499f54392864',
		'title'                 => 'Mediacase: selezione contenuti',
		'fields'                => $fields_mediacase,
		'location'              => array(
			array(
				array(
					'param'    => 'options_page',
					'operator' => '==',
					'value'    => 'acf-options-configurazione-hp-mediacase',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
		'recaptcha'             => 0,
	) );

	/**
	 * ACF for Trend Menu
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5e57a62aa76ec',
		'title'                 => 'Menu',
		'fields'                => array(
			array(
				'key'               => 'field_5e57a6444ed6e',
				'label'             => 'In primo piano',
				'name'              => 'tbm_menu_featured',
				'type'              => 'true_false',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'message'           => __( 'Evidenzia la voce di menu', 'lifegate' ),
				'default_value'     => 0,
				'ui'                => 0,
				'ui_on_text'        => '',
				'ui_off_text'       => '',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'nav_menu_item',
					'operator' => '==',
					'value'    => 'location/trends_navigation',
				),
			),
			array(
				array(
					'param'    => 'nav_menu_item',
					'operator' => '==',
					'value'    => 'location/channels_navigation',
				),
			),
			array(
				array(
					'param'    => 'nav_menu_item',
					'operator' => '==',
					'value'    => 'location/casehistory_navigation',
				),
			),
			array(
				array(
					'param'    => 'nav_menu_item',
					'operator' => '==',
					'value'    => 'location/global_navigation',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
	) );

}

add_action( 'init', 'lifegate_field_groups' );

add_action( 'init', function () {
	if ( function_exists( 'pll_current_language' ) && ( 'it' === pll_current_language() || false === pll_current_language() ) ) {
		acf_add_local_field_group( array(
			'key'                   => 'group_5ee13a149aa0f',
			'title'                 => 'Contenuti speciali HP [Italiano]',
			'fields'                => array(
				array(
					'key'               => 'field_5ee13ac6c2118',
					'label'             => 'Contenuti speciali a singola colonna',
					'name'              => 'tbm_special_manual_field',
					'type'              => 'repeater',
					'instructions'      => 'I contenuti speciali sono box formati da titolo, immagine, sottotitolo. Hanno un link attivo e possono essere collegati ad uno sponsor',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'collapsed'         => 'field_5ee13b13c2119',
					'min'               => 0,
					'max'               => 10,
					'layout'            => 'block',
					'button_label'      => 'Aggiungi contenuto speciale a singola colonna',
					'sub_fields'        => array(
						array(
							'key'               => 'field_5ee13b13c2119',
							'label'             => 'Titolo',
							'name'              => 'text',
							'type'              => 'text',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '100',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'prepend'           => '',
							'append'            => '',
							'maxlength'         => 50,
							'translations'      => 'translate',
						),
						array(
							'key'               => 'field_5ee13b73c211a',
							'label'             => 'Link',
							'name'              => 'url',
							'type'              => 'url',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '70',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'translations'      => 'copy_once',
						),
						array(
							'key'               => 'field_5ee13b88c211b',
							'label'             => 'Sponsor e Partner',
							'name'              => 'sponsor',
							'type'              => 'select',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '30',
								'class' => '',
								'id'    => '',
							),
							'choices'           => tbm_acf_sponsor_hp_select(),
							'default_value'     => array(),
							'allow_null'        => 1,
							'multiple'          => 0,
							'ui'                => 1,
							'ajax'              => 0,
							'return_format'     => 'value',
							'translations'      => 'copy_once',
							'placeholder'       => '',
						),
						array(
							'key'               => 'field_5ee13bb8c211c',
							'label'             => 'Immagine in evidenza',
							'name'              => 'image',
							'type'              => 'image',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '50',
								'class' => '',
								'id'    => '',
							),
							'return_format'     => 'array',
							'preview_size'      => 'medium',
							'library'           => 'all',
							'min_width'         => 600,
							'min_height'        => '',
							'min_size'          => '',
							'max_width'         => '',
							'max_height'        => '',
							'max_size'          => '',
							'mime_types'        => '',
							'translations'      => 'copy_once',
						),
						array(
							'key'               => 'field_5ee13be2c211d',
							'label'             => 'Sottotitolo',
							'name'              => 'excerpt',
							'type'              => 'textarea',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '50',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'maxlength'         => '140',
							'rows'              => 5,
							'new_lines'         => '',
							'translations'      => 'copy_once',
						),

					),
				),
				array(
					'key'               => 'field_5ee13ac6c3118',
					'label'             => 'Contenuti speciali a doppia colonna',
					'name'              => 'tbm_special_manual_field_bis',
					'type'              => 'repeater',
					'instructions'      => 'I contenuti speciali sono box formati da titolo, immagine, sottotitolo. Hanno un link attivo e possono essere collegati ad uno sponsor',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'collapsed'         => 'field_5ee13b13c3119',
					'min'               => 0,
					'max'               => 10,
					'layout'            => 'block',
					'button_label'      => 'Aggiungi contenuti speciali a doppia colonna',
					'sub_fields'        => array(
						array(
							'key'               => 'field_5ee13b13c3119',
							'label'             => 'Titolo (a)',
							'name'              => 'text_a',
							'type'              => 'text',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '100',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'prepend'           => '',
							'append'            => '',
							'maxlength'         => 50,
							'translations'      => 'translate',
						),
						array(
							'key'               => 'field_5ee13b73c311a',
							'label'             => 'Link (a)',
							'name'              => 'url_a',
							'type'              => 'url',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '70',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'translations'      => 'copy_once',
						),
						array(
							'key'               => 'field_5ee13b88c311b',
							'label'             => 'Sponsor (a)',
							'name'              => 'sponsor_a',
							'type'              => 'select',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '30',
								'class' => '',
								'id'    => '',
							),
							'choices'           => tbm_acf_sponsor_select(),
							'default_value'     => array(),
							'allow_null'        => 1,
							'multiple'          => 0,
							'ui'                => 1,
							'ajax'              => 0,
							'return_format'     => 'value',
							'translations'      => 'copy_once',
							'placeholder'       => '',
						),

						array(
							'key'               => 'field_5ee13bb8c311c',
							'label'             => 'Immagine in evidenza (a)',
							'name'              => 'image_a',
							'type'              => 'image',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '50',
								'class' => '',
								'id'    => '',
							),
							'return_format'     => 'array',
							'preview_size'      => 'medium',
							'library'           => 'all',
							'min_width'         => 600,
							'min_height'        => '',
							'min_size'          => '',
							'max_width'         => '',
							'max_height'        => '',
							'max_size'          => '',
							'mime_types'        => '',
							'translations'      => 'copy_once',
						),
						array(
							'key'               => 'field_5ee13be2c311d',
							'label'             => 'Sottotitolo (a)',
							'name'              => 'excerpt_a',
							'type'              => 'textarea',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '50',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'maxlength'         => '',
							'rows'              => 5,
							'new_lines'         => '',
							'translations'      => 'copy_once',
						),
						array(
							'key'               => 'field_5ee166fed13c2',
							'label'             => 'Titolo (b)',
							'name'              => 'text_b',
							'type'              => 'text',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '100',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'prepend'           => '',
							'append'            => '',
							'maxlength'         => 50,
							'translations'      => 'translate',
						),
						array(
							'key'               => 'field_5ee16714d13c3',
							'label'             => 'Link (b)',
							'name'              => 'url_b',
							'type'              => 'url',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '70',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'translations'      => 'copy_once',
						),
						array(
							'key'               => 'field_5ee1671dd13c4',
							'label'             => 'Sponsor	(b)',
							'name'              => 'sponsor_b',
							'type'              => 'select',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '30',
								'class' => '',
								'id'    => '',
							),
							'choices'           => tbm_acf_sponsor_select(),
							'default_value'     => array(),
							'allow_null'        => 1,
							'multiple'          => 0,
							'ui'                => 1,
							'ajax'              => 0,
							'return_format'     => 'value',
							'translations'      => 'copy_once',
							'placeholder'       => '',
						),

						array(
							'key'               => 'field_5ee1672ad13c5',
							'label'             => 'Immagine in evidenza (b)',
							'name'              => 'image_b',
							'type'              => 'image',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '50',
								'class' => '',
								'id'    => '',
							),
							'return_format'     => 'array',
							'preview_size'      => 'medium',
							'library'           => 'all',
							'min_width'         => 600,
							'min_height'        => '',
							'min_size'          => '',
							'max_width'         => '',
							'max_height'        => '',
							'max_size'          => '',
							'mime_types'        => '',
							'translations'      => 'copy_once',
						),
						array(
							'key'               => 'field_5ee16732d13c6',
							'label'             => 'Sottotitolo (b)',
							'name'              => 'excerpt_b',
							'type'              => 'textarea',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '50',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'maxlength'         => '',
							'rows'              => 5,
							'new_lines'         => '',
							'translations'      => 'copy_once',
						),
					),
				),
			),
			'location'              => array(
				array(
					array(
						'param'    => 'options_page',
						'operator' => '==',
						'value'    => 'acf-options-configurazione-hp-contenuti-speciali',
					),
				),
			),
			'menu_order'            => 0,
			'position'              => 'normal',
			'style'                 => 'default',
			'label_placement'       => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen'        => '',
			'active'                => true,
			'description'           => '',
			'recaptcha'             => 0,
		) );

		acf_add_local_field_group( array(
			'key' => 'group_5f2937abea254',
			'title' => 'Agisci ora',
			'fields' => array(
				array(
					'key' => 'field_5f2937b5eeca4',
					'label' => 'Agisci Ora [it]',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'field_5f293ea4df9a9',
					'label' => 'Testo azione',
					'name' => 'act_now_testo_azione',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'translations' => 'translate',
				),
				array(
					'key' => 'field_5f293ecbdf9aa',
					'label' => 'Link Azione',
					'name' => 'act_now_link_azione',
					'type' => 'url',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'translations' => 'copy_once',
				),
				array(
					'key' => 'field_5f293ed5df9ab',
					'label' => 'Icona',
					'name' => 'act_now_icona',
					'type' => 'image',
					'instructions' => '48x48',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'url',
					'preview_size' => 'medium',
					'library' => 'all',
					'min_width' => 48,
					'min_height' => 48,
					'min_size' => '',
					'max_width' => 48,
					'max_height' => 48,
					'max_size' => '',
					'mime_types' => '',
					'translations' => 'copy_once',
				),
			),
			'location' => array(
				array(
					array(
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'acf-options-conf-agisci-ora',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => true,
			'description' => '',
		));

			acf_add_local_field_group(array(
				'key' => 'group_5fb7aa7b8acdd',
				'title' => 'Abilita Widget Stories [it]',
				'fields' => array(
					array(
						'key' => 'field_5fb7ac123db74',
						'label' => 'Abilita Widget TOP',
						'name' => 'enable_widget_stories',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'default_value' => 0,
						'ui' => 0,
						'translations' => 'copy_once',
						'ui_on_text' => '',
						'ui_off_text' => '',
					),
					array(
						'key' => 'field_5fb7ac303db75',
						'label' => 'Codice Widget',
						'name' => 'code_widget_stories',
						'type' => 'textarea',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'rows' => '',
						'new_lines' => '',
						'translations' => 'copy_once',
					),
					array(
						'key' => 'field_5fb7ac123db44',
						'label' => 'Abilita Widget SQUARE',
						'name' => 'enable_widget_square',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'default_value' => 0,
						'ui' => 0,
						'translations' => 'copy_once',
						'ui_on_text' => '',
						'ui_off_text' => '',
					),
					array(
						'key' => 'field_5fb7ac303db35',
						'label' => 'Codice Widget SQUARE',
						'name' => 'code_widget_square',
						'type' => 'textarea',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'rows' => '',
						'new_lines' => '',
						'translations' => 'copy_once',
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'acf-options-conf-widget-stories',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));


	}


	if( function_exists('acf_add_local_field_group') ):

		acf_add_local_field_group(array(
			'key' => 'group_60754c01ed970',
			'title' => '3bmeteo',
			'fields' => array(
				array(
					'key' => 'field_60754c1485c86',
					'label' => 'Abilita il widget 3BMeteo',
					'name' => 'enable_3bmeteo',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'default_value' => 0,
					'ui' => 1,
					'ui_on_text' => '',
					'ui_off_text' => '',
					'translations' => 'copy_once',
				),
			),
			'location' => array(
				array(
					array(
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'acf-options-3bmeteo',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => true,
			'description' => '',
		));

	endif;

	if ( function_exists( 'pll_current_language' ) && ( 'en' === pll_current_language() || false === pll_current_language() ) ) {
		acf_add_local_field_group( array(
			'key'                   => 'group_x_5ee13a149aa0f',
			'title'                 => 'Contenuti speciali HP [Inglese]',
			'fields'                => array(
				array(
					'key'               => 'field_x_5ee13ac6c2118',
					'label'             => 'Contenuti speciali a singola colonna',
					'name'              => 'tbm_special_manual_field_en',
					'type'              => 'repeater',
					'instructions'      => 'I contenuti speciali sono box formati da titolo, immagine, sottotitolo. Hanno un link attivo e possono essere collegati ad uno sponsor',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'collapsed'         => 'field_x_5ee13b13c2119',
					'min'               => 0,
					'max'               => 10,
					'layout'            => 'block',
					'button_label'      => 'Aggiungi contenuto speciale a singola colonna',
					'sub_fields'        => array(
						array(
							'key'               => 'field_x_5ee13b13c2119',
							'label'             => 'Titolo',
							'name'              => 'text_en',
							'type'              => 'text',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '100',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'prepend'           => '',
							'append'            => '',
							'maxlength'         => 50,
							'translations'      => 'translate',
						),
						array(
							'key'               => 'field_x_5ee13b73c211a',
							'label'             => 'Link',
							'name'              => 'url_en',
							'type'              => 'url',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '70',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'translations'      => 'copy_once',
						),
						array(
							'key'               => 'field_x_5ee13b88c211b',
							'label'             => 'Sponsor e Partner',
							'name'              => 'sponsor_en',
							'type'              => 'select',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '30',
								'class' => '',
								'id'    => '',
							),
							'choices'           => tbm_acf_sponsor_hp_select(),
							'default_value'     => array(),
							'allow_null'        => 1,
							'multiple'          => 0,
							'ui'                => 1,
							'ajax'              => 0,
							'return_format'     => 'value',
							'translations'      => 'copy_once',
							'placeholder'       => '',
						),
						array(
							'key'               => 'field_x_5ee13bb8c211c',
							'label'             => 'Immagine in evidenza',
							'name'              => 'image_en',
							'type'              => 'image',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '50',
								'class' => '',
								'id'    => '',
							),
							'return_format'     => 'array',
							'preview_size'      => 'medium',
							'library'           => 'all',
							'min_width'         => 600,
							'min_height'        => '',
							'min_size'          => '',
							'max_width'         => '',
							'max_height'        => '',
							'max_size'          => '',
							'mime_types'        => '',
							'translations'      => 'copy_once',
						),
						array(
							'key'               => 'field_x_5ee13be2c211d',
							'label'             => 'Sottotitolo',
							'name'              => 'excerpt_en',
							'type'              => 'textarea',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '50',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'maxlength'         => '140',
							'rows'              => 5,
							'new_lines'         => '',
							'translations'      => 'copy_once',
						),

					),
				),
				array(
					'key'               => 'field_x_5ee13ac6c3118',
					'label'             => 'Contenuti speciali a doppia colonna',
					'name'              => 'tbm_special_manual_field_bis_en',
					'type'              => 'repeater',
					'instructions'      => 'I contenuti speciali sono box formati da titolo, immagine, sottotitolo. Hanno un link attivo e possono essere collegati ad uno sponsor',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'collapsed'         => 'field_x_5ee13b13c3119',
					'min'               => 0,
					'max'               => 10,
					'layout'            => 'block',
					'button_label'      => 'Aggiungi contenuti speciali a doppia colonna',
					'sub_fields'        => array(
						array(
							'key'               => 'field_x_5ee13b13c3119',
							'label'             => 'Titolo (a)',
							'name'              => 'text_a_en',
							'type'              => 'text',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '100',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'prepend'           => '',
							'append'            => '',
							'maxlength'         => 50,
							'translations'      => 'translate',
						),
						array(
							'key'               => 'field_x_5ee13b73c311a',
							'label'             => 'Link (a)',
							'name'              => 'url_a_en',
							'type'              => 'url',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '70',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'translations'      => 'copy_once',
						),
						array(
							'key'               => 'field_x_5ee13b88c311b',
							'label'             => 'Sponsor (a)',
							'name'              => 'sponsor_a_en',
							'type'              => 'select',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '30',
								'class' => '',
								'id'    => '',
							),
							'choices'           => tbm_acf_sponsor_select(),
							'default_value'     => array(),
							'allow_null'        => 1,
							'multiple'          => 0,
							'ui'                => 1,
							'ajax'              => 0,
							'return_format'     => 'value',
							'translations'      => 'copy_once',
							'placeholder'       => '',
						),

						array(
							'key'               => 'field_x_5ee13bb8c311c',
							'label'             => 'Immagine in evidenza (a)',
							'name'              => 'image_a_en',
							'type'              => 'image',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '50',
								'class' => '',
								'id'    => '',
							),
							'return_format'     => 'array',
							'preview_size'      => 'medium',
							'library'           => 'all',
							'min_width'         => 600,
							'min_height'        => '',
							'min_size'          => '',
							'max_width'         => '',
							'max_height'        => '',
							'max_size'          => '',
							'mime_types'        => '',
							'translations'      => 'copy_once',
						),
						array(
							'key'               => 'field_x_5ee13be2c311d',
							'label'             => 'Sottotitolo (a)',
							'name'              => 'excerpt_a_en',
							'type'              => 'textarea',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '50',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'maxlength'         => '',
							'rows'              => 5,
							'new_lines'         => '',
							'translations'      => 'copy_once',
						),
						array(
							'key'               => 'field_x_5ee166fed13c2',
							'label'             => 'Titolo (b)',
							'name'              => 'text_b_en',
							'type'              => 'text',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '100',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'prepend'           => '',
							'append'            => '',
							'maxlength'         => 50,
							'translations'      => 'translate',
						),
						array(
							'key'               => 'field_x_5ee16714d13c3',
							'label'             => 'Link (b)',
							'name'              => 'url_b_en',
							'type'              => 'url',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '70',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'translations'      => 'copy_once',
						),
						array(
							'key'               => 'field_x_5ee1671dd13c4',
							'label'             => 'Sponsor	(b)',
							'name'              => 'sponsor_b_en',
							'type'              => 'select',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '30',
								'class' => '',
								'id'    => '',
							),
							'choices'           => tbm_acf_sponsor_select(),
							'default_value'     => array(),
							'allow_null'        => 1,
							'multiple'          => 0,
							'ui'                => 1,
							'ajax'              => 0,
							'return_format'     => 'value',
							'translations'      => 'copy_once',
							'placeholder'       => '',
						),

						array(
							'key'               => 'field_x_5ee1672ad13c5',
							'label'             => 'Immagine in evidenza (b)',
							'name'              => 'image_b_en',
							'type'              => 'image',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '50',
								'class' => '',
								'id'    => '',
							),
							'return_format'     => 'array',
							'preview_size'      => 'medium',
							'library'           => 'all',
							'min_width'         => 600,
							'min_height'        => '',
							'min_size'          => '',
							'max_width'         => '',
							'max_height'        => '',
							'max_size'          => '',
							'mime_types'        => '',
							'translations'      => 'copy_once',
						),
						array(
							'key'               => 'field_x_5ee16732d13c6',
							'label'             => 'Sottotitolo (b)',
							'name'              => 'excerpt_b_en',
							'type'              => 'textarea',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '50',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'maxlength'         => '',
							'rows'              => 5,
							'new_lines'         => '',
							'translations'      => 'copy_once',
						),
					),
				),
			),
			'location'              => array(
				array(
					array(
						'param'    => 'options_page',
						'operator' => '==',
						'value'    => 'acf-options-configurazione-hp-contenuti-speciali',
					),
				),
			),
			'menu_order'            => 0,
			'position'              => 'normal',
			'style'                 => 'default',
			'label_placement'       => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen'        => '',
			'active'                => true,
			'description'           => '',
			'recaptcha'             => 0,
		) );

		acf_add_local_field_group( array(
			'key' => 'group_x_5f2937abea254',
			'title' => 'Agisci ora',
			'fields' => array(
				array(
					'key' => 'field_x_5f2937b5eeca4',
					'label' => 'Agisci Ora [en]',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'field_x_5f293ea4df9a9',
					'label' => 'Testo azione',
					'name' => 'act_now_testo_azione_en',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'translations' => 'translate',
				),
				array(
					'key' => 'field_x_5f293ecbdf9aa',
					'label' => 'Link Azione',
					'name' => 'act_now_link_azione_en',
					'type' => 'url',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'translations' => 'translate',
				),
				array(
					'key' => 'field_x_5f293ed5df9ab',
					'label' => 'Icona',
					'name' => 'act_now_icona_en',
					'type' => 'image',
					'instructions' => '48x48',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'url',
					'preview_size' => 'medium',
					'library' => 'all',
					'min_width' => 48,
					'min_height' => 48,
					'min_size' => '',
					'max_width' => 48,
					'max_height' => 48,
					'max_size' => '',
					'mime_types' => '',
					'translations' => 'copy_once',
				),
			),
			'location' => array(
				array(
					array(
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'acf-options-conf-agisci-ora',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => true,
			'description' => '',
		));

		acf_add_local_field_group(array(
			'key' => 'group_x_5fb7aa7b8acdd',
			'title' => 'Abilita Widget Stories [en]',
			'fields' => array(
				array(
					'key' => 'field_x_5fb7ac123db74',
					'label' => 'Abilita Widget',
					'name' => 'enable_widget_stories_en',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'default_value' => 0,
					'ui' => 0,
					'translations' => 'copy_once',
					'ui_on_text' => '',
					'ui_off_text' => '',
				),
				array(
					'key' => 'field_x_5fb7ac303db75',
					'label' => 'Codice Widget',
					'name' => 'code_widget_stories_en',
					'type' => 'textarea',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'maxlength' => '',
					'rows' => '',
					'new_lines' => '',
					'translations' => 'copy_once',
				),
			),
			'location' => array(
				array(
					array(
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'acf-options-conf-widget-stories',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => true,
			'description' => '',
		));

	}



} );



