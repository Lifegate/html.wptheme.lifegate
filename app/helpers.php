<?php

namespace App;

use Roots\Sage\Container;

/**
 * Get the sage container.
 *
 * @param string $abstract
 * @param array $parameters
 * @param Container $container
 *
 * @return Container|mixed
 */
function sage( $abstract = null, $parameters = [], Container $container = null ) {
	$container = $container ?: Container::getInstance();
	if ( ! $abstract ) {
		return $container;
	}

	return $container->bound( $abstract )
		? $container->makeWith( $abstract, $parameters )
		: $container->makeWith( "sage.{$abstract}", $parameters );
}

/**
 * Get / set the specified configuration value.
 *
 * If an array is passed as the key, we will assume you want to set an array of values.
 *
 * @param array|string $key
 * @param mixed $default
 *
 * @return mixed|\Roots\Sage\Config
 * @copyright Taylor Otwell
 * @link https://github.com/laravel/framework/blob/c0970285/src/Illuminate/Foundation/helpers.php#L254-L265
 */
function config( $key = null, $default = null ) {
	if ( is_null( $key ) ) {
		return sage( 'config' );
	}
	if ( is_array( $key ) ) {
		return sage( 'config' )->set( $key );
	}

	return sage( 'config' )->get( $key, $default );
}

/**
 * @param string $file
 * @param array $data
 *
 * @return string
 */
function template( $file, $data = [] ) {
	if ( ! is_admin() && remove_action( 'wp_head', 'wp_enqueue_scripts', 1 ) ) {
		wp_enqueue_scripts();
	}

	return sage( 'blade' )->render( $file, $data );
}

/**
 * Retrieve path to a compiled blade view
 *
 * @param $file
 * @param array $data
 *
 * @return string
 */
function template_path( $file, $data = [] ) {
	return sage( 'blade' )->compiledPath( $file, $data );
}


/**
 * @param $asset
 *
 * @return string
 */
function asset_path( $asset, $enqueue = true ) {

	if ( strpos( $asset, '.css' ) && $enqueue ) {


		$css     = get_query_var( 'notcss', array() );
		$handler = array_values( array_slice( explode( '/', $asset ), - 1 ) )[0];

		if ( in_array( $handler, $css, true ) ) {
			return null;
		}

		if ( false ) {
			$file    = get_theme_file_path() . '/dist/' . $asset;
			$handle1 = fopen( $file, "r" );
			$handle2 = fopen( get_theme_file_path() . '/dist/css/tbm.min.css', empty( $css ) ? 'w+' : 'a+' );

			stream_copy_to_stream( $handle1, $handle2 );

			fclose( $handle1 );
			fclose( $handle2 );

			$css = array_merge( $css, (array) $handler );
			set_query_var( 'notcss', $css );
		}

		$ver = wp_get_theme()->get( 'Version' );

		$css = array_merge( $css, (array) $handler );
		set_query_var( 'notcss', $css );

		$url = '<link rel="stylesheet" href="%s" media="print" onload="this.media=\'all\'">';

		return sprintf( $url, sage( 'assets' )->getUri( $asset ) );

	}

	/**
	 * Handle not CSS files
	 */
	return sage( 'assets' )->getUri( $asset );

}

function asset_path_regular( $asset, $enqueue = true ) {
	/**
	 * If css, enqueue the file directly
	 */
	if ( strpos( $asset, '.css' ) && $enqueue ) {
		$css     = get_query_var( 'notcss', array() );
		$handler = array_values( array_slice( explode( '/', $asset ), - 1 ) )[0];

		if ( in_array( $handler, $css, true ) ) {
			return null;
		}

		$ver = wp_get_theme()->get( 'Version' );

		$css = array_merge( $css, (array) $handler );
		set_query_var( 'notcss', $css );

		$url = '<link rel="stylesheet" href="%s" media="print" onload="this.media=\'all\'">';

		return sprintf( $url, sage( 'assets' )->getUri( $asset ) );
	}

	return sage( 'assets' )->getUri( $asset );
}

/**
 * @param $asset
 *
 * @return string
 */
function asset_path_old( $asset, $enqueue = true ) {

	/**
	 * If css, add to enqueue list and return nothing
	 */
	if ( strpos( $asset, '.css' ) && $enqueue ) {
		$ver     = wp_get_theme()->get( 'Version' );
		$handler = array_values( array_slice( explode( '/', $asset ), - 1 ) )[0];
		wp_enqueue_style( 'tbm_' . $handler, sage( 'assets' )->getUri( $asset ), 'custom', $ver );

		return null;
	}

	return sage( 'assets' )->getUri( $asset );
}

/**
 * @param string|string[] $templates Possible template files
 *
 * @return array
 */
function filter_templates( $templates ) {
	$paths         = apply_filters( 'sage/filter_templates/paths', [
		'views',
		'resources/views'
	] );
	$paths_pattern = "#^(" . implode( '|', $paths ) . ")/#";

	return collect( $templates )
		->map( function ( $template ) use ( $paths_pattern ) {
			/** Remove .blade.php/.blade/.php from template names */
			$template = preg_replace( '#\.(blade\.?)?(php)?$#', '', ltrim( $template ) );

			/** Remove partial $paths from the beginning of template names */
			if ( strpos( $template, '/' ) ) {
				$template = preg_replace( $paths_pattern, '', $template );
			}

			return $template;
		} )
		->flatMap( function ( $template ) use ( $paths ) {
			return collect( $paths )
				->flatMap( function ( $path ) use ( $template ) {
					return [
						"{$path}/{$template}.blade.php",
						"{$path}/{$template}.php",
						"{$template}.blade.php",
						"{$template}.php",
					];
				} );
		} )
		->filter()
		->unique()
		->all();
}

/**
 * @param string|string[] $templates Relative path to possible template files
 *
 * @return string Location of the template
 */
function locate_template( $templates ) {
	return \locate_template( filter_templates( $templates ) );
}

/**
 * Determine whether to show the sidebar
 * @return bool
 */
function display_sidebar() {
	static $display;
	isset( $display ) || $display = apply_filters( 'sage/display_sidebar', false );

	return $display;
}
