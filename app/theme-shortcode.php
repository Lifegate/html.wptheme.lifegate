<?php
/**
 * youtube shortcode
 */
add_shortcode( 'youtube', function ( $atts ) {
	$atts = shortcode_atts( array(
		'url' => ''
	), $atts, 'youtube' );

	$url = wp_oembed_get( $atts['url'] );

	if ( $url ) {
		return $url;
	}

} );

/**
 * The gallery shortcode
 */
remove_shortcode( 'gallery' );
add_shortcode( 'gallery', function ( $atts ) {


	/**
	 * Add code for [gallery ids="211352,211272,211271"]
	 */
	if ( ! empty( $atts['ids'] ) ) {
		$gallery = array();

		$args = array(
			'include'        => $atts['ids'],
			'post_type'      => 'attachment',
			'post_status'    => 'inherit',
			'post_mime_type' => 'image'
		);

		$images = get_posts( $args );

		if ( ! $images ) {
			return '';
		}

		foreach ( $images as $image ) {
			$image_id = $image->ID;
			$title    = $image->post_title;
			$caption  = $image->post_excerpt;
			$arr_ids = explode(",", $atts['ids']);
			$order = array_search($image_id, $arr_ids);

			$gallery[$order] = array( 'ID' => $image_id, 'title' => $title, 'caption' => $caption );
		}

		ksort($gallery);

		$sc          = new stdClass();
		$sc->gallery = isset( $gallery ) ? $gallery : array();
		$data['sc']  = $sc;

		ob_start();

		echo App\template( 'embedder/templates/_gallery_slider', $data );

		$output_string = ob_get_contents();
		ob_end_clean();
		wp_reset_postdata();

		return $output_string;
	}

	if ( isset( $atts['id'] ) && ! empty( $atts['id'] ) ) {
		$id = $atts['id'];
	}

	if ( HTML_GALLERY_POST_TYPE === get_post_type( $id ) ) {
		return do_shortcode( '[ghgallery id=' . $id . ' title="' . get_the_title( $id ) . '" layout="_gallery_slider"]' );
	}

	$id = lifegate_get_id_by_old_id( $atts['id'], HTML_GALLERY_POST_TYPE );

	if ( HTML_GALLERY_POST_TYPE === get_post_type( $id ) ) {
		return do_shortcode( '[ghgallery id=' . $id . ' title="' . get_the_title( $id ) . '" layout="_gallery_slider"]' );
	}

	return false;

} );

/**
 * The quote shortcode
 * [quote cite=”Ilaria Capua” url="https://www.google.it"]Non si sbaglia mai a rispettare la natura, anzi dobbiamo rispettarla sempre di più. [/quote]
 */
add_shortcode( 'quote', function ( $atts, $content = "" ) {

	if ( ! $content ) {
		return '';
	}

	$attributes = shortcode_atts(
		array(
			'cite' => '',
			'url'  => '',
		), $atts, 'quote' );

	if ( ! empty( $attributes['cite'] ) ) {
		$cite = sprintf( '<cite>%s</cite>', $attributes['cite'] );
	}

	if ( ! empty( $attributes['cite'] ) && ! empty( $attributes['url'] ) ) {
		$cite = sprintf( '<cite><a href="%s" target="_blank" rel="nofollow">%s</a></cite>', $attributes['url'], $attributes['cite'] );
	}

	$format = "<blockquote>{$content}{$cite}</blockquote>";

	return $format;

} );

/**
 * The Vimeo Shortcode [vimeo url="https://vimeo.com/269596231"]Video Cano Cristales[/vimeo]
 */
add_shortcode( 'video', function ( $atts ) {
	$atts = shortcode_atts( array(
		'url' => ''
	), $atts, 'youtube' );

	$url = wp_oembed_get( $atts['url'] );

	if ( $url ) {
		return $url;
	}

} );
