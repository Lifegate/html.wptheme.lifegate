<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package pmi
 */

use function App\asset_path;

/**
 * Rewrite the youtube iframe for lazyloading
 *
 * @param $id
 *
 * @return bool|false|string|string[]|null
 */
function lg_embed_lazy_video_from_id( $id = 0 ) {

	if ( ! $id ) {
		return '';
	}

	if ( ! function_exists( 'html_video_generate_video_player' ) ) {
		return '';
	}

	$code = html_video_generate_video_player( $id );

	if ( strpos( $code, '<iframe' ) === false ) {
		return $code;
	}

	$code = str_replace( 'src="https://www.youtube.com', 'class="lazyload" data-src="https://www.youtube.com', $code );

	return $code;
}


/**
 * Get the id of the new post querying the old id (before the import)
 *
 * @param int $old_id The old id. Required
 * @param array $post_type The post type. Required
 *
 * @return int|null
 */
function lifegate_get_id_by_old_id( $old_id = 0, $post_type = array() ) {

	if ( ! $old_id ) {
		return null;
	}

	if ( empty( $post_type ) ) {
		return null;
	}

	$posts = get_posts(
		array(
			'post_type'      => (array) $post_type,
			'posts_per_page' => 1,
			'fields'         => 'ids',
			'meta_key'       => 'old_id',
			'meta_value'     => $old_id
		)
	);

	if ( ! $posts ) {
		return null;
	}

	$post = reset( $posts );

	return $post;
}

/**
 * Get the date of the post
 *
 * @param null $post
 *
 * @return string
 */
function lifegate_snippet_post_date( $post = null ) {

	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}

	if ( get_field( 'tbm_hide_date', $post->ID ) ) {
		return '';
	}

	$lang = function_exists( 'pll_current_language' ) ? pll_current_language() : 'it';

	$html = '<label class="post__date post__date--' . $lang . '"><time class="entry-date published" datetime="%s">%s</time></label>';
	$date = get_the_date( get_option( 'date_format' ), $post->ID );

	/**
	 * Lovercase date
	 */
	$date = strtolower( $date );

	return sprintf( $html, get_the_date( 'c', $post ), $date );
}

function lifegate_snippet_card_post_translator( $post = array() ) {
	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}

	$template = '<a href="%1$s"><span class="post__author__name"> %3$s</span></a>';

	$translator = get_field( 'traduttore_news', $post->ID );

	if ( ! $translator ) {
		return '';
	}

	if ( ! is_array( $translator ) ) {
		$translator = (array) $translator;
	}

	foreach ( $translator as $author ) {

		$author_data = get_post( $author );
		if ( empty( $author_data ) ) {
			return '';
		}

		$args  = array(
			get_permalink( $author_data ),
			get_the_post_thumbnail( $author_data ),
			get_the_title( $author_data ),
		);
		$out[] = vsprintf( $template, $args );
	}

	// If authors are more than 2, we need other layout
	if ( count( $translator ) > 2 ) {
		return implode( ', ', array_slice( $out, 0, - 1 ) ) . ' ' . __( 'e', 'lifegate' ) . ' ' . array_slice( $out, - 1 )[0];
	}

	// If authors are 2 or 1
	return implode( ' ' . __( 'e', 'lifegate' ) . ' ', $out );

}

function lifegate_snippet_card_post_author( $post = null, $role = false, $editorial = false ) {
	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}

	$author_free = get_field( 'tbm_autore_news_free', $post->ID );

	if ( $author_free ) {
//		return __( 'di', 'lifegate' ) . ' <span class="post__author__name">' . $author_free . '</span>';

	}

	if ( is_single() ) {
		$template = '<a href="%1$s"><span class="post__author__name">%3$s</span><span class="post__author__role">%4$s</span></a>';
	} else {
		if ( $editorial ) {
			$template = '<a href="%1$s"><div><picture><img class="lazyload" data-src="%2$s" alt="%3$s"/></picture></div><div class="author__info"><span class="post__author__name">%3$s</span><span class="post__author__role">%4$s</span></div></a>';
		} else {
			$template = '<a href="%1$s"><span class="post__author__name">%3$s</span><span class="post__author__role">%4$s</span></a>';
		}
	}

	$authors = get_field( 'autore_news', $post->ID );

	if ( ! $authors && ! $author_free) {
		return '<span class="post__author__name">' . __( 'a cura della redazione', 'lifegate' ) . '</span>';
	}

	if ( $authors && ! is_array( $authors ) ) {
		$authors = (array) $authors;
	}

	if( $authors && is_array( $authors ) ){
		foreach ($authors as $author) {
			$args = array(
				get_permalink($author),
				tbm_get_the_post_thumbnail_url($author, array(92, 92)),
				get_the_title($author),
				$role === true ? get_field('ruolo_autore', $author) : ''
			);
			$out[] = vsprintf($template, $args);
		}
	}

	if ( $author_free ) {
		$out[] = $author_free;
	}

	// If authors are more than 2, we need other layout
	if ( count( $out ) > 2 ) {
		return implode( ', ', array_slice( $out, 0, - 1 ) ) . ' e ' . array_slice( $out, - 1 )[0];
	}

	// If authors are 2 or 1
	return __( 'di', 'lifegate' ) . '&nbsp;' . implode( ' ' . __( 'e', 'lifegate' ) . ' ', $out );

}

/**
 * Get the image sizes of one particular content
 *
 * @param string $content The content to query (ie. author, card ecc.)
 * @param string $size The name of the size
 *
 * @return array|string
 */
function lifegate_get_image_size( $content = 'card', $size = '' ) {

	if ( empty( $size ) ) {
		return array( 100, 100 );
	}

	$sizes = array();

	if ( $content === 'card' ) {
		$sizes = array(
			"srcImgFeaturedBig"       => array( 490, 490 ),
			"srcImgFeaturedBigx2"     => array( 980, 980 ),
			"srcImgCardHero"          => array( 490, 326 ),
			"srcImgCardHerox2"        => array( 980, 654 ),
			"srcImgCardSemiHero"      => array( 640, 250 ),
			"srcImgCardSemiHerox2"    => array( 1280, 500 ),
			"srcImgCardBig"           => array( 300, 200 ),
			"srcImgCardBigx2"         => array( 600, 400 ),
			"srcImgCardList"          => array( 148, 200 ),
			"srcImgCardSmall"         => array( 72, 148 ),
			"srcImgPostFeaturedBig"   => array( 640, 320 ),
			"srcImgPostFeaturedBigx2" => array( 1280, 640 ),
		);
	}

	if ( $content === 'author' ) {
		$sizes = array(
			"srcImgHero"   => array( 148, 148 ),
			"srcImgHerox2" => array( 296, 296 ),
			"srcImgBig"    => array( 96, 96 ),
			"srcImgSmall"  => array( 48, 48 ),
		);
	}

	if ( $content === 'relatedTrends' ) {
		$sizes = array(
			"srcImgSlider" => array( 300, 80 ),
		);
	}


	if ( $content === 'gallery' ) {
		$sizes = array(
			"srcImgHero" => array( 148, 148 ),
		);
	}

	if ( $content === 'video' ) {
		$sizes = array(
			"poster"       => array( 1920, 1280 ),
			"poster-media" => array( 1920, 1280 ),
		);
	}

	if ( isset( $sizes[ $size ] ) ) {
		return $sizes[ $size ];
	}

	return false;

}

/**
 * Return the label for cards (speciale, tag taxonomy or category). To use in the loop
 *
 * @param string $class optional Class to add to the returned anchor tag. Default none
 * @param string $type optional Type of the link: name or link. Default link
 *
 * @return bool|string
 */
function lifegate_snippet_label( $class = '', $type = 'link' ) {
	global $post;
	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}
	$out = array();

	$terms = wp_get_object_terms( $post->ID, SITE_SPECIAL_TAXONOMY );

	if ( is_wp_error( $terms ) || empty( $terms ) ) {
		$terms = wp_get_object_terms( $post->ID, 'category' );
	}

	if ( is_wp_error( $terms ) || empty( $terms ) ) {
		$terms = wp_get_object_terms( $post->ID, 'post_tag' );
	}


	if ( ! is_wp_error( $terms ) && ! empty( $terms ) ) {
		foreach ( $terms as $term ) {
			if ( $type == 'link' ) :
				$out[] = '<a class="' . $class . '" title="' . $term->name . '" href ="' . get_term_link( $term->term_id ) . '" > ' . $term->name . '</a >';
			elseif ( $type == 'name' ) :
				$out[] = $term->name;
			endif;
		}

		return join( '', array_slice( $out, 0, 1 ) );
	}

	return false;
}

/**
 * Get the HTML snippet from the sponsor term
 *
 * @param $term
 *
 * @return string
 */
function lifegate_sponsor_badge( $term ) {
	$term = get_term( $term );

	if ( empty( $term ) || is_wp_error( $term ) ) {
		return '';
	}

	/**
	 * Get the sponsor data
	 */
	$sponsor_array = tbm_get_sponsor( $term );

	if ( empty( $sponsor_array ) ) {
		return '';
	}

	/**
	 * Get the html
	 */
	$html = lifegate_html_sponsor_badge( $sponsor_array );

	if ( ! $html ) {
		return '';
	}

	return $html;
}

/**
 * Get the HTML snippet from the sponsored (not sponsor) term
 *
 * @param $sponsor WP Term object relative to the sponsor term
 *
 * @return string
 */
function lifegate_snippet_tax_sponsor_badge( $term, $type = "sponsorlink" ) {

	$term = get_term( $term );


	if ( empty( $term ) || is_wp_error( $term ) ) {
		return '';
	}

	$id = $term->taxonomy . '_' . $term->term_id;

	/**
	 * Get the sponsor term
	 */
	$sponsor_term = get_field( 'post_sponsor', $id );

	if ( ! $sponsor_term ) {
		return '';
	}

	/**
	 * Get the sponsor data
	 */
	$sponsor_array = tbm_get_sponsor( $sponsor_term );


	if($type == "magazinelink"){
		$termlink = lg_term_get_permalink($term);
		$sponsor_array['url'] = $termlink;
	}

	if ( empty( $sponsor_array ) ) {
		return '';
	}

	/**
	 * Get the html
	 */
	$html = lifegate_html_sponsor_badge( $sponsor_array );

	if ( ! $html ) {
		return '';
	}

	return $html;

}

/**
 * @param $term
 * @return bool
 */
function lifegate_tax_has_sponsor( $term ) {

	$term = get_term( $term );

	if ( empty( $term ) || is_wp_error( $term ) ) {
		return false;
	}

	$id = $term->taxonomy . '_' . $term->term_id;

	/**
	 * Get the sponsor term
	 */
	$sponsor_term = get_field( 'post_sponsor', $id );

	if ( ! $sponsor_term ) {
		return false;
	}

	/**
	 * Get the sponsor data
	 */
	$sponsor_array = tbm_get_sponsor( $sponsor_term );

	if ( empty( $sponsor_array ) ) {
		return false;
	}


	return true;
}





/**
 * @param null $post
 * @return bool
 */
function lifegate_post_is_sponsored($post = null){

	$post = get_post( $post );
	if ( empty( $post ) ) {
		return false;
	}

	/**
	 * Get the sponsor term
	 */
	$editorial_field = get_field( 'post_tipologia', $post->ID );
	if ( $editorial_field === 'tbm_ed_sponsored' ) {
		return true;
	}

	if(lifegate_post_has_sponsor($post)) {
		return true;
	}

	return false;
}



/**
 * @param null $post
 * @return bool
 */
function lifegate_post_has_sponsor($post = null){

	$post = get_post( $post );
	if ( empty( $post ) ) {
		return false;
	}

	$id = $post->ID;

	/**
	 * Get the sponsor term
	 */
	$sponsor_term = get_field( 'post_sponsor', $id );

	if ( ! $sponsor_term ) {
		return false;
	}

	$sponsor_array = tbm_get_sponsor( $sponsor_term );

	if ( empty( $sponsor_array ) ) {
		return false;
	}

	return true;
}
function lifegate_original_sponsor_snippet($post, $card = true){

	$post = get_post( $post);
	if ( empty( $post ) ) {
		return '';
	}
	$id = $post->ID;
	$ret = "";
	$sponsor_term = get_field( 'post_sponsor', $id );
	$sponsor_array = tbm_get_sponsor( $sponsor_term );
	if($card)
		$ret .= '<div class="box-labels opa down right sponsorizzato">';
	if ( ! $sponsor_term ) {
		if(lifegate_post_is_sponsored($id)){
			$ret .= __("Articolo sponsorizzato", "lifegate");
		}else{
			return "";
		}
	}else{
		$ret .= 'Sponsorizzato da <b>'.$sponsor_array["title"].'</b>';
	}
	if($card)
		$ret .="</div>";
	return $ret;
}

function lifegate_snippet_post_sponsor_badge( $post = null, $size = "logo") {

	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}

	$id = $post->ID;

	/**
	 * Get the sponsor term
	 */
	$sponsor_term = get_field( 'post_sponsor', $id );

	if ( ! $sponsor_term ) {
		if(lifegate_post_is_sponsored($id)){
			return '<div class="cardpartner"><span>'.__("Articolo sponsorizzato", "lifegate").' </span></div>';
		}else{
			return '';
		}
	}

	/**
	 * Get the sponsor data
	 */
	$sponsor_array = tbm_get_sponsor( $sponsor_term );

	if ( empty( $sponsor_array ) ) {
		return '';
	}

	/**
	 * Get the html
	 */
	$html = lifegate_html_sponsor_badge( $sponsor_array , $size);

	if ( ! $html ) {
		return '';
	}

	return $html;

}

function lifegate_html_sponsor_badge( $sponsor_array, $size = "logo") {

	$name  = '';
	$image = '';

	if ( empty( $sponsor_array['name'] ) && empty( $sponsor_array['image'] ) ) {
		return '';
	}

	if($size == "name") {
		if (!empty($sponsor_array['url'])) {
			$title = empty($sponsor_array['name']) ?  $sponsor_array['title'] : $sponsor_array['name'];
			$name = '<a href="' . $sponsor_array['url'] . '" target="_blank" rel="nofollow sponsored">' . $title . '</a>';
		} else {
			$name = empty($sponsor_array['name']) ? $sponsor_array['title'] : $sponsor_array['name'];
		}
	}

	if($size == "logo") {
		if (!empty($sponsor_array['imageId'])) {
			$image_url = tbm_wp_get_attachment_image_url($sponsor_array['imageId'], array(70, 70));
			$image_url_2x = tbm_wp_get_attachment_image_url($sponsor_array['imageId'], array(140, 140));
			if (!empty($sponsor_array['url'])) {
				$title = empty($sponsor_array['title']) ? '' : $sponsor_array['title'];
				$image = '<a href="' . $sponsor_array['url'] . '" target="_blank" rel="nofollow sponsored"><div class="partner__logo"><figure><img class="lazyload" data-srcset="' . $image_url . ', ' . $image_url_2x . ' 2x" alt="Logo ' . $title . '" title="' . $title . '" /></figure></div></a>';
			} else {
				$image = '<div class="partner__logo"><figure><img class="lazyload" data-srcset="' . $image_url . ', ' . $image_url_2x . ' 2x" alt="Logo ' . $sponsor_array['title'] . '" title="' . $sponsor_array['title'] . '"/></figure></div>';
			}
		}
	}


	if ( isset( $sponsor_array['type'] ) && $sponsor_array['type'] === 'tbm_sponsor_partner' ) {
		if ( function_exists( 'pll__' ) ) {
			$html = "<div class=\"cardpartner\"><span>" . pll__( 'a cura di', 'lifegate' ) . " %s </span>%s</div>";
		} else {
			$html = "<div class=\"cardpartner\"><span>" . __( 'a cura di', 'lifegate' ) . " %s </span>%s</div>";
		}

	} else {
		if ( function_exists( 'pll__' ) ) {
			$html = "<div class=\"cardpartner\"><span>" . pll__( 'sponsorizzato da', 'lifegate' ) . " %s </span>%s</div>";
		} else {
			$html = "<div class=\"cardpartner\"><span>" . __( 'sponsorizzato da', 'lifegate' ) . " %s </span>%s</div>";
		}
	}

	return sprintf( $html, $name, $image );
}

/**
 * The taxonomy this term is part of.
 *
 * @param string $taxonomy_name Taxonomy name for the term.
 * @param int $post_id Post ID for the term.
 */
function lifegate_get_primary_term( $taxonomy_name = 'category', $post ) {

	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}

	$post_id = $post->ID;

	$terms = get_the_terms( $post_id, $taxonomy_name );

	// Check if post has a tax term assigned.
	if ( ! $terms || is_wp_error( $terms ) ) {
		return null;
	}

	if ( ! class_exists( 'WPSEO_Primary_Term' ) ) {
		return reset( $terms );
	}


	// Show the post's 'Primary' term.
	// Check that the feature is available and that a primary term is set.
	$wpseo_primary_term = new WPSEO_Primary_Term( $taxonomy_name, $post_id );
	$wpseo_primary_term = $wpseo_primary_term->get_primary_term();

	// Set the term object.
	$primary_term = get_term( $wpseo_primary_term );
	if ( ! $primary_term || is_wp_error( $primary_term ) ) {
		$primary_term = reset( $terms );
	}

	return $primary_term;

}

/**
 * Get the term permalink
 *
 * @param int|WP_Term $term
 *
 * @return string|WP_Error
 */
function lg_term_get_permalink( $term ) {
	$term = get_term( $term );
	if ( empty( $term ) || is_wp_error( $term ) ) {
		return '';
	}

	return get_term_link( $term );

}

/**
 * Get the term title
 *
 * @param int|WP_Term $term
 *
 * @return string|WP_Error
 */
function lg_term_the_title( $term ) {
	$term = get_term( $term );
	if ( empty( $term ) || is_wp_error( $term ) ) {
		return '';
	}

	$id = $term->taxonomy . '_' . $term->term_id;

	if ( get_field( 'tbm_special_title', $id ) ) {
		return get_field( 'tbm_special_title', $id );
	}

	return $term->name;
}

/**
 * Get the term taxonomy Name
 *
 * @param int|WP_Term $term
 *
 * @return string|WP_Error
 */
function lg_term_the_taxonomy_name( $term ) {
	$term = get_term( $term );
	if ( empty( $term ) || is_wp_error( $term ) ) {
		return '';
	}

	$taxonomy = get_taxonomy( $term->taxonomy );

	return $taxonomy->labels->singular_name;
}

/**
 * Get the term description
 *
 * @param int|WP_Term $term
 *
 * @return string|WP_Error
 */
function lg_term_the_excerpt( $term ) {
	$term = get_term( $term );
	if ( empty( $term ) || is_wp_error( $term ) ) {
		return '';
	}

	return wp_strip_all_tags( $term->description );
}

/**
 * Get the term post count
 *
 * @param int|WP_Term $term
 *
 * @return int|WP_Error
 */
function lg_term_the_count( $term ) {
	$term = get_term( $term );
	if ( empty( $term ) || is_wp_error( $term ) ) {
		return '';
	}

	return $term->count;
}

/**
 * Get the term custom field image
 *
 * @param int|WP_Term $term
 *
 * @return string|WP_Error
 */
function lg_term_the_image_id( $term ) {
	$term = get_term( $term );
	if ( empty( $term ) || is_wp_error( $term ) ) {
		return '';
	}

	$id = $term->taxonomy . '_' . $term->term_id;

	return get_field( 'tbm_special_featured_img', $id );

}

/**
 * Get the day name from day number
 *
 * @param int $i The number of the day (1 = 'monday')
 *
 * @return string The name of the day
 */
function lg_get_the_day( $i = 0 ) {
	if ( empty( $i ) ) {
		return '';
	}

	return date_i18n( 'l', strtotime( "Sunday + $i Days" ) );
}


/**
 * Get the VIDEO of mediacase
 *
 * @return int|mixed|string|null
 */
function mediacase_video_url($item = null) {
	global $post;

	if($item) $post = get_post($item);
	if ( empty( $post ) ) {
		return '';
	}

	$is_video = get_field("is_video", $post);
	if(!$is_video)
		return false;

	$url_video_youtube__vimeo = get_field("url_video_youtube__vimeo", $post);
	if(!$url_video_youtube__vimeo)
		return false;


	$embed_code = wp_oembed_get( $url_video_youtube__vimeo);
	return $embed_code;
}


/**
 * calcolo il tempo di lettura
 * @param $content
 * @return false|float|string
 */
function lg_reading_time($content = ""){

	$wpm = 183;

	if(!$content){
		$content = get_the_content();
	}

	$number_of_images = substr_count( strtolower( $content ), '<img ' );

	$content = wp_strip_all_tags( $content );
	$word_count = count( preg_split( '/\s+/', $content ) );
	$additional_words_for_images = lg_calculate_images( $number_of_images, $wpm );
	$word_count  += $additional_words_for_images;

	$reading_time = $word_count / 183;



	if ( 1 > $reading_time ) {
		$reading_time =  '< 1';
	} else {
		$reading_time = ceil( $reading_time );
	}

	return $reading_time;
}

/**
 * @param $total_images
 * @param $wpm
 * @return float|int
 */
function lg_calculate_images( $total_images, $wpm ) {
	$additional_time = 0;
	// For the first image add 12 seconds, second image add 11, ..., for image 10+ add 3 seconds.
	for ( $i = 1; $i <= $total_images; $i++ ) {
		if ( $i >= 10 ) {
			$additional_time += 3 * (int) $wpm / 60;
		} else {
			$additional_time += ( 12 - ( $i - 1 ) ) * (int) $wpm / 60;
		}
	}
	return $additional_time;
}

