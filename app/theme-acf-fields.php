<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 23/04/2018
 * Time: 22:25
 */

function tbm_get_trend_link() {
	$link = '';

	if ( isset( $_GET['tag_ID'] ) && isset( $_GET['taxonomy'] ) ) {
		$link = get_term_link( $_GET['tag_ID'], $_GET['taxonomy'] );
	}

	if ( ! empty( $link ) && ! is_wp_error( $link ) ) {
		return '<a href="' . $link . '">Link</a>';
	}


	return '<em>Salvare prima lo Speciale </em>';

}

function tbm_get_gf_form() {

	$choices = array();
	//Show notice if Gravity Forms is not activated
	if ( ! class_exists( 'RGFormsModel' ) ) {
		return '';
	}
	$forms = RGFormsModel::get_forms( 1 );

	//Prevent undefined variable notice
	if ( isset( $forms ) ) {
		foreach ( $forms as $form ) {
			$choices[ intval( $form->id ) ] = ucfirst( $form->title );
		}
	}

	return $choices;
}

if ( function_exists( 'acf_add_local_field_group' ) ) {

	/**
	 * Useful URL in Post
	 */
	if ( isset( $_GET['post'] ) && get_post( $_GET['post'] ) ) {
		$amp_url = get_permalink($_GET['post']) . '?amp';
		if ( $amp_url ) {
			$private_amp_key = new TbmCommon\System\AmpUpdate( ROOT_DIR . '/amp-cache-private-key.pem' );
			$url             = $private_amp_key->getGoogleUrl( $amp_url );
			acf_add_local_field_group( array(
				'key'                   => 'group_5ee68183df978',
				'title'                 => 'Useful links',
				'fields'                => array(
					array(
						'key'               => 'field_5ee6818d4bdba',
						'label'             => 'AMP Clear Cache',
						'name'              => '',
						'type'              => 'message',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'message'           => $url,
						'new_lines'         => 'wpautop',
						'esc_html'          => 0,
					),
				),
				'location'              => array(
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'post',
						),
					),
				),
				'menu_order'            => 0,
				'position'              => 'normal',
				'style'                 => 'default',
				'label_placement'       => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen'        => '',
				'active'                => true,
				'description'           => '',
				'recaptcha'             => 0,
			) );
		}
	}


	/**
	 * Agisci ora
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5e5b93cf58977',
		'title'                 => 'Agisci ora',
		'fields'                => array(
			array(
				'key'               => 'field_5e5b93d55696a',
				'label'             => __( 'Titolo', 'lifegate' ),
				'name'              => 'tbm_act_now_title',
				'type'              => 'text',
				'instructions'      => '',
				'required'          => 1,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5e5b93ed5696b',
				'label'             => __( 'Sottotitolo', 'lifegate' ),
				'name'              => 'tbm_act_now_subtitle',
				'type'              => 'textarea',
				'instructions'      => '',
				'required'          => 1,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5e5b93fc5696c',
				'label'             => __( 'Cta (solo desktop)', 'lifegate' ),
				'name'              => 'tbm_act_now_cta',
				'type'              => 'text',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => 'Agisci ora!',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '60',
			),
			array(
				'key'               => 'field_5e5b94135696d',
				'label'             => __( 'Link', 'lifegate' ),
				'name'              => 'tbm_act_now_url',
				'type'              => 'url',
				'instructions'      => '',
				'required'          => 1,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
			),
			array(
				'key'               => 'field_5e5b94465696e',
				'label'             => __( 'Sfondo', 'lifegate' ),
				'name'              => 'tbm_act_now_background',
				'type'              => 'image',
				'instructions'      => 'Immagine da usare come sfondo. Almeno 1370 px di larghezza, 340 px di altezza',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'return_format'     => 'id',
				'preview_size'      => 'medium',
				'library'           => 'all',
				'min_width'         => 1370,
				'min_height'        => 340,
				'min_size'          => '',
				'max_width'         => '',
				'max_height'        => '',
				'max_size'          => 1,
				'mime_types'        => '',
			),
			array(
				'key'               => 'field_5ed24b9225a6f',
				'label'             => 'Colore del font',
				'name'              => 'tbm_act_now_font_color',
				'type'              => 'color_picker',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'translations'      => 'copy_once',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'widget',
					'operator' => '==',
					'value'    => 'act_now_widget',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
	) );

	/**
	 * Iniziative
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5e5bfe9ebf700',
		'title'                 => 'Iniziative',
		'fields'                => array(
			array(
				'key'               => 'field_5e5c04d8b26f9',
				'label'             => __( 'Strip', 'lifegate' ),
				'name'              => '',
				'type'              => 'message',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'message'           => 'Questi campi servono per creare la strip: una fascia di tre elementi pubblicata a fine post. Per poter essere pubblicata, la strip ha bisogno di tutti e tre gli elementi. In mancanza di uno, non verrà pubblicato nulla.',
				'new_lines'         => 'wpautop',
				'esc_html'          => 0,
			),
			array(
				'key'               => 'field_5e5b94438374sfe',
				'label'             => __( 'Sfondo Strip', 'lifegate' ),
				'name'              => 'tbm_iniziative_background',
				'type'              => 'image',
				'instructions'      => 'Immagine da usare come sfondo della strip',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'return_format'     => 'id',
				'preview_size'      => 'medium',
				'library'           => 'all',
				'min_width'         => 1920,
				'min_height'        => 600,
				'min_size'          => '',
				'max_width'         => '',
				'max_height'        => '',
				'max_size'          => 1,
				'mime_types'        => '',
			),
			array(
				'key'               => 'field_5e5bfef122aad',
				'label'             => __( 'Titolo Strip iniziative sx', 'lifegate' ),
				'name'              => 'tbm_iniziative_strip_sx_title',
				'type'              => 'text',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '50',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'maxlength'         => '',
				'rows'              => 4,
				'new_lines'         => '',
			),
			array(
				'key'               => 'field_5e5bff5d22aae',
				'label'             => __( 'Titolo Strip iniziative dx', 'lifegate' ),
				'name'              => 'tbm_iniziative_strip_dx_title',
				'type'              => 'text',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '50',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'maxlength'         => '',
				'rows'              => 4,
				'new_lines'         => '',
			),
			array(
				'key'               => 'field_5e5bfef188aad',
				'label'             => __( 'Strip iniziative sx', 'lifegate' ),
				'name'              => 'tbm_iniziative_strip_sx',
				'type'              => 'textarea',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '50',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'maxlength'         => '',
				'rows'              => 4,
				'new_lines'         => '',
			),
			array(
				'key'               => 'field_5e5bff5d88aae',
				'label'             => __( 'Strip iniziative dx', 'lifegate' ),
				'name'              => 'tbm_iniziative_strip_dx',
				'type'              => 'textarea',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '50',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'maxlength'         => '',
				'rows'              => 4,
				'new_lines'         => '',
			),
			array(
				'key'               => 'field_5e5bfea588aac',
				'label'             => __( 'Form di contatto', 'lifegate' ),
				'name'              => 'tbm_iniziative_gf_form',
				'instructions'      => 'Scegli il form di contatto da aggiungere alla fine della pagina',
				'required'          => 0,
				'conditional_logic' => 0,
				'choices'           => tbm_get_gf_form(),
				'default_value'     => array(),
				'allow_null'        => 1,
				'multiple'          => 0,
				'ui'                => 1,
				'ajax'              => 0,
				'return_format'     => 'value',
				'translations'      => 'copy_once',
				'placeholder'       => '',
				'type'              => 'select',
			),
			array(
				'key'               => 'field_5e888f699sf51',
				'label'             => __( 'Agisci ora url', 'lifegate' ),
				'name'              => 'single_iniziative_act_now',
				'type'              => 'url',
				'instructions'      => __( 'Indicare l\'url per il link "Ägisci ora"', 'lifegate' ),
				'required'          => 0,
				'conditional_logic' => 0,
			),
			array(
				'key'               => 'field_5e5bff9988aaf',
				'label'             => __( 'Conclusioni', 'lifegate' ),
				'name'              => 'tbm_iniziative_footer',
				'type'              => 'wysiwyg',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'iniziative',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
	) );

	/**
	 * Trend
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5a5e0d9a88e18',
		'title'                 => 'Speciale LifeGate',
		'fields'                => array(
			array(
				'key'               => 'field_5a5a03717cdw4',
				'label'             => __( 'No ADV', 'lifegate' ),
				'name'              => 'adv_disabled',
				'type'              => 'true_false',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '33',
					'class' => '',
					'id'    => '',
				),
				'message'           => '',
				'default_value'     => 0,
				'ui'                => 1,
				'ui_on_text'        => '',
				'ui_off_text'       => '',
			),
			array(
				'key'               => 'field_5a7b136b1efa7',
				'label'             => __( 'Sponsor', 'lifegate' ),
				'name'              => 'post_sponsor',
				'type'              => 'taxonomy',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'taxonomy'          => 'sponsor',
				'field_type'        => 'select',
				'allow_null'        => 1,
				'add_term'          => 0,
				'save_terms'        => 0,
				'load_terms'        => 0,
				'return_format'     => 'object',
				'multiple'          => 0,
			),
			array(
				'key'               => 'field_5e8dfa40dxxsd',
				'label'             => __( 'Scegli la tipologia di Trend', 'lifegate' ),
				'name'              => 'tbm_special_type',
				'type'              => 'radio',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'choices'           => array(
					'V0' => 'Versione 0',
					'V1' => 'Versione 1'
				),
				'allow_null'        => 0,
				'other_choice'      => 0,
				'default_value'     => '',
				'layout'            => 'horizontal',
				'return_format'     => 'value',
				'save_other_choice' => 0,
			),
			array(
				'key'               => 'field_5e5b93ddf786shdg',
				'label'             => __( 'Titolo', 'lifegate' ),
				'name'              => 'tbm_special_title',
				'type'              => 'text',
				'instructions'      => '',
				'required'          => 1,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_5e8dfa40dxxsd',
							'operator' => '==',
							'value'    => 'v1',
						),
					),
					array(
						array(
							'field'    => 'field_5e8dfa40dxxsd',
							'operator' => '==',
							'value'    => 'v2',
						),
					),
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5a5e0ff9d4386',
				'label'             => __( 'Contenuto', 'lifegate' ),
				'name'              => 'tbm_special_enrichment',
				'type'              => 'wysiwyg',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_5e8dfa40dxxsd',
							'operator' => '==',
							'value'    => 'v1',
						),
					),
					array(
						array(
							'field'    => 'field_5e8dfa40dxxsd',
							'operator' => '==',
							'value'    => 'v2',
						),
					),
				),
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'tabs'              => 'all',
				'toolbar'           => 'full',
				'media_upload'      => 0,
				'delay'             => 1,
			),
			array(
				'key'               => 'field_5a5e1080e76ff',
				'label'             => __( 'Immagine in evidenza', 'lifegate' ),
				'name'              => 'tbm_special_featured_img',
				'type'              => 'image',
				'instructions'      => 'Immagine da usare come apertura. Almeno 1920 px di larghezza, 600px di altezza',
				'required'          => 1,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_5e8dfa40dxxsd',
							'operator' => '==',
							'value'    => 'v1',
						),
					),
					array(
						array(
							'field'    => 'field_5e8dfa40dxxsd',
							'operator' => '==',
							'value'    => 'v2',
						),
					),
				),
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'return_format'     => 'id',
				'preview_size'      => 'medium',
				'library'           => 'all',
				'min_width'         => 1920,
				'min_height'        => 600,
				'min_size'          => '',
				'max_width'         => '',
				'max_height'        => '',
			),
			array(
				'key'               => 'field_5a5e120sde33ers',
				'label'             => __( 'Post in primo piano', 'lifegate' ),
				'name'              => 'tbm_special_high_posts',
				'type'              => 'relationship',
				'instructions'      => 'Elementi in primo piano. Al massimo 4 (richiesto)',
				'required'          => 1,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_5e8dfa40dxxsd',
							'operator' => '==',
							'value'    => 'v2',
						),
					),
				),
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'taxonomy'          => array(),
				'filters'           => array(
					0 => 'search',
					1 => 'post_type',
				),
				'elements'          => '',
				'min'               => 0,
				'max'               => 4,
				'return_format'     => 'id',
			),
			array(
				'key'               => 'field_5a5e120sde45rtf',
				'label'             => __( 'Post footer', 'lifegate' ),
				'name'              => 'tbm_special_featured_posts',
				'type'              => 'relationship',
				'instructions'      => 'Elementi in primo piano. Al massimo 2 (richiesto)',
				'required'          => 1,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_5e8dfa40dxxsd',
							'operator' => '==',
							'value'    => 'v1',
						),
					),
					array(
						array(
							'field'    => 'field_5e8dfa40dxxsd',
							'operator' => '==',
							'value'    => 'v2',
						),
					),
				),
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'taxonomy'          => array(),
				'filters'           => array(
					0 => 'search',
					1 => 'post_type',
					2 => 'taxonomy'
				),
				'elements'          => '',
				'min'               => 0,
				'max'               => 2,
				'return_format'     => 'id',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'taxonomy',
					'operator' => '==',
					'value'    => 'trend',
				),
			)
		),
		'menu_order'            => 0,
		'position'              => 'acf_after_title',
		'style'                 => 'seamless',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => 'Speciale',
		'recaptcha'             => 0,
	) );

	/**
	 * Category
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5a5e0d9z88x18',
		'title'                 => 'Categorie',
		'fields'                => array(
			array(
				'key' => 'field_5f01b609e0c50',
				'label' => 'Trend correlati',
				'name' => 'tbm_category_trend',
				'type' => 'taxonomy',
				'instructions' => 'Seleziona i trend che desideri associare a questa categoria',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'taxonomy' => 'trend',
				'field_type' => 'multi_select',
				'allow_null' => 0,
				'add_term' => 0,
				'save_terms' => 0,
				'load_terms' => 0,
				'return_format' => 'id',
				'translations' => 'ignore',
				'multiple' => 0,
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'taxonomy',
					'operator' => '==',
					'value'    => 'category',
				),
			)
		),
		'menu_order'            => 0,
		'position'              => 'acf_after_title',
		'style'                 => 'seamless',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => 'Speciale',
		'recaptcha'             => 0,
	) );

	/**
	 * Magazine
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5a5e0d9a8sdh6',
		'title'                 => 'Magazine LifeGate',
		'fields'                => array(
			array(
				'key'               => 'lg_mag_5ea5cdb8b0434',
				'label'             => __( 'Link del magazine', 'lifegate' ),
				'name'              => '',
				'type'              => 'message',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'message'           => 'Link allo speciale: ' . tbm_get_trend_link(),
				'new_lines'         => 'wpautop',
				'esc_html'          => 0,
			),
			array(
				'key'               => 'lg_mag_5a5a03717cdw4',
				'label'             => __( 'No ADV', 'lifegate' ),
				'name'              => 'adv_disabled',
				'type'              => 'true_false',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '33',
					'class' => '',
					'id'    => '',
				),
				'message'           => '',
				'default_value'     => 0,
				'ui'                => 1,
				'ui_on_text'        => '',
				'ui_off_text'       => '',
			),
			array(
				'key'               => 'lg_mag_5a7b136b1efa7',
				'label'             => __( 'Sponsor', 'lifegate' ),
				'name'              => 'post_sponsor',
				'type'              => 'taxonomy',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'taxonomy'          => 'sponsor',
				'field_type'        => 'select',
				'allow_null'        => 1,
				'add_term'          => 0,
				'save_terms'        => 0,
				'load_terms'        => 0,
				'return_format'     => 'object',
				'multiple'          => 0,
			),
			array(
				'key'          => 'lg_mag_5e5b93ddf786shdg',
				'label'        => 'Titolo',
				'name'         => 'tbm_magazine_title',
				'type'         => 'text',
				'instructions' => '',
				'required'     => 1,

				'default_value' => '',
				'placeholder'   => '',
				'prepend'       => '',
				'append'        => '',
				'maxlength'     => '',
			),
			array(
				'key'          => 'lg_mag_5a5e0ff9d4386',
				'label'        => 'Contenuto',
				'name'         => 'tbm_magazine_enrichment',
				'type'         => 'wysiwyg',
				'instructions' => '',
				'required'     => 0,

				'wrapper'       => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value' => '',
				'tabs'          => 'all',
				'toolbar'       => 'full',
				'media_upload'  => 0,
				'delay'         => 1,
			),
			array(
				'key'          => 'lg_mag_5a5e1080e76ff',
				'label'        => 'Immagine in evidenza',
				'name'         => 'tbm_special_featured_img',
				'type'         => 'image',
				'instructions' => 'Immagine da usare come apertura. Almeno 1370 px di larghezza, 340 px di altezza',
				'required'     => 1,

				'wrapper'       => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'return_format' => 'id',
				'preview_size'  => 'medium',
				'library'       => 'all',
				'min_width'     => 1370,
				'min_height'    => 340,
				'min_size'      => '',
				'max_width'     => '',
				'max_height'    => '',
			),
			array(
				'key'          => 'lg_mag_5a5e120sde33ers',
				'label'        => 'Post in primo piano',
				'name'         => 'tbm_magazine_high_posts',
				'type'         => 'relationship',
				'instructions' => 'Elementi in primo piano. Al massimo 4 (richiesto)',
				'required'     => 1,

				'wrapper'       => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'taxonomy'      => array(),
				'filters'       => array(
					0 => 'search',
					1 => 'post_type',
					2 => 'taxonomy'
				),
				'elements'      => '',
				'min'           => 4,
				'max'           => 4,
				'return_format' => 'id',
			),
			array(
				'key'          => 'lg_mag_5a5e120sde45rtf',
				'label'        => 'Post footer',
				'name'         => 'tbm_magazine_featured_posts',
				'type'         => 'relationship',
				'instructions' => 'Elementi in primo piano. Al massimo 2 (richiesto)',
				'required'     => 1,

				'wrapper'       => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'taxonomy'      => array(),
				'filters'       => array(
					0 => 'search',
					1 => 'post_type',
					2 => 'taxonomy'
				),
				'elements'      => '',
				'min'           => 0,
				'max'           => 2,
				'return_format' => 'id',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'taxonomy',
					'operator' => '==',
					'value'    => 'magazine',
				),
			)
		),
		'menu_order'            => 0,
		'position'              => 'acf_after_title',
		'style'                 => 'seamless',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => 'Speciale',
		'recaptcha'             => 0,
	) );

	/**
	 * Shows
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5e888f1a0w023',
		'title'                 => 'Campi Programmi',
		'fields'                => array(
			array(
				'key'               => 'field_5e888f249fer5',
				'label'             => __( 'ID programma su Spotify', 'lifegate' ),
				'name'              => 'lg_show_spotify_id',
				'type'              => 'text',
				'instructions'      => 'Indicare l\'id della Playlist su Spotify (deve essere una Playlist e ha precedenza su Spreaker)',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '50',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'min'               => '',
				'max'               => '',
				'step'              => '',
			),
			array(
				'key'               => 'field_5e888f249dac1',
				'label'             => __( 'ID programma su Spreaker', 'lifegate' ),
				'name'              => 'lg_show_spreaker_id',
				'type'              => 'number',
				'instructions'      => 'Indicare l\'id che ha il programma su Spreaker',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '50',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'min'               => '',
				'max'               => '',
				'step'              => '',
			),
			array(
				'key'               => 'field_5e888f699csd3',
				'label'             => __( 'Immagine del programma', 'lifegate' ),
				'name'              => 'lg_show_image',
				'type'              => 'url',
				'instructions'      => 'Indicare l\'url dell\'immagine. Se vuoto il sistema proverà a recuperarla da Spreaker',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '60',
					'class' => '',
					'id'    => '',
				),
			),
			array(
				'key'               => 'field_5e89ad093937f',
				'label'             => __( 'Orario', 'lifegate' ),
				'name'              => 'lg_show_time',
				'type'              => 'repeater',
				'instructions'      => 'Imposta l\'orario del programma',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '40',
					'class' => '',
					'id'    => '',
				),
				'collapsed'         => 'field_5e89ae1039380',
				'min'               => 0,
				'max'               => 0,
				'layout'            => 'table',
				'button_label'      => 'Aggiungi orario',
				'sub_fields'        => array(
					array(
						'key'               => 'field_5e89ae1039380',
						'label'             => __( 'Giorno', 'lifegate' ),
						'name'              => 'lg_show_time_day',
						'type'              => 'select',
						'instructions'      => '',
						'required'          => 1,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '75',
							'class' => '',
							'id'    => '',
						),
						'choices'           => array(
							1 => 'Lunedì',
							2 => 'Martedì',
							3 => 'Mercoledì',
							4 => 'Giovedì',
							5 => 'Venerdì',
							6 => 'Sabato',
							7 => 'Domenica',
						),
						'default_value'     => array(),
						'allow_null'        => 0,
						'multiple'          => 0,
						'ui'                => 1,
						'ajax'              => 0,
						'return_format'     => 'value',
						'placeholder'       => '',
					),
					array(
						'key'               => 'field_5e89aee339381',
						'label'             => __( 'Ora', 'lifegate' ),
						'name'              => 'lg_show_time_hour',
						'type'              => 'time_picker',
						'instructions'      => '',
						'required'          => 1,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '25',
							'class' => '',
							'id'    => '',
						),
						'display_format'    => 'H:i',
						'return_format'     => 'H:i',
					),
				),
			),

		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'show',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
	) );

	/**
	 * Rassegna stampa
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5ed4373a80632',
		'title'                 => 'Rassegna stampa',
		'fields'                => array(
			array(
				'key'               => 'field_5ed4377cec16f',
				'label'             => 'Anno Rassegna Stampa',
				'name'              => 'tbm_anno_rassegna_stampa',
				'type'              => 'number',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '30',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'min'               => '',
				'max'               => '',
				'step'              => '',
				'translations'      => 'sync',
			),
			array(
				'key'               => 'field_5ed437a8ec170',
				'label'             => 'Allegato',
				'name'              => 'tbm_allegato_rassegna_stampa',
				'type'              => 'file',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '70',
					'class' => '',
					'id'    => '',
				),
				'return_format'     => 'url',
				'library'           => 'all',
				'min_size'          => '',
				'max_size'          => '',
				'mime_types'        => '',
				'translations'      => 'sync',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'rassegna_stampa',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
		'recaptcha'             => 0,
	) );

	/**
	 * Autori
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5e9c80b8611e5',
		'title'                 => 'Autori',
		'fields'                => array(
			array(
				'key'               => 'field_5e9c80f9a03ca',
				'label'             => __( 'Nome Autore', 'lifegate' ),
				'name'              => 'nome_autore',
				'type'              => 'text',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '50',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5e9c8176a03cb',
				'label'             => __( 'Cognome Autore', 'lifegate' ),
				'name'              => 'cognome_autore',
				'type'              => 'text',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '50',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5e9c818ca03cc',
				'label'             => __( 'Email Autore', 'lifegate' ),
				'name'              => 'email_autore',
				'type'              => 'text',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '40',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5e9c81a6a03cd',
				'label'             => __( 'Ruolo Autore', 'lifegate' ),
				'name'              => 'ruolo_autore',
				'type'              => 'text',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '40',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5a5a03717cdfg7',
				'label'             => __( 'Nascondi in "Chi Siamo"', 'lifegate' ),
				'name'              => 'tbm_author_hide',
				'type'              => 'true_false',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '20',
					'class' => '',
					'id'    => '',
				),
				'message'           => '',
				'default_value'     => 0,
				'ui'                => 1,
				'ui_on_text'        => '',
				'ui_off_text'       => '',
			),
			array(
				'key'               => 'field_5e9c8223a0eee',
				'label'             => __( 'Esperienza autore', 'lifegate' ),
				'name'              => 'esperienza_autore',
				'type'              => 'wysiwyg',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'tabs'              => 'all',
				'toolbar'           => 'basic',
				'media_upload'      => 1,
				'delay'             => 1,
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'autori',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
	) );

	if( function_exists('acf_add_local_field_group') ):

		acf_add_local_field_group(array(
			'key' => 'group_60a651d77d9cb',
			'title' => 'Social Autore',
			'fields' => array(
				array(
					'key' => 'field_60a6520bab577',
					'label' => 'Social',
					'name' => 'social',
					'type' => 'repeater',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'collapsed' => '',
					'min' => 0,
					'max' => 0,
					'layout' => 'table',
					'button_label' => 'Aggiungi social',
					'sub_fields' => array(
						array(
							'key' => 'field_60a65220ab578',
							'label' => 'Nome social',
							'name' => 'nome_social',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
							'translations' => 'translate',
						),
						array(
							'key' => 'field_60a6522fab579',
							'label' => 'Link Social',
							'name' => 'link_social',
							'type' => 'url',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'translations' => 'copy_once',
						),
					),
				),
			),
			'location' => array(
				array(
					array(
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'autori',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => true,
			'description' => '',
		));

	endif;

	/**
	 * Pagina newsletter
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5eb6e0ca457a0',
		'title'                 => 'Campi Newsletter',
		'fields'                => array(
			array(
				'key'               => 'field_5eb6e0cfe4557',
				'label'             => __( 'Immagine di sfondo', 'lifegate' ),
				'name'              => 'tbm_newsletter_background_image',
				'type'              => 'image',
				'instructions'      => 'Imposta qui l\'immagine di sfondo per la pagina di iscrizione alla newsletter. Almeno 1920×1080 pixel',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'return_format'     => 'url',
				'preview_size'      => 'medium',
				'library'           => 'all',
				'min_width'         => 1920,
				'min_height'        => 1080,
				'min_size'          => '',
				'max_width'         => 3840,
				'max_height'        => 2160,
				'max_size'          => '',
				'mime_types'        => 'jpg,jpeg,png,webm',
				'translations'      => 'copy_once',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'page',
					'operator' => '==',
					'value'    => 'views/page-newsletter.blade.php',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
		'recaptcha'             => 0,
	) );

	/**
	 * Pagina Chi siamo
	 */
	acf_add_local_field_group( array(
		'key'    => 'group_5eb6e0cawq34q',
		'title'  => 'Pagina Chi Siamo',
		'fields' => array(
			array(
				'key'               => 'field_5ed0ee56642f2',
				'label'             => __( 'Autori in primo piano', 'lifegate' ),
				'name'              => 'tbm_author_order',
				'type'              => 'post_object',
				'instructions'      => 'Gli autori vengono ordinati per ordine alfabetico (cognome). In questo campo vanno indicati e ordinati gli autori che si desidera compaiano in testa alla lista.',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'post_type'         => array(
					0 => 'autori',
				),
				'taxonomy'          => '',
				'allow_null'        => 1,
				'multiple'          => 1,
				'return_format'     => 'id',
				'translations'      => 'copy_once',
				'ui'                => 1,
			),
		),

		'location'              => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'views/page-chisiamo.blade.php',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
		'recaptcha'             => 0,
	) );

	/**
	 * Pagina richiesta titolo
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5eb6e0caas938',
		'title'                 => 'Campi Richiesta titolo',
		'fields'                => array(
			array(
				'key'               => 'field_5eb6e0cfes938',
				'label'             => __( 'Immagine di sfondo', 'lifegate' ),
				'name'              => 'tbm_title_request_background_image',
				'type'              => 'image',
				'instructions'      => 'Imposta qui l\'immagine di sfondo per la pagina di iscrizione alla newsletter. Almeno 1920×1080 pixel',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'return_format'     => 'url',
				'preview_size'      => 'medium',
				'library'           => 'all',
				'min_width'         => 1920,
				'min_height'        => 1080,
				'min_size'          => '',
				'max_width'         => 3840,
				'max_height'        => 2160,
				'max_size'          => '',
				'mime_types'        => 'jpg,jpeg,png,webm',
				'translations'      => 'copy_once',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'page',
					'operator' => '==',
					'value'    => 'views/page-richiestatitoli.blade.php',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
		'recaptcha'             => 0,
	) );

	/**
	 * Pagina Come Ascoltare
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5ed28367ce808',
		'title'                 => 'Campi Come Ascoltare',
		'fields'                => array(
			array(
				'key'               => 'field_5ed28372d0949',
				'label'             => __( 'Blocchi testuali', 'lifegate' ),
				'name'              => 'tbm_text_howtolisten_blocks',
				'type'              => 'repeater',
				'instructions'      => 'Aggiungi i blocchi di testo e scegli una tra le icone disponibili',
				'required'          => 1,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'collapsed'         => '',
				'min'               => 3,
				'max'               => 10,
				'layout'            => 'table',
				'button_label'      => 'Aggiungi Blocco Testuale',
				'sub_fields'        => array(
					array(
						'key'               => 'field_5ed283c4d094a',
						'label'             => __( 'Testo', 'lifegate' ),
						'name'              => 'block_text',
						'type'              => 'textarea',
						'instructions'      => '',
						'required'          => 1,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '50',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'placeholder'       => '',
						'maxlength'         => '',
						'rows'              => 2,
						'new_lines'         => '',
						'translations'      => 'translate',
					),
					array(
						'key'               => 'field_5eeb9413523ksj',
						'label'             => __( 'URL per link', 'lifegate' ),
						'name'              => 'block_url',
						'type'              => 'url',
						'instructions'      => '',
						'required'          => 1,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '40',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'placeholder'       => '',
					),
					array(
						'key'               => 'field_5ed283f4d094b',
						'label'             => __( 'Icona', 'lifegate' ),
						'name'              => 'block_icon',
						'type'              => 'select',
						'instructions'      => '',
						'required'          => 1,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '10',
							'class' => '',
							'id'    => '',
						),
						'choices'           => array(
							'wifi'    => 'WiFi',
							'map'     => 'Mappa',
							'player'  => 'Play',
							'phone'   => 'Telefono',
							'ios'     => 'App Apple',
							'android' => 'App Android',
							'spotify' => 'Spotify',
						),
						'default_value'     => array(),
						'allow_null'        => 0,
						'multiple'          => 0,
						'ui'                => 1,
						'ajax'              => 0,
						'return_format'     => 'value',
						'translations'      => 'copy_once',
						'placeholder'       => '',
					),
				),
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'views/page-comeascoltare.blade.php',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
		'recaptcha'             => 0,
	) );

}

/**
 * cta amp su categoria
 */
if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_6045fce7ce8e4',
		'title' => 'CTA',
		'fields' => array(
			array(
				'key' => 'field_4445fcf298647',
				'label' => 'Titolo di dettaglio',
				'name' => 'titolo_dettaglio',
				'type' => 'text',
				'instructions' => 'Testo che se valorizzato sostituisce il nome del tag nel dettaglio della tassonomia.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'delay' => 0,
				'translations' => 'translate',
			),
			array(
				'key' => 'field_6045fcf298647',
				'label' => 'AMP CTA',
				'name' => 'amp_cta',
				'type' => 'wysiwyg',
				'instructions' => 'Testo e link da mostrare in fondo agli articoli AMP',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
				'delay' => 0,
				'translations' => 'translate',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'taxonomy',
					'operator' => '==',
					'value' => 'category',
				)
			),
			array(
				array(
					'param' => 'taxonomy',
					'operator' => '==',
					'value' => 'post_tag',
				)
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

endif;
if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_608142ae96dd7',
		'title' => 'ADK Domination',
		'fields' => array(
			array(
				'key' => 'field_608142b7c2a5e',
				'label' => 'ADK Domination',
				'name' => 'adk_domination',
				'type' => 'text',
				'instructions' => 'Inserisci la domination concordata con adkaora',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'translations' => 'translate',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
				),
			),
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
				),
			),
		),
		'menu_order' => 100,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

endif;

if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_605b6ace08939',
		'title' => 'CTA Newsletter',
		'fields' => array(
			array(
				'key' => 'field_605b6b0d6bb68',
				'label' => 'Titolo Blocco',
				'name' => 'titolo_blocco',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'translations' => 'translate',
			),
			array(
				'key' => 'field_605b6b246bb69',
				'label' => 'Testo Blocco',
				'name' => 'testo_blocco',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'translations' => 'translate',
			),
			array(
				'key' => 'field_605b6b2c6bb6a',
				'label' => 'CTA Blocco',
				'name' => 'cta_blocco',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'translations' => 'translate',
			),
			array(
				'key' => 'field_605b6b326bb6b',
				'label' => 'Link Blocco',
				'name' => 'link_blocco',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'translations' => 'translate',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'views/page-newsletter-manuale.blade.php',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

endif;

if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_606c8ca0a48e5',
		'title' => 'Immagine Canale',
		'fields' => array(
			array(
				'key' => 'field_606c8cef8259a',
				'label' => 'Immagine',
				'name' => 'immagine',
				'type' => 'image',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
				'preview_size' => 'medium',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
				'translations' => 'copy_once',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'taxonomy',
					'operator' => '==',
					'value' => 'channel',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

endif;



if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_6342428ca0a48e5',
		'title' => 'Immagine Case History',
		'fields' => array(
			array(
				'key' => 'field_43623213cef8259a',
				'label' => 'Immagine',
				'name' => 'immagine',
				'type' => 'image',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
				'preview_size' => 'medium',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
				'translations' => 'copy_once',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'taxonomy',
					'operator' => '==',
					'value' => 'casehistory',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

endif;


if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_60b4efe437ec5',
		'title' => 'MediaCase Video',
		'fields' => array(
			array(
				'key' => 'field_60b4eff247f09',
				'label' => 'Contenuto di tipo video',
				'name' => 'is_video',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 0,
				'ui' => 0,
				'translations' => 'copy_once',
				'ui_on_text' => '',
				'ui_off_text' => '',
			),
			array(
				'key' => 'field_60b4f01647f0a',
				'label' => 'url video Youtube / Vimeo',
				'name' => 'url_video_youtube__vimeo',
				'type' => 'url',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field_60b4eff247f09',
							'operator' => '==',
							'value' => '1',
						),
					),
				),
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'translations' => 'copy_once',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'mediacase',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

endif;



if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_6284f1e55f4a6',
		'title' => 'Archivio Newsletter',
		'fields' => array(
			array(
				'key' => 'field_6284f206549dc',
				'label' => 'Titolo Newsletter',
				'name' => 'titolo_newsletter',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'translations' => 'translate',
			),
			array(
				'key' => 'field_6284f21e549dd',
				'label' => 'Immagine quadrata',
				'name' => 'immagine_quadrata',
				'type' => 'image',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
				'preview_size' => 'medium',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
				'translations' => 'copy_once',
			),
			array(
				'key' => 'field_6284f23c549de',
				'label' => 'Logo Newsletter',
				'name' => 'logo_newsletter',
				'type' => 'image',
				'instructions' => '720x110',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
				'preview_size' => 'full',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
				'translations' => 'copy_once',
			),
			array(
				'key' => 'field_6284f251549df',
				'label' => 'Sottotitolo Newsletter',
				'name' => 'sottotitolo_newsletter',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'translations' => 'translate',
			),
			array(
				'key' => 'field_6284f267549e0',
				'label' => 'Newsletters',
				'name' => 'newsletters',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => 0,
				'max' => 0,
				'layout' => 'row',
				'button_label' => 'Aggiungi Newsletter',
				'sub_fields' => array(
					array(
						'key' => 'field_6284f282549e1',
						'label' => 'Data',
						'name' => 'data',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'translations' => 'translate',
					),
					array(
						'key' => 'field_6284f28a549e2',
						'label' => 'Titolo',
						'name' => 'titolo',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'translations' => 'translate',
					),
					array(
						'key' => 'field_6284f294549e3',
						'label' => 'Testo',
						'name' => 'testo',
						'type' => 'wysiwyg',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'tabs' => 'all',
						'toolbar' => 'full',
						'media_upload' => 0,
						'delay' => 0,
						'translations' => 'translate',
					),
					array(
						'key' => 'field_6284f2a2549e4',
						'label' => 'Immagine',
						'name' => 'immagine',
						'type' => 'image',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',
						'preview_size' => 'medium',
						'library' => 'all',
						'min_width' => '',
						'min_height' => '',
						'min_size' => '',
						'max_width' => '',
						'max_height' => '',
						'max_size' => '',
						'mime_types' => '',
						'translations' => 'copy_once',
					),
					array(
						'key' => 'field_6284f441ae5f9',
						'label' => 'Link Dettaglio Newsletter',
						'name' => 'link_dettaglio_newsletter',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'translations' => 'translate',
					),
				),
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'views/page-list-newsletter.blade.php',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => array(
			0 => 'the_content',
		),
		'active' => true,
		'description' => '',
	));

endif;


if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_635936b29caa1',
		'title' => 'Live blog',
		'fields' => array(
			array(
				'key' => 'field_635936bde4a52',
				'label' => 'Abilita live blog',
				'name' => 'abilita_live_blog',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 0,
				'ui' => 0,
				'translations' => 'copy_once',
				'ui_on_text' => '',
				'ui_off_text' => '',
			),
			array(
				'key' => 'field_635936cde4a53',
				'label' => 'Lista Live',
				'name' => 'lista_live',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field_635936bde4a52',
							'operator' => '==',
							'value' => '1',
						),
					),
				),
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => 0,
				'max' => 0,
				'layout' => 'block',
				'button_label' => 'Aggiungi aggiornamento',
				'sub_fields' => array(
					array(
						'key' => 'field_635936dae4a54',
						'label' => 'Data e ora',
						'name' => 'data_e_ora',
						'type' => 'date_time_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '30',
							'class' => '',
							'id' => '',
						),
						'display_format' => 'd/m/Y g:i a',
						'return_format' => 'G:i, j F Y',
						'first_day' => 1,
						'translations' => 'copy_once',
					),
					array(
						'key' => 'field_635936ebe4a55',
						'label' => 'Titolo',
						'name' => 'titolo',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '70',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'translations' => 'translate',
					),
					array(
						'key' => 'field_635936f0e4a56',
						'label' => 'Testo',
						'name' => 'testo',
						'type' => 'wysiwyg',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'tabs' => 'all',
						'toolbar' => 'full',
						'media_upload' => 1,
						'delay' => 0,
						'translations' => 'translate',
					),
				),
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

endif;
