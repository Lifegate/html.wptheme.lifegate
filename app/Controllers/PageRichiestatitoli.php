<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class PageRichiestatitoli extends Controller {

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/page-richiesta-titoli--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

}
