<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleAutori extends Controller {

	protected $custom_pagination = [];
	protected $page = '1';

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/archive-autore--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

	public function pagination() {

		$out = array(
			'custom' => true
		);

		return $out;
	}

	public function author_page() {
		$page = get_query_var( 'author_page' );

		if ( $page ) {
			$this->page = $page;
		}

		return $page;
	}

	public function single_author_role() {
		$out = get_field( 'ruolo_autore' );

		return $out;
	}

	public function single_author_bio() {
		$out = get_field( 'esperienza_autore' );

		return $out;
	}


	public function single_author_social() {
		$socials = get_field( 'social' );
		$out = "";
		if($socials){
			$out .="<p><strong>".__("Profili web:", "lifegate")."</strong><br>";
			foreach ($socials as $social) {
				$out .="<a href='".$social["link_social"]."'>".$social["nome_social"]."</a><br>";
			}
			$out .="</p>";
		}

		return $out;
	}

	public function single_author_last_post() {

		$post_types = array(
			HTML_VIDEO_POST_TYPE,
			HTML_GALLERY_POST_TYPE,
			HTML_CARD_POST_TYPE,
			'longform',
			'original',
			'post',
		);

		$my_args = array(
			'post_type'      => $post_types,
			'posts_per_page' => 1,
			'suppress_filters' => true,
			'meta_query'     => array(
				'relation' => 'OR',
				array(
					'key'     => 'autore_news',
					'value'   => '"'.get_the_ID().'"',
					'compare' => 'LIKE'
				),
				array(
					'key'     => 'autore_news',
					'value'   => get_the_ID(),
					'compare' => '='
				),
				array(
					'key'     => 'autore_news',
					'value'   => "a:1:{i:0;i:".get_the_ID().";}",
					'compare' => '='
				),
				array(
					'key'     => 'traduttore_news',
					'value'   => '"'.get_the_ID().'"',
					'compare' => 'LIKE'
				),
				array(
					'key'     => 'traduttore_news',
					'value'   => get_the_ID(),
					'compare' => '='
				),
				array(
					'key'     => 'traduttore_news',
					'value'   => "a:1:{i:0;i:".get_the_ID().";}",
					'compare' => '='
				),
			),
		);

		$the_posts = new \WP_Query( $my_args );

		return $the_posts;
	}


	public function single_author_last_originals() {

		$post_types = array('original');

		$args = array(
			'posts_per_page' => - 1,
			'post_type'      => $post_types,
			'suppress_filters' => true,
			'meta_query'     => array(
				'relation' => 'OR',
				array(
					'key'     => 'testimonial',
					'value'   => '"'.get_the_ID().'"',
					'compare' => 'LIKE'
				),
				array(
					'key'     => 'testimonial',
					'value'   => get_the_ID(),
					'compare' => '='
				),
				array(
					'key'     => 'testimonial',
					'value'   => "a:1:{i:0;i:".get_the_ID().";}",
					'compare' => '='
				)
			),
		);

		$the_posts = new \WP_Query( $args );

		return $the_posts;

	}
	public function single_author_last_posts() {
		$page = $this->page;

		$post_types = array(
			HTML_VIDEO_POST_TYPE,
			HTML_GALLERY_POST_TYPE,
			HTML_CARD_POST_TYPE,
			'original',
			'longform',
			'post',
		);

		$posts_per_page = get_option( 'posts_per_page' );
		$offset         = ( $page - 1 ) * $posts_per_page;

		$args = array(
			'posts_per_page' => - 1,
			'post_type'      => $post_types,
			'suppress_filters' => true,
			'fields'         => 'ids',
			'meta_query'     => array(
				'relation' => 'OR',
				array(
					'key'     => 'autore_news',
					'value'   => '"'.get_the_ID().'"',
					'compare' => 'LIKE'
				),
				array(
					'key'     => 'autore_news',
					'value'   => get_the_ID(),
					'compare' => '='
				),
				array(
					'key'     => 'autore_news',
					'value'   => "a:1:{i:0;i:".get_the_ID().";}",
					'compare' => '='
				),
				array(
					'key'     => 'traduttore_news',
					'value'   => '"'.get_the_ID().'"',
					'compare' => 'LIKE'
				),
				array(
					'key'     => 'traduttore_news',
					'value'   => get_the_ID(),
					'compare' => '='
				),
				array(
					'key'     => 'traduttore_news',
					'value'   => "a:1:{i:0;i:".get_the_ID().";}",
					'compare' => '='
				),
			),
		);

		$found_posts = count( get_posts( $args ) );

		$my_args = array(
			'posts_per_page' => $posts_per_page,
			'offset'         => $page > 1 ? $offset : $offset + 1, // In homepage we have one more post
			'post_type'      => $post_types,
			'suppress_filters' => true,
			'meta_query'     => array(
				'relation' => 'OR',
				array(
					'key'     => 'autore_news',
					'value'   => '"'.get_the_ID().'"',
					'compare' => 'LIKE'
				),
				array(
					'key'     => 'autore_news',
					'value'   => get_the_ID(),
					'compare' => '='
				),
				array(
					'key'     => 'autore_news',
					'value'   => "a:1:{i:0;i:".get_the_ID().";}",
					'compare' => '='
				),
				array(
					'key'     => 'traduttore_news',
					'value'   => '"'.get_the_ID().'"',
					'compare' => 'LIKE'
				),
				array(
					'key'     => 'traduttore_news',
					'value'   => get_the_ID(),
					'compare' => '='
				),
				array(
					'key'     => 'traduttore_news',
					'value'   => "a:1:{i:0;i:".get_the_ID().";}",
					'compare' => '='
				),
			),
		);

		$the_posts = new \WP_Query( $my_args );

		$this->custom_pagination = array(
			'page'          => $page,
			'max_num_pages' => ceil( $found_posts / $posts_per_page ),
			'is_paginable'  => true,
			'custom'        => true,
			'link'          => get_page_link()
		);

		return $the_posts;
	}

	public function navigator_data() {
		return $this->custom_pagination;
	}

}
