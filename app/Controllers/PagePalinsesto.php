<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class PagePalinsesto extends Controller {

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/page-palinsesto--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

	public function show_times() {

		$out = array();

		$args = array(
			'post_type'      => 'show',
			'posts_per_page' => - 1,
		);

		$posts = get_posts( $args );

		if ( ! $posts ) {
			return array();
		}


		/**
		 * Loop the posts
		 */
		foreach ( $posts as $post ) {

			$title = get_the_title( $post );
			$link  = get_the_permalink( $post );
			$times = get_field( 'lg_show_time', $post->ID );

			if ( ! $times || ! is_array( $times ) ) {
				continue;
			}

			/**
			 * Loop the show times
			 */
			foreach ( $times as $time ) {
				$out[ $time['lg_show_time_day'] ][] = array(
					'title'     => $title,
					'hour'      => $time['lg_show_time_hour'],
					'permalink' => $link
				);
			}

		}

		/**
		 * Sort the show times
		 */
		foreach ( $out as $key => $day ) {
			usort( $day, function ( $a, $b ) {
				return strtotime( $a['hour'] ) <=> strtotime( $b['hour'] );
			} );
			$out[ $key ] = $day;
		}

		/**
		 * Sort the days
		 */
		ksort( $out );

		return $out;

	}


}
