<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class ArchiveIniziative extends Controller {

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/archive-iniziative--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

	/**
	 * ritorna il titolo tag / categoria
	 * @return mixed
	 */
	public function tbm_custom_tax_title() {
		$category = get_queried_object();
		$titolo_dettaglio   = get_field( 'titolo_dettaglio', $category );
		if($titolo_dettaglio != "")
			return $titolo_dettaglio;
		else
			return get_the_archive_title();

	}


	public function all_iniziatives() {
		global $wp_query;

		return $wp_query;
	}

}
