<?php

namespace App\Controllers;

use App\Controllers\Partials\CommonSingle;
use App\Controllers\Partials\RelatedTaxonomy;
use Sober\Controller\Controller;

class SingleCard extends Controller {
	/**
	 * Common functions for single post
	 */
	use Partials\CommonSingle;

	/**
	 * Functions are here
	 */
	use Partials\LifegateSingle;

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/single-card--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

	public $page;
	public $paragraphs;

	public function paragraphs() {
		$paragraphs       = get_field( "html_card_paragrafo_repeater" );
		$this->paragraphs = $paragraphs;
	}

	public function paragraph_array() {
		return $this->paragraphs;
	}


	public function paged() {
		$page       = ( get_query_var( 'doc' ) ) ?: 1;
		$this->page = $page;

		return $page;
	}

	public function card() {
		return $this->page - 2;
	}

	public function total_cards() {
		return count( $this->paragraphs ) + 1;
	}


	public function get_nav() {

		global $post;
		$prev = '';
		$next = '';

		if ( is_preview() ) {
			$next = get_preview_post_link( $post, array( 'doc' => $this->page + 1 ) );
			$prev = get_preview_post_link( $post, array( 'doc' => $this->page - 1 ) );
		} else {
			$next = get_permalink( $post ) . '/doc/' . ( $this->page + 1 );
			if(( $this->page - 1 ) == 1)
				$prev = get_permalink( $post );
			else
				$prev = get_permalink( $post ) . '/doc/' . ( $this->page - 1 );
		}

		return array( 'prev' => $prev, 'next' => $next );
	}
}
