<?php


namespace App\Controllers;

use Sober\Controller\Controller;

class PagePartner extends Controller {

	public function last_partners() {
		$page = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

		$terms_per_page = get_option( 'posts_per_page' );
		$partners       = get_terms( array(
			'taxonomy'   => 'sponsor',
			'fields'     => 'ids',
			'meta_query' => array(
				array(
					'key'     => 'sponsor_type',
					'value'   => 'tbm_sponsor_partner',
					'compare' => '='
				)
			)
		) );

		$my_args = array(
			'taxonomy'   => 'magazine',
			'orderby'    => 'count',
			'count'      => true,
			'order'      => 'DESC',
			'meta_query' => array(
				array(
					'key'   => 'post_sponsor',
					'value' => $partners,
				)
			)
		);

		$the_terms = get_terms( $my_args );

		return $the_terms;
	}

	public function custom_pagination() {
		return $this->custom_pagination;
	}


	/**
	 * Returns the image attached to the term (with ACF)
	 *
	 * @param WP_Term $term Term object to get its image.
	 *
	 * @return string Image ID, or empty string if none available.
	 */
	public static function get_term_image( $term ) {

		$term = get_term( $term );

		if ( is_wp_error( $term ) || ! $term ) {
			return '';
		}

		$image = get_field( 'tbm_special_featured_img', $term->taxonomy . '_' . $term->term_id );

		return $image;
	}

}
