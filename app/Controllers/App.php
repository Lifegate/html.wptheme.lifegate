<?php

namespace App\Controllers;

use Sober\Controller\Controller;


class App extends Controller {

	/**
	 * STATIC FUNCTIONS
	 */

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/page--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;
	}

	/**
	 * Get the lifegate radio player
	 *
	 * @return string
	 */
	public function player_radio() {

		return get_page_link( get_page_by_path( "lifegate-radio" ) );
	}


	/**
	 * Check if the main loop have more posts
	 *
	 * @return bool
	 */
	public static function tbm_more_posts() {
		global $wp_query;

		return $wp_query->current_post + 1 < $wp_query->post_count;
	}

	/**
	 * Return the posts count of the main query
	 *
	 * @return bool
	 */
	public static function tbm_count_posts() {
		global $wp_query;

		return $wp_query->found_posts;
	}

	/**
	 * Get the title
	 */
	public static function title() {
		if ( is_home() ) {
			if ( $home = get_option( 'page_for_posts', true ) ) {
				return get_the_title( $home );
			}

			return __( 'Latest Posts', 'lifegate' );
		}
		if ( is_archive() ) {
			return get_the_archive_title();
		}
		if ( is_search() ) {
			return sprintf( __( 'Search Results for %s', 'lifegate' ), get_search_query() );
		}
		if ( is_404() ) {
			return __( 'Not Found', 'lifegate' );
		}

		return get_the_title();
	}

	/**
	 * Get the image, rewrited at 300x80, attached to a Storia term
	 *
	 * @param object $term The term object
	 *
	 * @return false|string|void The url of the image
	 */
	public static function tbm_get_storie_image( $id ) {

		$term = get_term( $id );

		if ( is_wp_error( $term ) ) {
			return $term;
		}

		if ( ! is_object( $term ) ) {
			return;
		}

		$id       = $term->taxonomy . '_' . $term->term_id;
		$thumb_id = get_field( 'tbm_trend_immagine', $id );
		$url      = tbm_wp_get_attachment_image_url( $thumb_id, array( 300, 80 ) );

		if ( $url ) {
			return $url;
		}

		return '';

	}

	/**
	 * Merge an array of ids to a query
	 *
	 * @return bool|array|string|\WP_Query
	 */
	public static function tbm_get_merged_hp_options( $array = array() ) {
		$post__in = array();

		foreach ( $array as $item ) {
			$post__in = array_merge( $post__in, $item );
		}

		if ( empty( $post__in ) ) {
			/**
			 * Se an impossibile ID
			 */
			$post__in = array( '1000000000000000' );
		}

		// Execute the query
		$the_query = new \WP_Query( array(
			'post_type' => 'any',
			'post__in'  => $post__in,
			'orderby'   => 'post__in',
			'post_type' => 'any'
		) );

		return $the_query;

	}


	/**
	 * Song card thumb (used in multiple pages)
	 *
	 * @return string
	 */
	public function song_card_thumbnail() {
		$bg = get_field( 'tbm_hp_song_card', 'options' );
		if ( $bg) {
			return $bg;
		}

		return '';
	}

	/**
	 * Query the TBM standard HP option framework
	 *
	 * @param string $option_name (Required) The option prefix
	 * @param int $posts_per_page The number of posts to retrieve. Default 1
	 * @param string|array $post_type The default post type to query
	 * @param string $return Either `object` to return the complete post object or `id` to return array of ids
	 * @param bool $ignore Ignore the $not_in variables?
	 * @param array $not_in Add post to $not_in array so don't show them.
	 *
	 * @return bool|array|string|\WP_Query
	 */
	public static function tbm_get_hp_options(
		$option_name = '', $posts_per_page = 1, $post_type = array( 'post' ), $return = 'object', $ignore = false, $custom_not_in = array()
	) {
		if ( empty( $option_name ) ) {
			return false;
		}

		$result = wp_cache_get( 'hp_' . $option_name );
		if ( false !== $result ) {
			return $result;
		}

		$lang = function_exists( 'pll_current_language' ) ? pll_current_language() : '';

		$v = $option_name;

		// Get excluded posts
		$not_in = get_query_var( 'notin', array() );


		// Defaults args
		$default_args = array(
			'posts_per_page' => $posts_per_page,
			'post_type'      => $post_type,
			'post__not_in'   => $ignore ? array() : empty( $custom_not_in ) ? $not_in : @array_merge( $not_in, $custom_not_in ),
			'lang'           => $lang
		);

		// If we have a default post type
		if ( get_field( $v . '_posttype', 'option' ) ) {
			$default_args['post_type'] = get_field( $v . '_posttype', 'option' );
		}

		// If post type is event, modify the main query
		if ( HTML_EVENT_POST_TYPE === $default_args['post_type'] ) {
			$today                      = date( 'Ymd' );

			$default_args['meta_query'] = array(
				// Use an OR relationship between the query in this array and the one in
				// the next array. (AND is the default.)
				'relation' => 'OR',
				// If an end_date exists, check that it is upcoming.
				array(
					'key'        => 'dateTo',
					'compare'    => '>=',
					'value'      => $today,
				),
				// OR!
				array(
					// A nested set of conditions for when the above condition is false.
					array(
						array(
							'key'        => 'dateTo',
							'compare'    => '=',
							'value'      => '',
						)
					),
					// AND, if the start date is upcoming.
					array(
						'key'        => 'dateFrom',
						'compare'    => '>=',
						'value'      => $today,
					),
				),
			);
			/*
			$default_args['meta_query'] = array(
				'date_event' => array(
					'key'     => 'dateFrom',
					'compare' => '>=',
					'value'   => $today,
				),
			);
			*/
			$default_args['meta_key'] = 'dateFrom'; //setting the meta_key which will be used to order
			$default_args['orderby']    = 'meta_value';
			$default_args['order']      = 'ASC';
		}

		// If we ant a random post type
		if ( get_field( $v . '_casuali', 'option' ) ) {
			$default_args['orderby'] = 'rand';
		}

		// If the post are selected manually
		//if ( function_exists( 'pll_current_language' ) && 'it' === pll_current_language() ) {
			if ( get_field( $v . '_singular', 'option' ) && get_field( $v . '_single', 'option' ) ) {
				$default_args = array(
					'post__in'  => get_field( $v . '_single', 'option' ),
					'orderby'   => 'post__in',
					'post_type' => 'any'
				);
			}
		//}



		// If posts are selected automatically from featured ones
		if ( get_field( $v . '_automatiche', 'option' ) && get_field( $v . '_featured', 'option' ) === true ) {
			$default_args['meta_key']   = '_featured';
			$default_args['meta_value'] = true;
		}

		// If posts are selected automatically from one category
		if ( get_field( $v . '_automatiche', 'option' ) && ! empty( get_field( $v . '_categories', 'option' ) ) ) {
			$default_args['category__in'] = get_field( $v . '_categories', 'option' );
		}

		if ( ! $default_args ) {
			return '';
		}

		// Execute the query
		$the_query = new \WP_Query( $default_args );

		if ( $the_query->have_posts() ) {
			// Save the returned ids in variable
			$not_in = @array_merge( $not_in, wp_list_pluck( $the_query->posts, 'ID' ) );
			set_query_var( 'notin', $not_in );
		}

		if ( $return === 'id' ) {
			return wp_list_pluck( $the_query->posts, 'ID' );
		}

		wp_cache_set( 'hp_' . $option_name, $result );

		return $the_query;
	}

	/**
	 * Get the post type name relative to the card
	 */
	public
	static function lifegate_get_post_type() {
		$lg_post_type_array = array(
			'show'               => 'radio',
			'persone_news'       => 'post',
			'persone'            => 'post',
			'persone_iniziative' => 'iniziativa',
			'persone_eventi'     => 'post',
			'persone_articoli'   => 'post',
			'sponsor'            => 'post',
			'rassegna_stampa'    => 'post',
			'clienti'            => 'post',
			'news'               => 'post',
		);

		$post_type = get_post_type();

		if ( isset( $lg_post_type_array[ $post_type ] ) ) {
			$post_type = $lg_post_type_array[ $post_type ];
		}

		return $post_type;
	}

	/**
	 * PUBLIC FUNCTIONS
	 */
	protected
		$acf = false;

	public
	function __construct() {
		if ( acf_get_valid_post_id( get_queried_object() ) ) {
			$this->acf = true;
		}
	}

	/**
	 * Get site name
	 *
	 * @return string|void
	 */
	public
	function siteName() {
		return get_bloginfo( 'name' );
	}

	/**
	 * Returns the theme version
	 *
	 * @return false|string
	 */
	public
	function theme_version() {
		return wp_get_theme()->get( 'Version' );
	}

	/**
	 * Returns 0
	 *
	 * @return int
	 */
	public
	function tbm_counter() {
		return 0;
	}

	/**
	 * Main Menu
	 *
	 * @return array
	 */
	public
	function main_menu() {
		$main_menu_args = [
			'theme_location' => 'global_navigation',
			'container'      => '',
			'menu_class'     => '',
			'echo'           => false,
			'items_wrap'     => '%3$s',
			'walker'         => new TBMMainNavMenu()
		];

		return $main_menu_args;
	}

	/**
	 * Magazine Menu
	 *
	 * @return array
	 */
	public
	function magazine_menu() {
		$main_menu_args = [
			'theme_location' => 'magazine_navigation',
			'container'      => '',
			'menu_class'     => '',
			'echo'           => false,
			'items_wrap'     => '%3$s',
			'walker'         => new LifegateMagazineMenu()
		];

		return $main_menu_args;
	}

	/**
	 * Radio Menu
	 *
	 * @return array
	 */
	public
	function radio_menu() {
		$main_menu_args = [
			'theme_location' => 'radio_navigation',
			'container'      => '',
			'menu_class'     => '',
			'echo'           => false,
			'items_wrap'     => '%3$s',
			'walker'         => new LifegateMagazineMenu()
		];

		return $main_menu_args;
	}

	/**
	 * Trend Menu arguments
	 *
	 * @return array
	 */
	public
	function trends_menu() {
		$trends_menu_args = [
			'theme_location' => 'trends_navigation',
			'items_wrap'     => '%3$s',
			'container'      => '',
			'menu_class'     => '',
			'echo'           => false,
			'walker'         => new LifegateTrendMenu()
		];

		return $trends_menu_args;
	}


	/**
	 * Channel Menu arguments
	 *
	 * @return array
	 */
	public
	function channels_menu() {
		$channels_menu_args = [
			'theme_location' => 'channels_navigation',
			'items_wrap'     => '%3$s',
			'container'      => '',
			'menu_class'     => '',
			'echo'           => false,
			'walker'         => new LifegateMagazineMenu()
		];

		return $channels_menu_args;
	}



	/**
	 * Casehistory Menu arguments
	 *
	 * @return array
	 */
	public
	function casehistory_menu() {
		$channels_menu_args = [
			'theme_location' => 'casehistory_navigation',
			'items_wrap'     => '%3$s',
			'container'      => '',
			'menu_class'     => '',
			'echo'           => false,
			'walker'         => new LifegateMagazineMenu()
		];

		return $channels_menu_args;
	}

	/**
	 * Hamburger Menu arguments
	 *
	 * @return array
	 */
	public
	function hamburger_menu() {
		$trends_menu_args = [
			'theme_location' => 'hamburger_navigation',
			'items_wrap'     => '%3$s',
			'container'      => '',
			'menu_class'     => '',
			'depth'          => 2,
			'echo'           => false,
			'walker'         => new LifegateHamburgerMenu()
		];

		return $trends_menu_args;
	}

	/**
	 * Lanaguage switcher
	 *
	 * @return string
	 */
	public
	function language_switcher() {

		if ( ! function_exists( 'pll_the_languages' ) ) {
			return '';
		}

		/** @var array $out */
		$out = array();

		/** @var bool $selected */
		$selected = '';

		/** @var array $translations Contains alla the language data */
		$translations = pll_the_languages( array( 'raw' => 1 ) );

		if ( $translations ) {
			foreach ( $translations as $key => $translation ) {
				/** @var string $selected */
				$selected = '';
				if ( $translation['current_lang'] ) {
					$selected = 'selected';
				}
				if ( $translation['no_translation'] !== true ) {
					$out[] = "<option value=\"{$translation['url']}\" {$selected}>{$translation['name']}</option>";
				}
			}
		}

		return implode( '', array_filter( $out ) );

	}

	/**
	 * Returns the language currently settled in browser
	 *
	 * @return string
	 */
	public
	function current_language() {
		if ( ! function_exists( 'pll_current_language' ) ) {
			return 'it';
		}

		return pll_current_language();
	}

	/** Returns the Facebook App ID */
	public
	function tbm_fb_app_id() {
		return get_field( 'tbm_fb_app_id', 'options' );
	}


	public static function tbm_get_hide_from_hp() {

		$lang = function_exists( 'pll_current_language' ) ? pll_current_language() : '';

		// Defaults args
		$args = array(
			'posts_per_page' => 10,
			'post_type'      => array(
				'post',
				HTML_EVENT_POST_TYPE,
				HTML_VIDEO_POST_TYPE,
				HTML_GALLERY_POST_TYPE,
				HTML_CARD_POST_TYPE
			),
			'fields'         => 'ids',
			'meta_query'     => array(
				array(
					'key'   => 'tbm_hide_hp',
					'value' => '1',
				)
			),
			'lang'           => $lang
		);

		$posts = get_posts( $args );

		return $posts;

	}


	public function adk_domination() {
		if(is_singular("post") || is_page()){
			$adk_domination = get_field( 'adk_domination' );
			if($adk_domination){
				$ret =  '<script>';
				$ret .= " var adk_domination = '".$adk_domination."'; ";
				$ret .= '</script>';
				return $ret;
			}
		}

	}

}
