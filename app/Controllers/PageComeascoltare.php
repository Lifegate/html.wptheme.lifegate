<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class PageComeascoltare extends Controller {

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/archive--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

	public function comeascoltare_repeater() {

		$blocks = get_field( 'tbm_text_howtolisten_blocks' );

		if ( ! $blocks || ! is_array( $blocks ) ) {
			return null;
		}

		/**
		 * Loop the blocks
		 */
		foreach ( $blocks as $block ) {
			$out[] = array(
				'text' => $block['block_text'],
				'icon' => $block['block_icon'],
				'url'  => $block['block_url'],
			);
		}

		return $out;
	}

}
