<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleMediacase extends Controller {
	/**
	 * Common functions for single post
	 */
	use Partials\CommonSingle;

	/**
	 * Functions are here
	 */
	use Partials\LifegateSingle;

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
				'/dist/css/critical/single-original--critical.min.css', array(
						'/dist/css/custom.min.css'
				)
		);
		return $css;

	}


		/**
	 * Get the ID of the thumb
	 *
	 * @return int|mixed|string|null
	 */
	public function post_thumb_id($item = null) {
		global $post;
		if(!$item) $item = $post;
		/**
		 * If image have not post thumbnail
		 */
		if ( ! has_post_thumbnail($item) ) {
			return null;
		}

		$thumb_id = get_post_thumbnail_id($item);

		return $thumb_id;

	}



	public function mediacase_next_to_the_title($post = null) {

		$post = get_post( $post );
		if ( empty( $post ) ) {
			return '';
		}

		$out = '';

		$post_channels = wp_get_object_terms($post->ID, array( 'casehistory' ), array(
				'orderby' => 'count',
				'count'   => true,
				'number'  => 1,
				'order'   => 'DESC'
		) );
		if($post_channels){
			$out = sprintf( '<a class="post__story" href="%s">%s</a>', get_term_link($post_channels[0]), $post_channels[0]->name );
		}

		return $out;
	}

	public function author_box($item = null){

		global $post;
		if($item) $post = $item;

		$authors  = get_field( 'testimonial' , $post);
		if ( ! $authors) {
			return "";
		}
		/**
		 * Old custom fields are strings. Cast to array
		 */
		if ( $authors && !is_array( $authors ) ) {
			$authors = (array) $authors;
		}

		foreach ( $authors as $author ) {
			$post = get_post( $author );
			if ( empty( $post ) ) {
				return "";
			}
			$thumb_id = SingleOriginal::post_thumb_id($author);
			$thumb_url = tbm_wp_get_attachment_image_url($thumb_id,array(200,200));
			ob_start();
			?>
			<div class="box-author">
				<a href="<?php echo get_permalink( $author ); ?>">
					<img src="<?php echo $thumb_url; ?>" alt="<?php echo get_the_title( $author ); ?>">
				</a>
				<a href="<?php echo get_permalink( $author ); ?>">
					<span><?php echo get_the_title( $author ); ?></span>
				</a>
			</div>
			<?php
			// prendo solo il primo elemento
			$ret = ob_get_contents();
			ob_end_clean();
			return $ret;

			break;
		}
	}

	public function author_card($item = "") {
		global $post;
		if(!$item) $item = $post;

		$authors  = get_field( 'testimonial' , $post);
		if ( ! $authors) {
			return "";
		}
		/**
		 * Old custom fields are strings. Cast to array
		 */
		if ( $authors && !is_array( $authors ) ) {
			$authors = (array) $authors;
		}

		foreach ( $authors as $author ) {
			$author = get_post( $author );
			if ( empty( $author ) ) {
				return "";
			}
			$thumb_id = SingleOriginal::post_thumb_id($author);
			$thumb_url = tbm_wp_get_attachment_image_url($thumb_id,array(200,200));
			ob_start();
			?>
			<div class="testimonial-card">
				<a href="<?php echo get_permalink( $author ); ?>">
					<img class="testimonial-img" src="<?php echo $thumb_url; ?>" alt="<?php echo get_the_title( $author ); ?>"/>
					<span class="testimonial-name">
						<?php echo get_the_title( $author ); ?>
					</span>
				</a>
			</div>
			<?php
			// prendo solo il primo elemento
			$ret = ob_get_contents();
			ob_end_clean();
			return $ret;

			break;
		}


	}


	public function single_original_authors() {
		$out="";
		$authors  = get_field( 'testimonial' );
		$template = '<a href=" % s">%s</a>';


		if ( ! $authors) {
			return "";
		}

		/**
		 * Old custom fields are strings. Cast to array
		 */
		if ( $authors && !is_array( $authors ) ) {
			$authors = (array) $authors;
		}

		foreach ( $authors as $author ) {
			$post = get_post( $author );
			if ( empty( $post ) ) {
				continue;
			}
			$out = sprintf( $template, get_permalink( $author ), get_the_title( $author ) );
			// prendo solo il primo
			continue;
		}

		return $out;

	}

	public function mediacase_hp(){
		return get_post_type_archive_link("mediacase");
	}
	public function single_mediacase_relateds() {
		wp_reset_postdata();
		$currentid = get_the_ID();

		// Get the special taxonomy
		$channels = wp_get_post_terms( $currentid, "casehistory" );
		$channels_array = array();
		if ( $channels && ! is_wp_error( $channels ) ) {
			foreach ( $channels as $channel ) {
				$channels_array[] = $channel->slug;
			}
		}

		// Query for special taxonomy
		$tax_query = '';
		if ( sizeof( $channels_array ) > 0 ) {
			$tax_query = array(
					array(
							'taxonomy' => "casehistory",
							'field'    => 'slug',
							'terms'    => $channels_array
					)
			);
		}

		$return_related_posts = new \WP_Query( array( 'post__not_in' => array($currentid), 'mediacase' => 'original' , "tax_query" => $tax_query,  'posts_per_page' => 3 ) );

		wp_reset_postdata();

		return $return_related_posts;
	}

}
