<?php
/**
 * Created by PhpStorm.
 * User: Francesco
 * Date: 10/11/2018
 * Time: 16:00
 */

namespace App\Controllers;

use App\Controllers\Partials\Pagination;
use Sober\Controller\Controller;

class TaxonomyShow extends Controller {

	use Partials\Pagination;

	public $acf_tax_id;
	public $show;

	public function __construct() {
		$this->run();
	}

	protected function run() {
		$queried_object = get_queried_object();

		if ( $queried_object ) {
			$term_id  = $queried_object->term_id;
			$taxonomy = $queried_object->taxonomy;

			$this->acf_tax_id = $taxonomy . '_' . $term_id;
			$this->show       = $queried_object;
		}
	}

	/**
	 * Get the attached img ID
	 *
	 * @return int
	 */
	public function acf_tax_id() {
		return $this->acf_tax_id;
	}

	/**
	 * Set pagination defaults
	 *
	 * @return array
	 */
	public function pagination() {

		$out = array(
			'custom' => true,
			'link'   => get_term_link( get_queried_object_id() )
		);

		return $out;

	}

	/**
	 * Get current taxonomy
	 *
	 * @return object
	 */
	public function show() {
		return $this->show;
	}

	public function address() {
		return get_field( 'tbm_show_marker', $this->acf_tax_id );
	}

	public function information() {
		return [
			'Quando'         => get_field( 'tbm_show_date', $this->acf_tax_id ),
			'Dove'           => get_field( 'tbm_show_location', $this->acf_tax_id ),
			'Organizzazione' => get_field( 'tbm_show_org', $this->acf_tax_id ),
			'Tipo di evento' => get_field( 'tbm_show_event', $this->acf_tax_id ),
			'Contatti'       => get_field( 'tbm_show_contact', $this->acf_tax_id ),
		];
	}

	public function featured_posts() {
		$posts = get_field( 'tbm_show_articoli_in_evidenza', $this->acf_tax_id );

		if ( ! $posts ) {
			$posts = array();
		}

		$ids = [];
		foreach ( $posts as $p ) {
			$ids[] = $p->ID;
		}
		$args = [
			'post_type'      => 'post',
			'post_status'    => 'publish',
			'posts_per_page' => 4,
			'post__in'       => $ids
		];

		return [
			'query'    => [
				'gutter'          => 40,
				'colNumber'       => 2,
				'componentNumber' => 4,
				'class'           => 'featured-slots',
				'component'       => 'partial-card-cards-big--V1'
			],
			'tbmposts' => ( count( $ids ) > 0 ) ? new \WP_Query( $args ) : null
		];
	}



}
