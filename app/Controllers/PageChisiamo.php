<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class PageChisiamo extends Controller {

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/chisiamo--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

	public function modal_close_link() {

		return get_bloginfo( 'url' );
	}

	public function tbm_authors() {

		$lang = function_exists( 'pll_current_language' ) ? pll_current_language() : '';

		$featured_authors = array();

		$featured_authors_id = get_field( 'tbm_author_order' );

		$args = array(
			'post_type'      => 'autori',
			'post_status'    => 'publish',
			'posts_per_page' => - 1,
			'meta_key'       => 'cognome_autore',
			'meta_query'     => array(
				'relation' => 'OR',
				array(
					'key'     => 'tbm_author_hide',
					'value'   => '1',
					'compare' => '!='
				),
				array(
					'key'     => 'tbm_author_hide',
					'compare' => 'NOT EXISTS'
				),
			),
			'lang'           => $lang
		);

		/**
		 * If we have featured authors saved in config, query only these
		 */
		if ( $featured_authors_id ) {
			$args['post__in']   = $featured_authors_id;
			$args['orderby']    = 'post__in';
			$featured_authors   = get_posts( $args );
		}

		if ( $featured_authors_id ) {
			unset( $args['post__in'] );
			$args['post__not_in'] = $featured_authors_id;
		}

		$args['orderby'] = 'meta_value';
		$args['order']   = 'ASC';
		$all_authors     = get_posts( $args );

		return array_merge( $featured_authors, $all_authors );

	}


}
