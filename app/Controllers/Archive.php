<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Archive extends Controller {
	use Partials\Pagination;

	private $archive_type = '';

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/archive--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}


	public function tbm_custom_tax_title() {

		if(is_post_type_archive("longform"))
			return pll__( "I nostri long-form", 'lifegate' );


		$category = get_queried_object();
		$titolo_dettaglio   = get_field( 'titolo_dettaglio', $category );
		if($titolo_dettaglio != "")
			return $titolo_dettaglio;
		else
			return get_the_archive_title();

	}

	public function pagination() {

		$out = array(
			'custom' => true
		);

		return $out;
	}



}
