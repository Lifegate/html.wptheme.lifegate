<?php


namespace App\Controllers;

use Sober\Controller\Controller;


class PageNotFound extends Controller {
	protected $template = "404";

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/page-404--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

}
