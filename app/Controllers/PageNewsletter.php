<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class PageNewsletter extends Controller {

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/page--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

	public function modal_close_link() {

		return get_bloginfo( 'url' );
	}


}
