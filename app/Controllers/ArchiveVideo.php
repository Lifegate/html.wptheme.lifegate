<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class ArchiveVideo extends Controller {
	use Partials\Pagination;

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/archive-video--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

	/**
	 * ritorna il titolo tag / categoria
	 * @return mixed
	 */
	public function tbm_custom_tax_title() {
		$category = get_queried_object();
		$titolo_dettaglio   = get_field( 'titolo_dettaglio', $category );
		if($titolo_dettaglio != "")
			return $titolo_dettaglio;
		else
			return get_the_archive_title();

	}


	public function pagination() {

		$out = array(
			'custom' => true,
			'link'   => get_post_type_archive_link( 'video' )
		);

		return $out;

	}

	public function featured_image() {
		$args = array(
			'post_type'      => HTML_VIDEO_POST_TYPE,
			'post_status'    => 'publish',
			'posts_per_page' => 1
		);

		$video_array = get_posts( $args );
		$video_post  = reset( $video_array );

		$desktop_url = tbm_get_the_post_thumbnail_url( $video_post, lifegate_get_image_size( 'video', 'poster' ) );
		$mobile_url  = tbm_get_the_post_thumbnail_url( $video_post, lifegate_get_image_size( 'video', 'poster-media' ) );

//		$format = '<picture style="height: 100%%" id="picturePreview"><source srcset="%s" media="(max-width: 736px)" /> <source srcset="%s" /><img id="imgPreview" src="%s" alt="Immagine di sfondo"/></picture>';

		$format = '<img id="imgPreview" src="%s" alt=""/>';

		//Check video url validity and output video
		if ( ! empty( $desktop_url ) && ! empty( $mobile_url ) ) {
			$out = sprintf( $format, $mobile_url, $desktop_url, $desktop_url );
		}


		return $out;
	}

	public function featured_video() {

		$out = '';

		$args = array(
			'post_type'      => HTML_VIDEO_POST_TYPE,
			'post_status'    => 'publish',
			'posts_per_page' => 1,
			'meta_query'     => array(
				'relation' => 'OR',
				array(
					'key'     => 'tbm_videoid',
					'value'   => '',
					'compare' => '!=',
				),
				array(
					'key'     => 'video_external_mp4',
					'value'   => '',
					'compare' => '!=',
				),
			),
		);

		/**
		 * Get the ID
		 */
		$video_array = get_posts( $args );
		$video_post  = reset( $video_array );

		/**
		 * Get the video url
		 */
		$video_url = tbm_video_get_file_url( $video_post );

		/**
		 * Get the poster
		 */
		$video_poster = tbm_video_get_poster( $video_post, lifegate_get_image_size( 'video', 'poster' ) );

		//Build html tag for video
		$format = '<video id="videoPreview" controls="false" autoplay loop muted="muted" preload="none" type="video/mp4" src="%s" poster="%s"></video>';

		//Check video url validity and output video
		if ( ! empty( $video_url ) && filter_var( $video_url, FILTER_VALIDATE_URL ) ) {
			$out = sprintf( $format, $video_url, $video_poster );
		}

		return $out;

	}

	public function featured_videos() {
		$args = array(
			'post_type'      => HTML_VIDEO_POST_TYPE,
			'post_status'    => 'publish',
			'posts_per_page' => 4,
		);

		return new \WP_Query( $args );
	}

	public function best_videos() {
		$args = array(
			'post_type'      => HTML_VIDEO_POST_TYPE,
			'post_status'    => 'publish',
			'posts_per_page' => 10,
			//'offset' => 5
		);

		return new \WP_Query( $args );
	}

	public function all_videos() {
		$args = array(
			'post_type'      => HTML_VIDEO_POST_TYPE,
			'post_status'    => 'publish',
			'posts_per_page' => 10,
//			'offset' => 20
		);

		return new \WP_Query( $args );
	}

}
