<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class PageNews extends Controller {

	private $notin = array();

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/frontpage-news--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

	/**
	 * Primo piano
	 *
	 * @return \WP_Query
	 */
	public function mg_hp_pp() {
		$ret = false;
		if ( function_exists( 'pll_current_language' ) && 'en' === pll_current_language() ) {
			$ret = App::tbm_get_hp_options( 'mpp_en', 1 );
		}else{
			$ret = App::tbm_get_hp_options( 'mpp', 1 );
		}

		$not_in      = @array_merge( $this->notin, wp_list_pluck( $ret->posts, 'ID' ) );
		$this->notin = $not_in;

		return $ret;
	}

	/**
	 * Secondo piano
	 *
	 * @return \WP_Query
	 */
	public function mg_hp_sp() {
		$ret = false;
		if ( function_exists( 'pll_current_language' ) && 'en' === pll_current_language() ) {
			$ret = App::tbm_get_hp_options( 'msp_en', 4 );
		}else{
			$ret = App::tbm_get_hp_options( 'msp', 4 );
		}

		$not_in      = @array_merge( $this->notin, wp_list_pluck( $ret->posts, 'ID' ) );
		$this->notin = $not_in;

		return $ret;
	}

	public function mg_hp_blocks() {
		$out = array();

		if ( function_exists( 'pll_current_language' ) && 'en' === pll_current_language() ) {
			$blocks = get_field( 'tbm_blocks_magazine_en', 'option' );
		} else {
			$blocks = get_field( 'tbm_blocks_magazine', 'option' );
		}

		// Defaults args
		$default_args = array(
			'posts_per_page' => 9,
			'post_type'      => array(
				'post',
				'longform',
				HTML_GALLERY_POST_TYPE,
				HTML_VIDEO_POST_TYPE,
				HTML_CARD_POST_TYPE
			),
			'lang'           => function_exists( 'pll_current_language' ) ? pll_current_language() : ''
		);

		foreach ( $blocks as $block ) {

			$args = array(
				'cat'          =>  $block->term_id,
				'post__not_in' => $this->notin
			);

			$block_args = wp_parse_args( $args, $default_args );

			// Execute the query
			$the_query = new \WP_Query( $block_args );

			if ( $the_query->have_posts() ) {
				// Save the returned ids in variable
				$not_in      = @array_merge( $this->notin, wp_list_pluck( $the_query->posts, 'ID' ) );
				$this->notin = $not_in;
			}

			$out[] = array(
				'name'  => $block->name,
				'url'   => get_category_link( $block ),
				'query' => $the_query
			);

		}

		return $out;

	}


}
