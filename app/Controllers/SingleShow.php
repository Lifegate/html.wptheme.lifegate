<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleShow extends Controller {
	/**
	 * Common functions for single post
	 */
	use Partials\CommonSingle;

	private $excluded_id;

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/single-programma--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

	public function single_post_relateds_special() {
		$args = array(
			'posts_per_page' => 1,
			'post_type'      => array( 'show' ),
			'orderby'        => 'rand'
		);

		$query = new \WP_Query( $args );

		/**
		 * Save id for exclude later
		 */
		if ( $query->have_posts() ) {
			$post_id           = wp_list_pluck( $query->posts, 'ID' );
			$this->excluded_id = $post_id;
		}

		return $query;
	}

	public function single_post_relateds() {
		$args  = array(
			'posts_per_page' => 4,
			'post__not_in'   => $this->excluded_id,
			'post_type'      => array( 'show' ),
			'orderby'        => 'rand'
		);
		$query = new \WP_Query( $args );

		return $query;
	}

	public function show_archive() {
		return get_post_type_archive_link( 'show' );
	}

	public function get_show_spreaker_image() {
		if ( get_field( 'lg_show_image' ) ) {
			return 1;
		};
	}

}
