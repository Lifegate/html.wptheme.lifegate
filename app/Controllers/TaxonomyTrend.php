<?php
/**
 * Created by PhpStorm.
 * User: Francesco
 * Date: 10/11/2018
 * Time: 16:00
 */

namespace App\Controllers;

use Sober\Controller\Controller;
use TbmCommon\Taxonomies\Sponsor;

class TaxonomyTrend extends Controller {

	use Partials\Pagination;

	public $acf_tax_id;
	public $trend;
	public $trend_name;
	public $trend_type;
	public $featured_ids;


	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/archive-trend--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

	public function __construct() {
		$this->run();
	}

	/**
	 * Runner function
	 */
	protected function run() {
		$queried_object = get_queried_object();

		/**
		 * If we get the objects
		 */
		if ( $queried_object ) {
			$term_id  = $queried_object->term_id;
			$taxonomy = $queried_object->taxonomy;

			$this->acf_tax_id = $taxonomy . '_' . $term_id;
			$this->trend      = $queried_object;
			$this->trend_name = $queried_object->name;
		}

	}

	public function set_high_posts() {
		$featured_posts = get_field( 'tbm_special_high_posts', $this->acf_tax_id );

		if ( $featured_posts ) {
			$this->featured_ids = $featured_posts;
		}

	}

	/**
	 * Get the attached img ID
	 *
	 * @return int
	 */
	public function acf_tax_id() {
		return $this->acf_tax_id;
	}

	/**
	 * Check if is special
	 *
	 * @return int
	 */
	public function special_type() {
		$special          = get_field( 'tbm_special_type', $this->acf_tax_id() );
		$this->trend_type = $special;

		return $special;
	}

	/**
	 * Set pagination defaults
	 *
	 * @return array
	 */
	public function pagination() {

		$out = array(
			'custom' => true,
			'link'   => get_term_link( get_queried_object_id() )
		);

		return $out;

	}

	/**
	 * Get current taxonomy
	 *
	 * @return object
	 */
	public function trend() {
		return $this->trend;
	}


	/**
	 * Get current taxonomy name
	 *
	 * @return object
	 */
	public function trend_name() {
		return $this->trend_name;
	}

	public function featured_posts() {
		$posts = get_field( 'tbm_special_featured_posts', $this->acf_tax_id );

		if ( ! $posts ) {
			return array();
		}

		$args = [
			'post_type'      => 'any',
			'post_status'    => 'publish',
			'orderby'        => 'post__in',
			'posts_per_page' => 2,
			'post__in'       => $posts
		];

		return new \WP_Query( $args );
	}

	/**
	 * One Left post in high position
	 *
	 * @return \WP_Query|null
	 */
	public function special_pp_post() {

		/**
		 * Act only if we are in a special template
		 */
		if ( $this->trend_type !== 'V2' ) {
			return null;
		}

		/**
		 * Get only first id
		 */
		$ids = array_slice( $this->featured_ids, 0, 1 );

		$my_args = array(
			'post__in'  => $ids,
			'orderby'   => 'post__in',
			'post_type' => 'any'
		);

		return new \WP_Query( $my_args );

	}

	/**
	 * One Center post in high position
	 *
	 * @return \WP_Query|null
	 */
	public function special_sp_post() {
		/**
		 * Act only if we are in a special template
		 */
		if ( $this->trend_type !== 'V2' ) {
			return null;
		}

		/**
		 * Get only first id
		 */
		$ids = array_slice( $this->featured_ids, 1, 1 );

		$my_args = array(
			'post__in'  => $ids,
			'orderby'   => 'post__in',
			'post_type' => 'any'
		);

		return new \WP_Query( $my_args );
	}

	/**
	 * Two right posts in high position
	 *
	 * @return \WP_Query|null
	 */
	public function special_list_posts() {
		/**
		 * Act only if we are in a special template
		 */
		if ( $this->trend_type !== 'V2' ) {
			return null;
		}

		/**
		 * Get only first id
		 */
		$ids = array_slice( $this->featured_ids, 2, 2 );

		$my_args = array(
			'post__in'  => $ids,
			'orderby'   => 'post__in',
			'post_type' => 'any'
		);

		return new \WP_Query( $my_args );
	}

	public function image() {
		$image = get_field( 'tbm_special_featured_img', $this->acf_tax_id );

		return array(
			'medium'        => tbm_wp_get_attachment_image_url( $image, [ 1370, 480 ] ),
			'large'         => tbm_wp_get_attachment_image_url( $image, [ 2740, 960 ] ),
			'medium_tablet' => tbm_wp_get_attachment_image_url( $image, [ 980, 343 ] ),
			'large_tablet'  => tbm_wp_get_attachment_image_url( $image, [ 1960, 700 ] ),
			'medium_mobile' => tbm_wp_get_attachment_image_url( $image, [ 400, 400 ] ),
			'large_mobile'  => tbm_wp_get_attachment_image_url( $image, [ 800, 800 ] )
		);
	}

	public function title() {
		$title = get_field( 'tbm_special_title', $this->acf_tax_id );

		if ( $title ) {
			return $title;
		}

		return get_the_archive_title();
	}

	public function content() {
		return get_field( 'tbm_special_enrichment', $this->acf_tax_id );
	}

	public function sponsor() {
		$sponsor = new Sponsor();

		if ( $sponsor->name !== null || $sponsor->url !== null ) {
			return lifegate_html_sponsor_badge( (array) $sponsor );
		}
	}

	public function share_link() {
		$url = get_term_link( $this->trend );

		if ( empty( $url ) || is_wp_error( $url ) ) {
			return '';
		}

		return rawurlencode( $url );

	}

}
