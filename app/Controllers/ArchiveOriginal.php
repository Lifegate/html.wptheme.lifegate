<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class ArchiveOriginal extends Controller {
	use Partials\Pagination;

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/archive-original--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

	/**
	 * ritorna il titolo tag / categoria
	 * @return mixed
	 */
	public function tbm_custom_tax_title() {
		$category = get_queried_object();
		$titolo_dettaglio   = get_field( 'titolo_dettaglio', $category );
		if($titolo_dettaglio != "")
			return $titolo_dettaglio;
		else
			return get_the_archive_title();

	}


	public function pagination() {

		$out = array(
			'custom' => true,
			'link'   => get_post_type_archive_link( 'original' )
		);

		return $out;

	}


	/**
	 * Primo piano
	 *
	 * @return \WP_Query
	 */
	public function or_hp_pp() {
		$ret = get_field("primo_piano_original", "options");
		return $ret;
	}

	/**
	 * Secondo piano
	 *
	 * @return \WP_Query
	 */
	public function or_hp_sp() {
		$ret = get_field("secondo_piano_original", "options");
		return $ret;
	}



}
