<?php

namespace App\Controllers\Partials;

trait Pagination {
	public function navigator_data() {
		$page          = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
		$max_num_pages = absint( $GLOBALS['wp_query']->max_num_pages );

		$is_paginable = $max_num_pages > 1 ? true : false;

		$out = array(
			'page'          => $page,
			'max_num_pages' => $max_num_pages,
			'is_paginable'  => $is_paginable
		);

		return $out;
	}

	public function replace_from() {
		$from = array( "<li><span aria-current='page' class='page-numbers current'>" );

		return $from;

	}

	public function replace_to() {
		$to = array( "<li class='selected-item'><span aria-current='page' class='page-numbers current'>" );

		return $to;
	}
}
