<?php

namespace App\Controllers\Partials;

trait CommonSingle {

	private $related_trend_output = array();

	public function post_source() {
		$sources = tbm_get_post_source( get_the_ID() ) ?: '';

		return $sources;
	}

	public function related_trend() {

		/**
		 * Get the trends tag
		 */
		$post_tags = wp_get_object_terms( get_the_ID(), array( 'trend' ), array(
			'orderby' => 'count',
			'count'   => true,
			'number'  => 1,
			'order'   => 'DESC'
		) );

		$output = [];

		if ( empty( $post_tags ) || is_wp_error( $post_tags ) ) {
			return $output;
		}

		$output = [
			'link' => get_term_link( $post_tags[0] ),
			'name' => $post_tags[0]->name
		];

		$this->related_trend_output = $output;

		return $output;
	}


	public function next_to_the_title() {
		$out = '';

		$post_magazine = wp_get_object_terms( get_the_ID(), array( 'magazine' ), array(
			'orderby' => 'count',
			'count'   => true,
			'number'  => 1,
			'order'   => 'DESC'
		) );
		if($post_magazine){
			$out = sprintf( '<a class="post__story" href="%s">%s</a>', get_term_link($post_magazine[0]), $post_magazine[0]->name );
		}else if ( get_field( 'post_tipologia' ) && get_field( 'post_tipologia' ) === 'tbm_ed_editorial' ) {
			$label = __( 'Editoriale', 'lifegate' );
			$out   = sprintf( '<span class="post__story post__story--editorial">%s</span>', $label );
		} else if ( get_field( 'post_tipologia' ) && get_field( 'post_tipologia' ) === 'tbm_ed_opinion' ) {
			$label = __( 'Opinione', 'lifegate' );
			$out   = sprintf( '<span class="post__story post__story--editorial">%s</span>', $label );
		} else  if ( $this->related_trend_output ) {
			$out = sprintf( '<a class="post__story" href="%s">%s</a>', $this->related_trend_output['link'], $this->related_trend_output['name'] );
		}

		return $out;
	}

	public function related_trends() {
		/**
		 * Get the trends tag
		 */
		$post_tags = wp_get_object_terms( get_the_ID(), array( 'trend' ), array(
			'orderby' => 'count',
			'count'   => true,
			'number'  => 3,
			'order'   => 'DESC'
		) );

		$output = [];

		if ( empty( $post_tags ) || is_wp_error( $post_tags ) ) {
			return $output;
		}

		foreach ( $post_tags as $tag ) {
			$output[] = [
				'link' => get_term_link( $tag ),
				'name' => $tag->name
			];
		}

		return $output;
	}

	public function single_post_authors() {

		$author_free = get_field( 'tbm_autore_news_free' );

		if ( $author_free ) {
			// return __( 'di', 'lifegate' ) . ' <span class="author vcard">' . $author_free . '</span>';
		}

		$authors  = get_field( 'autore_news' );
		$template = '<a href=" % s">%s</a>';

		global $authordata;


		if ( ! $authors  && ! $author_free) {
			$text = '<span class="author vcard">' . __( 'a cura della redazione', 'lifegate' ) . '</span>';

			return $text;
		}

		/**
		 * Old custom fields are strings. Cast to array
		 */
		if ( $authors && !is_array( $authors ) ) {
			$authors = (array) $authors;
		}

		foreach ( $authors as $author ) {
			$post = get_post( $author );
			if ( empty( $post ) ) {
				$out[] = __( 'Redazione LifeGate', 'lifegate' );
				continue;
			}
			$out[] = sprintf( $template, get_permalink( $author ), get_the_title( $author ) );
		}


		if ( $author_free ) {
			$out[] = $author_free;
		}


		/**
		 * If authors are more than 2, we need other form
		 */
		if ( count( $out ) > 2 ) {
			return __( 'di', 'lifegate' ) . ' ' . implode( ', ', array_slice( $out, 0, - 1 ) ) . ' '. __( 'e', 'lifegate' ) . ' ' . array_slice( $out, - 1 )[0];
		}

		/**
		 * If authors are 2 or 1
		 */
		return __( 'di', 'lifegate' ) . ' ' . implode( ' '.__( 'e', 'lifegate' ).' ', $out );
	}

	public function share_link() {
		$url = get_permalink( get_the_ID() );

		if ( empty( $url ) || is_wp_error( $url ) ) {
			return '';
		}

		return rawurlencode( $url );

	}

	public function single_language_switcher() {

		if ( ! function_exists( 'pll_get_post' ) ) {
			return null;
		}

		$ita_id = pll_get_post( get_the_ID(), 'it' );
		$eng_id = pll_get_post( get_the_ID(), 'en' );

		$ita = ( pll_get_post_language( get_the_ID() ) === 'en' && $ita_id && 'publish' === get_post_status( $ita_id ) ) ? get_permalink( $ita_id ) : '';
		$eng = ( pll_get_post_language( get_the_ID() ) === 'it' && $eng_id && 'publish' === get_post_status( $eng_id ) ) ? get_permalink( $eng_id ) : '';

		if ( $ita || $eng ) {
			$out = array(
				'it' => $ita,
				'en' => $eng
			);

			return $out;

		}


		return false;

	}

}
