<?php

namespace App\Controllers\Partials;

use TbmCommon\Taxonomies\Sponsor;

trait LifegateSingle {

	public function is_liveblog_post() {
		if ( ! class_exists( 'WPCOM_Liveblog' ) ) {
			return false;
		}

		return \WPCOM_Liveblog::is_liveblog_post( get_the_ID() );
	}

	public function single_post_location() {
		return get_field( 'tbm_post_location' );
	}


	public function single_post_iniziatives() {
		$posts = tbm_get_strict_related_posts( get_the_ID(), 10, array(), SITE_SPECIAL_TAXONOMY, 'iniziative', true, 'object' );

		return $posts;
	}


	public function single_post_relateds_special() {
		$posts = tbm_get_custom_related_posts( get_the_ID(), 3, array( get_the_ID() ), SITE_SPECIAL_TAXONOMY, array(
			'post',
			HTML_GALLERY_POST_TYPE,
			HTML_VIDEO_POST_TYPE,
			HTML_CARD_POST_TYPE
		), 'object' );

		return $posts;
	}

	public function single_post_relateds() {
		$posts = tbm_get_custom_related_posts( get_the_ID(), 6, array( get_the_ID() ), SITE_SPECIAL_TAXONOMY, array(
			'post',
			HTML_GALLERY_POST_TYPE,
			HTML_VIDEO_POST_TYPE,
			HTML_CARD_POST_TYPE
		), 'object' );

		return $posts;
	}

	public function sponsor() {
		$sponsor = new Sponsor();

		if ( $sponsor->name !== null || $sponsor->url !== null ) {
			return lifegate_html_sponsor_badge( (array) $sponsor );
		}
	}


	public function blockchain_footer() {

		$ret = "";
		if(get_field("blockchain_main_id")){
			$ret .= "<div class='section-blockchain'>L'autenticità di questa notizia è certificata in <span class='icon-blockchain'> blockchain</span>. ";
			$ret .= "<a  href='".home_url("/blockchain")."?bid=".get_the_ID()."' rel='nofollow'>Scopri di più</a></div>";
		}
		return $ret;
	}

}
