<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class PageRadio extends Controller {

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/frontpage-magazine--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

	/**
	 * featured section
	 *
	 * @return \WP_Query
	 */
	public function hp_pr() {
		/**
		 * Non standard request
		 */
		$posts = App::tbm_get_merged_hp_options( array(
			App::tbm_get_hp_options( 'pr_center', 1, array( 'post' ), 'id' ),
			App::tbm_get_hp_options( 'pr_dx', 1, array( 'post' ), 'id' )
		) );

		return $posts;
	}

	public function radio_hp_blocks() {
		$out = array();

		$cat_id = get_term_by( 'slug', 'musica', 'category' )->term_id;


		// Defaults args
		$default_args = array(
			'posts_per_page' => 9,
			'post_type'      => array(
				'post',
				'longform',
				HTML_GALLERY_POST_TYPE,
				HTML_VIDEO_POST_TYPE,
				HTML_CARD_POST_TYPE
			),
			'lang'           => function_exists( 'pll_current_language' ) ? pll_current_language() : ''
		);


		$not_in = get_query_var( 'notin', array() );

		$args = array(
			'cat'          => (int) $cat_id,
			'post__not_in' => $not_in,
		);

		$block_args = wp_parse_args( $args, $default_args );

		// Execute the query
		$the_query = new \WP_Query( $block_args );

		if ( $the_query->have_posts() ) {
			// Save the returned ids in variable
			$not_in      = @array_merge( $not_in, wp_list_pluck( $the_query->posts, 'ID' ) );
			$this->notin = $not_in;
		}

		$out[] = array(
			'name'  => get_cat_name( $cat_id ),
			'url'   => get_category_link( $cat_id ),
			'query' => $the_query
		);

		return $out;

	}


}
