<?php
/**
 * Created by PhpStorm.
 * User: Francesco
 * Date: 10/11/2018
 * Time: 16:00
 */

namespace App\Controllers;

use Sober\Controller\Controller;
use TbmCommon\Taxonomies\Sponsor;

class TaxonomyMagazine extends Controller {

	use Partials\Pagination;

	public $acf_tax_id;
	public $trend;
	public $trend_name;
	public $special_ids = null;
	public $featured_ids = null;

	public function __construct() {
		$this->run();
	}

	/**
	 * Runner function
	 */
	protected function run() {
		$queried_object = get_queried_object();

		/**
		 * If we get the objects
		 */
		if ( $queried_object ) {
			$term_id  = $queried_object->term_id;
			$taxonomy = $queried_object->taxonomy;

			$this->acf_tax_id = $taxonomy . '_' . $term_id;
			$this->trend      = $queried_object;
			$this->trend_name = $queried_object->name;
		}

	}

	/**
	 * Set ATF posts
	 */
	public function set_high_posts() {
		$featured_posts = get_field( 'tbm_magazine_high_posts', $this->acf_tax_id );

		if ( $featured_posts ) {
			$this->special_ids = $featured_posts;
		}
	}

	/**
	 * Get the attached img ID
	 *
	 * @return int
	 */
	public function acf_tax_id() {
		return $this->acf_tax_id;
	}

	/**
	 * Set pagination defaults
	 *
	 * @return array
	 */
	public function pagination() {

		$out = array(
			'custom' => true,
			'link'   => get_term_link( get_queried_object_id() )
		);

		return $out;

	}

	/**
	 * Get current taxonomy
	 *
	 * @return object
	 */
	public function trend() {
		return $this->trend;
	}

	/**
	 * Get current taxonomy name
	 *
	 * @return object
	 */
	public function trend_name() {
		return $this->trend_name;
	}

	/**
	 * Get featured posts in bottom of the content
	 *
	 * @return array|\WP_Query
	 */
	public function featured_posts() {
		$posts = get_field( 'tbm_magazine_featured_posts', $this->acf_tax_id );

		if ( ! $posts ) {
			return array();
		}

		$this->featured_ids = $posts;

		$args = [
			'post_type'      => 'any',
			'post_status'    => 'publish',
			'posts_per_page' => 2,
			'orderby'        => 'post__in',
			'post__in'       => $posts
		];

		return new \WP_Query( $args );
	}

	/**
	 * Get first position posts
	 *
	 * @return \WP_Query|null
	 */
	public function special_pp_post() {

		if ( ! $this->special_ids ) {
			return null;
		}

		/**
		 * Get only first id
		 */
		$ids = array_slice( $this->special_ids, 0, 1 );

		$my_args = array(
			'post_type' => 'any',
			'post__in'  => $ids,
			'orderby'   => 'post__in',
			'post_type' => 'any'
		);

		return new \WP_Query( $my_args );

	}

	/**
	 * One Center post in high position
	 *
	 * @return \WP_Query|null
	 */
	public function special_sp_post() {

		if ( ! $this->special_ids ) {
			return null;
		}

		/**
		 * Get only first id
		 */
		$ids = array_slice( $this->special_ids, 1, 1 );

		$my_args = array(
			'post_type' => 'any',
			'post__in'  => $ids,
			'orderby'   => 'post__in',
			'post_type' => 'any'
		);

		return new \WP_Query( $my_args );
	}

	/**
	 * Two right posts in high position
	 *
	 * @return \WP_Query|null
	 */
	public function special_list_posts() {

		if ( ! $this->special_ids ) {
			return null;
		}


		/**
		 * Get only first id
		 */
		$ids = array_slice( $this->special_ids, 2, 2 );

		$my_args = array(
			'post_type' => 'any',
			'post__in'  => $ids,
			'orderby'   => 'post__in',
			'post_type' => 'any'
		);

		return new \WP_Query( $my_args );
	}

	public function image() {
		$image = get_field( 'tbm_special_featured_img', $this->acf_tax_id );

		return array(
			'medium_mobile'  => tbm_wp_get_attachment_image_url( $image, [ 398, 398 ] ),
			'large_mobile'   => tbm_wp_get_attachment_image_url( $image, [ 796, 796 ] ),
			'medium_desktop' => tbm_wp_get_attachment_image_url( $image, [ 1368, 342 ] ),
			'large_desktop'  => tbm_wp_get_attachment_image_url( $image, [ 2736, 684 ] )
		);
	}

	public function sponsor() {
		$sponsor = new Sponsor();

		if ( $sponsor->name !== null || $sponsor->url !== null ) {
			return lifegate_html_sponsor_badge( (array) $sponsor );
		}
	}

	public function share_link() {
		$url = get_term_link( $this->trend );

		if ( empty( $url ) || is_wp_error( $url ) ) {
			return '';
		}

		return rawurlencode( $url );

	}

}
