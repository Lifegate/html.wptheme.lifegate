<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleIniziative extends Controller {
	use Partials\CommonSingle;

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/single-iniziativa--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

	public function get_strip() {
		$tbm_iniziative_strip_sx       = get_field( 'tbm_iniziative_strip_sx' );
		$tbm_iniziative_strip_dx       = get_field( 'tbm_iniziative_strip_dx' );
		$tbm_iniziative_strip_sx_title = get_field( 'tbm_iniziative_strip_sx_title' );
		$tbm_iniziative_strip_dx_title = get_field( 'tbm_iniziative_strip_dx_title' );

		$tbm_iniziative_background = get_field( 'tbm_iniziative_background' );

		if ( $tbm_iniziative_strip_sx && $tbm_iniziative_strip_dx ) {
			return array(
				'sx'       => apply_filters( 'the_content', $tbm_iniziative_strip_sx ),
				'dx'       => apply_filters( 'the_content', $tbm_iniziative_strip_dx ),
				'title_sx' => $tbm_iniziative_strip_sx_title,
				'title_dx' => $tbm_iniziative_strip_dx_title,
				'image'    => tbm_wp_get_attachment_image_url( $tbm_iniziative_background, array( 1920, 370 ) )
			);
		}

		return false;

	}

	public function get_contacts() {
		$form = get_field( 'tbm_iniziative_gf_form' );

		if ( $form ) {
			gravity_form_enqueue_scripts( $form, true );

			return gravity_form( $form, true, true, false, '', true, 1, false );
		}

		return '';

	}


	public function get_footer() {
		return get_field( 'tbm_iniziative_footer' );
	}

}

