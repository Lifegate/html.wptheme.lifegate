<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller {

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/homepage--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

	/**
	 * Primo piano
	 *
	 * @return \WP_Query
	 */
	public function hp_pp() {

		$custom_not_in = App::tbm_get_hide_from_hp();

		if ( function_exists( 'pll_current_language' ) && 'en' === pll_current_language() ) {
			return App::tbm_get_hp_options( 'pp_en', 1, array( 'post' ), 'object', false, $custom_not_in );
		}

		return App::tbm_get_hp_options( 'pp', 1, array( 'post' ), 'object', false, $custom_not_in );

	}



	/**
	 * Secondo piano
	 *
	 * @return \WP_Query
	 */
	public function hp_sp() {

		$custom_not_in = App::tbm_get_hide_from_hp();

		if ( function_exists( 'pll_current_language' ) && 'en' === pll_current_language() ) {
			return App::tbm_get_hp_options( 'sp_en', 3, array( 'post' ), 'object', false, $custom_not_in );
		}

		return App::tbm_get_hp_options( 'sp', 3, array( 'post' ), 'object', false, $custom_not_in );
	}

	/**
	 * Agisci ora
	 *
	 * @return \WP_Query
	 */
	public function hp_ao() {
		if ( function_exists( 'pll_current_language' ) && 'en' === pll_current_language() ) {
			return App::tbm_get_hp_options( 'ao_en', 2, 'iniziative' );
		}

		return App::tbm_get_hp_options( 'ao', 2, 'iniziative' );
	}

	/**
	 * Magazine Primo Piano
	 *
	 * @return \WP_Query
	 */
	public function hp_mp() {
		return App::tbm_get_hp_options( 'mp', 1 );
	}

	/**
	 * Magazine Secondo Piano
	 *
	 * @return \WP_Query
	 */
	public function hp_ms() {
		return App::tbm_get_hp_options( 'ms', 2 );
	}

	/**
	 * Media Primo Piano
	 *
	 * @return \WP_Query
	 */
	public function hp_xp() {
		return App::tbm_get_hp_options( 'xp', 2 );
	}

	/**
	 * Media Seconda Piano
	 *
	 * @return \WP_Query
	 */
	public function hp_xs() {
		return App::tbm_get_hp_options( 'xs', 5 );
	}

	/**
	 * Iniziative
	 *
	 * @return \WP_Query
	 */
	public function hp_iz() {
		return App::tbm_get_hp_options( 'iz', 10, 'iniziative', 'object', true );
	}


	/**
	 * Long Form
	 *
	 * @return \WP_Query
	 */
	public function hp_lf() {
		if ( function_exists( 'pll_current_language' ) && 'en' === pll_current_language() ) {
			return App::tbm_get_hp_options( 'lf_en', 10, 'longform', 'object', true );
		}
		return App::tbm_get_hp_options( 'lf', 10, 'longform', 'object', true );
	}

	/**
	 * Evento Primo piano
	 *
	 * @return \WP_Query
	 */
	public function hp_ep() {
		return App::tbm_get_hp_options( 'ep', 1, HTML_EVENT_POST_TYPE );
	}

	/**
	 * Eventi Secondo piano
	 *
	 * @return \WP_Query
	 */
	public function hp_es() {
		return App::tbm_get_hp_options( 'es', 4, HTML_EVENT_POST_TYPE );
	}

	/**
	 * Video Primo piano
	 *
	 * @return \WP_Query
	 */
	public function hp_vp() {
		return App::tbm_get_hp_options( 'vp', 1, HTML_VIDEO_POST_TYPE );
	}

	/**
	 * Video Secondo piano
	 *
	 * @return \WP_Query
	 */
	public function hp_vs() {
		return App::tbm_get_hp_options( 'vs', 4, HTML_VIDEO_POST_TYPE );
	}

	/**
	 * Programmi centro e destra
	 *
	 * @return \WP_Query
	 */
	public function hp_pr() {
		/**
		 * Non standard request
		 */

		$posts = App::tbm_get_merged_hp_options( array(
			App::tbm_get_hp_options( 'pr_center', 1, array( 'post' ), 'id' ),
			App::tbm_get_hp_options( 'pr_dx', 1, array( 'post' ), 'id' )
		) );

		if ( $posts ) {
			return $posts;
		}
	}

	/**
	 * Fondo primo piano
	 *
	 * @return \WP_Query
	 */
	public function hp_fp() {
		return App::tbm_get_hp_options( 'fp', 4 );
	}

	/**
	 * Fondo secondo piano
	 *
	 * @return \WP_Query
	 */
	public function hp_fs() {
		return App::tbm_get_hp_options( 'fs', 10 );
	}

	/**
	 * Video background
	 *
	 * @return string
	 */
	public function video_background() {
		$bg = get_field( 'tbm_hp_video_bg', 'options' );
		if ( $bg ) {
			return tbm_wp_get_attachment_image_url( $bg["ID"], array( 1920, 1280 ) );
		}

		return '';
	}

	/**
	 * Contenuti speciali in primo piano
	 *
	 * @return \WP_Query
	 */
	public function hp_cs() {
		if ( function_exists( 'pll_current_language' ) && 'en' === pll_current_language() ) {
			return get_field( 'hp_cs_en', 'option' );
		}

		return get_field( 'hp_cs', 'option' );
	}



	/**
	 * Contenuti speciali in secondo Primo PIano
	 *
	 * @return array
	 */
	public function hp_pp2() {
		if ( function_exists( 'pll_current_language' ) && 'en' === pll_current_language() ) {
			return get_field( 'hp_pp2_en', 'option' );
		}

		return get_field( 'hp_pp2', 'option' );
	}


	/**
	 * Contenuti speciali in secondo piano
	 *
	 * @return array
	 */
	public function hp_ss() {
		if ( function_exists( 'pll_current_language' ) && 'en' === pll_current_language() ) {
			return get_field( 'hp_ss_en', 'option' );
		}

		return get_field( 'hp_ss', 'option' );
	}

	/**
	 * Contenuti speciali manuali ad una colonna
	 *
	 * @return string
	 */
	public function hp_man_a() {

		$suffix = function_exists( 'pll_current_language' ) && 'en' === pll_current_language() ? '_en' : '';

		$out = array();

		$repeaters = get_field( 'tbm_special_manual_field' . $suffix, 'option' );

		if ( ! $repeaters ) {
			return '';
		}

		foreach ( $repeaters as $repeater ) {

			\App\asset_path( 'css/components/partials/card-longform-big.min.css' );
			$template = '<article class="card-longform-big flex"><div class="card__thumbnail--big">
							<picture data-link="%1$s">
								<img class="lazyload" data-srcset="%2$s, %3$s 2x" alt="%4$s"/>
							</picture>
							</div><div class="card__main-container">
								<div class="card__title"><a href="%1$s"><h3>%4$s</h3></a></div>
								<div class="card__content"><p class="abstract">%5$s</p></div>
								<div class="card__footer">%6$s</div>
							</div></article>';

			$title      = $repeater[ 'text' . $suffix ];
			$url        = $repeater[ 'url' . $suffix ];
			$excerpt    = $repeater[ 'excerpt' . $suffix ];
			$image_b    = tbm_wp_get_attachment_image_url( $repeater[ 'image' . $suffix ]['ID'], array( 470, 220 ) );
			$image_b_2x = tbm_wp_get_attachment_image_url( $repeater[ 'image' . $suffix ]['ID'], array( 840, 440 ) );
			$sponsor    = term_exists( (int) $repeater[ 'sponsor' . $suffix ], 'sponsor' ) ? lifegate_sponsor_badge( (int) $repeater[ 'sponsor' . $suffix ] ) : '';

			$out[] = sprintf( $template, $url, $image_b, $image_b_2x, $title, $excerpt, $sponsor );

		}

		return implode( '', $out );

	}

	/**
	 * Contenuti speciali manuali a due colonne
	 *
	 * @return string
	 */
	public function hp_man_b() {
		$suffix = function_exists( 'pll_current_language' ) && 'en' === pll_current_language() ? '_en' : '';

		$out       = array();
		$repeaters = get_field( 'tbm_special_manual_field_bis' . $suffix, 'option' );

		if ( ! $repeaters ) {
			return $out;
		}

		foreach ( $repeaters as $repeater ) {

			$template =  '<article class="card-speciale-big %7$s"><div class="card__title flex"><span class="post__story"></span><div>
				<a href="%1$s"><h3>%2$s</h3></a><p class="abstract">%3$s</p></div></div>
			<div class="card__thumbnail--big"><picture data-link="%1$s"><img class="lazyload" data-srcset="%4$s, %5$s 2x"alt="%2$s"/>
			</picture></div>%6$s</article>';


			$title_a    = $repeater[ 'text_a' . $suffix ];
			$url_a      = $repeater[ 'url_a' . $suffix ];
			$excerpt_a  = $repeater[ 'excerpt_a' . $suffix ];
			$image_a_1x = tbm_wp_get_attachment_image_url( $repeater[ 'image_a' . $suffix ]['ID'], array( 470, 118 ) );
			$image_a_2x = tbm_wp_get_attachment_image_url( $repeater[ 'image_a' . $suffix ]['ID'], array( 940, 236 ) );
			$sponsor_a  = term_exists( (int) $repeater[ 'sponsor_a' . $suffix ], 'sponsor' ) ? lifegate_sponsor_badge( (int) $repeater[ 'sponsor_a' . $suffix ] ) : '';
			$class_a = ""; //if($sponsor_a) $class_a = "sponsored";

			$title_b    = $repeater[ 'text_b' . $suffix ];
			$url_b      = $repeater[ 'url_b' . $suffix ];
			$excerpt_b  = $repeater[ 'excerpt_b' . $suffix ];
			$image_b_1x = tbm_wp_get_attachment_image_url( $repeater[ 'image_b' . $suffix ]['ID'], array( 470, 118 ) );
			$image_b_2x = tbm_wp_get_attachment_image_url( $repeater[ 'image_b' . $suffix ]['ID'], array( 940, 236 ) );
			$sponsor_b  = term_exists( (int) $repeater[ 'sponsor_b' . $suffix ], 'sponsor' ) ? lifegate_sponsor_badge( (int) $repeater[ 'sponsor_b' . $suffix ] ) : '';
			$class_b = ""; //if($sponsor_b) $class_b = "sponsored";


			$out[] = '<div><div class="container-cycle-col-2">' . sprintf( $template, $url_a, $title_a, $excerpt_a, $image_a_1x, $image_a_2x, $sponsor_a, $class_a )
			         . sprintf( $template, $url_b, $title_b, $excerpt_b, $image_b_1x, $image_b_2x, $sponsor_b, $class_b ) . '</div></div>';
		}

		return implode( '', $out );

	}

	/**
	 * Above the fold
	 *
	 * @return string
	 */
	public function atf_v() {
		return get_field( 'tbm_hp_atf', 'option' ) ?: 'V1';
	}

	/**
	 * Get the iniziative post type archive link
	 *
	 * @return string
	 */
	public function iniziative_hp() {
		return get_post_type_archive_link( 'iniziative' );
	}

	/**
	 * Get the longform post type archive link
	 *
	 * @return string
	 */
	public function longform_hp() {
		return get_post_type_archive_link( 'longform' );
	}

	/**
	 * Get the magazine post type archive link
	 *
	 * @return string
	 */
	public function magazine_hp() {
//		$page = get_page_by_path("news");
//		$t_page = pll_get_post($page->ID, pll_current_language());

//		return get_the_permalink($t_page);
		return pll_home_url(  pll_current_language())."news";
	}

	/**
	 * Get the lifegate radio player
	 *
	 * @return string
	 */
	public function player_radio() {

		return get_page_link( get_page_by_path( "lifegate-radio" ) );
	}

	/**
	 * Get the radio post type archive link
	 *
	 * @return string
	 */
	public function radio_hp() {
		return get_page_link( get_page_by_path( "radio" ) );
	}

}
