<?php

namespace App\Controllers;

use App\Controllers\Partials\Pagination;
use Sober\Controller\Controller;

class Category extends Controller {
	use Partials\Pagination;

	public function tbm_related_category() {
		$category = get_queried_object();
		$trends   = get_field( 'tbm_category_trend', $category );

		if ( ! $trends ) {
			return '';
		}

		$out = array();

		foreach ( $trends as $trend ) {
			$out[] = sprintf( '<a href="%s">%s</a>', get_term_link( $trend, 'trend' ), get_term_field( 'name', $trend, 'trend' ) );
		}

		return implode( ', ', $out );

	}


	/**
	 * ritorna il titolo tag / categoria
	 * @return mixed
	 */
	public function tbm_custom_tax_title() {
		$category = get_queried_object();
		$titolo_dettaglio   = get_field( 'titolo_dettaglio', $category );
		if($titolo_dettaglio != "")
			return $titolo_dettaglio;
		else
			return get_the_archive_title();

	}

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/archive--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

	public function pagination() {

		$out = array(
			'custom' => true
		);

		return $out;

	}

	public function hero() {
		$category = get_queried_object();

		return new \WP_Query( array( 'cat' => $category->term_id, 'posts_per_page' => 1 ) );
	}

	public function evidence() {
		$category = get_queried_object();
		$id       = $category->term_id;
		$blocks   = [
			'blocks' => array(
				[
					'template'    => 'partial-card-post-big--k2',
					'posts'       => new \WP_Query( array( 'cat' => $id, 'posts_per_page' => 2, 'offset' => 1, ) ),
					'post_number' => 1
				],
				[
					'template'    => 'partial-card-post-small--k2',
					'posts'       => new \WP_Query( array( 'cat' => $id, 'posts_per_page' => 7, 'offset' => 3, ) ),
					'post_number' => 1
				]
			)
		];

		return $blocks;
	}

	public function archive() {
		$category = get_queried_object();
		$paged    = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

		return new \WP_Query( array(
			'cat'            => $category->term_id,
			'posts_per_page' => 10,
			'offset'         => 9 * $paged,
			'paged'          => $paged
		) );
	}


	public function paged() {
		$page = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

		return $page;
	}

}
