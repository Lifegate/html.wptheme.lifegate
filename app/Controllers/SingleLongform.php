<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleLongform extends Controller {
	/**
	 * Common functions for single post
	 */
	use Partials\CommonSingle;

	/**
	 * Functions are here
	 */
	use Partials\LifegateSingle;

}
