<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class PageTrend extends Controller {
    use Partials\Pagination;

    protected $custom_pagination = [];

    public function last_terms() {
        $page = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

        $terms_per_page = get_option( 'posts_per_page' );
        $found_terms    = wp_count_terms( 'trend' );
        $offset         = ( $page - 1 ) * $terms_per_page;

        $my_args = array(
            'taxonomy' => 'trend',
            'number'   => $terms_per_page,
            'orderby'  => 'count',
            'count'    => true,
            'order'    => 'DESC',
            'offset'   => $offset
        );

        if ( false === ( $the_terms = get_transient( 'list_trend_terms_xx_' . $offset ) ) ) {
            $the_terms = new \WP_Term_Query( $my_args );
            set_transient( 'list_trend_terms_' . $offset, $the_terms, 86400 );
        }

        $this->custom_pagination = array(
	        'page'          => $page,
	        'max_num_pages' => ceil( $found_terms / $terms_per_page ),
	        'is_paginable'  => true,
	        'custom'        => true,
	        'link'          => get_page_link()
        );

        return $the_terms->get_terms();
    }

    public function custom_pagination() {
        return $this->custom_pagination;
    }


    /**
     * Returns the image attached to the term (with ACF)
     *
     * @param WP_Term $term Term object to get its image.
     *
     * @return string Image ID, or empty string if none available.
     */
    public static function get_term_image( $term ) {

        if ( ! is_object( $term ) || ! property_exists( $term, 'term_id' ) || ! property_exists( $term, 'taxonomy' ) ) {
            return '';
        }

	    $image = get_field( 'tbm_trend_immagine', $term->taxonomy . '_' . $term->term_id );

        return $image;
    }

}
