<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SinglePost extends Controller {
	/**
	 * Common functions for single post
	 */
	use Partials\CommonSingle;

	/**
	 * Functions are here
	 */
	use Partials\LifegateSingle;

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/single-post--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

	public function act_now_box() {

		return false;
		$initiative = get_field( 'tbm_post_initiative' );

		if ( ! $initiative ) {
			$suffix = function_exists( 'pll_current_language' ) && 'en' === pll_current_language() ? '_en' : '';

			$url = 	get_field( 'act_now_link_azione'. $suffix, 'option' );
			if($url  == "")
				$url = 'https://store.lifegate.com/';

			$testo = 	get_field( 'act_now_testo_azione'. $suffix, 'option' );
			if($testo == "")
				$testo = __("Agisci ora!", "lifegate");

			$icona = 	get_field( 'act_now_icona'. $suffix, 'option' );

			if($icona == "")
				return sprintf( '<a href="%s" target="_blank" class="cta cta--sticky cta--icon cta-custom" data-tooltip="%s"></a>', $url, $testo );
			else
				return sprintf( '<a href="%s" target="_blank" class="cta cta--sticky cta--icon cta-custom cta-no-icon" data-tooltip="%s" style="background-image: url(%s); background-position:0px 0px; background-size:48px 48px;"></a>', $url, $testo, $icona );


		}

		/**
		 * Enqueue the styles
		 */

		$title    = get_the_title( $initiative );
		$subtitle = get_the_excerpt( $initiative );
		$cta      = __('Agisci ora', "lifegate");
		$link     = get_permalink( $initiative );
		$image    = tbm_get_the_post_thumbnail_url( $initiative, array(
			980,
			280
		) );

		$code = "<div class=\"card-act-now\"><div class=\"lazyload\" data-bg=\"%s\"><div class=\"inner-cta\">
			<h3>%s</h3><div class=\"inner-cta__content\"><p>%s</p><a href=\"%s\" class=\"cta cta--icon cta--solid cta--icon-right cta--act-now\">%s</a></div></div></div></div>";

		$mobile_cta = '<a href="%s" class="cta cta--sticky cta--icon" data-tooltip="' . __( 'Agisci ora', 'lifegate' ) . '!"></a>';

		return sprintf( $code, $image, $title, $subtitle, $link, $cta ) . sprintf( $mobile_cta, $link );
	}

	/**
	 * stampa tags
	 */

	public function lg_post_tag(){

		$tags = wp_get_post_tags(get_the_ID(), array("orderby" => "count", "order" => "desc", "number" => 4));
		$html = "";
		if(count($tags)) {
			$html .= '<div class="post_tags"> <p>' . __("Leggi altri articoli su questi temi:", "lifegate").' ';
			$c = 0;
			foreach ($tags as $tag) {
				if ($c) $html .= ", ";
				$c++;
				$tag_link = get_tag_link($tag->term_id);

				$html .= "<a href='{$tag_link}' title='{$tag->name} Tag' class='{$tag->slug}'>";
				$html .= "{$tag->name}</a>";
			}
			$html .= '</p></div>';
		}
		return $html;
	}

	/**
	 * Get the ID of the thumb
	 *
	 * @return int|mixed|string|null
	 */
	public function post_thumb_id() {

		/**
		 * If image have not post thumbnail
		 */
		if ( ! has_post_thumbnail() ) {
			return null;
		}

		$thumb_id = get_post_thumbnail_id();

		/**
		 * If article was published after Mon, 01 Jun 2020 00:00:00 +0000, return raw thumb_id
		 */
		if ( get_the_date( 'U' ) > 1590969600 ) {
			return $thumb_id;
		}

		if ( function_exists( 'tbm_get_thumbor_img_original_size' ) ) {
			$image = tbm_get_thumbor_img_original_size( $thumb_id );

			if ( $image[0] < 980 ) {
				return get_field( 'tbm_site_default_image', 'option' );
			}
		}


		return $thumb_id;

	}

	/**
	 * Get News homepage
	 *
	 * @return bool|false|string
	 */
	public function post_hp() {
		return get_page_link( get_page_by_path( "news" ) );
	}

}
