<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleVideo extends Controller {
	use Partials\CommonSingle;

	/**
	 * Functions are here
	 */
	use Partials\LifegateSingle;

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/single-video--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

	public function print_video() {

		$out = '';

		if ( function_exists( 'html_video_generate_video_player' ) ) {
			return html_video_generate_video_player( get_the_ID(), '', false );
		}

		//Build html tag for video
		$tpl_out          = '<video src="%%VIDEOURL%%" controls controlsList="nodownload"></video>';
		$video_properties = get_field( 'tbm_videoid', get_the_ID() );

		//Analyze video properties retrieved
		if ( ! is_array( $video_properties ) ) {//Should be an id
			$video_url = wp_get_attachment_url( $video_properties );
		} else {//Array of properties
			$video_url = $video_properties['url'];
		}

		//Check video url validity
		if ( ! empty( $video_url ) && filter_var( $video_url, FILTER_VALIDATE_URL ) ) {
			$out = str_replace( '%%VIDEOURL%%', $video_url, $tpl_out );
		}

		return $out;

	}

	public function socialsection() {
		$args  = array(
			'post_type'      => 'post',
			'post_status'    => 'publish',
			'posts_per_page' => 2
		);
		$posts = new \WP_Query( $args );
		$query = [
			'colNumber'       => 2,
			'componentNumber' => 2,
			'type'            => 'posts',
			'component'       => 'partial-card-social-instagram--k2',
			'posts'           => $posts,
			'class'           => 'partial-card-social-instagram--k2',
			'gutter'          => 10

		];

		return $query;
	}

	public function single_video_relateds_special() {
		$relateds = tbm_get_custom_related_posts( get_the_ID(), 1, array( get_the_ID() ), SITE_SPECIAL_TAXONOMY, array(
			HTML_VIDEO_POST_TYPE,
		), 'object' );

		return $relateds;
	}

	public function single_video_relateds() {
		$relateds = tbm_get_custom_related_posts( get_the_ID(), 4, array( get_the_ID() ), SITE_SPECIAL_TAXONOMY, array(
			HTML_VIDEO_POST_TYPE,
		), 'object' );

		return $relateds;
	}

	public function video_hp() {
		return get_post_type_archive_link( HTML_VIDEO_POST_TYPE );
	}


}


