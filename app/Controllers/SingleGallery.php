<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleGallery extends Controller {

	use Partials\CommonSingle;

	public $photo_array;
	public $page;
	public $photos;
	public $nav = array();
	public $next_gallery = '';
	public $next_social_gallery = '';

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/single-gallery--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

	public function photos() {

		$photos       = get_field( "tbm_gallery" );
		$this->photos = $photos;

		// Save the public var
		$page              = ( get_query_var( 'page' ) ) ?: 1;
		$photo_id          = $page - 1;
		$this->photo_array = $photos[ $photo_id ];

		return $photos;

	}

	public function photo_array() {

		return $this->photo_array;

	}

	public function is_slider() {
		if ( isset( $_GET['g'] ) && $_GET['g'] == 's' ) {
			return true;
		}

		return false;

	}

	public function paged() {

		$page       = ( get_query_var( 'page' ) ) ?: 1;
		$this->page = $page;

		return $page;

	}

	public function get_related_gallery_small() {

		$articles = tbm_get_custom_related_posts( null, 3, array(), 'storia', 'gallery', 'object' );

		return $articles;

	}

	public function get_related_videos() {

		$videos = tbm_get_custom_related_posts( null, 3, array(), 'storia', 'video', 'object' );

		return $videos;

	}

	public function get_ratio() {
		if ( ! function_exists( 'tbm_get_thumbor_img_ratio' ) ) {
			return '60';
		}

		$ratio = tbm_get_thumbor_img_ratio( $this->photo_array['ID'], array( 640, 0 ) );

		if ( $ratio ) {
			return $ratio;
		}

		return '60';
	}

	public function next_social_gallery() {
		$posts = get_posts(
			array(
				'post_type'      => HTML_GALLERY_POST_TYPE,
				'tag'            => 'play',
				'orderby'        => 'rand',
				'posts_per_page' => - 1,
				'post__not_in'   => array( get_the_ID() )
			)
		);

		if ( $posts ) {
			$permalink                 = get_permalink( $posts[0] );
			$this->next_social_gallery = $permalink;

			return $permalink;
		}

		return '';

	}

	public function related() {

		$gallery            = tbm_get_custom_related_posts( get_the_ID(), 1, [], 'listino', 'gallery', true );
		$this->next_gallery = $gallery[0]->permalink;

		if ( has_tag( 'play' ) ) {

			$posts = get_posts(
				array(
					'post_type'      => HTML_GALLERY_POST_TYPE,
					'tag'            => 'play',
					'orderby'        => 'rand',
					'posts_per_page' => - 1,
					'post__not_in'   => array( get_the_ID() )
				)
			);

			if ( $posts ) {
				$permalink          = get_permalink( $posts[0] );
				$this->next_gallery = $permalink;
			}

		}

	}

	public function bottom_related() {
		global $post;


		$blocks = [
			'blocks' => array(
				[
					'template'    => 'partial-card-post-big--V1',
					'posts'       => tbm_get_custom_related_posts( $post->ID, 1, [], 'trend', 'post', 'object' ),
					'post_number' => 1
				],
				[
					'template'    => 'partial-card-video-big--k2',
					'posts'       => tbm_get_custom_related_posts( $post->ID, 1, [], 'trend', 'video', 'object' ),
					'post_number' => 1
				],
				[
					'template'    => 'partial-card-post-small--k2',
					'posts'       => tbm_get_custom_related_posts( $post->ID, 3, [], 'trend', 'gallery', 'object' ),
					'post_number' => 1
				]
			)
		];

		return $blocks;
	}

	public function get_nav() {

		$prev = '';
		$next = '';

		$page_array = paginate_links( array(
			'base'    => get_permalink( get_the_ID() ) . "/%#%",
			'format'  => '/%#%',
			'current' => $this->page,
			'total'   => count( $this->photos ),
			'type'    => 'array'
		) );


		if ( preg_match( '#href="([^"]+)"#', $page_array[0], $matches ) ) {
			$prev = isset( $matches[1] ) ? $matches[1] : '';
		}

		if ( preg_match( '#href="([^"]+)"#', $page_array[ count( $page_array ) - 1 ], $matches ) ) {
			$next = isset( $matches[1] ) ? $matches[1] : '';
		} else {
			$next = $this->next_gallery;
		}

		if ( $this->page == 2 ) {
			$prev = get_permalink( get_the_ID() );
		}

		$nav       = array( 'prev' => $prev, 'next' => $next );
		$this->nav = $nav;

		return $nav;
	}

	public function authorimage() {
		global $post;
		$author_id = get_post_field( 'post_author', $post->ID );

		return get_avatar_url( $author_id, [ 'size' => 32 ] );
	}

	public function topics() {
		global $post;
		$post_type  = $post->post_type;
		$taxonomies = get_object_taxonomies( $post_type );
		$output     = [];
		foreach ( $taxonomies as $taxonomy ) {
			$terms = get_the_terms( $post->ID, $taxonomy );
			if ( ! empty( $terms ) && $taxonomy !== 'category' ) {
				foreach ( $terms as $term ) {
					$output[] = [
						'link' => get_term_link( $term ),
						'name' => $term->name
					];
				}
			}
		}

		return $output;
	}

	public function socialsection() {
		$query = [
			'colNumber'       => 2,
			'componentNumber' => 2,
			'type'            => 'posts',
			'component'       => 'partial-card-social-instagram--k2'

		];

		return $query;
	}

	public function posts() {
		$args = array(
			'post_type'      => 'post',
			'post_status'    => 'publish',
			'posts_per_page' => 4
		);

		return new \WP_Query( $args );
	}

	public function related_post() {
		$args = array(
			'post_type'      => 'post',
			'post_status'    => 'publish',
			'posts_per_page' => 1,
			'meta_key'       => 'tbm_gallery_to_post',
			'meta_value'     => get_the_ID()
		);

		$posts = new \WP_Query( $args );

		if ( $posts->have_posts() ) {
			return $posts;
		}

		return null;

	}

	public function modal_close_link() {

		return get_post_type_archive_link( HTML_GALLERY_POST_TYPE );
	}

	public function is_play() {
		return has_tag( 'play' );
	}

	/**
	 * Preload next page
	 */
	public function preload_ur() {

		//do_action( 'tbm_preload_url', $this->nav['next'] );
	}

}
