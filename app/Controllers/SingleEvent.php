<?php
/**
 * Created by PhpStorm.
 * User: Francesco
 * Date: 10/11/2018
 * Time: 20:08
 */

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleEvent extends Controller {

	use Partials\CommonSingle;

	public $gmaps;


	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css(
			'/dist/css/critical/single-evento--critical.min.css', array(
				'/dist/css/custom.min.css'
			)
		);

		return $css;

	}

	public function event_section() {
		$data = tbm_get_event_data( get_the_ID() );
		if ( ( isset( $data['from'] ) && isset( $data['to'] ) ) && ( $data['from'] != $data['to'] ) ) {
			$out[] = '<div class="table__row"><label class="table__date">';
			$out[] = __("Data", "lifegate");
			$out[] = '</label><span>';
			$out[] = __("dal", "lifegate")." ";
			$out[] = $data['j']." ".ucfirst($data['f'])." ";
			$out[] = __("al", "lifegate")." ";
			$out[] = $data['tj']." ".ucfirst($data['tf'])." ";
			$out[] = "</span></div>";
//			$out[] = sprintf( '<div class="table__row"><label class="table__date">Data</label><span> dal %d %s al %d %s</span></div>', $data['j'], strtolower($data['f']), $data['tj'], strtolower($data['tf']), $data['y'] );
		}
		if ( ( isset( $data['from'] ) && isset( $data['to'] ) ) && ( $data['from'] == $data['to'] ) ) {
			$out[] = sprintf( '<div class="table__row"><label class="table__date">'.__("Data", "lifegate").'</label><span> %d %s %d</span></div>', $data['j'], ucfirst($data['f']), $data['y'] );
		}
		if ( ( isset( $data['location'] ) && ! empty( $data['location'] ) ) ) {
			$out[] = sprintf( '<div class="table__row"><label class="table__location">'.__("Luogo", "lifegate").'</label><span> %s</span></div>', ucfirst( $data['location'] ) );
		}
		if ( ( isset( $data['gmaps'] ) && ! empty( $data['gmaps'] ) ) ) {
			$gmaps = maybe_unserialize( $data['gmaps'] );
			$out[] = sprintf( '<div class="table__row"><label class="table__address">'.__("Indirizzo", "lifegate").'</label><span> %s</span></div>', ucfirst( $gmaps['address'] ) );
		}
		/*
		if ( ( isset( $data['price'] ) && ! empty( $data['price'] ) ) && ( isset( $data['euro'] ) && ! empty( $data['euro'] ) ) ) {
			$out[] = sprintf( '<div class="table__row"><label class="table__date">Prezzo</label><span> %s (%d euro)</span></div>', $data['price'], $data['euro'] );
		} else if ( isset( $data['price'] ) && ! empty( $data['price'] ) ) {
			$out[] = sprintf( '<div class="table__row"><label class="table__date">Prezzo</label><span> %s</span></div>', $data['price'] );
		}
		*/
		if ( isset( $data['organizzazione'] ) && ! empty( $data['organizzazione'] ) ) {
			$out[] = sprintf( '<div class="table__row"><label class="table__corp">'.__("Organizzazione", "lifegate").'</label><span> %s</span></div>', ucfirst( $data['organizzazione'] ) );
		}
		/*
		if ( ( isset( $data['url'] ) && ! empty( $data['url'] ) ) ) {
			$out[] = sprintf( '<div class="table__row"><label class="table__date">Sito Web</label><span> <a href="%s" target="_blank" rel="nofollow">%s</a></span></div>', $data['url'], parse_url( $data['url'] )['host'] );
		}
		*/
		if ( ( isset( $data['tipologia'] ) && ! empty( $data['tipologia'] ) ) ) {
			$out[] = sprintf( '<div class="table__row"><label class="table__type">'.__("Tipo evento", "lifegate").'</label><span> %s</span></div>', ucfirst( __($data['tipologia'], "lifegate") ) );
		}
		/*
				if ( ( isset( $data['telefono'] ) && ! empty( $data['telefono'] ) ) || ( isset( $data['email'] ) && ! empty( $data['email'] ) ) ) {
					if ( isset( $data['telefono'] ) && ! empty( $data['telefono'] ) ) {
						$address[] = sprintf( '%s', $data['telefono'] );
					}
					if ( isset( $data['email'] ) && ! empty( $data['email'] ) ) {
						$address[] = sprintf( '<a href="%s">%s</a>', $data['email'], $data['email'] );
					}
					$out[] = sprintf( '<div class="table__row"><label class="table__date">Contatti</label><span> %s</span></div>', implode( ', ', $address ) );
				}
		*/
		$this->gmaps = ( isset( $data['gmaps'] ) && ! empty( $data['gmaps'] ) ) ? $gmaps : '';

		return implode( '', $out );

	}

	public function gmaps() {
		return $this->gmaps;
	}

	public function single_post_relateds_special() {
		$posts = tbm_get_custom_related_posts( get_the_ID(), 3, array( get_the_ID() ), SITE_SPECIAL_TAXONOMY, array(
			'post',
			HTML_GALLERY_POST_TYPE,
			HTML_VIDEO_POST_TYPE,
			HTML_CARD_POST_TYPE
		), 'object' );


	}

	public function single_post_relateds() {
		$posts = tbm_get_custom_related_posts( get_the_ID(), 6, array( get_the_ID() ), SITE_SPECIAL_TAXONOMY, array(
			'post',
			HTML_GALLERY_POST_TYPE,
			HTML_VIDEO_POST_TYPE,
			HTML_CARD_POST_TYPE
		), 'object' );

		return $posts;
	}

}
