<?php


namespace App\Controllers;

use Sober\Controller\Controller;

class PageSponsor extends Controller {

	public function last_partners() {
		$sponsors       = get_terms( array(
			'taxonomy'   => 'sponsor',
			'fields'     => 'ids',
			'meta_query' => array(
				array(
					'key'     => 'sponsor_type',
					'value'   => 'tbm_sponsor_sponsor',
					'compare' => '='
				),
				array(
					'key'     => 'sponsor_type',
					'compare' => 'NOT EXISTS'
				),
				'relation' => 'OR'
			)
		) );

		$my_args = array(
			'taxonomy'   => 'magazine',
			'orderby'    => 'count',
			'count'      => true,
			'order'      => 'DESC',
			'meta_query' => array(
				array(
					'key'   => 'post_sponsor',
					'value' => $sponsors,
				)
			)
		);

		$the_terms = get_terms( $my_args );

		return $the_terms;
	}

	/**
	 * Returns the image attached to the term (with ACF)
	 *
	 * @param WP_Term $term Term object to get its image.
	 *
	 * @return string Image ID, or empty string if none available.
	 */
	public static function get_term_image( $term ) {

		$term = get_term( $term );

		if ( is_wp_error( $term ) || ! $term ) {
			return '';
		}

		$image = get_field( 'tbm_special_featured_img', $term->taxonomy . '_' . $term->term_id );

		return $image;
	}
}
