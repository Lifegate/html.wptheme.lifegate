<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 02/01/2018
 * Time: 23:24
 */

namespace App\Controllers;

/**
 * Custom walker class.
 */
class TBMMainNavMenu extends \Walker_Nav_Menu {

	// Displays start of a level. E.g '<ul>'
	// @see Walker::start_lvl()
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= "";
	}

	// Displays end of a level. E.g '</ul>'
	// @see Walker::end_lvl()
	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= "";
	}

	// Displays start of an element. E.g '<li> Item Name'
	// @see Walker::start_el()
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

		// Check if is a special trend instructed by ACF attribute (check theme-filters.php)
		$class = ! empty( $item->featured ) ? 'special-trend' : '';

		$current     = '';
		$current_cat = '';

		if ( is_tax() || is_search() ) {
			$current = 'no-current';
		}

		if ( is_front_page() && $item->title == 'Home' ) {
			$current = 'current';
		}

		if ( is_page() || is_tax( 'listino' ) || is_singular( 'post' ) ) {
			$queried_object = get_queried_object();
			if ( is_page() ) {
				if ( $item->object_id == $queried_object->ID ) {
					$motori_link_active_cls = 'active';
				}
			}


			if ( is_singular( 'post' ) ) {
				if ( $item->title == 'Magazine' ) {
					$motori_link_active_cls = 'active';
				}
			}

		}

		if ( $item->type === 'post_type_archive' ) {
			if ( is_singular( $item->object ) || is_post_type_archive( $item->object ) ) {
				$current = 'current';
			}
		}

		if ( $item->object === 'category' && ! ( is_front_page() || is_post_type_archive() ) ) {

			if ( ! is_singular( HTML_EVENT_POST_TYPE ) ) {

				$categories = get_the_category();

				if ( ! empty( $categories ) ) {
					$current_cat = $categories[0]->term_id;
				}

				if ( $current_cat && $item->object_id == $current_cat ) {
					$current = 'current';
				}

			}
		}

		// Depth-dependent classes.
		$depth_classes     = array(
			( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
			( $depth >= 2 ? 'sub-sub-menu-item' : '' ),
			( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
			'menu-item-depth-' . $depth
		);
		$depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

		// Passed classes.
		$classes           = empty( $item->classes ) ? array() : (array) $item->classes;
		$class_names       = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

		// Link attributes.
		$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) . '"' : '';
		$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) . '"' : '';
		$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) . '"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) . '"' : '';
		$attributes .= ' class="' . $class . ' menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';

		$pre  = "<li class='{$current}'>";
		$post = "";


		// Build HTML output and pass through the proper filter.
		$item_output = sprintf( '%1$s%2$s<a%3$s>%4$s%5$s%6$s</a>%7$s%8$s',
			$args->before,
			$pre,
			$attributes,
			$args->link_before,
			apply_filters( 'the_title', $item->title, $item->ID ),
			$args->link_after,
			$post,
			$args->after
		);
		$output      .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

	}

	// Displays end of an element. E.g '</li>'
	// @see Walker::end_el()
	function end_el( &$output, $item, $depth = 0, $args = array() ) {

		$output .= "</li>\n";
	}
}

//TODO Strutturare meglio il walker usando apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
class LifegateMagazineMenu extends \Walker_Nav_Menu {
	/**
	 * Start the element output.
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Menu item data object.
	 * @param int $depth Depth of menu item. May be used for padding.
	 * @param array $args Additional strings.
	 *
	 * @return void
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

		$active_cls = '';

		if ( is_category() ) {
			$cat = get_queried_object();
			if ( $cat->term_id == $item->object_id ) {
				$active_cls = 'active ';
			}
		}

		if ( is_singular( 'post' ) ) {
			$categories = get_the_category();

			if ( ! empty( $categories ) ) {
				$current_cat = $categories[0]->term_id;
			}

			if ( $current_cat && $item->object_id == $current_cat ) {
				$active_cls = 'active ';
			}
		}

		// Attributes of the a href tag
		$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) . '"' : '';
		$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) . '"' : '';
		$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) . '"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) . '"' : '';
		$attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';
		$output     .= '<li class="' . $active_cls . '"><a ' . $attributes . '>' . apply_filters( 'the_title', $item->title, $item->ID );
	}

	/**
	 * @param string $output Passed by reference. Used to append additional content.
	 *
	 * @return void
	 * @see Walker::start_lvl()
	 *
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= '';
	}

	/**
	 * @param string $output Passed by reference. Used to append additional content.
	 *
	 * @return void
	 * @see Walker::end_lvl()
	 *
	 */
	public function end_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= '';
	}

	/**
	 * @param string $output Passed by reference. Used to append additional content.
	 *
	 * @return void
	 * @see Walker::end_el()
	 *
	 */
	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= '</a></li>';
	}
}

//TODO Strutturare meglio il walker usando apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
class LifegateTrendMenu extends \Walker_Nav_Menu {
	/**
	 * Start the element output.
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Menu item data object.
	 * @param int $depth Depth of menu item. May be used for padding.
	 * @param array $args Additional strings.
	 *
	 * @return void
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		// Check if is a special trend instructed by ACF attribute (check theme-filters.php)
		$class = ! empty( $item->featured ) ? 'special-trend' : '';

		// Attributes of the a href tag
		$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) . '"' : '';
		$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) . '"' : '';
		$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) . '"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) . '"' : '';
		$attributes .= ' class="' . $class . ' swiper-slide"';

		$output .= '<a ' . $attributes . '>' . apply_filters( 'the_title', $item->title, $item->ID );
	}

	/**
	 * @param string $output Passed by reference. Used to append additional content.
	 *
	 * @return void
	 * @see Walker::start_lvl()
	 *
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= '';
	}

	/**
	 * @param string $output Passed by reference. Used to append additional content.
	 *
	 * @return void
	 * @see Walker::end_lvl()
	 *
	 */
	public function end_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= '';
	}

	/**
	 * @param string $output Passed by reference. Used to append additional content.
	 *
	 * @return void
	 * @see Walker::end_el()
	 *
	 */
	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= '</a>';
	}
}


//TODO Strutturare meglio il walker usando apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
class LifegateChannelMenu extends \Walker_Nav_Menu {
	/**
	 * Start the element output.
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Menu item data object.
	 * @param int $depth Depth of menu item. May be used for padding.
	 * @param array $args Additional strings.
	 *
	 * @return void
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		// Check if is a special trend instructed by ACF attribute (check theme-filters.php)
		$class = ! empty( $item->featured ) ? 'special-trend' : '';

		// Attributes of the a href tag
		$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) . '"' : '';
		$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) . '"' : '';
		$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) . '"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) . '"' : '';
		$attributes .= ' class="' . $class . ' swiper-slide"';

		$output .= '<a ' . $attributes . '>' . apply_filters( 'the_title', $item->title, $item->ID );
	}

	/**
	 * @param string $output Passed by reference. Used to append additional content.
	 *
	 * @return void
	 * @see Walker::start_lvl()
	 *
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= '';
	}

	/**
	 * @param string $output Passed by reference. Used to append additional content.
	 *
	 * @return void
	 * @see Walker::end_lvl()
	 *
	 */
	public function end_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= '';
	}

	/**
	 * @param string $output Passed by reference. Used to append additional content.
	 *
	 * @return void
	 * @see Walker::end_el()
	 *
	 */
	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= '</a>';
	}
}

class LifegateCasehistoryMenu extends \Walker_Nav_Menu {
	/**
	 * Start the element output.
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Menu item data object.
	 * @param int $depth Depth of menu item. May be used for padding.
	 * @param array $args Additional strings.
	 *
	 * @return void
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		// Check if is a special trend instructed by ACF attribute (check theme-filters.php)
		$class = ! empty( $item->featured ) ? 'special-trend' : '';

		// Attributes of the a href tag
		$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) . '"' : '';
		$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) . '"' : '';
		$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) . '"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) . '"' : '';
		$attributes .= ' class="' . $class . ' swiper-slide"';

		$output .= '<a ' . $attributes . '>' . apply_filters( 'the_title', $item->title, $item->ID );
	}

	/**
	 * @param string $output Passed by reference. Used to append additional content.
	 *
	 * @return void
	 * @see Walker::start_lvl()
	 *
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= '';
	}

	/**
	 * @param string $output Passed by reference. Used to append additional content.
	 *
	 * @return void
	 * @see Walker::end_lvl()
	 *
	 */
	public function end_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= '';
	}

	/**
	 * @param string $output Passed by reference. Used to append additional content.
	 *
	 * @return void
	 * @see Walker::end_el()
	 *
	 */
	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= '</a>';
	}
}



//TODO Strutturare meglio il walker usando apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
class LifegateHamburgerMenu extends \Walker_Nav_Menu {
	/**
	 * Start the element output.
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Menu item data object.
	 * @param int $depth Depth of menu item. May be used for padding.
	 * @param array $args Additional strings.
	 *
	 * @return void
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$open  = '';
		$class = array();

		if ( $depth === 0 ) {
			$open    = '<!-- start_el ' . $depth . ' --><div class="drawer-menu__block">' . PHP_EOL;
			$class[] = 'drawer-menu__item';
		} else if ( $depth === 1 ) {
			$open = "<!-- start_el " . $depth . " --><li>" . PHP_EOL;
		}

		// Check if is a special trend instructed by ACF attribute (check theme-filters.php)
		if ( strpos( $item->target, '_blank' ) !== false ) {
			$class[] = 'drawer-menu__item--external';
		}

		// Attributes of the a href tag
		$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) . '"' : '';
		$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) . '"' : '';
		$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) . '"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) . '"' : '';
		$attributes .= ' class="' . implode( ' ', $class ) . ' "';

		$output .= $open . '<a ' . $attributes . '>' . apply_filters( 'the_title', $item->title, $item->ID ) . '</a>' . PHP_EOL;
	}

	/**
	 * @param string $output Passed by reference. Used to append additional content.
	 *
	 * @return void
	 * @see Walker::end_el()
	 *
	 */
	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		if ( $depth == 0 ) {
			$output .= "<!-- end_el " . $depth . " --></div>" . PHP_EOL;
		} else {
			$output .= "<!-- end_el " . $depth . " --></li>" . PHP_EOL;
		}
	}

	/**
	 * @param string $output Passed by reference. Used to append additional content.
	 *
	 * @return void
	 * @see Walker::start_lvl()
	 *
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= "<!-- start_lvl " . $depth . " --><div class=\"flex\"><div class=\"drawer-menu__block__submenu\"><ul class=\"container-cycle-col-2\">" . PHP_EOL;
	}

	/**
	 * @param string $output Passed by reference. Used to append additional content.
	 *
	 * @return void
	 * @see Walker::end_lvl()
	 *
	 */
	public function end_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= "<!-- end_lvl " . $depth . " --></ul></div></div>" . PHP_EOL;
	}


}
