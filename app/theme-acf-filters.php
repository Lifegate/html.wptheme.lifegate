<?php


add_filter( 'tbm_dashboard_post_types_admin', function ( $enabled_post_types ) {

	unset( $enabled_post_types['autori'] );
	unset( $enabled_post_types['playlist-video'] );
	unset( $enabled_post_types['sponsor'] );

	return $enabled_post_types;
} );

/**
 * Add drafts to related post object
 */
add_filter( 'acf/fields/post_object/query/name=tbm_gallery_to_post', function ( $args ) {
	$args['post_status'] = array( 'pending', 'draft', 'future', 'publish' );

	return $args;
}, 10, 3 );

/**
 * Transform checkbox in image  in ATF Homepage field
 */
add_filter( 'acf/prepare_field/key=field_5e8dfa40dd0db', function ( $arg ) {

	if ( isset( $arg['choices']['V1'] ) ) {
		$arg['choices']['V1'] = '<img src="' . get_theme_file_uri() . '/dist/images/atf_v1.png" />';
	}

	if ( isset( $arg['choices']['V2'] ) ) {
		$arg['choices']['V2'] = '<img src="' . get_theme_file_uri() . '/dist/images/atf_v2.png" />';
	}

	return $arg;
} );

/**
 * Transform checkbox in image Trend taxonomy ACF field
 */
add_filter( 'acf/prepare_field/key=field_5e8dfa40dxxsd', function ( $arg ) {

	if ( isset( $arg['choices']['V0'] ) ) {
		$arg['choices']['V0'] = '<img src="' . get_theme_file_uri() . '/dist/images/trend_v0.png" />';
	}

	if ( isset( $arg['choices']['V1'] ) ) {
		$arg['choices']['V1'] = '<img src="' . get_theme_file_uri() . '/dist/images/trend_v1.png" />';
	}

	if ( isset( $arg['choices']['V2'] ) ) {
		$arg['choices']['V2'] = '<img src="' . get_theme_file_uri() . '/dist/images/trend_v2.png" />';
	}

	return $arg;
} );

/**
 * Limit the taxonomy showed in relationship field
 */
add_action( 'acf/render_field', function () {
	add_filter( 'acf/get_taxonomies', function ( $taxonomies, $args ) {
		return array( 0 => 'magazine', 1 => 'trend', 2 => 'category', 3 => 'sponsor' );
	}, 10, 3 );
} );

