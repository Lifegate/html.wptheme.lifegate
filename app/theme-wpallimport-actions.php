<?php

/*
 * This action fires when WP All Import saves a post of any type. The post ID, the record's data from your file,
 * and a boolean value showing if the post is being updated are provided.
 *
 */
add_action( 'pmxi_saved_post', function ( $post_id, $xml_node, $is_update ) {

	/**
	 * Set the term to the encligh language
	 */
	if ( 'post' === get_post_type( $post_id ) ) {

		/**
		 * Set english category
		 */
		foreach ( array( 'category_en', 'tag_en' ) as $tax ) {

			$terms = get_the_terms( $post_id, $tax );

			if ( is_wp_error( $terms ) || empty( $terms ) ) {
				return;
			}

			foreach ( $terms as $term ) {
				pll_set_term_language( $term->term_id, 'en' );
			}
		}

		return;
	}


	/**
	 * Set the term to the encligh language
	 */
	if ( term_exists( $post_id, 'category_en' ) || term_exists( $post_id, 'tag_en' ) ) {

		/**
		 * Set english category
		 */
		pll_set_term_language( $post_id, 'en' );

		return;
	}


}, 10, 3 );


