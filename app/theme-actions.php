<?php

/**
 * Delete Trend and Magazine created by users
 */
add_action( 'create_term', function ( $term_id, $tt_id, $taxonomy ) {
	if ( ! current_user_can( 'activate_plugins' ) ) {
		if ( $taxonomy == 'trend' || $taxonomy == 'magazine' || $taxonomy == 'sponsor' ) {
			tbm_enable_error_message( sprintf( '<ul style="list-style: inherit;padding: 0 20px;"><li>' . __( 'Non hai permessi per creare un termine nella tassonomia <strong>%s</strong>. Il termine è stato cancellato','lifegate' ) . '.</li></ul>', get_taxonomy( $taxonomy )->name ) );
			wp_delete_term( $term_id, $taxonomy );
		}
	}
}, 10, 3 );


/**
 * Remove manage category cap to non admin
 */
add_action( 'after_switch_theme', function () {
	$roles = wp_roles();
	foreach ( $roles->role_names as $slug => $name ) {
		$role = get_role( $slug );
		if ( $slug === 'administrator' || $slug === 'superadmin' ) {
			$role->add_cap( 'manage_categories', true );
		} else {
			$role->add_cap( 'manage_categories', false );
		}
	}
} );


/**
 * Add feed in category and tags
 */
add_action( 'wp_head', function () {
	if ( is_category() ) {
		$term = get_queried_object();

		if ( $term ) {
			$title = sprintf( __( '%1$s %2$s %3$s Category Feed' ), get_bloginfo( 'name' ), _x( '&raquo;', 'feed link' ), $term->name );
			$href  = get_category_feed_link( $term->term_id );
		}
	} elseif ( is_tax( SITE_SPECIAL_TAXONOMY ) ) {
		$term = get_queried_object();

		if ( $term ) {
			$tax   = get_taxonomy( $term->taxonomy );
			$title = sprintf( __( '%1$s %2$s %3$s %4$s Feed' ), get_bloginfo( 'name' ), _x( '&raquo;', 'feed link' ), $term->name, $tax->labels->singular_name );
			$href  = get_term_feed_link( $term->term_id, $term->taxonomy );
		}
	}
	if ( isset( $title ) && isset( $href ) ) {
		echo '<link rel="alternate" type="' . feed_content_type() . '" title="' . esc_attr( $title ) . '" href="' . esc_url( $href ) . '" />' . "\n";
	}

} );

/**
 * Add rewrite rule for author
 */
add_action( 'init', function () {
	add_rewrite_rule( '(?:autore|author)/([^/]+)/page/([0-9]+)/?', 'index.php?autori=$matches[1]&author_page=$matches[2]', 'top' );
}, 10, 0 );

/**
 * Remove unusued box from all sites
 */

add_action( 'add_meta_boxes', function () {
	remove_meta_box( 'trackbacksdiv', get_post_types(), 'normal' ); // Trackbacks meta box
	remove_meta_box( 'postcustom', 'post', 'normal' ); // Custom fields meta box
	remove_meta_box( 'commentsdiv', 'post', 'normal' ); // Comments meta box
	remove_meta_box( 'formatdiv', 'post', 'normal' ); // Post format meta box
	remove_meta_box( 'commentstatusdiv', 'post', 'normal' ); // Comment status meta box

	remove_meta_box( 'gdrts-metabox', 'post', 'normal' ); // GD Star Rating
}, 11 );

/**
 * Set content width
 */
add_action( 'after_setup_theme', function () {
	$GLOBALS['content_width'] = apply_filters( 'lifegate_content_width', 640 );
}, 0 );

/**
 * Save spreaker show image on show save
 */
add_action( 'acf/save_post', function ( $post_id ) {

	if ( 'show' !== get_post_type( $post_id ) ) {
		return;
	}

	if ( get_field( 'lg_show_image', $post_id ) ) {
		return;
	}

	/**
	 * Get the show id
	 */
	$lg_show_spreaker_id = get_field( 'lg_show_spreaker_id', $post_id );
	if ( ! $lg_show_spreaker_id ) {
		return;
	}

	/**
	 * Require the show data  from spreaker endpoint
	 */
	$response = wp_remote_get( 'https://api.spreaker.com/v2/shows/' . $lg_show_spreaker_id, array(
		'timeout'   => 60,
		'sslverify' => false
	) );

	/**
	 * Check the response. If error, return empty string
	 */
	if ( is_wp_error( $response ) ) {
		return '';
	}

	/**
	 * Check the response. If false, return empty string
	 */
	if ( ! is_array( $response ) || 200 != wp_remote_retrieve_response_code( $response ) ) {
		return '';
	}

	/**
	 * Retrieve the body
	 */
	$body = $response['body'];

	/**
	 * Check the body. If false, return empty string
	 */
	if ( $body == "false" ) {
		return '';
	}

	$response_array = json_decode( $body, true );

	if ( ! empty( $response_array['response']['show']['cover_image_url'] ) ) {
		update_field( 'lg_show_image', $response_array['response']['show']['cover_image_url'], $post_id );
	}

	return true;

} );

/**
 * Fire a callback only when my-custom-post-type posts are transitioned to 'publish'.
 *
 * @param string $new_status New post status.
 * @param string $old_status Old post status.
 * @param WP_Post $post Post object.
 */
add_action( 'transition_post_status', function ( $new_status, $old_status, $post ) {

	/**
	 * Act only in production
	 */
	if ( ! defined( 'WP_ENV' ) || WP_ENV !== 'production' ) {
		return;
	}

	/**
	 * Act only if post is transitioned from or to publish
	 */
	if ( $new_status != 'publish' ) {
		return;
	}

	/**
	 * Instantiate the purger class
	 */
	$purger = new LG_Purge_Cache();

	/**
	 * Dispatch the request. The `handle()` functioncs will receive array( 'postid' => '100' ) as `$_POST` data.
	 */
	$purger->data( array( 'postid' => $post->ID ) );
	$purger->dispatch();


}, 10, 3 );




add_filter( 'pll_copy_taxonomies', 'lifegate_pll_copy_taxonomies', 10, 2);

function lifegate_pll_copy_taxonomies($taxonomies) {

	if (in_array('category', $taxonomies)) {
		$key = array_search('category', $taxonomies);
		unset($taxonomies[$key]);
	}

	return $taxonomies;
}

remove_action( 'template_redirect', 'rest_output_link_header', 11 );
remove_action( 'template_redirect', 'wp_shortlink_header', 11);

add_action( 'template_redirect', 'lifegate_shortlink_header', 12);
function lifegate_shortlink_header(){
	if ( headers_sent() ) {
		return;
	}
	global $wp;
	if ( substr(add_query_arg( $_GET, home_url( $wp->request ) ), -3, 3 ) == "amp" )
		return;

	$shortlink = wp_get_shortlink( 0, 'query' );

	if ( empty( $shortlink ) ) {
		return;
	}

	header( 'Link: <' . $shortlink . '>; rel=shortlink', false );
}


add_filter( 'amp_post_template_data', 'adk_amp_scripts' );
function adk_amp_scripts($data){
	if ( empty( $data['amp_component_scripts']['amp-iframe'] ) ) {
		$data['amp_component_scripts']['amp-iframe'] = 'https://cdn.ampproject.org/v0/amp-iframe-0.1.js';
	}
	if ( empty( $data['amp_component_scripts']['amp-list'] ) ) {
		$data['amp_component_scripts']['amp-list'] = 'https://cdn.ampproject.org/v0/amp-list-0.1.js';
	}
	if ( empty( $data['amp_component_scripts']['amp-mustache'] ) ) {
		$data['amp_component_scripts']['amp-mustache'] = 'https://cdn.ampproject.org/v0/amp-mustache-0.2.js';
	}
	if ( empty( $data['amp_component_scripts']['amp-social-share'] ) ) {
		$data['amp_component_scripts']['amp-social-share'] = 'https://cdn.ampproject.org/v0/amp-social-share-0.1.js';
	}

	return $data;
}

// fix soil bug
add_action('init', function () {
	remove_filter('script_loader_tag', 'Roots\\Soil\\CleanUp\\clean_script_tag');
});


// aggiungo featured in preload

add_action( 'wp_head', function(){
	if(is_singular()){

		if ( ! has_post_thumbnail() ) {
			return;
		}

		$thumb_id = get_post_thumbnail_id();

		if(is_singular("post")){
			?>
			<link rel="preload" imagesrcset="<?php echo tbm_wp_get_attachment_image_url($thumb_id,array(360,180)); ?>, <?php echo tbm_wp_get_attachment_image_url($thumb_id,array(720,360)); ?> 2x" as="image" media="(max-width: 736px)">
			<link rel="preload" imagesrcset="<?php echo tbm_wp_get_attachment_image_url($thumb_id,array(980,490)); ?>, <?php echo tbm_wp_get_attachment_image_url($thumb_id,array(1960,980)); ?> 2x" as="image" media="(min-width: 736.1px)">
			<?php
		}
	}
});



// svuoto le tabelle index yoast dopo ogni nuova pubblicazione
/*
add_action( 'transition_post_status', function ( $new_status, $old_status, $post )
{

	if( 'publish' == $new_status && 'publish' != $old_status ) {
		global $wpdb;

		$wpdb->delete( $wpdb->prefix . 'postmeta', [ 'meta_key' => '_yst_prominent_words_version' ] );
		$wpdb->query( 'UPDATE ' . $wpdb->prefix . 'yoast_indexable SET prominent_words_version = NULL' );
		$wpdb->query( 'TRUNCATE TABLE ' . $wpdb->prefix . 'yoast_prominent_words' );
		WPSEO_Options::set( 'prominent_words_indexing_completed', false );
		\delete_transient( 'total_unindexed_prominent_words' );

		// reset internal link count
		$wpdb->query( 'UPDATE ' . $wpdb->prefix . 'yoast_indexable SET link_count = NULL, incoming_link_count = NULL' );
		\delete_transient( 'wpseo_unindexed_post_link_count' );
		\delete_transient( 'wpseo_unindexed_term_link_count' );


		// phpcs:disable WordPress.DB.DirectDatabaseQuery.SchemaChange -- We know what we're doing. Really.
		$wpdb->query( 'DROP TABLE IF EXISTS ' . $wpdb->prefix . 'yoast_indexable' );
		$wpdb->query( 'DROP TABLE IF EXISTS ' . $wpdb->prefix . 'yoast_indexable_hierarchy' );
		$wpdb->query( 'DROP TABLE IF EXISTS ' . $wpdb->prefix . 'yoast_migrations' );
		$wpdb->query( 'DROP TABLE IF EXISTS ' . $wpdb->prefix . 'yoast_primary_term' );
		$wpdb->query( 'DROP TABLE IF EXISTS ' . $wpdb->prefix . 'yoast_seo_links' );
		// phpcs:enable WordPress.DB.DirectDatabaseQuery.SchemaChange
		WPSEO_Options::set( 'indexing_started', null );
		WPSEO_Options::set( 'indexables_indexing_completed', false );
		WPSEO_Options::set( 'indexing_first_time', true );

		// Found in Indexable_Post_Indexation_Action::TRANSIENT_CACHE_KEY.
		\delete_transient( 'wpseo_total_unindexed_posts' );
		// Found in Indexable_Post_Type_Archive_Indexation_Action::TRANSIENT_CACHE_KEY.
		\delete_transient( 'wpseo_total_unindexed_post_type_archives' );
		// Found in Indexable_Term_Indexation_Action::TRANSIENT_CACHE_KEY.
		\delete_transient( 'wpseo_total_unindexed_terms' );

		\delete_option( 'yoast_migrations_premium' );
		\delete_option( 'yoast_migrations_free' );

	}
}, 10, 3 );


*/

add_action( 'template_redirect', static function () { remove_action( 'wp_head', 'amp_add_amphtml_link' ); } );

add_action('wp', function () {
	if (function_exists('header_remove')) {
		header_remove('x-powered-by');
	}
});
