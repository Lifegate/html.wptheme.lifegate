<?php

/**
 * Remove Gutenberg block
 */

add_filter( 'allowed_block_types', function ( $allowed_blocks, $post ) {
	$registered_blocks = WP_Block_Type_Registry::get_instance()->get_all_registered();

//		print_r($registered_blocks);

//	return $allowed_blocks;
	if(($post->post_type == "original") || ($post->post_type == "mediacase")) {
		$allowed_blocks = array(
			'core/image',
			'core/paragraph',
			'core/heading',
			'core/list',
			'core/list-item',
			'core/audio',
			'core/table',
			'core/html',
			'core/preformatted',
			'core/spacer',
			'core/separator',
			'core/shortcode',
			'core/group',
			'core/embed',
			'core/social-link',
			'core-embed/twitter',
			'core-embed/youtube',
			'core-embed/facebook',
			'core-embed/instagram',
			'core-embed/speaker',
			'core-embed/spotify',
			'core-embed/vimeo',
			'core-embed/quote',
			'acf/block-quote',
			'acf/block-sponsor',
			'acf/block-indice',
			'themeisle-blocks/accordion',
			'themeisle-blocks/slider',
			'themeisle-blocks/circle-counter',
			'themeisle-blocks/advanced-heading',
			'themeisle-blocks/font-awesome-icons',
			'themeisle-blocks/google-map',
			'themeisle-blocks/leaflet-map',
			'themeisle-blocks/icon-list',
			'themeisle-blocks/icon-list-item',
			'themeisle-blocks/progress-bar',
			'themeisle-blocks/advanced-columns',
			'themeisle-blocks/testimonials',
			'themeisle-blocks/lottie',
			'acf/acfb-content-timeline',
			'acf/acfb-highlighted-headline',
			'acf/acfb-counternumber'
		);

	}
	if($post->post_type == "longform"){
		$allowed_blocks = array(
			'core/image',
			'core/paragraph',
			'core/heading',
			'core/list',
			'core/list-item',
			'core/audio',
			'core/table',
			'core/html',
			'core/preformatted',
			'core/spacer',
			'core/embed',
			'core-embed/twitter',
			'core-embed/youtube',
			'core-embed/facebook',
			'core-embed/instagram',
			'core-embed/speaker',
			'core-embed/spotify',
			'core-embed/vimeo',
			'core-embed/quote',
			'acf/video-img-cta',
			'acf/fullwidth-video',
			'acf/sticky-content',
			'acf/fullwidth-list',
			'acf/fullwidth-sentences',
			'acf/fullwidth-gallery',
			'acf/fullwidth-img-cta',
			'acf/background-mutation',
			'acf/img-txt',
			'acf/block-2-cols',
			'acf/block-quote',
			'acf/block-sponsor',
			'acf/block-indice',
			'acf/block-image',
			'acf/acfb-content-timeline',
			'acf/acfb-highlighted-headline',
			'acf/acfb-counternumber'
		);
	}
	return $allowed_blocks;

}, 10, 2 );

/**
 * Create block in Gutenberg
 */
add_action( 'init', function () {
	if ( function_exists( 'acf_register_block_type' ) ) :

		/**
		 * 3 columns: video, image and CTA
		 */
		acf_register_block_type( array(
			'name'            => 'video_img_cta',
			'title'           => __( 'Video + Immagine + CTA' ),
			'description'     => __( 'Un blocco di tre elementi fra video, immagine o CTA.' ),
			'render_callback' => function ( $block ) {
				echo \App\template( "components.sections.longform-fullwidth-video-image-cta", [ 'block' => $block ] );
			},
			'category'        => 'lifegate',
			'icon'            => 'admin-comments',
			'keywords'        => array( 'columns', 'lifegate', 'fullwidth' ),
			'enqueue_style'   => get_theme_root_uri() . '/lifegate-2020/dist/css/components/sections/video-image-cta.min.css'
		) );

		/**
		 * Fullwidth Video
		 */
		acf_register_block_type( array(
			'name'            => 'fullwidth_video',
			'title'           => __( 'Video a schermo pieno' ),
			'description'     => __( 'Un video in autoplay distribuito su tutto lo schermo' ),
			'render_callback' => function ( $block ) {
				echo \App\template( "components.sections.longform-fullwidth-video", [ 'block' => $block ] );
			},
			'category'        => 'lifegate',
			'icon'            => 'admin-comments',
			'keywords'        => array( 'video', 'lifegate', 'fullwidth' ),
			'enqueue_style'   => get_theme_root_uri() . '/lifegate-2020/dist/css/components/sections/video-fullwidth.min.css'
		) );


		/**
		 * Fullwidth 2 columns 1/3 -2/3
		 */
		acf_register_block_type( array(
			'name'            => 'fullwidth_img_cta',
			'title'           => __( 'Immagine + CTA' ),
			'description'     => __( 'Immagine + CTA full width' ),
			'render_callback' => function ( $block ) {
				echo \App\template( "components.sections.longform-fullwidth-img-cta", [ 'block' => $block ] );
			},
			'category'        => 'lifegate',
			'icon'            => 'admin-comments',
			'keywords'        => array( 'cta', 'lifegate', 'fullwidth' ),
			'enqueue_style'   => get_theme_root_uri() . '/lifegate-2020/dist/css/components/sections/fullwidth-img-cta.css'
		) );

		/**
		 * Sticky content (column)
		 */
		acf_register_block_type( array(
			'name'            => 'sticky_content',
			'title'           => __( 'Sticky content' ),
			'description'     => __( 'Un elenco di contenuti che scorre lungo un\'immagine fissa' ),
			'render_callback' => function ( $block ) {
				echo \App\template( "components.sections.longform-sticky-content", [ 'block' => $block ] );
			},
			'category'        => 'lifegate',
			'icon'            => 'admin-comments',
			'keywords'        => array( 'sticky content', 'sticky', 'lifegate', 'fullwidth' )
		) );

		/**
		 * Fullwidth list
		 */
		acf_register_block_type( array(
			'name'            => 'fullwidth_list',
			'title'           => __( 'Lista a schermo pieno' ),
			'description'     => __( 'Una lista di testo distribuita su tutto lo schermo' ),
			'render_callback' => function ( $block ) {
				echo \App\template( "components.sections.longform-fullwidth-list", [ 'block' => $block ] );
			},
			'category'        => 'lifegate',
			'icon'            => 'admin-comments',
			'keywords'        => array( 'video', 'lifegate', 'fullwidth' )
		) );

		/**
		 * Fullwidth sentences
		 */
		acf_register_block_type( array(
			'name'            => 'fullwidth_sentences',
			'title'           => __( 'Citazioni animate' ),
			'description'     => __( 'Un elenco di citazioni che con ad ingresso temporizzato' ),
			'render_callback' => function ( $block ) {
				echo \App\template( "components.sections.longform-fullwidth-sentences", [ 'block' => $block ] );
			},
			'category'        => 'lifegate',
			'icon'            => 'admin-comments',
			'keywords'        => array( 'sentences', 'scorrimento', 'lifegate', 'fullwidth' ),
			'enqueue_style'   => get_theme_root_uri() . '/lifegate-2020/dist/css/components/sections/fullwidth-sentences.min.css'
		) );

		/**
		 * Fullwidth gallery
		 */
		acf_register_block_type( array(
			'name'            => 'fullwidth_gallery',
			'title'           => __( 'Galleria a schermo pieno' ),
			'description'     => __( 'Una galleria di immagini distribuita a tutto schermo, con titolo e didascalia' ),
			'render_callback' => function ( $block ) {
				echo \App\template( "components.sections.longform-fullwidth-gallery", [ 'block' => $block ] );
			},
			'category'        => 'lifegate',
			'icon'            => 'admin-comments',
			'keywords'        => array( 'sentences', 'scorrimento', 'lifegate', 'fullwidth' ),
			'enqueue_style'   => get_theme_root_uri() . '/lifegate-2020/dist/css/components/sections/fullwidth-gallery.min.css'
		) );

		/**
		 * Faded background
		 */
		acf_register_block_type( array(
			'name'            => 'background_mutation',
			'title'           => __( 'Sfondo sfumato' ),
			'description'     => __( 'Un testo che cambia sfondo quando si esegue lo scroll' ),
			'render_callback' => function ( $block ) {
				echo \App\template( "components.sections.longform-background-mutation", [ 'block' => $block ] );
			},
			'category'        => 'lifegate',
			'icon'            => 'admin-comments',
			'keywords'        => array( 'background', 'sfondo', 'sfumato', 'fade' ),
		) );

		/**
		 * Immagine + testo full
		 */
		acf_register_block_type( array(
			'name'            => 'img_txt',
			'title'           => __( 'Immagine + testo overlap' ),
			'description'     => __( 'Immagine e testo su 2 colonne in overlap' ),
			'render_callback' => function ( $block ) {
				echo \App\template( "components.sections.longform-fullwidth-image-text", [ 'block' => $block ] );
			},
			'category'        => 'lifegate',
			'icon'            => 'format-gallery',
			'keywords'        => array( 'image', 'fullwidth' ),
			'enqueue_style'   => get_theme_root_uri() . '/lifegate-2020/dist/css/components/sections/fullwidth-image-text.css'
		) );

		/**
		 * Blocco a 2 colonne
		 */
		acf_register_block_type( array(
			'name'            => 'block_2_cols',
			'title'           => __( 'Blocco 2 colonne' ),
			'description'     => __( 'Blocchi su 2 colonne' ),
			'render_callback' => function ( $block ) {
				echo \App\template( "components.sections.longform-block-2-columns", [ 'block' => $block ] );
			},
			'category'        => 'lifegate',
			'icon'            => 'grid-view',
			'keywords'        => array( '2 colonne', 'boxed' ),
			'enqueue_style'   => get_theme_root_uri() . '/lifegate-2020/dist/css/components/sections/block-2-columns.css'
		) );


		/**
		 * Blocco citazione
		 */
		acf_register_block_type( array(
			'name'            => 'block_quote',
			'title'           => __( 'Citazione' ),
			'description'     => __( 'Citazione' ),
			'render_callback' => function ( $block ) {
				echo \App\template( "components.sections.longform-block-quote", [ 'block' => $block ] );
			},
			'category'        => 'lifegate',
			'icon'            => 'nametag',
			'keywords'        => array( 'quote', 'citazione' ),
			'enqueue_style'   => get_theme_root_uri() . '/lifegate-2020/dist/css/components/sections/block-quote.css'
		) );


		/**
		 * Blocco immagine full
		 */
		acf_register_block_type( array(
			'name'            => 'block_image',
			'title'           => __( 'Immagine full' ),
			'description'     => __( 'Immagine full' ),
			'render_callback' => function ( $block ) {
				echo \App\template( "components.sections.longform-block-image", [ 'block' => $block ] );
			},
			'category'        => 'lifegate',
			'icon'            => 'format-image',
			'keywords'        => array( 'immagine' ),
			'enqueue_style'   => get_theme_root_uri() . '/lifegate-2020/dist/css/components/sections/block-image.css'
		) );


		/**
		 * Blocco sponsor
		 */
		acf_register_block_type( array(
			'name'            => 'block_sponsor',
			'title'           => __( 'Sponsor' ),
			'description'     => __( 'Sponsor' ),
			'render_callback' => function ( $block ) {
				echo \App\template( "components.sections.longform-block-sponsor", [ 'block' => $block ] );
			},
			'category'        => 'lifegate',
			'icon'            => 'nametag',
			'keywords'        => array( 'firma', 'sponsor' ),
			'enqueue_style'   => get_theme_root_uri() . '/lifegate-2020/dist/css/components/sections/block-sponsor.css'
		) );

				/**
				 * Blocco indice / menu
				 */
		acf_register_block_type( array(
			'name'            => 'block_indice',
			'title'           => __( 'Indice' ),
			'description'     => __( 'Indice' ),
			'render_callback' => function ( $block ) {
				echo \App\template( "components.sections.longform-block-indice", [ 'block' => $block ] );
			},
			'category'        => 'lifegate',
			'icon'            => 'menu-alt2',
			'keywords'        => array( 'indice', 'menu' ),
			'enqueue_style'   => get_theme_root_uri() . '/lifegate-2020/dist/css/components/sections/block-indice.css'
		) );

	endif;
} );

/**
 * Create fields in Gutenberg block
 */
if ( function_exists( 'acf_add_local_field_group' ) ):

	/**
	 * Longform
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5e692626ce865',
		'title'                 => 'Campi supplementari per autore',
		'fields'                => array(
			array(
				'key'               => 'field_5e69262c60a11',
				'label'             => 'Ruolo Autore',
				'name'              => 'lg_author_role',
				'type'              => 'text',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'user_form',
					'operator' => '==',
					'value'    => 'all',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
	) );

	/**
	 * Citazioni animate
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5e66bed714436',
		'title'                 => 'Citazioni animate',
		'fields'                => array(
			array(
				'key'               => 'field_5e66bed8050a6',
				'label'             => 'Lista citazioni',
				'name'              => 'longform_sentences',
				'type'              => 'repeater',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'collapsed'         => '',
				'min'               => 0,
				'max'               => 0,
				'layout'            => 'block',
				'button_label'      => 'Aggiungi frase',
				'sub_fields'        => array(
					array(
						'key'               => 'field_5e66bed806402',
						'label'             => 'Contenuto',
						'name'              => 'contenuto',
						'type'              => 'textarea',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'placeholder'       => '',
						'maxlength'         => '',
						'rows'              => 3,
						'new_lines'         => '',
					),
					array(
						'key'               => 'field_5e70cf09867f0',
						'label'             => 'Dimensione font',
						'name'              => 'dimensione_font',
						'type'              => 'range',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => 8,
						'min'               => 1,
						'max'               => 15,
						'step'              => '0.5',
						'prepend'           => '',
						'append'            => 'vmax',
					),
				),
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'block',
					'operator' => '==',
					'value'    => 'acf/fullwidth-sentences',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
	) );

	/**
	 * Gallery Fullwidth
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5e66c1c04a047',
		'title'                 => 'Gallery Fullwidth',
		'fields'                => array(
			array(
				'key'               => 'field_5e66c1c7ecc95',
				'label'             => 'Gallery Fullwidth',
				'name'              => 'longform_gallery_fullwidth',
				'type'              => 'gallery',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'return_format'     => 'array',
				'preview_size'      => 'thumbnail',
				'insert'            => 'append',
				'library'           => 'all',
				'min'               => 3,
				'max'               => 15,
				'min_width'         => '',
				'min_height'        => '',
				'min_size'          => '',
				'max_width'         => '',
				'max_height'        => '',
				'max_size'          => '',
				'mime_types'        => '',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'block',
					'operator' => '==',
					'value'    => 'acf/fullwidth-gallery',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
	) );

	/**
	 * Lista Fullwidth
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5e66bc7067ee0',
		'title'                 => 'Lista Fullwidth',
		'fields'                => array(
			array(
				'key'               => 'field_5e67dd14d8ec5',
				'label'             => 'Lista fullwidth titolo',
				'name'              => 'longform_lista_fullwidth_title',
				'type'              => 'text',
				'instructions'      => '',
				'required'          => 1,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5e66bca03b667',
				'label'             => 'Lista fullwidth',
				'name'              => 'longform_lista_fullwidth',
				'type'              => 'repeater',
				'instructions'      => '',
				'required'          => 1,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'collapsed'         => '',
				'min'               => 1,
				'max'               => 0,
				'layout'            => 'block',
				'button_label'      => 'Aggiungi contenuto',
				'sub_fields'        => array(
					array(
						'key'               => 'field_5e66bcc23b668',
						'label'             => 'Contenuto',
						'name'              => 'contenuto',
						'type'              => 'textarea',
						'instructions'      => '',
						'required'          => 1,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'placeholder'       => '',
						'maxlength'         => '',
						'rows'              => 5,
						'new_lines'         => '',
					),
				),
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'block',
					'operator' => '==',
					'value'    => 'acf/fullwidth-list',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
	) );

	/**
	 * Sticky Content
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5e66c952e008b',
		'title'                 => 'Sticky Content',
		'fields'                => array(
			array(
				'key'               => 'field_5e675029e8cb5',
				'label'             => 'Layout',
				'name'              => 'longform_sticky_content_layout',
				'type'              => 'radio',
				'instructions'      => 'Scegli il layout',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'choices'           => array(
					'columns'   => 'A colonne',
					'fullwidth' => 'Schermo pieno',
				),
				'allow_null'        => 0,
				'other_choice'      => 0,
				'default_value'     => 'columns',
				'layout'            => 'horizontal',
				'return_format'     => 'value',
				'save_other_choice' => 0,
			),
			array(
				'key'               => 'field_5e66c9697a5c4',
				'label'             => 'Sticky content',
				'name'              => 'longform_sticky_content',
				'type'              => 'repeater',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'collapsed'         => '',
				'min'               => 0,
				'max'               => 0,
				'layout'            => 'block',
				'button_label'      => 'Aggiungi contenuto',
				'sub_fields'        => array(
					array(
						'key'               => 'field_5e66c9b97a5c5',
						'label'             => 'Immagine',
						'name'              => 'immagine',
						'type'              => 'image',
						'instructions'      => 'Minimo 960x960 px',
						'required'          => 1,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'return_format'     => 'id',
						'preview_size'      => 'thumbnail',
						'library'           => 'all',
						'min_width'         => 960,
						'min_height'        => 960,
						'min_size'          => '',
						'max_width'         => '',
						'max_height'        => '',
						'max_size'          => '',
						'mime_types'        => '',
					),
					array(
						'key'               => 'field_5e66c9cf7a5c6',
						'label'             => 'Titolo',
						'name'              => 'titolo',
						'type'              => 'text',
						'instructions'      => '',
						'required'          => 1,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'placeholder'       => '',
						'prepend'           => '',
						'append'            => '',
						'maxlength'         => '',
					),
					array(
						'key'               => 'field_5e66c9df7a5c7',
						'label'             => 'Contenuto',
						'name'              => 'contenuto',
						'type'              => 'wysiwyg',
						'instructions'      => '',
						'required'          => 1,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'tabs'              => 'all',
						'toolbar'           => 'full',
						'media_upload'      => 0,
						'delay'             => 1,
					),
				),
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'block',
					'operator' => '==',
					'value'    => 'acf/sticky-content',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
	) );

	/**
	 * Video Fullwidth
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5e66ba35b71e9',
		'title'                 => 'Video Fullwidth',
		'fields'                => array(
			array(
				'key'               => 'field_5e66ba39c1618',
				'label'             => 'Video fullwidth',
				'name'              => 'longform_video_fullwidth',
				'type'              => 'file',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'return_format'     => 'url',
				'library'           => 'all',
				'min_size'          => '',
				'max_size'          => '',
				'mime_types'        => 'mp4',
			),
			array(
				'key'               => 'field_4346ba96c1619',
				'label'             => 'Url Youtube Video fullwidth',
				'name'              => 'youtube_video_fullwidth',
				'type'              => 'text',
				'instructions'      => 'Inserisci la url del video YouTube, accertandoti abbia questo fomato: https://www.youtube.com/watch?v=xxxxxx. Se valorizzata ha priorità sul file video.',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key' => 'field_6565656508a56fa7',
				'label' => 'Autoplay Youtube',
				'name' => 'youtube_video_autoplay',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 0,
				'ui' => 0,
				'translations' => 'copy_once',
				'ui_on_text' => '',
				'ui_off_text' => '',
			),
			array(
				'key'               => 'field_5e66ba96c1619',
				'label'             => 'Titolo Video fullwidth',
				'name'              => 'titolo_video_fullwidth',
				'type'              => 'text',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5e66baaac161a',
				'label'             => 'Contenuto Video fullwidth',
				'name'              => 'contenuto_video_fullwidth',
				'type'              => 'textarea',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'maxlength'         => '',
				'rows'              => 4,
				'new_lines'         => '',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'block',
					'operator' => '==',
					'value'    => 'acf/fullwidth-video',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
	) );

	/**
	 * 3 colonne
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5e5f987b7c110',
		'title'                 => 'Test',
		'fields'                => array(
			array(
				'key'               => 'field_5e668e2079cb7',
				'label'             => 'Longform: 3 colonne',
				'name'              => 'longform_3_colonne',
				'type'              => 'flexible_content',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'layouts'           => array(
					'layout_5e668e2c46aa3' => array(
						'key'        => 'layout_5e668e2c46aa3',
						'name'       => 'video',
						'label'      => 'Video',
						'display'    => 'block',
						'sub_fields' => array(
							array(
								'key'               => 'field_5e668e3f79cb8',
								'label'             => 'Video',
								'name'              => 'video',
								'type'              => 'file',
								'instructions'      => '',
								'required'          => 0,
								'conditional_logic' => 0,
								'wrapper'           => array(
									'width' => '',
									'class' => '',
									'id'    => '',
								),
								'return_format'     => 'id',
								'library'           => 'all',
								'min_size'          => '',
								'max_size'          => '',
								'mime_types'        => 'mp4',
							),
							array(
								'key'               => 'field_4556ba96c1619',
								'label'             => 'Url Youtube Video',
								'name'              => 'youtube_video',
								'type'              => 'text',
								'instructions'      => 'Inserisci la url del video YouTube, accertandoti abbia questo fomato: https://www.youtube.com/watch?v=xxxxxx. Se valorizzata ha priorità sul file video.',
								'required'          => 0,
								'conditional_logic' => 0,
								'wrapper'           => array(
									'width' => '',
									'class' => '',
									'id'    => '',
								),
								'default_value'     => '',
								'placeholder'       => '',
								'prepend'           => '',
								'append'            => '',
								'maxlength'         => '',
							),
							array(
								'key' => 'field_64356511108a56fa7',
								'label' => 'Autoplay Youtube',
								'name' => 'youtube_video_autoplay',
								'type' => 'true_false',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'message' => '',
								'default_value' => 0,
								'ui' => 0,
								'translations' => 'copy_once',
								'ui_on_text' => '',
								'ui_off_text' => '',
							),
						),
						'min'        => '',
						'max'        => '',
					),
					'layout_5e668e5779cb9' => array(
						'key'        => 'layout_5e668e5779cb9',
						'name'       => 'cta',
						'label'      => 'CTA',
						'display'    => 'block',
						'sub_fields' => array(
							array(
								'key'               => 'field_5e668e6879cba',
								'label'             => 'Titolo',
								'name'              => 'titolo',
								'type'              => 'text',
								'instructions'      => '',
								'required'          => 1,
								'conditional_logic' => 0,
								'wrapper'           => array(
									'width' => '',
									'class' => '',
									'id'    => '',
								),
								'default_value'     => '',
								'placeholder'       => '',
								'prepend'           => '',
								'append'            => '',
								'maxlength'         => '',
							),
							array(
								'key'               => 'field_5e668e7979cbb',
								'label'             => 'Contenuto',
								'name'              => 'contenuto',
								'type'              => 'textarea',
								'instructions'      => '',
								'required'          => 1,
								'conditional_logic' => 0,
								'wrapper'           => array(
									'width' => '',
									'class' => '',
									'id'    => '',
								),
								'default_value'     => '',
								'placeholder'       => '',
								'maxlength'         => '',
								'rows'              => 4,
								'new_lines'         => '',
							),
							array(
								'key'               => 'field_5e668e8179cbc',
								'label'             => 'CTA',
								'name'              => 'cta',
								'type'              => 'text',
								'instructions'      => '',
								'required'          => 1,
								'conditional_logic' => 0,
								'wrapper'           => array(
									'width' => '',
									'class' => '',
									'id'    => '',
								),
								'default_value'     => '',
								'placeholder'       => '',
								'prepend'           => '',
								'append'            => '',
								'maxlength'         => '',
							),
							array(
								'key'               => 'field_5e668e8a79cbd',
								'label'             => 'Link CTA',
								'name'              => 'link_cta',
								'type'              => 'url',
								'instructions'      => '',
								'required'          => 0,
								'conditional_logic' => 0,
								'wrapper'           => array(
									'width' => '',
									'class' => '',
									'id'    => '',
								),
								'default_value'     => '',
								'placeholder'       => '',
							),
						),
						'min'        => '',
						'max'        => '',
					),
					'layout_5e668e9679cbe' => array(
						'key'        => 'layout_5e668e9679cbe',
						'name'       => 'immagine',
						'label'      => 'Immagine',
						'display'    => 'block',
						'sub_fields' => array(
							array(
								'key'               => 'field_5e668ea079cbf',
								'label'             => 'Immagine',
								'name'              => 'immagine',
								'type'              => 'image',
								'instructions'      => '',
								'required'          => 0,
								'conditional_logic' => 0,
								'wrapper'           => array(
									'width' => '',
									'class' => '',
									'id'    => '',
								),
								'return_format'     => 'id',
								'preview_size'      => 'medium',
								'library'           => 'all',
								'min_width'         => '',
								'min_height'        => '',
								'min_size'          => '',
								'max_width'         => '',
								'max_height'        => '',
								'max_size'          => '',
								'mime_types'        => '',
							),
						),
						'min'        => '',
						'max'        => '',
					),
				),
				'button_label'      => 'Aggiungi elemento',
				'min'               => 3,
				'max'               => 3,
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'block',
					'operator' => '==',
					'value'    => 'acf/video-img-cta',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
	) );

	/**
	 * Background mutation
	 */
	acf_add_local_field_group(array(
		'key' => 'group_5efd7639cdd84',
		'title' => 'Background mutation',
		'fields' => array(
			array(
				'key' => 'field_5efd76e3c0d0f',
				'label' => 'Paragrafo',
				'name' => 'longform_mutation_text',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'basic',
				'media_upload' => 0,
				'delay' => 1,
				'translations' => 'translate',
			),
			array(
				'key' => 'field_5efd76433515f',
				'label' => 'Colore del testo',
				'name' => 'longform_mutation_text_color',
				'type' => 'color_picker',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '50',
					'class' => '',
					'id' => '',
				),
				'default_value' => '#FFFFFF',
				'translations' => 'ignore',
			),
			array(
				'key' => 'field_5efd76a335160',
				'label' => 'Colore dello sfondo',
				'name' => 'longform_mutation_background_color',
				'type' => 'color_picker',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '50',
					'class' => '',
					'id' => '',
				),
				'default_value' => '#000000',
				'translations' => 'ignore',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'block',
					'operator' => '==',
					'value' => 'acf/background-mutation',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

endif;



if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_5f22ab96e4c9f',
		'title' => 'Longform: immagine + testo',
		'fields' => array(
			array(
				'key' => 'field_5f22abb6f3adf',
				'label' => 'Immagine',
				'name' => 'longform_immagine_testo_img',
				'type' => 'image',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format'     => 'id',
				'preview_size'      => 'medium',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
				'translations' => 'copy_once',
			),
			array(
				'key' => 'field_5f22ac09f3ae0',
				'label' => 'Testo',
				'name' => 'longform_immagine_testo_txt',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'basic',
				'media_upload' => 1,
				'delay' => 0,
				'translations' => 'translate',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'block',
					'operator' => '==',
					'value' => 'acf/img-txt',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

endif;

/* img è cta 1/3 - 2/3 */

if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_7777ab96e4c9f',
		'title' => 'Longform: immagine + cta',
		'fields' => array(
			array(
				'key' => 'field_5f22ab666df',
				'label' => 'Immagine',
				'name' => 'longform_immagine_cta_img',
				'type' => 'image',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format'     => 'url',
				'preview_size'      => 'medium',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
				'translations' => 'copy_once',
			),
			array(
				'key' => 'field_7777c09f3ae0',
				'label' => 'Titolo',
				'name' => 'longform_immagine_cta_titolo',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'basic',
				'media_upload' => 1,
				'delay' => 0,
				'translations' => 'translate',
			),
			array(
				'key' => 'field_778888f3ae0',
				'label' => 'Sottotitolo',
				'name' => 'longform_immagine_cta_sottotitolo',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'basic',
				'media_upload' => 1,
				'delay' => 0,
				'translations' => 'translate',
			),
			array(
				'key' => 'field_777634f3ae0',
				'label' => 'Label CTA',
				'name' => 'longform_immagine_cta_label',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'basic',
				'media_upload' => 1,
				'delay' => 0,
				'translations' => 'translate',
			),
			array(
				'key' => 'field_6542894f3ae0',
				'label' => 'Link CTA',
				'name' => 'longform_immagine_cta_link',
				'type' => 'url',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'basic',
				'media_upload' => 1,
				'delay' => 0,
				'translations' => 'translate',
			),
			array(
				'key' => 'field_589763466df',
				'label' => 'Logo sponsor',
				'name' => 'longform_immagine_cta_sponsor',
				'type' => 'image',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format'     => 'url',
				'preview_size'      => 'medium',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
				'translations' => 'copy_once',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'block',
					'operator' => '==',
					'value' => 'acf/fullwidth-img-cta',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

endif;


if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_5f23d0775e7ea',
		'title' => 'Longform: 2 columns block',
		'fields' => array(
			array(
				'key' => 'field_5f23d08a56fa7',
				'label' => 'Blocco boxed',
				'name' => 'longform_2_cols_boxed',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 0,
				'ui' => 0,
				'translations' => 'copy_once',
				'ui_on_text' => '',
				'ui_off_text' => '',
			),
			array(
				'key' => 'field_5f23d0ab56fa8',
				'label' => 'Colonna sinistra',
				'name' => 'longform_2_cols_first_col',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
				'delay' => 0,
				'translations' => 'translate',
			),
			array(
				'key' => 'field_5f23d0c156fa9',
				'label' => 'Colonna destra',
				'name' => 'longform_2_cols_second_col',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
				'delay' => 0,
				'translations' => 'translate',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'block',
					'operator' => '==',
					'value' => 'acf/block-2-cols',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

endif;



if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_5f23d0775e8ea',
		'title' => 'Longform: Citazione',
		'fields' => array(
			array(
				'key' => 'field_5f23d08a57fa7',
				'label' => 'Testo citazione',
				'name' => 'longform_blockquote_testo_citazione',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => "",
				'translations' => 'translate',
				'ui_on_text' => '',
				'ui_off_text' => '',
			),
			array(
				'key' => 'field_5f23d0ab57fa9',
				'label' => 'Autore citazione',
				'name' => 'longform_blockquote_autore_citazione',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'translations' => 'translate',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'block',
					'operator' => '==',
					'value' => 'acf/block-quote',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

endif;



if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_5f23d6665e8ea',
		'title' => 'Longform: Sponsor',
		'fields' => array(
			array(
				'key' => 'field_5f66608a57fa7',
				'label' => 'Label',
				'name' => 'longform_sponsor_label',
				'type' => 'text',
				'instructions' => 'es: powered by',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => "",
				'translations' => 'translate',
				'ui_on_text' => '',
				'ui_off_text' => '',
			),
			array(
				'key' => 'field_666d0ab57fa9',
				'label' => 'Link sponsor',
				'name' => 'longform_sponsor_link',
				'type' => 'url',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'translations' => 'translate',
			),
			array(
				'key'               => 'field_5e66c9b966665',
				'label'             => 'Logo',
				'name'              => 'longform_sponsor_logo',
				'type'              => 'image',
				'required'          => 1,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'return_format'     => 'id',
				'preview_size'      => 'thumbnail',
				'library'           => 'all',
				'min_width'         => '',
				'min_height'        => '',
				'min_size'          => '',
				'max_width'         => '',
				'max_height'        => '',
				'max_size'          => '',
				'mime_types'        => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'block',
					'operator' => '==',
					'value' => 'acf/block-sponsor',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

endif;



if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_6f22ab96e5c9f',
		'title' => 'Longform: immagine full',
		'fields' => array(
			array(
				'key' => 'field_6f22abb6f3adf',
				'label' => 'Immagine',
				'name' => 'longform_image',
				'type' => 'image',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format'     => 'id',
				'preview_size'      => 'medium',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
				'translations' => 'copy_once',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'block',
					'operator' => '==',
					'value' => 'acf/block-image',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

endif;

if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_60f18551dd156',
		'title' => 'Indice',
		'fields' => array(
			array(
				'key' => 'field_60f1856617f2b',
				'label' => 'Numero colonne',
				'name' => 'longform_indice_numero_colonne',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					1 => '1',
					2 => '2',
				),
				'default_value' => 1,
				'allow_null' => 0,
				'multiple' => 0,
				'ui' => 0,
				'return_format' => 'value',
				'translations' => 'copy_once',
				'ajax' => 0,
				'placeholder' => '',
			),
			array(
				'key' => 'field_60f1859117f2c',
				'label' => 'Indice Colonna 1',
				'name' => 'longform_indice_indice_colonna_1',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => 0,
				'max' => 0,
				'layout' => 'row',
				'button_label' => 'Aggiungi link',
				'sub_fields' => array(
					array(
						'key' => 'field_60f185ac17f2d',
						'label' => 'Voce Indice',
						'name' => 'voce_indice',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'translations' => 'translate',
					),
					array(
						'key' => 'field_60f185bf17f2e',
						'label' => 'Link Indice',
						'name' => 'link_indice',
						'type' => 'link',
						'instructions' => 'Utilizza il # per linkare le ancore in pagina',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'url',
						'translations' => 'copy_once',
					),
				),
			),
			array(
				'key' => 'field_60f185e917f2f',
				'label' => 'Indice Colonna 2',
				'name' => 'longform_indice_indice_colonna_2',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field_60f1856617f2b',
							'operator' => '>',
							'value' => '1',
						),
					),
				),
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => 0,
				'max' => 0,
				'layout' => 'row',
				'button_label' => 'Aggiungi link',
				'sub_fields' => array(
					array(
						'key' => 'field_60f185e917f30',
						'label' => 'Voce Indice',
						'name' => 'voce_indice',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'translations' => 'translate',
					),
					array(
						'key' => 'field_60f185e917f31',
						'label' => 'Link Indice',
						'name' => 'link_indice',
						'type' => 'link',
						'instructions' => 'Utilizza il # per linkare le ancore in pagina',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'url',
						'translations' => 'copy_once',
					),
				),
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'block',
					'operator' => '==',
					'value' => 'acf/block-indice',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

endif;
