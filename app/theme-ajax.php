<?php

function lg_get_newsletter_response( $code ) {

	switch ( $code ) {
		case 0:
			$message = __( 'Operazione completata. Grazie per aver usufruito dei nostri servizi.', 'lifegate' );
			break;
		case 1:
			$message = __( 'Qualcosa è andato storto. Riprova fra qualche minuto.', 'lifegate' );
			break;
		case 2:
			$message = __( 'Indirizzo non valido.', 'lifegate' );
			break;
		case 3:
			$message = __( 'L\'indirizzo risulta già iscritto ai nostri servizi.', 'lifegate' );
			break;
		case 10:
			$message = __( 'Per piacere, accetta i termini del servizio.', 'lifegate' );
			break;
		case 11:
			$message = __( 'Per piacere, accetta i termini addizionali del servizio.', 'lifegate' );
			break;
		default:
			$message = __( 'Qualcosa è andato storto. Riprova fra qualche minuto.', 'lifegate' );
			break;
	}

	return $message;


}

function lg_newsletter_subscribe( $email ) {
	$post_data = '';

	$post_data .= '?source=generic';
	$post_data .= "&Email=" . urlencode( $email );
	$post_data .= "&List=1";
	$post_data .= "&group=26";
	$post_data .= "&retCode=1";

	if ( ! function_exists( 'curl_init' ) ) {
		tbm_gump_send_error_response( 'curl_init' );
	}

	$ch = curl_init();
	curl_setopt( $ch, CURLOPT_URL, 'https://c4f0i.emailsp.com/frontend/xmlSubscribe.aspx/' );
	curl_setopt( $ch, CURLOPT_POST, 1 );
	curl_setopt( $ch, CURLOPT_POSTFIELDS, $post_data );
	curl_setopt( $ch, CURLOPT_HEADER, 0 );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
	$response = curl_exec( $ch );

	$response = trim( $response );
	if ( ( $response != '0' ) && ( $response != '1' ) && ( $response != '2' ) && ( $response != '3' ) && ( $response != '-1011' ) ) {
		tbm_gump_send_error_response( 'Generic error: ' . $response );
	}

	return lg_get_newsletter_response( $response );
}


/**
 * Ajax endpoint for mail subscription
 */
add_action( 'wp_ajax_nopriv_mailup_subscribe', 'lifegate_mailup_subscribe' );
add_action( 'wp_ajax_mailup_subscribe', 'lifegate_mailup_subscribe' );
function lifegate_mailup_subscribe() {

	if ( $_SERVER['REQUEST_METHOD'] !== 'GET' ) {
		tbm_gump_send_error_response( 'Invalid request' );
	}

	$gump = new GUMP();
	$_GET = $gump->sanitize( $_GET );

	/**
	 * ########## VALIDATION
	 */
	$gump->validation_rules( array(
		'Email'       => 'required|valid_email',
		'termsAccept' => 'required|contains_list,yes;no',
	) );

	/**
	 * Run the method to validate data
	 */
	$validated_data = $gump->run( $_GET );

	/**
	 * If data are not valid
	 */
	if ( false === $validated_data ) {
		tbm_gump_send_error_response( $gump );

		exit;
	}

	$registration = lg_newsletter_subscribe( $validated_data['Email'] );

	if ( $registration ) {
		echo tbm_gump_send_success_response( $registration, '', '', array( 'Cache-Control' => 'max-age=3600, public' ) );
	}

	exit;
}

/**
 * Ajax endpoint for song search
 */
add_action( 'wp_ajax_nopriv_search_song', 'lifegate_search_song' );
add_action( 'wp_ajax_search_song', 'lifegate_search_song' );
function lifegate_search_song() {

	if ( $_SERVER['REQUEST_METHOD'] !== 'GET' ) {
		tbm_gump_send_error_response( 'Invalid request' );
	}

	/**
	 * Define array
	 */
	$songs = array();
	$out   = array();

	/**
	 * Call the $_GET filed validator
	 */
	$gump = new GUMP();
	$_GET = $gump->sanitize( $_GET );

	/**
	 * ########## VALIDATION
	 */
	$gump->validation_rules( array(
		'strDay'      => 'required|date,Y-m-d',
		'strHour'     => 'required|date,H:i',
		'termsAccept' => 'required|contains_list,yes;no',
	) );

	/**
	 * Run the method to validate data
	 */
	$validated_data = $gump->run( $_GET );

	/**
	 * If data are not valid
	 */
	if ( false === $validated_data ) {
		tbm_gump_send_error_response( $gump );
	}

	/**
	 * If date is not valid
	 */
	if ( date( 'U', strtotime( $validated_data['strDay'] . ' ' . $validated_data['strHour'] ) ) > current_time( 'U' ) ) {
		tbm_gump_send_error_response( 'La data richiesta è più recente della data attuale' );
	}

	/**
	 * Subscribe to email
	 */
	if ( $validated_data['Email'] && 'yes' === $validated_data['termsAccept'] ) {
		$registration = lg_newsletter_subscribe( $validated_data['Email'] );
		if ( $registration ) {
			$out['newsletter'] = $registration;
		}
	}


	/**
	 * Elaborate date. Add one hour to from date
	 */
	$user_date = $validated_data['strDay'] . ' ' . $validated_data['strHour'];
	$from      = date( 'Y-m-d H:00', strtotime( $user_date ) );
	$to        = date( 'Y-m-d H:00', strtotime( $user_date ) + ( 1 * HOUR_IN_SECONDS ) );
	$url       = 'http://lifesrvradio/gsimportexportservice/gsimportexportservice.asmx?wsdl';

	/**
	 * Check transient
	 */
	$key = substr( md5( $user_date ), 0, 6 );
	if ( $results = get_transient( 'tbm_song_' . $key ) ) {
		tbm_gump_send_success_response( $results, '', '', array( 'Cache-Control' => 'max-age=3600, public' ) );
		exit;
	}

	$options = array(
		'encoding'           => 'UTF-8',
		'verifypeer'         => false,
		'verifyhost'         => false,
		'soap_version'       => SOAP_1_2,
		'trace'              => true,
		'exceptions'         => true,
		'connection_timeout' => 180,
	);

	$params = array(
		"stationID"          => 1,
		"schedID"            => 1,
		"strDateTimeFrom"    => $from,
		"strDateTimeTo"      => $to,
		"details"            => true,
		"ignorePastSchedule" => false,
	);

	/**
	 * Send request; get response; elaborate response; save transient; send response
	 */
	try {
		/**
		 *  Send request
		 */
		$client   = new SoapClient( $url, $options );
		$response = $client->GetStationSchedule( $params )->GetStationScheduleResult;

		if ( ! $response ) {
			tbm_gump_send_error_response( 'Error in response' );
			exit;
		}

		/**
		 * Get response
		 */
		$xml = @simplexml_load_string( $response );

		if ( empty( $xml->IsExportError ) || $xml->IsExportError != 'False' ) {
			tbm_gump_send_error_response( 'Nessun risultato' );
			exit;
		};

		$xs = $xml->Day->Event;

		/**
		 * Elaborate response
		 * TODO This should go in external function
		 */
		foreach ( $xs as $x ) {
			if ( $x->attributes()->entryType == 'Song' ) {
				$airtime  = (string) $x->attributes()->airTime;
				$airdate  = (string) $x->attributes()->airDate;
				$duration = (string) $x->attributes()->duration;
				$title    = (string) $x->ProgramElement->children( 'gs_s', true )->children( 'gs_s', true )[0]->attributes()->name;
				$artist   = (string) $x->ProgramElement->children( 'gs_s', true )->children( 'gs_s', true )[1]->attributes()->name;
				$album    = (string) $x->ProgramElement->children( 'gs_s', true )->children( 'gs_s', true )[3]->attributes()->name;
				$songs[]  = array(
					'duration' => $duration,
					'airtime'  => $airtime,
					'airdate'  => $airdate,
					'title'    => $title,
					'artist'   => ucwords( strtolower( $artist ) ),
					'album'    => $album
				);
			}

		};

		if ( empty( $songs ) ) {
			tbm_gump_send_error_response( 'Nessun risultato' );
			exit;
		}

		/**
		 * Loop the songs array and try to find the song played in the user request date.
		 * If available, returns the song before and after
		 * TODO This should go in external function
		 */
		foreach ( $songs as $key => $song ) {

			// If song is not aired
			if ( empty( $song['airtime'] ) ) {
				continue;
			}

			$date_aired     = (int) date( 'U', strtotime( $song['airtime'] . '' . $song['airdate'] ) + $song['duration'] );
			$date_requested = (int) date( 'U', strtotime( $user_date ) );

			// If song is first aired after user date request
			if ( $date_aired >= $date_requested ) {
				$results['result'] = $songs[ $key ];

				// If is not the first key, return the song played before
				if ( isset( $songs[ $key - 1 ] ) ) {
					$results['before'] = $songs[ $key - 1 ];
				}

				// If is not the last key, return the song played after
				if ( $songs[ $key + 1 ] ) {
					$results['after'] = $songs[ $key + 1 ];
				}
				$results['date_aired']     = $date_aired;
				$results['date_requested'] = $date_requested;
				break;
			}
		}

		if ( empty( $results ) ) {
			tbm_gump_send_error_response( 'Nessun risultato' );
			exit;
		}

		/**
		 * Save transient
		 */
		set_transient( 'tbm_song_' . $key, $results, 7 * DAY_IN_SECONDS );

		/**
		 * Send response
		 */
		$out['song'] = $results;
		tbm_gump_send_success_response( $out, '', '', array( 'Cache-Control' => 'max-age=3600, public' ) );
	} catch ( Throwable $e ) {
		tbm_gump_send_error_response( 'Error in service' );
	}

	exit;

}

/**
 * Loop the songs array and try to find the song played in the user request date.
 */
function tbm_choose_the_song( $blocks ) {
	$results = array();
	opcache_reset();
	foreach ( $blocks as $key => $song ) {

		// If song is not aired
		if ( empty( $song['airtime'] ) ) {
			continue;
		}

		// If asset is not song, continue
		if ( strtolower( $song['asset'] ) !== 'song' ) {
			continue;
		}

		$date_aired     = (int) date( 'U', strtotime( $song['airtime'] ) + $song['duration'] );
		$date_requested = current_time( 'U' );

		// If song is first aired after date request
		if ( $date_aired >= $date_requested ) {
			$results['result']         = $blocks[ $key ];
			$results['date_aired']     = $date_aired;
			$results['date_requested'] = $date_requested;

			return $results;
		}

	}

	return $results;
}

add_action( 'wp_ajax_nopriv_playing_song', 'lifegate_playing_song' );
add_action( 'wp_ajax_playing_song', 'lifegate_playing_song' );
function lifegate_playing_song() {

	/**
	 * Define array
	 */
	$blocks = array();
	$out    = array();


	/**
	 * Set the url
	 */
	$url = 'http://lifesrvradio:3132/StatusFeed?wsdl';

	/**
	 * Check transient
	 */
	if ( $results = get_transient( 'tbm_playing_songs' ) ) {
		$out = tbm_choose_the_song( $results );
		if ( $out ) {
			tbm_gump_send_success_response( $out, '', '', array( 'Cache-Control' => 'max-age=3600, public' ) );
		}
	}

	$options = array(
		'encoding'           => 'UTF-8',
		'verifypeer'         => false,
		'verifyhost'         => false,
		'trace'              => 1,
		'exceptions'         => 1,
		'connection_timeout' => 180,
	);

	$params = array(
		"stationID" => 1,
	);

	/**
	 * Send request; get response; elaborate response; save transient; send response
	 */
	try {
		/**
		 *  Send request
		 */
		$client   = new SoapClient( $url, $options );
		$response = $client->GetStationFull( $params )->GetStationFullResult;

		if ( ! $response ) {
			tbm_gump_send_error_response( 'Error in response' );
		}


		if ( empty( $response->Queue ) && empty( $response->Queue->Event ) || ! is_array( $response->Queue->Event ) ) {
			tbm_gump_send_error_response( 'Nessun risultato' );
		};

		foreach ( $response->Queue->Event as $block ) {
			/**
			 * Comvert UTC to local
			 */
			$dt = new DateTime( $block->AirTimeUtc, new DateTimeZone( 'UTC' ) );
			$dt->setTimezone( new DateTimeZone( 'Europe/Rome' ) );
			$date = $dt->format( 'd-m-Y H:i' );

			/**
			 * Build the array
			 */
			$out['airtime']  = $date;
			$out['artist']   = ucwords( strtolower( $block->Artist ) );
			$out['title']    = $block->Title;
			$out['asset']    = $block->AssetTypeName;
			$out['duration'] = $block->DurationInSeconds; //seconds

			/**
			 * Save all iterations in $blocks array
			 */
			$blocks[] = $out;
		}

		if ( empty( $blocks ) ) {
			tbm_gump_send_error_response( 'Nessun risultato' );
		}

		/**
		 * Save transient
		 */
		set_transient( 'tbm_playing_songs', $blocks, 45 * MINUTE_IN_SECONDS );

		/**
		 * Choose the song
		 */
		$out = tbm_choose_the_song( $blocks );

		/**
		 * Check response
		 */
		if ( empty( $out ) ) {
			tbm_gump_send_error_response( 'Nessun risultato' );
		}

		/**
		 * Send response
		 */
		tbm_gump_send_success_response( $out, '', '', array( 'Cache-Control' => 'max-age=3600, public' ) );
		exit;
	} catch ( Throwable $e ) {
		tbm_gump_send_error_response( 'Error in service' );
		exit;
	}

}



/**
 * Ajax endpoint for mail subscription
 */
add_action( 'wp_ajax_nopriv_trebmeteo', 'lifegate_trebmeteo' );
add_action( 'wp_ajax_trebmeteo', 'lifegate_trebmeteo' );
function lifegate_trebmeteo() {

	if ( $_SERVER['REQUEST_METHOD'] !== 'GET' ) {
		tbm_gump_send_error_response( 'Invalid request' );
	}

	$url = "https://api.3bmeteo.com/publicv3/api_aria/inquinanti_geo/". $_GET["lat"] . "/" . $_GET["long"] . "?X-API-KEY=lYZS9cgHhRlTsLlV4TvmfvvPPNVXLmCcuYWWPM39&format=json2";
	$response = wp_remote_get( $url );

	if ( is_array( $response ) && ! is_wp_error( $response ) ) {
		$headers = $response['headers']; // array of http header lines
		$body    = $response['body']; // use the content
	}else{
		tbm_gump_send_error_response( 'Error response' );
	}
	$out = $body;

	if ( empty( $out ) ) {
		tbm_gump_send_error_response( 'No values' );
	}

	/**
	 * Send response
	 */
	tbm_gump_send_success_response( $out, '', '', array( 'Cache-Control' => 'max-age=3600, public' ) );
	exit;

}
