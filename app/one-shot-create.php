<?php


/**
 * Array [Template => Titolo]
 */
$pages = array(
	'views/page-magazine.blade.php'             => [ 'Magazine', 'magazine' ],
	'views/page-trend.blade.php'                => [ 'Trend', 'trend' ],
	'views/page-newsletter.blade.php'           => [ 'Iscriviti alla Newsletter di LifeGate', 'newsletter' ],
	'views/page-palinsesto.blade.php'           => [ 'Palinsesto', 'palinsesto' ]
);

if ( isset( $_GET["cmd"] ) && $_GET["cmd"] == 'create' ) {
	foreach ( $pages as $key => $page ) {
		if ( ! get_page_by_title( $page ) ) {
			$post_details = array(
				'post_title'    => $page[0],
				'post_name'     => $page[1],
				'post_content'  => '',
				'post_status'   => 'publish',
				'post_type'     => 'page',
				'page_template' => $key
			);
			wp_insert_post( $post_details );
		}
	};
}
