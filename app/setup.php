<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

/* Flush rewrite rules when update theme. */
add_action( 'after_setup_theme', function () {

	$current_version = wp_get_theme()->get( 'Version' );
	$old_version     = get_option( 'tbm_2018_theme_version' );

	if ( $old_version !== $current_version ) {

		flush_rewrite_rules( false );

		// update not to run twice
		update_option( 'tbm_2018_theme_version', $current_version );
	}
} );

/**
 * Dequeue the jQuery script
 *
 * Hooked to the wp_print_scripts action, with a late priority (100),
 * so that it is after the script was enqueued.
 */

add_action( 'wp_print_scripts', function () {
	wp_dequeue_script( 'jquery' );
}, 100 );


/**
 * Theme setup
 */
add_action( 'after_setup_theme', function () {
	/**
	 * Enable features from Soil when plugin is activated
	 * @link https://roots.io/plugins/soil/
	 */
	add_theme_support( 'soil-clean-up' );
	add_theme_support( 'soil-nav-walker' );

	/**
	 * Enable plugins to manage the document title
	 * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
	 */
	add_theme_support( 'title-tag' );

	/**
	 * Register navigation menus
	 * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
	 */
	register_nav_menus( [
		'global_navigation'    => __( 'Menu Globale', 'lifegate' ),
		'magazine_navigation'  => __( 'Menu News', 'lifegate' ),
		'radio_navigation'     => __( 'Menu Radio', 'lifegate' ),
		'trends_navigation'    => __( 'Menu Trend', 'lifegate' ),
		'hamburger_navigation' => __( 'Menu Hamburger', 'lifegate' ),
		'channels_navigation'  => __( 'Menu Channel', 'lifegate' ),
		'casehistory_navigation'  => __( 'Menu Mediacase', 'lifegate' ),
	] );


	/**
	 * Enable post thumbnails
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	/**
	 * Enable HTML5 markup support
	 * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
	 */
	add_theme_support( 'html5', [ 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ] );

	/**
	 * Enable selective refresh for widgets in customizer
	 * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
	 */
	add_theme_support( 'customize-selective-refresh-widgets' );

}, 20 );

/**
 * Register sidebars
 */
add_action( 'widgets_init', function () {
	$config = [
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<span>',
		'after_title'   => '</span>'
	];


	register_sidebar( [
		                  'name' => 'Footer Sidebar Sx',
		                  'id'   => 'footer_sidebar1'
	                  ] + $config );
	register_sidebar( [
		                  'name' => 'Footer Sidebar Center',
		                  'id'   => 'footer_sidebar2'
	                  ] + $config );
	register_sidebar( [
		'name'          => 'Footer Sidebar Dx',
		'id'            => 'footer_sidebar3',
		'before_widget' => '<summary>',
		'after_widget'  => '</summary>',
		'before_title'  => '<span>',
		'after_title'   => '</span>'
	] );
	register_sidebar( [
		                  'name' => 'Post Footer',
		                  'id'   => 'post_footer'
	                  ] + $config );

	register_widget( '\App\Act_Now' );
} );

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action( 'the_post', function ( $post ) {
	sage( 'blade' )->share( 'post', $post );
} );

/**
 * Setup Sage options
 */
add_action( 'after_setup_theme', function () {
	/**
	 * Add JsonManifest to Sage container
	 */
	sage()->singleton( 'sage.assets', function () {
		return new JsonManifest( config( 'assets.manifest' ), config( 'assets.uri' ) );
	} );

	/**
	 * Add Blade to Sage container
	 */
	sage()->singleton( 'sage.blade', function ( Container $app ) {
		$cachePath = config( 'view.compiled' );
		if ( ! file_exists( $cachePath ) ) {
			wp_mkdir_p( $cachePath );
		}
		( new BladeProvider( $app ) )->register();

		return new Blade( $app['view'] );
	} );

	/**
	 * Create @asset() Blade directive
	 */
	sage( 'blade' )->compiler()->directive( 'asset', function ( $asset ) {
		return "<?php echo " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
	} );


	/**
	 * Blade directive to dump template variables. Accepts single parameter
	 * but also could be invoked without parameters to dump all defined variables.
	 * It does not stop script execution.
	 * @example @d
	 * @example @d(auth()->user())
	 */
	sage( 'blade' )->compiler()->directive( 'd', function ( $data ) {
		return sprintf( "<h3 style='margin-top: 10px;font-family: arial'>Log</h3>\n<div style='font-family: \"Courier New\", Courier, monospace'><pre><?php print_r(%s);?></pre><p>Finito</p></div>",
			null !== $data ? $data : "get_defined_vars()['__data']"
		);
	} );
	/**
	 * Blade directive to dump template variables. Accepts single parameter
	 * but also could be invoked without parameters to dump all defined variables.
	 * It works similar to dd() function and does stop script execution.
	 * @example @dd
	 * @example @dd(auth()->user())
	 */
	sage( 'blade' )->compiler()->directive( 'dd', function ( $data ) {
		return sprintf( "<h3 style='margin-top: 10px;font-family: arial'>Log</h3>\n<div style='font-family: \"Courier New\", Courier, monospace'><pre><?php print_r(%s); exit; ?></pre><p>Finito</p></div>",
			null !== $data ? $data : "get_defined_vars()['__data']"
		);
	} );

} );

/**
 * Set post content width
 */
add_action( 'template_redirect', function () {
	global $content_width;

	$content_width = 640;
} );
