<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter( 'body_class', function ( array $classes ) {
	/** Add page slug if it doesn't exist */
	if ( is_single() || is_page() && ! is_front_page() ) {
		if ( ! in_array( basename( get_permalink() ), $classes ) ) {
			$classes[] = basename( get_permalink() );
		}
	}

	/** Add class if sidebar is active */
	if ( display_sidebar() ) {
		$classes[] = 'sidebar-primary';
	}

	/** Clean up class names for custom templates */
	$classes = array_map( function ( $class ) {
		return preg_replace( [ '/-blade(-php)?$/', '/^page-template-views/' ], '', $class );
	}, $classes );

	return array_filter( $classes );
} );

/**
 * Add "… Continued" to the excerpt
 */
add_filter( 'excerpt_more', function () {
	return ' &hellip; <a href="' . get_permalink() . '">' . __( 'Continued', 'lifegate' ) . '</a>';
} );

/**
 * Template Hierarchy should search for .blade.php files
 */
collect( [
	'index',
	'404',
	'archive',
	'author',
	'category',
	'tag',
	'taxonomy',
	'date',
	'home',
	'frontpage',
	'page',
	'paged',
	'search',
	'single',
	'singular',
	'attachment',
	'embed'
] )->map( function ( $type ) {
	add_filter( "{$type}_template_hierarchy", __NAMESPACE__ . '\\filter_templates' );
} );

/**
 * Render page using Blade
 */
add_filter( 'template_include', function ( $template ) {
	$data = collect( get_body_class() )->reduce( function ( $data, $class ) use ( $template ) {
		return apply_filters( "sage/template/{$class}/data", $data, $template );
	}, [] );
	if ( $template ) {
		echo template( $template, $data );

		return get_stylesheet_directory() . '/index.php';
	}

	return $template;
}, PHP_INT_MAX );

/**
 * Tell WordPress how to find the compiled path of comments.blade.php
 */
add_filter( 'comments_template', function ( $comments_template ) {
	$comments_template = str_replace(
		[ get_stylesheet_directory(), get_template_directory() ],
		'',
		$comments_template
	);

	return template_path( locate_template( [
		"views/{$comments_template}",
		$comments_template
	] ) ?: $comments_template );
}, 100 );


/**
 * Add correct logo to structured data (ld+json)
 */
add_filter( 'amp_site_icon_url', function ( $data ) {
	$logo = "https://www.lifegate.it/app/themes/lifegate-2020/dist/images/apple-touch-icon-180x180.png";
	if ( $logo ) {
		return $logo;
	}
}, 11 );


add_filter( 'rss2_ns', function(){
	echo 'xmlns:media="http://search.yahoo.com/mrss/"';
});

// insert the image object into the RSS item (see MB-191)
add_action('rss2_item', function(){
	global $post;
	if (has_post_thumbnail($post->ID)){
		$thumbnail_ID = get_post_thumbnail_id($post->ID);
		$thumbnail = wp_get_attachment_image_src($thumbnail_ID, 'large');
		if (is_array($thumbnail)) {
			echo '<media:content medium="image" url="' . $thumbnail[0]
				. '" width="' . $thumbnail[1] . '" height="' . $thumbnail[2] . '" />';
		}
	}
});



//add_filter( 'wpseo_schema_graph_pieces', __NAMESPACE__ . '\\remove_breadcrumbs_from_schema', 11, 2 );
//add_filter( 'wpseo_schema_webpage',  __NAMESPACE__ . '\\remove_breadcrumb_schema_ref', 10, 1);

/**
 * Remove Yoast Pieces from the graph
 *
 * @param array                 $pieces graph pieces to output.
 * @param \WPSEO_Schema_Context $context Yoast object with context variables.
 */
function remove_breadcrumbs_from_schema ( $pieces, $context ) {
	return \array_filter( $pieces, function( $piece ) {
		return ! $piece instanceof \Yoast\WP\SEO\Generators\Schema\Breadcrumb;
	} );
}

function remove_breadcrumb_schema_ref ( $pieces ) {
	unset( $pieces['breadcrumb'] );
	return $pieces;
}


function cards_canonical($url) {
	global $post;

	if ( get_post_type( $post->ID ) == 'card' ) {

		$doc = get_query_var("doc");
		if(!$doc) return $url;

		return $url."/doc/".$doc;

	} else {
		return $url;
	}
}

add_filter( 'wpseo_canonical',  __NAMESPACE__ . '\\cards_canonical', 10, 1);

function autori_canonical($url) {
	global $post;

	if ( get_post_type( $post->ID ) == 'autori' ) {

		$doc = get_query_var("author_page");
		if(!$doc) return $url;

		return $url."/page/".$doc;

	} else {
		return $url;
	}
}
add_filter( 'wpseo_canonical',  __NAMESPACE__ . '\\autori_canonical', 10, 1);


/**
 * canonical original eng
 * @param $url
 * @return mixed|string
 */
function original_canonical($url)
{
	if (is_post_type_archive("original")) {
		if ( function_exists( 'pll_current_language' ) && 'en' === pll_current_language() ){
			return "https://www.lifegate.it/original";
		}
	}
	return $url;
}
add_filter( 'wpseo_canonical',  __NAMESPACE__ . '\\original_canonical', 10, 1);



/**
 * deregistro hashtags di liveblog
 */
function unregister_hashtags_for_posts() {
	unregister_taxonomy_for_object_type( 'hashtags', 'post' );
}
add_action( 'init',  __NAMESPACE__ . '\\unregister_hashtags_for_posts' );

// restringo i redirect automatici in modalità struct
// add_filter( 'strict_redirect_guess_404_permalink', '__return_true' );


add_filter( 'wpseo_sitemap_exclude_post_type',  __NAMESPACE__ .'\\lg_sitemap_exclude_post_type', 10, 2 );
function lg_sitemap_exclude_post_type( $value, $post_type ) {
	$post_type_to_exclude = array('mediacase','playlist-video', 'book');
	if( in_array( $post_type, $post_type_to_exclude ) ) return true;
}


// excludo Casa Bertallot
function exlude_podcasts( $query ) {
	if ( is_admin() || ! $query->is_main_query() )
		return;

	if ( $query->is_post_type_archive("show") ) {
		$query->set( 'post__not_in', array( 186296,285837,186304,186281, 186440 ) );
	}
}
add_action( 'pre_get_posts', __NAMESPACE__ .'\\exlude_podcasts', 1 );


add_filter( 'wpseo_robots',  __NAMESPACE__ .'\\lifegate_fix_noindex' );
function lifegate_fix_noindex( $robotsstr ) {
	if ( is_archive() && is_paged() ) {
	//	return 'noindex,follow';
	}
	if(is_singular("card")){
		$doc = get_query_var("doc");
		if($doc){
			return 'noindex,follow';
		}
	}

	return $robotsstr;
}


add_filter( 'wpseo_schema_organization',  __NAMESPACE__ .'\\lifegate_change_schema_type' );
function lifegate_change_schema_type( $data ) {
	return false;
	/*
	if (array_key_exists('@type', $data)) {
		if($data['@type'] == "Organization")
			$data['@type'] = 'NewsMediaOrganization';

		//$data["logo"] = "https://cdn.lifegate.it/0nPiIdREB8IP_raN9lnx0a5yRYI=/180x180/smart/https://www.lifegate.it/app/uploads/2020/09/logo-lg.png";
	}

	return $data;
	*/
}
add_action('wp_head',  function (){ ?>
	<script type="application/ld+json">{"@context": "https://schema.org", "@type": "NewsMediaOrganization", "url": "<?php echo get_home_url(); ?>", "logo": "https://cdn.lifegate.it/0nPiIdREB8IP_raN9lnx0a5yRYI=/180x180/smart/https://www.lifegate.it/app/uploads/2020/09/logo-lg.png"}	</script>
	<?php
});


add_filter( 'wpseo_schema_article',  __NAMESPACE__ .'\\lifegate_change_article_type' );
function lifegate_change_article_type( $data ) {

	if (array_key_exists('@type', $data)) {
		if(!is_singular("longform")) {
			if ($data['@type'] == "Article")
				$data['@type'] = 'NewsArticle';
		}
	}

	return $data;
}


add_action( 'wp_enqueue_scripts', __NAMESPACE__ .'\\lifegate_dequeue_dashicons' );
function lifegate_dequeue_dashicons() {
	if ( ! is_user_logged_in() ) {
		wp_deregister_style( 'dashicons' );
	}
}

// Disabilita l'editor di blocchi per i widget
add_filter( 'use_widgets_block_editor', '__return_false' );
