<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 20/12/2017
 * Time: 10:02
 */

/**
 *  Crea i post type necessari
 */

use PostTypes\PostType;
use PostTypes\Taxonomy;


/**
 * REGISTRAZIONE POST TYPE
 */
if ( ! defined( 'HTML_GALLERY_POST_TYPE' ) ) {
	define( 'HTML_GALLERY_POST_TYPE', 'gallery' );
}
if ( ! defined( 'HTML_VIDEO_POST_TYPE' ) ) {
	define( 'HTML_VIDEO_POST_TYPE', 'video' );
}
if ( ! defined( 'HTML_CARD_POST_TYPE' ) ) {
	define( 'HTML_CARD_POST_TYPE', 'card' );
}
if ( ! defined( 'HTML_EVENT_POST_TYPE' ) ) {
	define( 'HTML_EVENT_POST_TYPE', 'event' );
}
/**
 * Original
 */
$cpts = [
	'persone',
	'persone_news',
	'persone_iniziative',
	'persone_eventi',
	'persone_articoli',
	'sponsor',
	'rassegna_stampa',
	'aeria_gallery',
	'autori',
	'clienti',
	'longform',
	HTML_GALLERY_POST_TYPE,
	HTML_VIDEO_POST_TYPE,
	HTML_CARD_POST_TYPE
];

/**
 * Original
 */
$cpts = [
	'persone',
	'news',
	'iniziative',
	'sponsor',
	'rassegna_stampa',
	'clienti',
	'show',
	'longform',
	HTML_EVENT_POST_TYPE, //event
	HTML_GALLERY_POST_TYPE, //gallery
	HTML_VIDEO_POST_TYPE, //video
	HTML_CARD_POST_TYPE, //card
];

/**
 * New
 */
$cpts = [
	'iniziative',
	'show',
	'longform',
	'autori',
	'aeria_gallery',
	'sponsor'
];

/**
 * Iniziative
 */
$tbm_labels  = [
	'add_new' => __( 'Aggiungi', 'lifegate' )
];
$tbm_options = [
	'has_archive'  => true,
	'supports'     => array(
		"title",
		"editor",
		"thumbnail",
		"excerpt",
		"author",
		"excerpt",
		"page-attributes",
		"thumbnail",
		"custom-fields"
	),
	'show_in_rest' => true,
	'public'       => true
];
$tbm_names   = [
	'name'     => 'iniziative',
	'singular' => __( 'Iniziativa', 'lifegate' ),
	'plural'   => __( 'Iniziative', 'lifegate' ),
	'slug'     => 'iniziativa'
];
$tbm         = new PostType( $tbm_names, $tbm_options, $tbm_labels );
$tbm->icon( 'dashicons-megaphone' );
$tbm->taxonomy( 'post_tag' );
$tbm->taxonomy( 'category' );
$tbm->register();

/**
 * Programmi
 */
$tbm_labels  = [
	'add_new' => 'Aggiungi'
];
$tbm_options = [
	'rewrite'      => array( 'slug' => 'podcast' ),
	'has_archive'  => true,
	'supports'     => array(
		"title",
		"editor",
		"thumbnail",
		"excerpt",
		"author",
		"excerpt",
		"page-attributes",
		"thumbnail",
		"custom-fields"
	),
	'show_in_rest' => true,
	'public'       => true
];
$tbm_names   = [
	'name'     => 'show',
	'singular' => __( 'Programma', 'lifegate' ),
	'plural'   => __( 'Programmi', 'lifegate' ),
	'slug'     => 'show'
];
$tbm         = new PostType( $tbm_names, $tbm_options, $tbm_labels );
$tbm->icon( 'dashicons-microphone' );
$tbm->taxonomy( 'post_tag' );
$tbm->taxonomy( 'category' );
$tbm->register();

/**
 * Longform
 */
$tbm_labels  = [
	'add_new' => 'Aggiungi',
];
$tbm_options = [
	'has_archive'  => true,
	'supports'     => array(
		"title",
		"editor",
		"thumbnail",
		"excerpt",
		"author",
		"excerpt",
		"page-attributes",
		"thumbnail",
		"custom-fields"
	),
	'show_in_rest' => true,
	'public'       => true
];
$tbm_names   = [
	'name'     => 'longform',
	'singular' => __( 'Longform', 'lifegate' ),
	'plural'   => __( 'Longform', 'lifegate' ),
	'slug'     => 'longform'
];
$tbm         = new PostType( $tbm_names, $tbm_options, $tbm_labels );
$tbm->icon( 'dashicons-media-document' );
$tbm->taxonomy( 'post_tag' );
$tbm->taxonomy( 'category' );
$tbm->register();

/**
 * Rassegna stampa
 */
$tbm_labels  = [
	'add_new' => 'Aggiungi',
];
$tbm_options = [
	'has_archive'  => true,
	'supports'     => array(
		"title",
		"editor",
		"thumbnail",
		"excerpt",
		"author",
		"excerpt",
		"page-attributes",
		"thumbnail",
		"custom-fields"
	),
	'show_in_rest' => true,
	'public'       => true
];
$tbm_names   = [
	'name'     => 'rassegna_stampa',
	'singular' => __( 'Rassegna Stampa', 'lifegate' ),
	'plural'   => __( 'Rassegna Stampa', 'lifegate' ),
	'slug'     => 'rassegna_stampa'
];
$tbm         = new PostType( $tbm_names, $tbm_options, $tbm_labels );
$tbm->icon( 'dashicons-admin-site-alt' );
$tbm->register();


/**
 * Autore
 */
$tbm_labels = [
	'add_new' => 'Aggiungi',
];

$tbm_options = [
	'has_archive'  => true,
	'supports'     => array(
		"title",
		"editor",
		"thumbnail",
		"excerpt",
		"excerpt",
		"thumbnail",
	),
	'show_in_rest' => true,
	'public'       => true
];
$tbm_names   = [
	'name'     => 'autori',
	'singular' => __( 'Autore', 'lifegate' ),
	'plural'   => __( 'Autori', 'lifegate' ),
	'slug'     => 'autore'
];
$tbm         = new PostType( $tbm_names, $tbm_options, $tbm_labels );
$tbm->icon( 'dashicons-admin-users' );
$tbm->taxonomy( 'post_tag' );
$tbm->taxonomy( 'category' );
$tbm->register();


/**
 * Lifegate Original
 */
$tbm_labels  = [
	'add_new' => 'Aggiungi',
];
$tbm_options = [
	'has_archive'  => true,
	'supports'     => array(
		"title",
		"editor",
		"thumbnail",
		"excerpt",
		"author",
		"excerpt",
		"page-attributes",
		"thumbnail",
		"custom-fields"
	),
	'show_in_rest' => true,
	'public'       => true
];
$tbm_names   = [
	'name'     => 'original',
	'singular' => __( 'Originals', 'lifegate' ),
	'plural'   => __( 'Original', 'lifegate' ),
	'slug'     => 'original'
];
$tbm         = new PostType( $tbm_names, $tbm_options, $tbm_labels );
$tbm->icon( 'dashicons-awards' );
$tbm->taxonomy( 'post_tag' );
$tbm->taxonomy( 'category' );
$tbm->taxonomy( 'channel' );

$tbm->register();


/**
 * Lifegate CAse History
 */
$tbm_labels  = [
	'add_new' => 'Aggiungi',
];
$tbm_options = [
	'has_archive'  => true,
	'supports'     => array(
		"title",
		"editor",
		"thumbnail",
		"excerpt",
		"author",
		"excerpt",
		"page-attributes",
		"thumbnail",
		"custom-fields"
	),
	'show_in_rest' => true,
	'public'       => true
];
$tbm_names   = [
	'name'     => 'mediacase',
	'singular' => __( 'Media Case', 'lifegate' ),
	'plural'   => __( 'Media Case', 'lifegate' ),
	'slug'     => 'mediacase'
];
$tbm         = new PostType( $tbm_names, $tbm_options, $tbm_labels );
$tbm->icon( 'dashicons-media-interactive' );
$tbm->taxonomy( 'casehistory' );

$tbm->register();


/**
 * REGISTRAZIONE TASSONOMIE
 */

/** case history tax  */


$case_labels  = [
	'add_new_item' => 'Aggiungi'
];
$case_names   = [
	'name'     => 'casehistory',
	'singular' => __( 'Case History', 'lifegate' ),
	'plural'   => __( 'Case History', 'lifegate' ),
	'slug'     => 'casehistory'
];
$case_options = [
	'hierarchical' => true,
	'show_in_rest' => true,
	'capabilities' => array(
		'manage_terms' => 'manage_categories',
		'edit_terms'   => 'manage_categories',
		'delete_terms' => 'manage_categories',
		'assign_terms' => 'edit_posts'
	)
];
$case         = new Taxonomy( $case_names, $case_options, $case_labels );
$case->posttype( "mediacase" );
$case->register();
/**
 * Original
 */

$channel_labels  = [
	'add_new_item' => 'Aggiungi'
];
$channel_names   = [
	'name'     => 'channel',
	'singular' => __( 'Canale', 'lifegate' ),
	'plural'   => __( 'Canali', 'lifegate' ),
	'slug'     => 'channel'
];
$channel_options = [
	'hierarchical' => true,
	'show_in_rest' => true,
	'capabilities' => array(
		'manage_terms' => 'manage_categories',
		'edit_terms'   => 'manage_categories',
		'delete_terms' => 'manage_categories',
		'assign_terms' => 'edit_posts'
	)
];
$channel         = new Taxonomy( $channel_names, $channel_options, $channel_labels );
$channel->posttype( "original" );
$channel->register();


$special_cpts = [
	'iniziative',
	'show',
	'longform',
	HTML_EVENT_POST_TYPE, //event
	HTML_GALLERY_POST_TYPE, //gallery
	HTML_VIDEO_POST_TYPE, //video
	HTML_CARD_POST_TYPE, //video
];

/**
 * Taxonomy Trend
 */
$trend_labels  = [
	'add_new_item' => 'Aggiungi'
];
$trend_names   = [
	'name'     => 'trend',
	'singular' => __( 'Trend', 'lifegate' ),
	'plural'   => __( 'Trends', 'lifegate' ),
	'slug'     => 'trend'
];
$trend_options = [
	'hierarchical' => false,
	'show_in_rest' => true,
	'capabilities' => array(
		'manage_terms' => 'manage_categories',
		'edit_terms'   => 'manage_categories',
		'delete_terms' => 'manage_categories',
		'assign_terms' => 'edit_posts'
	)
];
$trend         = new Taxonomy( $trend_names, $trend_options, $trend_labels );
foreach ( $special_cpts as $cpt ) {
	$trend->posttype( $cpt );
}
$trend->posttype( 'post' );
$trend->register();

/**
 * Taxonomy Magazine
 */
$magazine_labels  = [
	'add_new_item' => 'Aggiungi'
];
$magazine_names   = [
	'name'     => 'magazine',
	'singular' => __( 'Magazine', 'lifegate' ),
	'plural'   => __( 'Magazine', 'lifegate' ),
	'slug'     => 'magazine'
];
$magazine_options = [
	'hierarchical' => false,
	'show_in_rest' => true,
	'capabilities' => array(
		'manage_terms' => 'manage_categories',
		'edit_terms'   => 'manage_categories',
		'delete_terms' => 'manage_categories',
		'assign_terms' => 'edit_posts'
	)
];
$magazine         = new Taxonomy( $magazine_names, $magazine_options, $magazine_labels );
foreach ( $special_cpts as $cpt ) {
	$magazine->posttype( $cpt );
}
$magazine->posttype( 'post' );
$magazine->register();


/**
 * Sponsor
 */
// Create a new Taxonomy object for an existing taxonomy.
$sponsors = new Taxonomy( 'sponsor' );
$sponsors->posttype( 'post' );
$sponsors->register();
