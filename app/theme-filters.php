<?php

/**
 * Add class to caption (after shortcode execution)
 */
add_filter( 'the_content', function ( $content ) {
	return str_replace( 'wp-caption-text', 'wp-caption-text lg-mycap-text', $content );
}, 11 );

/**
 * Change Schema.org author
 */

//add_filter( 'wpseo_schema_organization_social_profiles',  '__return_empty_array');
add_filter( 'wpseo_schema_person_social_profiles',  '__return_empty_array');

add_filter( 'wpseo_schema_author', function ( $data ) {

	$author_free = get_field( 'tbm_autore_news_free' );

	if ( $author_free ) {
		unset($data);
		$data['type'] = "Person";
		$data['@id'] = "/#/schema/person/".md5($author_free);
		$data['name']  = $author_free;
		$data['image'] = array();

		return $data;
	}

	$authors = get_field( 'autore_news' );

	if ( ! $authors ) {
		unset($data);
		$data['type'] = "Person";
		$data['@id'] = "/#/schema/person/".md5("Redazione LifeGate");
		$data['name']  = __( 'Redazione LifeGate', 'lifegate' );
		$data['image'] = array();

		return $data;
	}

	/**
	 * Old custom fields are strings. Cast to array
	 */
	if ( ! is_array( $authors ) ) {
		$authors = (array) $authors;
	}

	$author = $authors[0];

	$post = get_post( $author );
	if ( empty( $post ) ) {
		unset($data);
		$data['type'] = "Person";
		$data['@id'] = "/#/schema/person/".md5("Redazione LifeGate");
		$data['name']  = __( 'Redazione LifeGate', 'lifegate' );
		$data['image'] = array();

		return $data;
	}

	$data['@id'] = "/#/schema/person/".md5(get_the_title( $author ));
	$data['name']             = get_the_title( $author );
	$data['url']             = get_permalink( $author );
	$data['image']['url']     = tbm_get_the_post_thumbnail_url( $author, array( 200, 200 ) );
	$data['image']['caption'] = get_the_title( $author );
	// recupero i social eventualmente inseriti
	$socials = get_field( 'social' , $author);
	if(!$socials){
		unset($data['sameAs']) ;
	}else{
		$data['sameAs'] = array();
		foreach ($socials as $social) {
			$data['sameAs'][]=$social["link_social"];
		}
	}

	return $data;
} );

/** personalizzo id author */
add_filter( 'wpseo_schema_article', function ( $data ) {
	$author_free = get_field( 'tbm_autore_news_free' );

	if ( $author_free ) {
		unset($data["author"]);
		$data["author"]["@id"] = "/#/schema/person/".md5($author_free);
		return $data;
	}

	$authors = get_field( 'autore_news' );
	if ( ! $authors ) {
		unset($data["author"]);
		$data["author"]["@id"] = "/#/schema/person/".md5("Redazione LifeGate");
		return $data;
	}

	/**
	 * Old custom fields are strings. Cast to array
	 */
	if ( ! is_array( $authors ) ) {
		$authors = (array) $authors;
	}

	$author = $authors[0];

	$post = get_post( $author );
	if ( empty( $post ) ) {
		unset($data["author"]);
	}

	$data["author"]["@id"] = "/#/schema/person/".md5(get_the_title( $author ));
	return $data;
});



/**
 * Filter AMP image in schema.org
 */
add_filter( 'amp_post_template_metadata', function ( $metadata = array(), $post = 0 ) {

	// setto a mano il logo amp
	$logo_amp = get_theme_root_uri()."/lifegate-2020/dist/images/logo-lifegate.png";
	$metadata["publisher"]["logo"]["url"] = $logo_amp;

	if ( ! $post ) {
		return $metadata;
	}

	/**
	 * Check the image size
	 */
	$thumb_id = get_post_thumbnail_id( $post );

	if ( function_exists( 'tbm_get_thumbor_img_original_size' ) ) {
		$image = tbm_get_thumbor_img_original_size( $thumb_id );

		// se l'immagine è minore di 1960 restituisco il formato allargato
		if ( ! $image || $image[0] < 1960 ) {
			$width = 1960;
			if ($image[1])
				$height = intval($image[1] * (1960 / $image[0]));
			else
				$height = 1200;
		}else {
			$width = $image[0];
			$height = $image[1];
		}

		$image_orig = tbm_get_the_post_thumbnail_url($post, array(
			$width,
			$height
		));

		$image_1x1  = tbm_get_the_post_thumbnail_url( $post, array(
			1200,
			1200
		) );

		// nel caso che l'originale sia già 4/3  metto un formato alternativo
		if($height != intval($width * 0.75)) {
			$image_4x3 = tbm_get_the_post_thumbnail_url($post, array(
				$width,
				intval($width * 0.75)
			));
		}else{
			$image_4x3 = tbm_get_the_post_thumbnail_url($post, array(
				$width,
				intval($width * 0.60)
			));
		}

		// nel caso che l'originale sia già 16/9  metto un formato alternativo
		if($height != intval(( $width * 9 ) / 16)){
			$image_16x9 = tbm_get_the_post_thumbnail_url( $post, array(
				$width,
				intval(( $width * 9 ) / 16)
			) );
		}else{
			$image_16x9 = tbm_get_the_post_thumbnail_url( $post, array(
				$width,
				intval(( $width * 9 ) / 14)
			) );
		}

	}

	$metadata['image'] = array( $image_orig, $image_1x1, $image_4x3, $image_16x9 );

	return $metadata;
}, 10, 2 );

/**
 * Filter the longform body class removing a messy class in our CSS
 */
add_filter( 'body_class', function ( $classes ) {
	return array_diff( $classes, [ 'single-longform' ] );
} );

// add new buttons
add_filter( 'mce_buttons', function ( $buttons ) {
	array_push( $buttons, 'separator', 'tbm_tinymce_button' );

	return $buttons;
} );

// Load the TinyMCE plugin : editor_plugin.js (wp2.5)
add_filter( 'mce_external_plugins', function ( $plugin_array ) {
	$plugin_array['tbm_tinymce_button'] = get_theme_file_uri() . '/tinymce/tinymce-plugin.js';

	return $plugin_array;
} );

/**
 * Add custom class to Gravity Form send button
 */
add_filter( 'gform_submit_button', function ( $button, $form ) {
	$dom = new DOMDocument();
	$dom->loadHTML( '<?xml encoding="utf-8" ?>' . $button );
	$input   = $dom->getElementsByTagName( 'input' )->item( 0 );
	$classes = $input->getAttribute( 'class' );
	$classes .= " cta cta--icon cta--solid cta--icon-right cta--send";
	$input->setAttribute( 'class', $classes );

	return $dom->saveHtml( $input );
}, 10, 2 );

/**
 * Add query var for author page
 */
add_filter( 'query_vars', function ( $vars ) {
	$vars[] = 'author_page';

	return $vars;
} );

/**
 * Make the yoast description an excerpt backup
 */
/*
add_filter( 'get_the_excerpt', function ( $excerpt, $post ) {

	if ( $post->post_excerpt !== '' ) {
		return $excerpt;
	}

	$yoast_desc   = get_post_meta( $post->ID, '_yoast_wpseo_metadesc', true );
	if(function_exists("wpseo_replace_vars"))
		$metadesc_val = wpseo_replace_vars( $yoast_desc, $post );
	$metadesc_val = apply_filters( 'wpseo_metadesc', $metadesc_val );

	if ( $metadesc_val ) {
		return $metadesc_val;
	}

	return $excerpt;
}, 10, 2 );

*/
/**
 * Redirect Rassegna stampa to PDF
 */
add_action( 'template_redirect', function () {

	if ( is_singular( 'rassegna_stampa' ) ) {
		$postid = get_queried_object_id();

		$url = get_field( 'tbm_allegato_rassegna_stampa', $postid );

		if ( ! filter_var( $url, FILTER_VALIDATE_URL ) && wp_get_attachment_url( $url ) ) {
			wp_redirect( wp_get_attachment_url( $url ), 301 );
			exit;
		}

		if ( $url ) {
			wp_redirect( $url );
			exit;
		}

		tbm_404_template_filter();

	}

} );

/**
 * Added sponsor taxonomy to pll
 */
add_filter( 'pll_get_taxonomies', function ( $taxonomies, $is_settings ) {

	$taxonomies['sponsor']     = 'sponsor';
	$taxonomies['trend']       = 'trend';
	$taxonomies['magazine']    = 'magazine';
	//$taxonomies['category_en'] = 'category_en';
	//$taxonomies['tag_en']      = 'tag_en';

	return $taxonomies;
}, 10, 2 );

/**
 * Added sponsor taxonomy to pll
 */
add_filter( 'pll_get_post_types', function ( $post_types, $is_settings ) {

	// enables language and translation management for 'my_cpt'
	$post_types[ HTML_CARD_POST_TYPE ]    = HTML_CARD_POST_TYPE;
	$post_types[ HTML_GALLERY_POST_TYPE ] = HTML_GALLERY_POST_TYPE;
	$post_types[ HTML_VIDEO_POST_TYPE ]   = HTML_VIDEO_POST_TYPE;
	$post_types[ HTML_EVENT_POST_TYPE ]   = HTML_EVENT_POST_TYPE;
	$post_types['show']                   = 'show';
	$post_types['longform']               = 'longform';
	$post_types['autori']                 = 'autori';
	$post_types['iniziative']             = 'iniziative';

	return $post_types;
}, 10, 2 );

/**
 * Disable tablepress css
 */
add_filter( 'tablepress_use_default_css', '__return_false' );

/**
 * Add async attribute
 */
add_filter( 'script_loader_tag', function ( $tag, $handle ) {

	if ( strpos( $handle, 'tbmasync' ) === false ) {
		return $tag;
	}

	return str_replace( ' src', ' async src', $tag );
}, 10, 2 );

/**
 * Disable more in excerpt
 */
add_filter( 'excerpt_more', function ( $more ) {
	return '';
} );

/**
 * Filter pararaph blocks in longform
 */
add_filter( 'render_block_data', function ( $block ) {

	if ( 'longform' !== get_post_type() ) {
		return $block;
	}

	$wrapper = '<div class="site inner-site--noskin"><div class="wrapper"><div class="container"><div class="post__content editorial editorial--longform">%s</div></div></div></div>';

	if ( isset( $block['blockName'] ) && strpos( $block['blockName'], 'lifegate' ) === false  ) {
		foreach ( $block['innerContent'] as $key => $chunk ) {
			$block['innerContent'][ $key ] = sprintf( $wrapper, $chunk );
		}
	}

	return $block;

} );

/**
 * Add lifegate block to Gutenberg blocks
 */
add_filter( 'block_categories', function ( $categories, $post ) {

	$new_block = array(
		array(
			'slug'  => 'lifegate',
			'title' => __( 'LifeGate', 'lifegate' ),
		),
	);

	return array_merge( $new_block, $categories );
}, 10, 2 );

/**
 * Enable Gutenberg in Longform
 */
add_filter( 'use_block_editor_for_post', function ( $image ) {
	if ( 'longform' === tbm_get_post_type_in_admin() ) {
		return true;
	}
	if ( 'original' === tbm_get_post_type_in_admin() ) {
		return true;
	}
	if ( 'mediacase' === tbm_get_post_type_in_admin() ) {
		return true;
	}

} );

/**
 * Replace whitespace with %20 in images
 */
add_filter( 'wp_get_attachment_image_src', function ( $image ) {
	if ( isset( $image[0] ) && ! empty( $image[0] ) ) {
		$image[0] = str_replace( ' ', '%20', $image[0] );
	}

	return $image;
} );

/**
 * Filter the hamburger menu item and grab ACF field
 */
add_filter( 'wp_nav_menu_objects', function ( $items, $args ) {

	/**
	 * Filter hamburger navigation
	 */
	if ( isset( $args->theme_location ) && $args->theme_location === 'hamburger_navigation' ) {
		foreach ( $items as $item ) {
			$featured = get_field( 'tbm_menu_external', $item );
			if ( $featured ) {
				$item->external = true;
			}
		}

		return $items;
	}

	/**
	 * Filter hamburger navigation
	 */
	if ( isset( $args->theme_location ) && ( $args->theme_location === 'trends_navigation' || $args->theme_location === 'global_navigation'  || $args->theme_location === 'channels_navigation' || $args->theme_location === 'casehistory_navigation' ) ) {

		foreach ( $items as $item ) {
			$featured = get_field( 'tbm_menu_featured', $item );
			if ( $featured ) {
				$item->featured = true;
			}
		}

		return $items;
	}

	return $items;


}, 10, 2 );

/**
 * Add magazine homepage to cache purge list
 */
add_filter( 'tbm_add_url_to_invalidate', function ( $urls, $id ) {

	if ( ! is_array( $urls ) ) {
		return $urls;
	}

	$new_urls = array( get_page_link( get_page_by_path( "magazine" ) ) );

	return array_merge( $urls, $new_urls );
}, 10, 2 );

/**
 * Change Archive title
 */
add_filter( 'get_the_archive_title', function ( $title ) {

	if ( is_tax( 'trend' ) || is_tax( 'magazine' ) ) {

		$queried_object = get_queried_object();

		if ( $queried_object ) {
			$term_id  = $queried_object->term_id;
			$taxonomy = $queried_object->taxonomy;

			$acf_tax_id = $taxonomy . '_' . $term_id;
		}

		if ( get_field( 'tbm_' . $taxonomy . '_title', $acf_tax_id ) ) {
			$title = get_field( 'tbm_' . $taxonomy . '_title', $acf_tax_id );
		}
	}


	return $title;

} );

/**
 * Remove <p> from excerpt
 */
remove_filter( 'the_excerpt', 'wpautop' );

/**
 * Remove <p> from archive description
 */
add_filter( 'get_the_archive_description', function ( $title ) {

	if(is_post_type_archive("longform"))
		return pll__( "I nostri approfondimenti, i nostri reportage e le nostre inchieste sul campo. Qui trovate tutti i long-form di LifeGate scritti da giornalisti della redazione diffusa in ogni continente. Ambiente, società, energia, diritti, turismo, futuro. Ogni long-form è un viaggio all’interno di un tema che ci riguarda da vicino anche se lontano grazie a video, immagini, contenuti audio e altri contributi di esperti, protagonisti e attivisti che vivono in prima persona la transizione verso un modello di sviluppo sostenibile.", 'lifegate' );

	if ( is_paged() ) {
		return '';
	}

	if ( is_tax( 'storia' ) ) {
		return $title;
	}

	return strip_tags( $title, '<strong><a><em>' );
}, 10, 3 );

/**
 * Change the title with "titolo strillo". Only in HP
 */
add_filter( 'the_title', function ( $title, $id ) {

	if ( ! $id ) {
		return $title;
	}

	$strillo = get_field( 'post_titolo_strillo', $id );

	if ( ! $strillo ) {
		return $title;
	}

	if ( is_front_page() ) {
		return $strillo;
	}

	return $title;

}, 10, 2 );

/**
 * Add excerpt to page
 */
add_post_type_support( 'page', 'excerpt' );

/**
 * Add translator at the bottom of the website
 */
add_filter( 'the_content', function ( $content ) {

	$translator = lifegate_snippet_card_post_translator( get_the_ID() );

	if ( ! $translator ) {
		return $content;
	}

	$translated_note = pll__( 'Tradotto da', 'lifegate' ) . $translator;

	return $content . '<p>' . $translated_note . '</p>';

} );

/**
 * Filters 'img' elements in post content to add image credit (copy of wp_make_content_images_responsive)
 *
 * @param string $content The raw post content to be filtered.
 *
 * @return string Converted content with 'srcset' and 'sizes' attributes added to images.
 */
add_filter( 'the_content', function ( $content ) {
	if ( ! preg_match_all( '/<img [^>]+>/', $content, $matches ) ) {
		return $content;
	}

	$selected_images = $attachment_ids = array();

	foreach ( $matches[0] as $image ) {
		if ( false === strpos( $image, ' srcset=' ) && preg_match( '/wp-image-([0-9]+)/i', $image, $class_id ) &&
			( $attachment_id = absint( $class_id[1] ) ) ) {

			/*
			 * If exactly the same image tag is used more than once, overwrite it.
			 * All identical tags will be replaced later with 'str_replace()'.
			 */
			$selected_images[ $image ] = $attachment_id;
			// Overwrite the ID when the same image is included more than once.
			$attachment_ids[ $attachment_id ] = true;
		}
	}

	if ( count( $attachment_ids ) > 1 ) {
		/*
		 * Warm the object cache with post and meta information for all found
		 * images to avoid making individual database calls.
		 */
		_prime_post_caches( array_keys( $attachment_ids ), false, true );
	}

	/*foreach ( $selected_images as $image => $attachment_id ) {
		$image_credit = tbm_get_thumb_credit( $attachment_id );

		if ( has_shortcode( $content, 'caption' ) && $image_credit ) {
			$content = str_replace( '[/caption]', ' &copy; ' . $image_credit . '[/caption]', $content );
		}

		if ( ! has_shortcode( $content, 'caption' ) && $image_credit ) {
			$content = str_replace( $image, '<figure>' . $image . '<figcaption>&copy; ' . $image_credit . '</figcaption></figure>', $content );
		}
	}*/

	return $content;
}, 1 );

/**
 * Filters 'img' elements in post content to add lazyload
 *
 * @param string $content The raw post content to be filtered.
 *
 * @return string Converted content with 'data-srcset' and 'data-src' attributes
 */
add_filter( 'the_content', function ( $content ) {
	if ( tbm_is_amp() ) {
		return $content;
	}

	if ( is_feed() ) {
		return $content;
	}

	if ( ! preg_match_all( '/<img [^>]+>/', $content, $matches ) ) {
		return $content;
	}

	$selected_images = $attachment_ids = array();

	foreach ( $matches[0] as $image ) {

		if ( preg_match( '/wp-image-([0-9]+)/i', $image, $class_id ) && ( $attachment_id = absint( $class_id[1] ) ) ) {

			/**
			 * Add lazyload to images
			 */
			if ( strpos( $image, 'class=' ) ) {
				$new_image = str_replace( 'class="', 'class="lazyload ', $image );
				$new_image = str_replace( 'src=', 'data-src=', $new_image );
				$new_image = str_replace( 'srcset=', 'data-srcset=', $new_image );
			}

			/*
			 * If exactly the same image tag is used more than once, overwrite it.
			 * All identical tags will be replaced later with 'str_replace()'.
			 */
			$selected_images[ $image ] = $new_image;
		}
	}

	foreach ( $selected_images as $image => $new_image ) {
		$content = str_replace( $image, $new_image, $content );
	}

	return $content;
}, 10 );

/**
 * Filters youtube shortcode in AMP page
 *
 * @param string $content The raw post content to be filtered.
 *
 * @return string Converted content with youtube shortcode converted in url
 */
add_filter( 'the_content', function ( $content ) {

	$regex = '#\[youtube url="([^"]+)"\](?:.*\[/youtube\])?#';

	if ( ! tbm_is_amp() ) {
		return $content;
	}

	if ( ! is_feed() ) {
		return $content;
	}

	if ( ! preg_match_all( $regex, $content, $matches ) ) {
		return $content;
	}

	foreach ( $matches[0] as $key => $urls ) {
		$content = str_replace( $matches[0][ $key ], $matches[1][ $key ], $content );
	}


	return $content;

}, 1 );

/**
 * Change breadcrumb title
 */
add_filter( 'bcn_breadcrumb_title', function ( $title, $type, $id ) {
	return $title;
}, 3, 10 );

/**
 * Filter html lang
 */
add_filter( 'language_attributes', function ( $output, $doctype ) {
	if ( get_field( 'tbm_alternate_language' ) ) {
		return str_replace( '"' . get_bloginfo( 'language' ) . '"', '"' . get_field( 'tbm_alternate_language' ) . '"', $output );
	}

	return $output;
}, 99, 2 );

/**
 * Filter link rel="alternate" hreflangs
 */
add_filter( 'pll_rel_hreflang_attributes', function ( $hreflangs ) {
	if ( get_field( 'tbm_alternate_language' ) ) {
		unset( $hreflangs['en'] );
	}

	return $hreflangs;
}, 10, 2 );

/**
 * Disabilito l'aggiunta di tag e trend agli utenti editori
 * @param $term
 * @param $taxonomy
 * @return WP_Error
 */
function disallow_insert_term($term, $taxonomy) {
	$user = wp_get_current_user();
	if ( ($taxonomy === 'post_tag' || $taxonomy === 'trend') && in_array('editor', $user->roles) ) {
		return new WP_Error(
			'disallow_insert_term',
			__('Your role does not have permission to add terms to this taxonomy')
		);

	}

	return $term;

}

add_filter('pre_insert_term', 'disallow_insert_term', 10, 2);



/**
 * Add adv in content
 * anticipo il filtro nativo wpautop, per posizionarmi prima dell'esecuzione degli hook
 */
remove_filter('the_content','wpautop');
add_filter('the_content','wpautop', -101);

add_filter( 'the_content', function ( $content ) {

	// escludo per original
	if(is_singular("original"))
		return $content;

	// escludo per mediacase
	if(is_singular("mediacase"))
		return $content;

	if ( is_single() && ! is_admin() ) {

		$closing_p = '</p>';
		$paragraphs = explode( $closing_p, $content );

		$totalp = count($paragraphs);

		if($totalp > 1){

			foreach ($paragraphs as $index => $paragraph) {
				$paragraphs[$index] .= $closing_p;

				$n = $index + 1;
				if($n == 2){
					if(is_amp_endpoint())
						$paragraphs[$index] .= tbm_get_the_banner( 'AMP_ARTICLE_TOP','','',false,false );
					else
						$paragraphs[$index] .= tbm_get_the_banner( 'ARTICLE_TOP','','',false,false );
				}
				if($totalp > 5){
					if ($n == 4) {
						if(is_amp_endpoint())
							$paragraphs[$index] .= tbm_get_the_banner( 'AMP_ARTICLE_MIDDLE','','',false,false );
						else
							$paragraphs[$index] .= tbm_get_the_banner('ARTICLE_MIDDLE', '', '', false, false);
					}
				}
				if($totalp > 8) {
					if ($n == 7) {
						if (is_amp_endpoint())
							$paragraphs[$index] .= tbm_get_the_banner('AMP_ARTICLE_BOTTOM', '', '', false, false);
						else
							$paragraphs[$index] .= tbm_get_the_banner('ARTICLE_BOTTOM', '', '', false, false);
					}
				}

				if($totalp > 11){
					if ($n == 10) {
						if(!is_amp_endpoint())
							$paragraphs[$index] .= tbm_get_the_banner('ARTICLE_BOTTOM2', '', '', false, false);
					}
				}

				if($totalp > 15){
					if ($n == ($totalp -2)) {
						if(!is_amp_endpoint())
							$paragraphs[$index] .= tbm_get_the_banner('ARTICLE_BOTTOM3', '', '', false, false);
					}
				}
			}

		}

		$content = implode( '', $paragraphs );

	}

	return $content;
}, -100 );


add_filter('paginate_links','untrailingslashit');


/**
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function lifegate_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 'post' === get_post_type() ) {
		$sizes = '(max-width: 880px) 100vw, 640px';
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'lifegate_content_image_sizes_attr', 10, 2 );


// estendo la durata del plugin di preview

add_filter( 'ppp_nonce_life', 'lg_nonce_life' );
function lg_nonce_life() {
	return 60 * 60 * 24 * 730; // 2 anni
}

// add_filter( 'yoast_seo_development_mode', '__return_true' );

add_filter( 'wpseo_schema_main_image', 'change_json_article_main_image', 100, 1 );
add_filter( 'wpseo_schema_mainimage', 'change_json_article_main_image', 100, 1 );


/**
 * cambio il formato immagine jsonld
 * @param $graph_piece
 * @param $context
 * @return mixed
 */
function change_json_article_main_image( $graph_piece ) {
	global $post;

	if ( ! $post ) {
		return $graph_piece;
	}

	$thumb_id = get_post_thumbnail_id( $post );
	if ( function_exists( 'tbm_get_thumbor_img_original_size' ) ) {
		$image = tbm_get_thumbor_img_original_size($thumb_id);

		if ($image[0] < 1960) {
			$width = 1960;
			if($image[1])
				$height = intval($image[1] * (1960/$image[0]));
			else
				$height = 980;

			$image_big = tbm_wp_get_attachment_image_url($thumb_id,  array($width, $height));

			$graph_piece["url"] = $image_big;
			$graph_piece["contentUrl"] = $image_big;
			$graph_piece["width"] = $width;
			$graph_piece["height"] = $height;
		}
	}

	return $graph_piece;
}


// escludo hreflang per gli autori

add_filter( 'pll_rel_hreflang_attributes',  function ( $hreflangs ) {

	$paged = (get_query_var('author_page')) ? get_query_var('author_page') : 1;

	if($paged > 1){
		if( $hreflangs["it"] && strpos( $hreflangs["it"], "/autore/") != false)
			unset($hreflangs);

		if( $hreflangs["en"] && strpos( $hreflangs["en"], "/author/") != false)
			unset($hreflangs);
	}

	return $hreflangs;
}, 10, 1 );


remove_filter( 'wp_robots', 'wp_robots_max_image_preview_large' );


/**
 * Filtro per escludere original dalla sitemap
 */
add_filter( 'wpseo_sitemap_exclude_post_type', function ( $excluded, $post_type ) {
	if ( function_exists( 'pll_current_language' ) && 'en' === pll_current_language() )
		return $post_type === 'original';
}, 10, 2 );


/**
 * Filtro per escludere global inline css
 */
add_action( 'wp_enqueue_scripts', function(){
	wp_dequeue_style( 'global-styles' );
});
