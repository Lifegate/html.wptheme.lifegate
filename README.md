# [Lifegate](https://www.lifegate.it/)
Lifegate 2020 is a WordPress theme based on [Sage](https://roots.io/sage/), [Bedrock](https://roots.io/bedrock/) and [Rogiostrap](https://bitbucket.org/triboomedia/rogiostrap/)™.

## Prerequisites
Use [Npm](https://nodejs.org)  and [Grunt](https://gruntjs.com/)

## Install packages (only first time)
First install global grunt `npm install -g grunt-cli` and then install the required packages `npm install`

## Develop theme
### Install rogiostrap theme files
`npm upgrade rogiostrap.lifegate && npm run build`: Npm will upgrade Rogiostrap Lifegate and execute grunt production command which will copy all front-end files in dist directory.

### Coding standard
Use [Wordpress PHP Coding Standards](https://make.wordpress.org/core/handbook/best-practices/coding-standards/php/). In particular:
- Use tabs indentation
- Use `elseif`, not `else if`
- No Shorthand PHP Tags
- Use lowercase letters in variable, action/filter, and function names (never `camelCase`). Separate words via underscores.
- When doing logical comparisons involving variables, always put the variable on the right side and put constants, literals, or function calls on the left side (p.es. `if ( true == $the_force ) [...]`)
- Braces should always be used: single-statement inline control structures (`if($foo) bar();`) are prohibited

## Update front-end
### Enter the theme directory
`cd /xxx/xxx/html.wptheme.lifegate`

### Update repo (always)
`git pull origin master`

### Update rogiostrap
`grunt update_source`

Npm will upgrade Rogiostrap Lifegate and execute `grunt production` command which will copy all front-end files in `dist` directory. 

### Add new files and commit `dist` directory
`grunt commit_source`

Npm will commit all new files in `dist` directory. 

## Bump the package and push
**Caution**: this command will push code to repo.

`grunt bump`  

This command will bump version in package.json and style.css, commit both files, tag them and **push** to origin master. You can use `grunt bump:patch` (0.0.1 > 0.0.2), `grunt bump:minor` (0.0.1 > 0.1.0) or `grunt bump:major` (0.0.1 > 1.0.0). Please, refer to [semantic versioning](https://semver.org/lang/it/) docs to choose wich bump use.

## Deploy theme
Deploy is powered by Bedrock. Go to htmldev02, change directory `cd deploy/tbm-deploy/` and run `cap lifegate.development deploy` or `cap lifegate.production deploy`
