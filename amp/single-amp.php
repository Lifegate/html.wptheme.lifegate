<?php
/**
 * Variabili
 */
$logo  = tbm_wp_get_attachment_image_url( get_field( 'tbm_site_amp_logo', 'options' ), array( 0, 58 ) );
$fb_id = get_field( 'tbm_fb_app_id', 'options' );
$ver   = wp_get_theme()->get( 'Version' ) ?: '000';
?>
<!doctype html>
<html amp lang="it">

<?php while ( have_posts() ) :
	the_post();
	$post_id = get_the_ID();
	?>

	<?php
	if ( ! function_exists( 'pll_current_language' ) ) {
		$lang = 'it';
	}else{
		$lang = pll_current_language();
	}

	if($lang == "en"){
		$preconnecturl = "https://iubenda.lifegate.com/";
		$iubendaurl = 	"https://iubenda.lifegate.com/adk-iubenda-amp.html";
	} else {
		$preconnecturl = "https://iubenda.lifegate.it/";
		$iubendaurl = 	"https://iubenda.lifegate.it/adk-iubenda-amp.html";
	}


	?>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1">

		<link rel="preconnect dns-prefetch" href="https://cdn.adkaora.space/">
		<link rel="preconnect dns-prefetch" href="https://cdn.iubenda.com/">
		<link rel="preconnect dns-prefetch" href="https://www.iubenda.com/">
		<link rel="preconnect dns-prefetch" href="<?php echo $preconnecturl; ?>">


		<?php do_action( 'amp_post_template_head', $this ); ?>

		<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
		<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
		<script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
		<script async custom-element="amp-position-observer" src="https://cdn.ampproject.org/v0/amp-position-observer-0.1.js"></script>
		<script async custom-element="amp-animation" src="https://cdn.ampproject.org/v0/amp-animation-0.1.js"></script>
		<script async custom-element="amp-consent" src="https://cdn.ampproject.org/v0/amp-consent-0.1.js"></script>
		<script async custom-element="amp-sticky-ad" src="https://cdn.ampproject.org/v0/amp-sticky-ad-1.0.js"></script>

		<link rel="shortcut icon" type="image/x-icon" href="https://www.lifegate.it/app/themes/lifegate-2020/dist/images/favicon.ico">

		<style amp-custom>
			<?php do_action( 'amp_post_template_css', $this ); ?>
			.logo{ margin-top:10px; }
			.cta_amp_category{ border-top: solid 1px #ccc; padding-top:20px; border-bottom: solid 1px #ccc; margin-bottom:30px;}
			.editorial .wprm-recipe-container ul li:before { display: none; }
			.tbm-amp .cta_amp_category p{ font-size:18px; }
			.tbm-amp .cta_amp_category p a{ text-decoration:underline; }
			.tbm-amp .cta_amp_category p a.btn{ background-color: #006454; width:300px; border: 1px solid transparent;color: #fff;padding: 10px 0px 10px 0px;text-transform: uppercase;text-decoration:none;font-weight:bold;font-size:18px;margin:auto;margin-top:20px;display: flex;justify-content: center;align-items: center;}
			.footer__bottom__menu .custom-html-widget  ul{ padding:0px;margin:0px;}
			.footer__bottom__menu .textwidget.custom-html-widget{padding:0px;margin:0px;}


			/** Codice AMP Pushdown **/
			:root {
				--font-chiusura-video: 'Arial', open-sans;
				--colore-x-chiusura-video: #FFFFFF;
				--colore-sfondo-x-chiusura-video: #000000;
				--colore-sfondo-contenitore-video: rgba(229, 229, 229, 0.8);
			}
			@-webkit-keyframes appear-x{0%{opacity:0}100%{opacity:1}}@-moz-keyframes appear-x{0%{opacity:0}100%{opacity:1}}100%{opacity:1}.flyingvideoampThumb {display: none;} .flyingvideoamp-target div {display: none;} .flyingvideoamp {display: none;} @supports (display: flow-root) {@media (max-width: 480px) { .flyingvideoamp, .flyingvideoamp-target div {display: block;} .flyingvideoamp {margin-top: -100%;} .relative .flyingvideoamp {position: relative;z-index: 2;width: 100%;height: 56vw;overflow: visible;} .flyingvideoampThumb {height: 56vw;width: 100%;display: block;} .flyingvideoampThumb.adk-fullwidth-image {width:100%;max-width:650px;} .flyingvideoamp .flyingvideoamp-target {position: fixed;right: 14px; width: 100%;height: 56vw;transform: scale(0.93);transform-origin: 100% 100%;background: var(--colore-sfondo-contenitore-video);z-index: 1;} .flyingvideoamp .flyingvideoamp-target.cinquanta {bottom: 55px;} .flyingvideoamp .flyingvideoamp-target.cento {bottom: 105px} .flyingvideoamp .flyingvideoamp-target div {display: block;opacity: 0;width: 24px;height: 24px;transform: scale(1.5);position: fixed;padding:0;right:-12px;bottom: calc(45vw + 40px);z-index: 100000; -webkit-animation: appear-x 0.6s; -moz-animation: appear-x 0.6s; animation: appear-x 0.6s; -webkit-animation-delay: 5s; -moz-animation-delay: 5s; animation-delay: 5s; -webkit-animation-fill-mode: forwards; -moz-animation-fill-mode: forwards; animation-fill-mode: forwards;} .relative .flyingvideoamp .linkvideoampThumb, .relative .flyingvideoamp .flyingvideoampThumb {display: none;} .relative .flyingvideoamp, .relative .flyingvideoamp .flyingvideoamp-target {position: static;transform: scale(1);} .relative .flyingvideoamp .flyingvideoamp-target {bottom: auto;right: auto;} .relative .flyingvideoamp .closing-button, .relative .flyingvideoamp .flyingvideoamp {display: none;} .relative .flyingvideoamp .flyingvideoamp-target:before {display: none;} } }
			.flyingvideoamp amp-img {background: none;}
			/** Codice Dynamic AMP AD **/
			.adk-adv-amp-list {margin-top: -50%;} .adk-adv-amp-list, .adk-adv-amp-list div[role=list] {position: relative;height: auto;margin-top: -50%;}
			#adk_footer-amp {max-height: 0.5px; overflow: hidden;} #adk_footer-amp.adk-adv-amp-list, #adk_footer-amp.adk-adv-amp-list div[role=list] {position: relative; height: auto; margin-top: 0;}
			#adk_pushdown-amp {width: 100%;}
			/** Codice Custom ADK **/
			.adk-box-amp {text-align: center;} .hidden {display: none;}
			/* CSS Consigliato da Iubenda */
			.popupOverlay {position: fixed;top: 0;bottom: 0;left: 0;right: 0;} amp-iframe {margin: 0;} amp-consent.amp-active {top: 0;bottom: 0;left: 0;right: 0;position: fixed;}

		</style>

	</head>

	<body class="tbm-amp">

	<?php
	// in viewport per anticipare il caricamento
	echo tbm_get_the_banner( 'AMP_ARTICLE_FOOTER','','',false,false );
	?>


	<?php
	/***** HEADER *****/
	?>
	<header class="header header--fixed" style="height: 60px;">
		<div role="button" on="tap:sidenav1.toggle" tabindex="0" class="hamburger">☰</div>
		<?php if ( $logo ) : ?>
			<a class="logo" href="<?php echo get_home_url(); ?>">
				<amp-img src="<?php echo $logo; ?>"
						 height="28"
						 alt="Logo <?php echo get_bloginfo( 'name' ); ?>"
						 noloading>
				</amp-img>
			</a>
		<?php endif; ?>
	</header>
	<!-- /***** CONTENT *****/ -->
	<div class="base">
		<div class="wrapper">
			<div class="container container--nopadding">

				<?php
				/***** CAROUSEL *****/
				if ( has_nav_menu( 'amp_slider_navigation' ) ) : ?>
					<div class="partial-amp-carousel-trend" style="height:50px;overflow: hidden;">
						<amp-carousel layout="fixed-height" height="50" type="carousel">
							<?php
							$trend_menu_args = [
									'theme_location' => 'amp_slider_navigation',
									'container'      => '',
									'items_wrap'     => '%3$s'
							];
							wp_nav_menu( $trend_menu_args );
							?>
						</amp-carousel>
					</div>
				<?php endif; ?>

				<!-- /***** POST *****/ -->
				<div class="single-post">
					<main class="single-post__main">

						<article class="editorial">
							<div class="container">
								<div class="col-8">
									<div class="inner-padding">
										<h1>
											<?php echo esc_html( $this->get( 'post_title' ) ); ?>
											<amp-position-observer on="enter:hideAnim.start; exit:showAnim.start"
																   layout="nodisplay"></amp-position-observer>
										</h1>


										<?php
										/***** FEATURED IMAGE *****/
										if ( has_post_thumbnail() ) : ?>
											<figure class="img_evidenza">
												<amp-img data-hero class="single-post__featured-img"
														 src="<?php echo tbm_get_the_post_thumbnail_url( $post_id, array(
																 1200,
																 800
														 ) ); ?>"
														 srcset="<?php echo tbm_get_the_post_thumbnail_url( $post_id, array(
																 1200,
																 800
														 ) ); ?> 1200w,<?php echo tbm_get_the_post_thumbnail_url( $post_id, array(
																 720,
																 480
														 ) ); ?> 720w, <?php echo tbm_get_the_post_thumbnail_url( $post_id, array(
																 600,
																 400
														 ) ); ?> 600w, <?php echo tbm_get_the_post_thumbnail_url( $post_id, array(
																 360,
																 240
														 ) ); ?> 360w"
														 width="600"
														 height="400"
														 layout="responsive"
														 alt="<?php echo tbm_get_the_post_thumbnail_alt($post_id); ?>"
														 noloading>
												</amp-img>
												<figcaption>
													<?php echo wp_get_attachment_caption(get_post_thumbnail_id()); ?>
												</figcaption>
											</figure>
										<?php endif; ?>


										<?php
										/***** AUTHOR *****/
										$authors = get_field( 'autore_news' );
										$author_free  = get_field( 'tbm_autore_news_free' );


										if ( ! $authors && ! $author_free ) {
											$html =  __( 'a cura della redazione', 'lifegate' );

										}/* else if ( $author ) {
											$html = 'di <span class="author__name" style="display: inline-block;" rel="author">' . $author . '</span>';

										}*/ else {

											$template = '<a class="author__name" style="display: inline-block;" href="%s" rel="author">%s</a>';

											if ( $authors && !is_array( $authors ) ) {
												$authors = (array) $authors;
											}

											foreach ( $authors as $author ) {
												$post = get_post( $author );
												if ( empty( $post ) ) {
													$out[] = __( 'Redazione LifeGate', 'lifegate' );
													continue;
												}
												$out[] = sprintf( $template, get_permalink( $author ), get_the_title( $author ) );
											}


											if ( $author_free ) {
												$out[] = $author_free;
											}

											/**
											 * If authors are more than 2, we need other form
											 */
											if ( count( $out ) > 2 ) {
												$html = __( 'di', 'lifegate' ) . ' ' . implode( ', ', array_slice( $out, 0, - 1 ) ) . ' e ' . array_slice( $out, - 1 )[0];
											} else {
												$html = __( 'di', 'lifegate' ) . ' ' . implode( ' e ', $out );
											}
											?>

											<?php
										}
										?>

										<div class="single-post__author">
											<?php echo tbm_get_pub_date(); ?>
											<span class="author__info">
													<?php echo ' ' . $html; ?>
												</span>
										</div>

									</div>

									<div class="inner-padding strillo">
										<?php
										$strillo = get_the_excerpt();
										if(!$strillo)
											$strillo = get_field( 'post_titolo_strillo', $id );
										echo wpautop($strillo);
										?>
									</div>

									<?php
									// recupero la CTA associata alla categoria
									$post_categories = wp_get_post_categories( $post_id );
									if($post_categories){
										$cta =  get_field( "amp_cta", "category_". $post_categories[0]);
										if(trim($cta) != ""){

										}
									}
									?>
									<!-- /***** SOCIAL *****/ -->
									<div class="single-post__social">
										<?php if ( $fb_id ) : ?>
											<amp-social-share type="facebook"  height="32"
															  data-param-app_id="<?php echo $fb_id; ?>"></amp-social-share>
										<?php endif; ?>
										<amp-social-share type="twitter" height="32"></amp-social-share>
										<amp-social-share type="linkedin" height="32"></amp-social-share>
										<amp-social-share type="whatsapp"  height="32"></amp-social-share>
									</div>

									<?php
									echo tbm_get_the_banner( 'AMP_ARTICLE_VIDEO','','',false,false );
									?>

									<!-- /***** CONTENT *****/ -->
									<div class="inner-padding">
										<?php
										echo $this->get( 'post_amp_content' ); ?>


										<?php
										// recupero la CTA associata alla categoria
										$post_categories = wp_get_post_categories( $post_id );
										if($post_categories) {
											$cta = get_field("amp_cta", "category_" . $post_categories[0]);
											if (trim($cta) != "") {
												echo '<div class="cta_amp_category">' . $cta . '</div>';
											}
										}
										?>

										<!-- /***** SOCIAL *****/ -->
										<div class="single-post__social" style="margin-bottom:30px">
											<?php if ( $fb_id ) : ?>
												<amp-social-share type="facebook"  height="32"
																  data-param-app_id="<?php echo $fb_id; ?>"></amp-social-share>
											<?php endif; ?>
											<amp-social-share type="twitter" height="32"></amp-social-share>
											<amp-social-share type="linkedin" height="32"></amp-social-share>
											<amp-social-share type="whatsapp"  height="32"></amp-social-share>
										</div>



										<?php
										//										include_once( HTML_TBM_COMMON_PLUGIN_DIR . 'templates/partials/amp/post-footer.php' );
										?>

									</div>

								</div>

							</div>
						</article>
					</main>
				</div>

				<div class="partial-amp-footer">

					<div class="footer-menus">
						<div class="footer__bottom__menu">
							<?php dynamic_sidebar('footer_sidebar1'); ?>
						</div>
						<div class="footer__bottom__menu">
							<?php dynamic_sidebar('footer_sidebar2'); ?>
						</div>
					</div>

					<a class="logo" href="<?php echo get_home_url(); ?>">
						<?php
						$logo_footer = get_template_directory_uri()."/../dist/images/logo-lifegate-light.svg";
						?>
						<amp-img src="<?php echo $logo_footer; ?>"
								 width="150"
								 layout="fill"
								 height="34"
								 alt="Logo <?php echo get_bloginfo( 'name' ); ?>"
								 noloading>
						</amp-img>
					</a>


					<div class="textwidget custom-html-widget">Per tanti, la sostenibilità sta diventando una necessità impellente, per altri è soprattutto un obbligo. Spesso diventa un accessorio da sfoggiare, a volte un lasciapassare, altre un mero attestato sociale. Per noi, la sostenibilità ambientale e umana rappresenta un autentico stile di vita, definisce il nostro modo di stare al mondo e nel mondo, un atteggiamento incentrato sulla civiltà della consapevolezza e sulla concretezza del fare. Da 20 anni, operiamo per essere i catalizzatori del cambiamento sociale, per risvegliare e alimentare una nuova coscienza ecologica, per ispirare e diffondere nuovi modelli di business e nuovi modelli di consumo per le persone e le aziende. Siamo il luogo dove l’educazione diventa determinazione, il sentimento diventa azione, lo scopo diventa soluzione e risultato. Siamo per chi sceglie di farsi guidare dai valori dell’etica, nel completo rispetto dell’ecosistema e di tutte le forme viventi in esso presenti. Siamo per chi decide di vivere con sentimento e dare uno scopo alla propria vita, agendo per rendere il mondo un posto migliore.</div>

					<p class="credits">LifeGate | <?php echo date( 'Y' ); ?></p>
				</div>

				<a href="#top" id="topBtn" title="Go to top" on="tap:top-page.scrollTo(duration=200)"></a>


			</div>
		</div>
	</div>

	<?php
	/***** SIDEBAR *****/
	if ( has_nav_menu( 'amp_hamburger_navigation' ) ) : ?>
		<amp-sidebar id="sidenav1" layout="nodisplay" side="left">
			<div role="button" aria-label="close sidenav" on="tap:sidenav1.toggle" tabindex="0" class="close-sidenav">
				✕
			</div>
			<?php
			$main_menu_args = [
					'theme_location' => 'amp_hamburger_navigation',
					'menu_class'     => 'sidenav',
					'depth'          => 1,
					'container'      => ''
			];
			wp_nav_menu( $main_menu_args );
			?>
		</amp-sidebar>
	<?php endif; ?>


	<?php do_action( 'amp_post_template_footer', $this ); ?>


	<amp-animation id="showAnim"
				   layout="nodisplay">
		<script type="application/json">
			{
				"duration": "200ms",
				"fill": "both",
				"iterations": "1",
				"direction": "alternate",
				"animations": [
					{
						"selector": "#topBtn",
						"keyframes": [
							{
								"opacity": "1",
								"visibility": "visible"
							}
						]
					}
				]
			}
		</script>
	</amp-animation>
	<amp-animation id="hideAnim" layout="nodisplay">
		<script type="application/json">
			{
				"duration": "200ms",
				"fill": "both",
				"iterations": "1",
				"direction": "alternate",
				"animations": [
					{
						"selector": "#topBtn",
						"keyframes": [
							{
								"opacity": "0",
								"visibility": "hidden"
							}
						]
					}
				]
			}
		</script>
	</amp-animation>



	<amp-consent id="adkIubenda" layout="nodisplay">
		<!--
			È preferibile impostare il consent ID come "consent" + site ID
			Per richiedere il consenso ai soli utenti UE sostituisci "consentRequired": true con "promptIfUnknownForGeoGroup": "eu"
		-->
		<script type="application/json">
			{
				"consentInstanceId": "consent2077371",
				"consentRequired": "remote",
				"checkConsentHref": "https://cdn.iubenda.com/cs/amp/checkConsent",
				"promptUI": "adkCookieSolution"
			}
		</script>
		<div id="adkCookieSolution" class="popupOverlay">

			<amp-iframe layout="fill" sandbox="allow-scripts allow-same-origin allow-popups allow-popups-to-escape-sandbox" src="<?php echo $iubendaurl; ?>">
				<div style="opacity:0" placeholder>Loading</div>
			</amp-iframe>
		</div>
	</amp-consent>


	</body>
	<?php
	break;
endwhile; ?>
</html>
