module.exports = function (grunt) {
	require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

	// Imposta la url di riscrittura degli asset
	var url = '';
	var jsComponent = '/app/themes/lifegate-2020/dist/js/components';
	var jsBase = '/app/themes/lifegate-2020/dist';
	var phpUrl = 'https://dev.lifegate.it/app/themes/lifegate-2020';

	imgArray = {
		"srcImgFeaturedBig": "490,490",
		"srcImgFeaturedBigx2": "980,980",
		"srcImgCardHero": "490,326",
		"srcImgCardHerox2": "980,654",
		"srcImgCardSemiHero": "640,250",
		"srcImgCardSemiHerox2": "1280,500",
		"srcImgCardBig": "300,200",
		"srcImgCardBigx2": "600,400",
		"srcImgCardList": "148,200",
		"srcImgCardSmall": "72,148",
		"srcImgPostFeaturedBig": "640,320",
		"srcImgPostFeaturedBigx2": "1280,640",
		"srcImgFeaturedBig1": "490,326",
		"srcImgFeaturedBig1x2": "980,652",
		"srcImgFeaturedBig2": "162,162",
		"srcImgFeaturedBig3": "162,162",
		"srcImgFeaturedBig4": "162,162",
		"srcImgCardSemiHero1": "514,250",
		"srcImgCardSemiHero1x2": "1028,500",
		"srcImgCardBig1": "300,200",
		"srcImgCardBig1x2": "600,400",
		"srcImgCardBig2": "149,149",
		"srcImgCardBig3": "149,149",
		"srcImgCardList1": "148,125",
		"srcImgCardList2": "73,73",
		"srcImgCardList3": "73,73",
		"srcImgCardSmall1": "73,73",
		"srcImgCardSmall2": "73,73",
		"srcImgSlider": "300,80",
		"srcImgTrendFeaturedBig": "300,480",
		"srcImgTrendFeaturedBigx2": "600,960",
		"srcImgHero": "148,148",
		"srcImgHerox2": "296,296",
		"srcImgBig": "96,96",
		"srcImgSmall": "48,48"
	};

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		notify_hooks: {
			options: {
				enabled: true,
				success: true,
				duration: 3
			}
		},
		replace: {
			test: {
				options: {
					patterns: [
						{
							match: /\{%[^%]+set query(?:[^%]+)type: ([^,]+)(?:[^%]+)categoryName: '([^']+)'(?:[^%]+)component: '([^']+)'[^%]+%\}(?:[^']+)@include\('components.sections.section-title--block'\)/igs,
							replacement: function (e, f, g, h) {
								return "@include('components.sections.section-title--block',array('blocks' => array(['template' => '" + f + "','categoryName' => '" + g + "','component' => '" + h + "'])))";
							}
						}
					]
				},
				files: [
					{
						expand: true,
						cwd: 'resources/views/',
						src: ['**/front-page.blade.php'],
						dest: 'resources/views/'
					}
				]
			},
			customloader: {
				options: {
					patterns: [
						{
							match: 'components',
							replacement: jsComponent
						},
						{
							match: 'url',
							replacement: jsBase
						}
					]
				},
				files: [
					{
						expand: true,
						cwd: 'resources/views/',
						src: ['**/customModulesLoader.blade.php'],
						dest: 'resources/views/'
					}
				]
			},
			html: {
				options: {
					patterns: [
						{
							match: /\/app\/themes\/lifegate-2020\/dist\//ig,
							replacement: phpUrl + '/dist/'
						}
					]
				},
				files: [
					{
						expand: true,
						flatten: true,
						src: ['templates/*.html'],
						dest: 'templates/'
					}
				]
			},
			js: {
				options: {
					patterns: [
						{
							match: /\/app\/themes\/lifegate-2020\/dist\//ig,
							replacement: url + '/app/themes/lifegate-2020/dist/'
						}
					]
				},
				files: [
					{
						expand: true,
						flatten: true,
						src: ['dist/js/*.js'],
						dest: 'dist/js/'
					}
				]
			},
			twig: {
				options: {
					patterns: [


						{
							match: /<link rel="stylesheet" href="@@url\/([^@]+)@css_extension\.css" \/>/ig,
							replacement: '@asset(\'$1.min.css\')'
						},
						{
							match: /{% ?set queryItem = ([^%]+) ?%}/ig,
							replacement: '{{-- query: $1 --}}'
						},
						{
							match: /{{ titles\.title\((?:([^,\)]+),?)?(?:([^,\)]+),?)?(?:([^,\)]+),?)?(?:([^,\)]+),?)?(?:([^,\)]+),?)?\) }}/ig,
							replacement: function (x, a, b, c, d) {
								var out = [];
								if (typeof a !== "undefined") {
									out.push("'text' => " + a + "");
								}
								if (typeof b !== "undefined") {
									out.push("'textlink' => " + b + "");
								}
								if (typeof c !== "undefined") {
									out.push("'url' => " + c + "");
								}
								if (typeof d !== "undefined") {
									out.push("'css' => " + d + "");
								}
								return "@include('components.partials.title-section',array(" + out.join(',') + "))";
							}
						},
						{
							match: /\{%[^%]+set query = {([^%]+)}[^%]+%}/igs,
							replacement: function (x, a) {
								var abis = a.replace(/(class|colNumber|gutter|componentNumber|type|component) ?:/g, "'$1' => ");
								var myStr = "@include('components.sections.section-horizontal-iteration',[" +
									"'tbmposts' => $last_used_cars," +
									"'query' => [" + abis + "]" +
									"])";
								return myStr;
							}
						},
						{
							match: /{% import "components\/partials\/forms\.twig" as forms %}|{% block head %}|<title>{{ site.name}} - <\/title>|{{ parent\(\) }}/ig,
							replacement: ""
						},
						{
							match: /<a href="{{ author.link }}"><span>{% if loop.index == 2 %}e {% endif %}<label>{{ author.name }}<\/label><\/span><\/a>/ig,
							replacement: "<div class=\"post__author\">di {!! $single_post_authors !!}</div>"
						},
						{
							match: /<a href="#" class="post__story" href="{{ queryItem.relatedTrends\[0].link }}">{{ queryItem.relatedTrends\[0].name }}<\/a>/ig,
							replacement: "@if($related_trend)<a class=\"post__story\" href=\"{!! $related_trend['link'] !!}\">{!! $related_trend['name'] !!}</a>@endif"
						},
						{
							match: /{{ forms\.newsletter\("single-newsletter", "single-newsletter", null, "text", "la tua email\.\.\.", true, false, true, true\) }}/ig,
							replacement: "@include('components.custom.custom-post-newsletter-box')"
						},
						{
							match: /<form method="POST" action="my\/url\/some\.php">/ig,
							replacement: "<form method=\"POST\" action=\"{!! admin_url( 'admin-ajax.php' ) !!}\">"
						},

						{
							match: /<label class="post__date">{{queryItem.publishedAt}}<\/label>/ig,
							replacement: "<label class=\"post__date\">{!! lifegate_snippet_post_date() !!}</label>"
						},
						{
							match: /\{% for relatedTrend in queryItem\.relatedTrends %\}(?:[\s]*)\{% if loop.index == 1 %\}([\s\S]*)\{% endif %\}(?:[\s]*)\{% endfor %\}/ig,
							replacement: function (e, f) {
								return f;
							}
						},
						{
							match: /\{% for relatedPost in queryItem\.relatedPosts %\}(?:[\s\S]+)\{% endfor %\}/ig,
							replacement: function (e, f) {
								return '{!! lifegate_tbm_get_post_related() !!}';
							}
						},

						{
							match: /{# ([^#}]+) #}/ig,
							replacement: '{{-- $1 --}}'
						},
						{
							match: /{% block content %}/,
							replacement: '@section(\'content\')'
						},
						{
							match: /{% endblock %}/ig,
							replacement: '@endsection'
						},
						{
							match: /{% ?include (?:'|")([^'"]+)(?:'|") ?%}/ig,
							replacement: function (e, f) {
								f = f.replace('../partials/', 'components/partials');
								f = f.replace(/\//g, '.');
								f = f.replace('.twig', '');
								return "@include('" + f + "')";
							}
						},
						{
							match: /{% extends (?:'|")([^'"]+)(?:'|") %}/ig,
							replacement: function (e, f) {
								f = f.replace(/\//g, '.');
								f = f.replace('.twig', '');
								return "@extends('" + f + "')";
							}
						},

						{
							match: /<a class="([^"]+)" href="\{\{ relatedTrend\.link \}\}">\{\{ relatedTrend\.name \}\}<\/a>/ig,
							replacement: function (e, f) {
								return '\{!! lifegate_snippet_label("' + f + '") !!\}';
							}
						},
						{
							match: /{% for queryItem in query.col1.type.items.children %}/,
							replacement: '@foreach ($blocks as $block)'
						},

						{
							match: /{{ queryItem\.(srcImg[^\s]+) }}/ig,
							replacement: function (e, f) {
								if (imgArray[f] !== undefined) {
									return '{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(' + imgArray[f] + ')) !!}';
								} else {
									console.log(f);
								}
							}
						},
						{
							match: /{{ ?queryItem\.link ?}}/ig,
							replacement: '{!! get_permalink() !!}'
						},
						{
							match: /{{ ?queryItem\.title ?}}/ig,
							replacement: '{!! the_title() !!}'
						},
						{
							match: /{{ ?queryItem\.abstract ?}}/ig,
							replacement: '{!! the_excerpt() !!}'
						},
						{
							match: /{{ ?queryItem.photoNumber ?}}/ig,
							replacement: '{!! tbm_get_gallery_photo_number(get_the_ID()) !!}'
						}
					]
				},
				files: [
					{
						expand: true,
						cwd: 'resources/views/',
						// src: ['**/partial-card-post-hero--fuji.blade.php'],
						src: ['**/*.blade.php'],
						dest: 'resources/views/'
					}
				]
			},
			domain: {
				options: {
					patterns: [
						{
							match: /https?:\/\/(?:(?:dev|www|tbm)\.)?lifegate\.it\/app\/themes\//ig,
							replacement: '/app/themes/'
						}
					]
				},
				files: [
					{
						expand: true,
						src: ['dist/css/**/*.css'],
						dest: ''
					}
				]
			},
		},
		copy: {
			assets: {
				files: [
					{expand: true, cwd: 'node_modules/rogiostrap.lifegate/dist/', src: ['**'], dest: 'dist/'},
					{expand: true, cwd: 'node_modules/source.amp/dist/', src: ['**'], dest: 'dist/'}
				]
			},
			templates: {
				files: [
					{
						expand: true,
						cwd: 'node_modules/rogiostrap.lifegate/templates/',
						src: ['**'],
						dest: 'templates/'
					}
				]
			},
			twig: {
				files: [
					{
						expand: true,
						cwd: 'templates/twig/',
						src: ['**'],
						dest: 'resources/views/',
						rename: function (dest, src) {
							return dest + src.replace('.twig', '.blade.php');
						},
						filter: function (filepath) {
							path = filepath.replace('templates\\twig', 'resources\\views');
							path = path.replace('.twig', '.blade.php');
							return !(grunt.file.exists(path));
						}
					}
				]
			}
		},
		prettify: {
			options: {
				"preserve_newlines": 1,
				"indent_char": "  ",
				"padcomments": true,
				"unformatted": ["section", "code"]
			},
			// Prettify a directory of files
			all: {
				expand: true,
				cwd: 'templates/twig/',
				ext: '.twig',
				src: ['*.twig'],
				dest: 'templates/twig/pretty/'
			},
		},
		concat: {
			options: {
				separator: ';\n',
			},
			dist: {
				src: ['dist/js/libs/picturefill.min.js', 'dist/js/customModulesLoader.js', 'dist/js/libs/basket.min.js', 'dist/js/scriptloader.js'],
				dest: 'dist/js/tbm.min.js',
			}
		},
		exec: {
			update_source: 'npm upgrade rogiostrap.lifegate source.amp && npm install',
			commit: {
				cmd: function () {
					var out = [];

					if (grunt.file.exists('node_modules/rogiostrap.lifegate/package.json')) {
						out.push("Rogiostrap to version " + grunt.file.readJSON('node_modules/rogiostrap.lifegate/package.json').version);
					}
					if (grunt.file.exists('node_modules/source.amp/package.json')) {
						out.push("AMP source to version " + grunt.file.readJSON('node_modules/source.amp/package.json').version);
					}

					return 'git add dist && git commit -m "Updated ' + out.join(' and ') + '" dist/';

				}
			}
		}
	});
	grunt.registerTask('default', 'Copia gli asset nella directory del tema, riscrive i path e copia i template', [
			'copy',
			'replace:html',
			'replace:js',
			'replace:twig'
		]
	);

	/**
	 * Production is the task executed on every install
	 */
	grunt.registerTask('production', 'Copia gli asset nella directory del tema e riscrive i path', [
		'copy:assets',
		'replace:js',
		'concat',
		'replace:domain',
	]);
	grunt.registerTask('update_source', 'Update Rogiostrap Source', ['exec:update_source']);
	grunt.registerTask('commit_source', 'Commit Rogiostrap Source', ['exec:commit']);
};
