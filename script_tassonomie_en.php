<?php
$handle = fopen( "script_en.csv", "r" );
for ( $i = 0; $row = fgetcsv( $handle ); ++ $i ) {
	if ( $row[0] === 'tipologia' ) {
		'continue';
	}

	echo "echo -e 'Comando numero\\e[32m {$i} \\e[0m' &&" . PHP_EOL;

	if ( $row[2] === 'viene fusa in (e cancellata)' ) {
		if ( $row[0] === 'category' ) {
			$tax_from = 'category_name';
		} else if ( $row[0] === 'post_tag' ) {
			$tax_from = 'tag';
		} else {
			$tax_from = $row[0];
		}
		$tax_to = $row[3];
		$skip   =  '--skip-plugins';
		echo "for x in $(wp post list --{$tax_from}={$row[1]} --format=ids) ; do wp post term add \$x {$tax_to} {$row[4]} --by=slug {$skip}; done; wp term delete {$row[0]} {$row[1]} --by=slug &&" . PHP_EOL;
	}

	if ( $row[2] === 'cambio di tassonomia' ) {
		//TODO Controllare che la tassonomia di destinazione non sia già attiva
		$skip   =  '--skip-plugins';
		echo "wp term migrate {$row[1]} --by=slug --from={$row[0]} --to={$row[3]} {$skip} &&" . PHP_EOL;
	}

	if ( $row[2] === 'ridenominazione' ) {
		$slug  = $row[4] ? '--slug=' . $row[4] : '';
		$name  = $row[5] ? '--name="' . $row[5] . '"' : '';
		$child = $row[6] ? '--parent=$(wp term get category_en ' . $row[6] . ' --by=slug --field=term_id)' : '';
		$skip   =  '--skip-plugins';
		echo "wp term update {$row[0]} {$row[1]} --by=slug {$slug} {$name} {$child} {$skip} &&" . PHP_EOL;
	}

	if ( $row[2] === 'co-tassonomia' ) {
		if ( $row[0] === 'category' ) {
			$tax_from = 'category_name';
		} else if ( $row[0] === 'post_tag' ) {
			$tax_from = 'tag';
		} else {
			$tax_from = $row[0];
		}
		$skip   =  '--skip-plugins';
		$tax_to = $row[3];
		echo "for x in $(wp post list --$tax_from={$row[1]} --format=ids ) ; do wp post term add \$x $tax_to {$row[4]} --by=slug {$skip}; done &&" . PHP_EOL;
	}

	if ( $row[2] === 'creazione' ) {
		echo "wp term create $row[3] \"$row[5]\" --slug=$row[4] &&" . PHP_EOL;
	}

	if ( $row[2] === 'cancellazione' ) {
		echo "wp term delete $row[0] \"$row[1]\" --by=slug &&" . PHP_EOL;
	}

	if ( $row[2] === 'diventa principale' ) {
		echo "wp term update {$row[0]} {$row[1]} --by=slug --parent=0 &&" . PHP_EOL;
	}

	if ( $row[2] === 'diventa figlia di' ) {
		echo "wp term update {$row[0]} {$row[1]} --by=slug --parent=$(wp term get category_en {$row[4]} --by=slug --field=term_id) &&" . PHP_EOL;
	}

}
fclose( $handle );
